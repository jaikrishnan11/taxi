aws dynamodb create-table \
    --table-name Trip \
    --attribute-definitions \
        AttributeName=bookingId,AttributeType=S \
    --key-schema \
        AttributeName=bookingId,KeyType=HASH \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --table-class STANDARD \
    --endpoint-url http://localhost:8000 \
    --profile gogo-admin  \
    --region ap-south-1