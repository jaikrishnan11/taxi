const { add } = require("lodash");
let { customAlphabet, nanoid } = require('nanoid')
const moment = require('moment')
const _ = require('lodash')
const elasticsearchClient = require('../clients/elastic-search-client')
const dynamoDBClient = require('../clients/dynamo-db-client')
nanoid = customAlphabet("0123456789", 6)

class ItemRepairer {
    async repairCabVendor(cabVendor) {
        let addMeta, addCreatedDate, addCabVendorId;
        addMeta = addCreatedDate = addCabVendorId = false;

        if (!cabVendor.meta) {
            addMeta = true;
        }
        if (!cabVendor.createdDate) {
            addCreatedDate = true;
        }
        if (!cabVendor.externalId) {
            addCabVendorId = true;
        }

        let setExpression = []
        let expressionName = {}
        let expressionValue = {}
        if (addMeta) {
            setExpression.push("#meta=:meta")
            expressionName["#meta"] = "meta"
            expressionValue[":meta"] = {}
        }
        if (addCreatedDate) {
            setExpression.push("#createdDate=:createdDate")
            expressionName["#createdDate"] = "createdDate"
            expressionValue[":createdDate"] = moment().subtract(10, "months").toISOString()
        }
        if (addCabVendorId) {
            setExpression.push("#externalId=:externalId")
            expressionName["#externalId"] = "externalId"
            expressionValue[":externalId"] = await this.generateUniqueExternalId()
        }
        if (setExpression.length > 0) {
            console.log("repairing ", setExpression, cabVendor.cabVendorId, expressionName, expressionValue)
            return await dynamoDBClient.repairCabVendor(cabVendor.cabVendorId, {
                expressionAttrName: expressionName,
                expressionAttrValue: expressionValue,
                updateExpression: `SET ${setExpression.join(",")}`
            })
        }
        return null;
    }

    async generateUniqueExternalId(attempt = 1) {
        let candidateExternalIds = Array(3).fill().map(() => nanoid())
        let existingExternalIds =  await elasticsearchClient.getCabVendorByExternalIds(candidateExternalIds)
        let uniqueExternalIds  = _.difference(candidateExternalIds, existingExternalIds)
        if (uniqueExternalIds.length > 0) {
            return uniqueExternalIds[0]
        } else if (attempt < 5) {
            return this.generateUniqueExternalId(attempt + 1)
        } else {
            throw "Could not find unique external id"
        }
    }
}

new ItemRepairer().generateUniqueExternalId().then(console.log, console.log)
module.exports = new ItemRepairer();