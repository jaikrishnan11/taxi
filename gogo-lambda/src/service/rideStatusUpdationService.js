const moment = require('moment')
const _ = require('lodash')

const Constants = require('./constants')
const dynamoDBClient = require('../clients/dynamo-db-client')
const telegramBot = require('../clients/telegram-bot')
const mailClient = require('../clients/mail-sender')
const invoiceClient = require('../clients/invoice-generator-client')
const whatsappClient = require('../clients/whatsapp-client')
const smsClient = require('../clients/digimile-client')

module.exports = {
    // Move UnApproved to Expired_UnApproved if tripEndTime is in the past
    updateUnApprovedRides: async function() {
        const curTime = moment().toISOString()
        console.debug("curTime ", curTime)
        let rides = await dynamoDBClient.queryRideStatusAndFilterByTripEndTime(Constants.RideStatus.UnApproved, curTime)
       if(!_.isEmpty(rides)){
        await telegramBot.sendDevMessageText("The following unapproved rides will be expired " + JSON.stringify(_.map(rides, ride => ride.bookingId)))
       }
        for(let i =0 ; i< rides.length; i++){
            let ride = rides[i];
            console.debug("Updating UnApproved ride - " , ride.bookingId)
            await dynamoDBClient.changeRideStatus(ride.bookingId, Constants.RideStatus.UnApproved, Constants.RideStatus.ExpiredUnApproved)
        }
    },

    updateUnAssignedRides: async function() {
        const curTime = moment().toISOString()
        console.debug("curTime ", curTime)
        let rides = await dynamoDBClient.queryRideStatusAndFilterByTripEndTime(Constants.RideStatus.UnAssigned, curTime)
        if(!_.isEmpty(rides)){
            await telegramBot.sendDevMessageText("The following UnAssigned rides will be expired " + JSON.stringify(_.map(rides, ride => ride.bookingId)))
           }
        for(let i =0 ; i< rides.length; i++){
            let ride = rides[i];
            console.debug("Updating UnAssigned ride - " , ride.bookingId)
            await dynamoDBClient.changeRideStatus(ride.bookingId, Constants.RideStatus.UnAssigned, Constants.RideStatus.Expired)
        }
    },

    //Ride got assinged in the cloudflare api. Send necessary notifications 1. To Admin 2. To Customer 3. To Public group so that people will calling our admin
    processAssignedRide: async function({bookingId, cabVendorId}) {
        let {ride, cabVendor} = await dynamoDBClient.getRideAndVendor(bookingId, cabVendorId)
        if(ride.rideStatus != Constants.RideStatus.Assigned){
            throw `Ride ${bookingId} is not in Assinged State (is ${ride.rideStatus}) but asking to send notf`
        } else if(ride.cabVendor.cabVendorId != cabVendorId){
            throw `[processAssignedRide] cabVendorId mismatch ${bookingId} - ${cabVendorId}`
        }
        let adminGroupPromise =  telegramBot.sendRideAssignedMessage(ride, cabVendor)
        let whatsappPromise =  whatsappClient.sendDriverAssignedMessage(ride, cabVendor)
        let smsPromise = smsClient.sendDriverMessage(ride, cabVendor)
        await telegramBot.sendRideAssignedToDriversGroup(ride, cabVendor)
        await Promise.all([adminGroupPromise, whatsappPromise, smsPromise])
    },

    //Lambda runs every 30m. We query records till 45m meaning in the worst case we will change the status to Ongoing 15m before the pickup time. 
    updateAssingedRideStatus: async function (){
        let endTime = moment().add("45", "minutes").toISOString()
        console.debug("endtime", endTime)
        let rides = await dynamoDBClient.queryAssignedRides(Constants.RideStatus.Assigned, endTime)
        
        for(let i =0 ; i< rides.length; i++){
            let ride = rides[i];
            const estRideStartTime = moment(ride.bookingDetails.tripStartTime).subtract(45, "minutes")
            if(moment().isBefore(estRideStartTime)) {
                console.debug("still having time to change it to onoing" + ride.bookingId)
                continue;
            }
            // Send push notification to the vendor
            // Change status to Ongoing
            console.debug("Updating Assigned ride - " + ride.bookingId)
            await dynamoDBClient.updateRideStatusToOngoing(ride.bookingId, ride.cabVendor.cabVendorId)
            console.debug("Done processing. Set to OnGoing - " + ride.bookingId)
        }
    },

    updateOnGoingRides: async function() {
        console.log("Updating onGoing rides")
        let rides = await dynamoDBClient.queryAssignedRides(Constants.RideStatus.OnGoing)
        for(let i=0; i < rides.length; i++) {
            let ride = rides[i];
            if(!ride.bookingDetails.estRideEndTime){
                console.error("No estimated ride completion time, Unable to determine whether ride is completed")
                continue;
            }
            // With Drivers closing the rides going forward... we should autoclose if driver missed to close the ride
            const estRideEndTime = moment(ride.bookingDetails.estRideEndTime).add(12, "hours")
            if(moment().isAfter(estRideEndTime)) {
                console.debug("Updating OnGoing ride - " + ride.bookingId)
                await completeOnGoingRide(ride)
            }
        }
        
        console.log(rides)
        // Update total earnings 
    },

    processRideClosedEvent: async function(bookingId) {
        let ride = await dynamoDBClient.getRideByBookingId(bookingId, false/*consistentRead*/)
        if(ride.rideStatus != Constants.RideStatus.OnGoing || !ride.bookingDetails.rideCloseTime) {
            console.error("[processRideClosedEvent] Ride not closed", ride)
            throw "Trying to process ride which is not closed. WTF!"
        }
        await completeOnGoingRide(ride)
        await telegramBot.sendRideClosedMessage(ride)
    }
}

async function completeOnGoingRide(ride) {
    await dynamoDBClient.completeRide(ride.bookingId, ride.cabVendor.cabVendorId, ride.priceDetails.rateCard.total - ride.priceDetails.cabVendor.transactionFee)

    // send invoice mail and whatsapp
    let invoicePdfBuffer = await invoiceClient.generateInvoice(ride)
    let mailPromise = mailClient.sendRideCompletedEmail(ride.customerDetails.email, ride.bookingId, ride, invoicePdfBuffer)
    let whatsappPromise = whatsappClient.sendRideCompletedWithInvoice(ride.bookingId, ride, invoicePdfBuffer)
    await Promise.all([mailPromise, whatsappPromise])
    console.debug("Done processing. Set to Completed - " + ride.bookingId)
    console.debug("Sending notification to Feedback channel - " + ride.bookingId)
    await telegramBot.sendFeedbackMessage(ride)
}
