const dynamoDBClient = require('../clients/dynamo-db-client')
const telegramClient = require('../clients/telegram-bot')
const whatsappClient = require('../clients/whatsapp-client')
const razorpayClient = require('../clients/razorpay-client')

const {VendorStatus} = require('../service/constants')

module.exports = {
    initiateVendorApprovalFlow: async function(cabVendorId){
        let cabVendor = await dynamoDBClient.getVendorById(cabVendorId);
        if(cabVendor == null || cabVendor.vendorStatus != VendorStatus.UnApproved){
            console.warn("CabVendor not found / status not UnApproved ", cabVendor)
            throw "CabVendor not in UnApproved state"
        }

        let telegramAdminPayload = {
            CabVendorId: cabVendor.cabVendorId,
            Name: cabVendor.profile.name,
            Cell: cabVendor.profile.phoneNo,
            Email: cabVendor.profile.email,
            VehicleRegNo: Object.keys(cabVendor.vehicles)
        }
        await telegramClient.sendVendorDetailsForApproval(cabVendorId, telegramAdminPayload)
    },

    approveVendor: async function(cabVendorId) {
        await this.createPaymentUrl(cabVendorId, {forceCreate: false})
        let cabVendor = await dynamoDBClient.changeVendorStatus(cabVendorId, VendorStatus.UnApproved, VendorStatus.Approved)
        await whatsappClient.sendDriverApproved(cabVendorId, cabVendor)
        return cabVendor;
    },

    createPaymentUrl: async function(cabVendorId, cabVendor, {forceCreate = true}) {
        if(cabVendor == null){
            cabVendor = await dynamoDBClient.getVendorById(cabVendorId);
        };
        
        if(!cabVendor){
            throw "CabVendor not found"
        }
        else if(cabVendor.wallet?.razorpay?.paymentUrl && forceCreate == false){
            throw "PaymentUrl is already added"
        }
        console.log("Creating UPI QR for " + cabVendorId)
        const qrInfo = await razorpayClient.createQrCode(cabVendorId);
        await dynamoDBClient.updateRazorpayQrDetails(cabVendorId, {paymentUrl: qrInfo.paymentUrl, qrId: qrInfo.id, qrImageUrl: qrInfo.image_url}, {forceUpdate: forceCreate})
    },

    cabVendorCreated: async function(cabVendorId, version) {
        if(version != "v2") {
            console.error("CabVendor version not supported", cabVendorId, version)
            return;
        }

        let cabVendor = await dynamoDBClient.getVendorById(cabVendorId);
        await this.createPaymentUrl(cabVendorId, cabVendor, {forceCreate: false})
        await telegramClient.sendGogoAdminMessage("CabVendor(V2) onboarded", {
            cabVendorId,
            ...cabVendor.profile
        })
    }
}