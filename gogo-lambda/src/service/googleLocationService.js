const axios = require('axios').create()
const Config = require('../config')
const _ = require('lodash')

axios.interceptors.request.use((config) => {
  config.headers['request-startTime'] = process.hrtime()
  return config
})

axios.interceptors.response.use((response) => {
  const start = response.config.headers['request-startTime']
  const end = process.hrtime(start)
  const milliseconds = Math.round((end[0] * 1000) + (end[1] / 1000000))
  response.headers['request-duration'] = milliseconds
  return response
})


class LocationService {

  async suggestLocations(location, options = {}) {
    try {
      let requestData = {
        params: {
          input: location,
          types: "(regions)",
          key: Config.mapsAPIKey,
          components: "country:in",
          location: "12.9716, 77.5946",
          radius: 870316,
          strictbounds: ""
        }
      }
      let response = await axios.get("https://maps.googleapis.com/maps/api/place/autocomplete/json", requestData);
      return response.data.predictions.map(prediction => {
        return {
          id: prediction.place_id,
          displayName: prediction.description,
          shortName: prediction.structured_formatting.main_text,
          address: { stateCode: prediction.structured_formatting.secondary_text } // sample stateCode : "Tamil Nadu, India" . need to correct this
        };
      })
    } catch (err) {
      console.log("[Error][SuggestLocations] :" + err);
      return [];
    }
  }

  async getPlaceInfo(placeId) {
    let requestData = { params: {
      key: Config.mapsAPIKey,
      place_id: placeId,
      fields: "formatted_address,name,address_component,type"
    }}
   let response =  await axios.get(" https://maps.googleapis.com/maps/api/place/details/json", requestData);
   if(response.data.status != "OK"){
    console.warn("Something went wrong while fetching placeDetails api", JSON.stringify(response.data))
   }
   let result = response.data.result || [];
    let parsedData = {
      shortName: this.getShortName(result.address_components),
      fullAddress: result.formatted_address,
      result
    }
    return parsedData
  }

  getShortName(addressComponents) {
    let addressComponent = addressComponents.find(cmp => _.intersection(cmp.types, ["locality", "administrative_area_level_3"]).length > 0)
    if(!addressComponent){
      addressComponent = addressComponents[0]
    }
    return addressComponent.short_name
  }

  async calcDistance(src, dst) {
    try {
      let requestData = {
        params: {
          origins: `place_id:${src.id}`,
          destinations: `place_id:${dst.id}`,
          key: Config.mapsAPIKey,
          mode: "driving"
        }
      }

      let response = await axios.get("https://maps.googleapis.com/maps/api/distancematrix/json", requestData)

      return {
        distance: response.data.rows[0].elements[0].distance.value / 1000,
        time: response.data.rows[0].elements[0].duration.value / 60,
        durationText: response.data.rows[0].elements[0].duration.text
      }
    }
    catch (err) {
      console.log("[ERROR][calcDistance] :" + err);
      throw err
    }
  }

  async getLocationFromCoOrdinates(lat, lon) {
    try {
      let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lon}&key=${Config.mapsAPIKey}&result_type=administrative_area_level_3`
      let response = await axios.get(url)
      return {
        id: response.data.results[0].place_id,
        displayName: response.data.results[0].formatted_address,
        shortName: response.data.results[0].address_components[0].short_name,
        address: { stateCode: response.data.results[0].formatted_address} // BUG BUG : this wont match the stateCode correctly
      }
    }
    catch (err) {
      console.log("[Error][getLocationFromCoOrdinates] :" + err);
      throw err;
    }
  }

  validateLocations(locations = []) {
    locations.forEach(location => {
      if (Config.servicableStates.indexOf(location.address.stateCode) == -1)
        throw { status: 412, message: 'Not serviceable' }
      return true;
    })
  }

  async getCord(location) {
    if (location.address == undefined || location.coOrd == undefined || location.coOrd.lat == undefined || location.coOrd.lon == undefined) {
      if (!location.id)
        throw { status: 400, message: "Malformed input. id not set" }
      let response = await axios.get(`https://lookup.search.hereapi.com/v1/lookup?apiKey=5TS1MuGOBQu8P3241B4wp4fhzRPnFRRmC_Ueie1mu4A&id=${location.id}`)
      let data = response.data
      location.address = response.data.address
      location.coOrd = {
        lat: data.position.lat,
        lon: data.position.lng
      }
    }
    return location.coOrd
  }

  getGoogleMapUrls(srcPlace, dstPlace, intermediates) {
    // Reference to construct url 
    //  https://developers.google.com/maps/documentation/urls/get-started#directions-action
    let result =  {
      srcUrl: encodeURI(`https://www.google.com/maps/search/?api=1&query=${srcPlace.fullAddress}&query_place_id=${srcPlace.placeId}`),
      dstUrl: encodeURI(`https://www.google.com/maps/search/?api=1&query=${dstPlace.fullAddress}&query_place_id=${dstPlace.placeId}`),
      directionUrl: encodeURI(`https://www.google.com/maps/dir/?api=1&origin_place_id=${srcPlace.placeId}&destination_place_id=${dstPlace.placeId}&origin=${srcPlace.fullAddress}&destination=${dstPlace.fullAddress}`)
    }
    if (intermediates && intermediates.length > 0) {
      const waypoint_place_ids = intermediates.map(i => i.placeId).join("|")
      const waypoints = intermediates.map(i => i.fullAddress).join("|")
      result.directionUrl = encodeURI(`https://www.google.com/maps/dir/?api=1&origin_place_id=${srcPlace.placeId}&destination_place_id=${dstPlace.placeId}&origin=${srcPlace.fullAddress}&destination=${dstPlace.fullAddress}&waypoints=${waypoints}&waypoint_place_ids=${waypoint_place_ids}`)
    }
    return result
  }
}

module.exports = new LocationService()