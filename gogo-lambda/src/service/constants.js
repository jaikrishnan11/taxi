module.exports = {
    ROUND_TRIP : "ROUND_TRIP",
    ONE_WAY: "ONE_WAY",
    RideStatus: {
        UnPublished: "UNPUBLISHED",
        UnApproved: "UNAPPROVED",
        UnAssigned: "UNASSIGNED",
        Assigned: "ASSIGNED",
        OnGoing: "ONGOING",
        Completed: "COMPLETED",
        CancelledByCustomer: "CANCELLED_BY_CUSTOMER",
        CancelledByAdmin: "CANCELLED_BY_ADMIN",
        ExpiredUnApproved: "EXPIRED_UNAPPROVED",
        Expired: "EXPIRED"
    },
    SQSMsgType: {
        DeviceConfigUpdate: "DEVICE_CONFIG_UPDATE",
        VendorApprovalPendingNotification: "VENDOR_APPROVAL_PENDING",
        RideAssigned: "RIDE_ASSIGNED",
        CabVendorCreated: "CAB_VENDOR_CREATED",
        ReCreateVendorUPI: "RECREATE_VENDOR_UPI_CODE",
        PublishRideForV1Vendors: "PUBLISH_RIDE_FOR_V1_VENDORS",
        RideClosed: "RIDE_CLOSED"
    },
    VendorStatus: {
        UnApproved: "UNAPPROVED",
        PaymentPending: "PAYMENT_PENDING",
        Approved: "APPROVED",
        Blocked: "BLOCKED"
    },
    TelegramCommand: {
        ApproveRide: "APPROVE_RIDE",
        CancelRideByAdmin: "CANCEL_RIDE_BY_ADMIN",
        ApproveVendor: "APPROVE_VENDOR"
    },
    AddMoneyStatus: {
        INITIATED: "INITIATED",
        COMPLETED: "COMPLETED",
        FAILED: "FAILED"
    },
    WallerLedgerAction: {
        AddMoney: "ADD_MONEY",
        DeductMoney: "DEDUCT_MONEY"
    },
    PaymentGateway: {
        Razorpay: "RAZORPAY",
        Ippopay: "IPPOPAY"
    },
    FirebaseTopic: {
        CabVendorsV2: "CabVendorsV2",
        AllVendors: "ALL_VENDORS"
    },
    OpensearchIndex: {
        Rides: "table-rides",
        CabVendors: "table-cab-vendors",
        Drivers: "table-cab-drivers",
        Vehicles: "table-cab-vehicles",
        GenericTableDrivers: "drivers_generic",
        GenericTable: "generic-table",
        WalletLedger: "wallet-ledger",
        Location: "driver-location"
    },
    GenericTableNamespace: {
        PhoneNoVerification: "PHONE_NO_VERIFICATION",
        VendorCreationPhoneNoVerification: "VENDOR_CREATION_PHONE_NO_VERIFICATION",
        DriverAdditionPhoneNoVerification: "DRIVER_ADDITION_PHONE_NO_VERIFICATION",
        Drivers: "drivers"
    },
    AdminAction: {
        ApproveRide: "APPROVE_RIDE",
        RemoveRide: "REMOVE_RIDE"
    }
}