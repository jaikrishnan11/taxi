const VEHICLE_REGEX = /^[A-Z]{2}\d{2}[A-Z]{1,2}\d{4}$/

module.exports = {
    removeCountryCode(phoneNumber) {
        const countryCode = "+91";
        const countryCode2 = "91";
        const countryCode3 = "+";
        const formattedNumber = phoneNumber.trim().replace(/\s+/g, ""); // remove whitespace and format number

        if (formattedNumber.startsWith(countryCode)) {
            return formattedNumber.substr(countryCode.length);
        } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode2)) {
            return formattedNumber.substr(countryCode2.length);
        } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode3)) {
            return formattedNumber.substr(countryCode3.length);
        } else {
            return formattedNumber;
        }
    },
    parseVehicleNumber(vehicleNumber) {
        return vehicleNumber.toUpperCase().replace(/\s/g, '');
    },
    validateVehicleNumber(vehicleNumber) {
        return vehicleNumber.match(VEHICLE_REGEX)
    },
    getTableNameFromEventSource(arn) {
        let ddbTable = arn.split(':')[5].split('/')[1]
        return ddbTable
    }
}