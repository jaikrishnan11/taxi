
let CONFIG;
if (process.env.NODE_ENV == 'Prod')
    CONFIG = require('./prod-config.json')
else
    CONFIG = require('./default-config.json')

if (process.env.NODE_ENV) {
    CONFIG.isProduction = process.env.NODE_ENV == 'Prod';
}

if (process.env.SEARCH_USERNAME) {
    CONFIG.opensearch.credentials.username = process.env.SEARCH_USERNAME
    CONFIG.opensearch.credentials.password = process.env.SEARCH_PASSWORD

    CONFIG.elasticSearchConfig.credentials.username = process.env.SEARCH_USERNAME
    CONFIG.elasticSearchConfig.credentials.password = process.env.SEARCH_PASSWORD
} 

if (process.env.WHATSAPP_TOKEN) {
    CONFIG.whatsapp.token = process.env.WHATSAPP_TOKEN
}


module.exports = CONFIG;