const dynamoDBClient = require('../clients/dynamo-db-client')
const telegramClient = require('../clients/telegram-bot')

module.exports.paymentsWebhook = async (event, context) => {
    console.log(event.body);
    try {
        let payload = JSON.parse(event.body)
        if (payload.event == 'qr_code.credited') {
            let cabVendorId = payload.payload.qr_code.entity?.notes?.cabVendorId
            if (cabVendorId == null) {
                console.error("CabVendor is null... manually consolidate the amount")
                await telegramClient.sendDevMessage({ error: "CabVendor id not found in QR payment. consolidate the amount" })
            }
            let qr = payload.payload.qr_code.entity
            let payment = payload.payload.payment.entity

            let creditDetails = {
                cabVendorId,
                payment: {
                    paymentId: payment.id,
                    createdDate: new Date(payment.created_at * 1000).toISOString(),
                    amountInPaise: payment.amount,
                    status: payment.status
                },
                qr: {
                    qrId: qr.id,
                    qrImageUrl: qr.image_url
                }
            }

            if (creditDetails.payment.status != 'captured') {
                console.error("Payment status is not captured. Skipping")
                return
            }
            console.log("Constructed details", creditDetails)
            /*
                TODO:
                  1. Verify secret present in the Header 
                  1. Check if cabVendor is present . add a conditio expression 
                  1. Verify if QR details are same 
                  2. Better to get the payment details from razorpay again and cross check
                  3. What is CabVendorId + createdDate combo exists in ledger . aka two operations in the same second ... is that possibility ?
            */
            await dynamoDBClient.captureRazorapyQRPayment(cabVendorId, creditDetails)
            await telegramClient.sendDevMessageText(`You got paid ₹${creditDetails.payment.amountInPaise/100} bro.`)
        } else {
            console.debug("Skipping event " + payload.event)
        }
    } catch (err) {
        console.error("Error processing ", err)
        await telegramClient.sendDevMessage({msg: "Error payment webhook", err})
    }
}
//7tsY&K66RtZTXo^r