const TelegramBot = require('node-telegram-bot-api');
const {TelegramCommand, RideStatus} = require('../service/constants')
const ddbClient = require('../clients/dynamo-db-client')
const telegramClient = require('../clients/telegram-bot');
const vendorMgmtService  = require('../service/vendorMgmtService')
const firebaseClient = require('../clients/firebase-admin-client')
const mailClient = require('../clients/mail-sender')
const whatsappClient = require('../clients/whatsapp-client');
const sqsClient = require('../clients/sqs-client');
const Constants = require('../service/constants')

const moment = require('moment')
const token = '5143210951:AAHdTsRzC516Q-6oX3xWqcR55AVNXobBdRY';
bot = new TelegramBot(token, {polling: false});

module.exports.telegramBot = async (event, context) => {
    console.log("Received event ", event)
    let body = JSON.parse(event.body)
    console.log(event.body);
    if(body.callback_query){ 
        let response = await answerCallbackQuery(body.callback_query)
        await Promise.all([
          bot.answerCallbackQuery(body.callback_query.id, {  text: response.message || response.code   }),
          response.code == "Successful" ? 
            bot.editMessageReplyMarkup({
              inline_keyboard: [response.inlinekeyboard]
            }, {
              chat_id: body.callback_query.message.chat.id,
              message_id: body.callback_query.message.message_id
            }) 
                : Promise.resolve()
        ])
    }
}

answerCallbackQuery = async (callbackQuery) => {
  try{
    let {id, data} = callbackQuery;
    let [command, arg] = data.split(' ')
    let inlinekeyboard = []
    if(command == TelegramCommand.ApproveRide){
        let ride = await ddbClient.changeRideStatus(arg, RideStatus.UnApproved, RideStatus.UnAssigned)
        let sqsPromise = sqsClient.sendRideNotficationForVendorsV1(arg)
        // let telegramPromise1 = telegramClient.sendRideMessage({
        //     ride: ride, //Kevalamana hack... should have sent the entire ride object
        //     from: ride.bookingDetails.pickup.fullAddress, 
        //     to: ride.bookingDetails.drop.fullAddress,
        //     tripType: ride.bookingDetails.tripType,
        //     carType: ride.bookingDetails.cabType,
        //     time: moment(ride.bookingDetails.tripStartTime).utcOffset("+0530").format('MMM Do YYYY, h:mm a'),
        //     amount: ride.priceDetails.rateCard.total,
        //     commissionFee: ride.priceDetails.cabVendor.transactionFee 
        // })
        let telegramPromise2 =  firebaseClient.publishNewRide({
              from: ride.bookingDetails.pickup.shortName, 
              to: ride.bookingDetails.drop.shortName,
              carType: ride.bookingDetails.cabType == "xylo" ? "Any SUV" : ride.bookingDetails.cabType, 
              amount: ride.priceDetails.rateCard.total, 
              tripType: ride.bookingDetails.tripType,
              topicName: Constants.FirebaseTopic.CabVendorsV2
          })
        // NOTE: !!! Comment the code  in customer-server.js before uncommenting here !!! 
        let whatsappClientPromise = Promise.resolve() //whatsappClient.sendBookingConfirmation(ride.bookingId, ride)
        await Promise.all([sqsPromise, telegramPromise2, whatsappClientPromise])
        inlinekeyboard.push({
          text: "Remove",
          callback_data: `${Constants.TelegramCommand.CancelRideByAdmin} ${ride.bookingId}`
      })
    } else if (command == TelegramCommand.CancelRideByAdmin){
        await ddbClient.changeRideStatusIfAnyOf(arg, [RideStatus.UnApproved, RideStatus.UnAssigned], RideStatus.CancelledByAdmin)
    } else if (command == TelegramCommand.ApproveVendor){
        await vendorMgmtService.approveVendor(arg)
    } else {
        return  {code: "Unknown command - " + command} ;
    }
    return {code: "Successful", inlinekeyboard}
  } catch(err){
    console.error(err)
    return {code: "ERROR", message: err.message}
  }
   
}

module.exports.answerCallbackQuery = answerCallbackQuery
/*
{
    update_id: 854367049,
    callback_query: {
      id: '3045511683238860686',
      from: {
        id: 709088445,
        is_bot: false,
        first_name: 'Sankar',
        last_name: 'Varadarajan',
        username: 'thanos1_2',
        language_code: 'en'
      },
      message: {
        message_id: 973,
        from: [Object],
        chat: [Object],
        date: 1676783466,
        reply_to_message: [Object],
        text: 'Message',
        reply_markup: [Object]
      },
      chat_instance: '-3734066993266349940',
      data: 'option2 - callback'
    }
  }
  */