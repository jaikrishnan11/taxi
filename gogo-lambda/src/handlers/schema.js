const Joi = require('joi')

const CoOrd = Joi.object({
    "lat":   Joi.number().min(-90).max(90).optional(),
    "lon": Joi.number().min(-180).max(180).optional()
})

const SearchLocation  = Joi.object({
    "id" : Joi.string().required(),
    "coOrd": CoOrd.optional()
})

const CalculateRateCards = Joi.object({
    "tripType": Joi.string().valid("ROUND_TRIP", "ONE_WAY").required(),
    "src" : SearchLocation.required(),
    "dst" : SearchLocation.required(),
    "pickUpDate": Joi.date().allow(""),
    "dropDate": Joi.date().allow("")
})

module.exports = {
    CalculateRateCards,
    CoOrd
}