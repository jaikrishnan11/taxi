
const openSearchClient = require('../clients/opensearch-client')
const elasticSearchClient = require('../clients/elastic-search-client')
const itemRepairer = require('../service/ItemRepairer')
const telegramClient = require('../clients/telegram-bot')
const AWS = require('aws-sdk')
const { getTableNameFromEventSource } = require('../commons/utils')
const { OpensearchIndex, GenericTableNamespace } = require('../service/constants')
const _ = require('lodash')

function processGenericTableUpdate(updatedItem) {
    let documentId = `${updatedItem.namespace}_${updatedItem.primaryKey}`
    return [{ documentId: documentId, document: updatedItem, index: OpensearchIndex.GenericTable }]
}

function processCabVendorUpdate(updatedItem) {
    let documentsToUpsert = []

    let cabVendorId = updatedItem.cabVendorId
    let vehicles = updatedItem.vehicles
    let drivers = updatedItem.drivers

    delete updatedItem.vehicles
    delete updatedItem.drivers
    delete updatedItem.rides
    delete updatedItem?.wallet?.razorpay?.paymentUrl

    // TODO: Don't process if only device token is updated

    updatedItem.wallet.total = updatedItem.wallet.total / 100
    documentsToUpsert.push({ index: OpensearchIndex.CabVendors, documentId: cabVendorId, document: updatedItem })

    if (vehicles) {
        for (let vehicle of Object.values(vehicles)) {
            vehicle.cabVendorId = cabVendorId
            vehicle.createdDate = updatedItem.createdDate
            vehicle.externalVendorId = updatedItem.externalId
            documentsToUpsert.push({ index: OpensearchIndex.Vehicles, document: vehicle, documentId: vehicle.vehicleRegNo })
        }
    }

    if (drivers && updatedItem.version != "v1") {
        for (let driver of Object.values(drivers)) {
            driver.cabVendorId = cabVendorId
            driver.externalVendorId = updatedItem.externalId
            driver.createdDate = updatedItem.createdDate
            documentsToUpsert.push({ index: OpensearchIndex.Drivers, document: driver, documentId: driver.licenseNumber })
        }
    }

    // We are updating ES index directly
    // if (updatedItem.location) {
    //     for (let locationOwnerId of Object.keys(updatedItem.location)) {
    //         let document = Object.assign({}, updatedItem.location[locationOwnerId])
            
    //         if (locationOwnerId == "cabVendor") {
    //             locationOwnerId = cabVendorId
    //         }
            
    //         document.cabVendorId = cabVendorId
    //         document.locationOwnerId = locationOwnerId
    //         documentsToUpsert.push({index: OpensearchIndex.Location, document, documentId: locationOwnerId})
    //     }
    // }

    return documentsToUpsert;
}

function processLedgerUpdate(ledgerEntry) {
    let documentId = `${ledgerEntry.cabVendorId}_${ledgerEntry.createdDate}`
    ledgerEntry.opDetails.amount = ledgerEntry.opDetails.amount / 100
    return [{ index: OpensearchIndex.WalletLedger, documentId: documentId, document: ledgerEntry }]
}

function processRides(updatedItem) {
    let bookingId = updatedItem.bookingId
    delete updatedItem?.priceDetails?.rateCard?.priceSplit
    delete updatedItem?.priceDetails?.rateCard?.fareSplitText
    return [{ index: OpensearchIndex.Rides, document: updatedItem, documentId: bookingId }]
}

module.exports.transformRide = processRides
module.exports.transformLedger = processLedgerUpdate
module.exports.transformGeneric = processGenericTableUpdate
module.exports.transformCabVendor = processCabVendorUpdate

module.exports.ddbStreamsProcessor = async (event, context) => {
    try {
        let documentsToUpsert = []
        for (let record of event.Records) {
            console.log("Processing Record", record)
            if (record.eventSource == "aws:dynamodb") {
                await processDDBStream(record, documentsToUpsert)
            } else if (record.eventSource == "aws:sqs") {
               processSQSStream(record, documentsToUpsert)
            } else {
                console.error("Unknown event source " + record.eventSource)
            }
        }
        if (!_.isEmpty(documentsToUpsert)) {
            await elasticSearchClient.bulkIndexDocuments(documentsToUpsert);
        } else {
            console.debug("Empty documentsToUpsert", documentsToUpsert)
        }
    } catch (err) {
        console.error("Error processing record", err)
        await telegramClient.sendDevMessage({err: JSON.stringify(err), msg: "Ingestion failure "})
    }
}

async function processDDBStream(record, documentsToUpsert) {
    let tableName = getTableNameFromEventSource(record.eventSourceARN)
    let newImage = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage)
    switch (tableName) {
        case "rides":
            if (record.eventName == "INSERT" || record.eventName == "MODIFY")
                documentsToUpsert.push(...await processRides(newImage))
            else if (record.eventName == "REMOVE")
                console.warn("Removing ride need to implement !?! " + record.eventName)
            break
        case "generic-table":
            if (record.eventName == "INSERT" || record.eventName == "MODIFY")
                documentsToUpsert.push(...await processGenericTableUpdate(newImage))
            else if (record.eventName == "REMOVE")
                console.warn("Removing generic table entry need to implement !?! " + record.eventName)
            break

        case "cab-vendors":
            if (record.eventName == "INSERT" || record.eventName == "MODIFY") {
                try {
                    await itemRepairer.repairCabVendor(newImage)
                } catch (ex) {
                    console.error("CabVendor repair failed", ex)
                }
                documentsToUpsert.push(...await processCabVendorUpdate(newImage))
            }
            else if (record.eventName == "REMOVE")
                console.warn("Why are we removing CabVendor ??")

            break
        case "wallet-book-keeping":
            if (record.eventName == "INSERT" || record.eventName == "MODIFY")
                documentsToUpsert.push(...await processLedgerUpdate(newImage))
            else if (record.eventName == "REMOVE")
                console.error("Why are we removing ledger entry ??")
            break
        default:
            throw "Unknown table " + tableName
    }
}

function processSQSStream(record, documentsToUpsert) {
    let message = JSON.parse(record.body).payload
    let cabVendorId = message.cabVendorId
    let documentId = message.locationOwnerId
    if (documentId == "cabVendor") {
        documentId = cabVendorId
    }
    documentsToUpsert.push({index: OpensearchIndex.Location, document: message, documentId})
}
