const telegramClient = require('../clients/telegram-bot')
const _ = require('lodash')
const whatsappClient = require('../clients/whatsapp-client')

const VERIFICATION_TOKEN = "Mb5UGsfRPifQVppDjfncwnN4WJvS%f#jG&U25B52!D5wHQ##*^tcTMGWUJTDpH%bvwSinn@6tVE@dqYLTVJxAwx4XguW&6PvLM8w&%ZL@fErG4%3w@5TrUyE"

const REPLY_MSG = "This is a automated account for sending ride updates. Please contact our customer support number(+916385588806) for queries"

module.exports.whatsappWebHook = async(event, context) => {
    const path = event.rawPath
    const method = event.requestContext.http.method
    console.debug(`${path} : ${method}`)
    console.log(event.body);

    if(method == "GET" && path.includes("webhooks") != -1){
        let queryParams = event.queryStringParameters
        console.log(queryParams)
        let statusCode = 400
        let responseString = "NOT_VERIFIED"
        if(queryParams["hub.verify_token"] == VERIFICATION_TOKEN){
            statusCode = 200
            responseString = queryParams["hub.challenge"]
        }
        return {
            statusCode,
            body: responseString
        };
    } else {
        let body = JSON.parse(event.body)
        console.debug(event.body)
        if(body.object != "whatsapp_business_account"){
            console.debug("Not whatsapp_business_account")
            await telegramClient.sendDevMessageText(`[WhatsApp]Received ${body.object} event. Check logs`)
            return { statusCode: 200,  body: "OK" }
        }
        try {
            for(let entry of body.entry){
                for(let change of entry.changes){
                    if(change.field == "messages" && change.value.messages){
                        console.debug("processing messages")
                        let receivedTo = change.value.metadata.display_phone_number
                        let message = change.value.messages.map(msg => `${msg.from} : ${msg?.text?.body}`).join(' \n ')
                        //TODO: Messages from different numbers can be clubbed but we are sending autoreply only to the first message
                        let whatsAppReplyPromise =  whatsappClient.sendTextMessage(change.value.messages[0].from, REPLY_MSG)  
                        let telegramPromise =  telegramClient.sendWhatsappMessage(`[WhatsApp] ${message}`)
                        await Promise.all([whatsAppReplyPromise, telegramPromise])
                    } else if(change.field == "template_performance_metrics"){
                        await telegramClient.sendDevMessage({WhatsAppTemplateMetrics: change.value.templates_performance_metrics})
                    } else {
                        console.log("Ignoring ", change)
                    }
                }
            }
        } catch(err) {
            console.error(err)
            await telegramClient.sendDevMessageText(`[WhatsApp] Error processing event ${err}`)
        }
        return {
            statusCode: 200,
            body: "OK"
        }
    }

}