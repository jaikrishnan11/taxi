const dynamoDBClient = require('../clients/dynamo-db-client')
const telegramClient = require('../clients/telegram-bot')
const firebaseClient = require('../clients/firebase-admin-client')
const vendorMgmtService = require('../service/vendorMgmtService')
const rideStatusUpdationService = require('../service/rideStatusUpdationService')

const Constants = require('../service/constants')
const moment = require('moment')

async function deviceRegistration(payload){
    let {cabVendorId, deviceToken} = payload
    await firebaseClient.subscribeDeviceToTopic(deviceToken)
    let cabVendor = await dynamoDBClient.getVendorById(cabVendorId)
    if (cabVendor.version == "v2") {
        await firebaseClient.subscribeDeviceToTopic(deviceToken, Constants.FirebaseTopic.CabVendorsV2)
    }
}

 async function publishRideForV1Vendors(bookingId) {
    let ride = await dynamoDBClient.getRideByBookingId(bookingId)
    if (ride.rideStatus != Constants.RideStatus.UnAssigned) {
        console.log("[publishRideForV1Vendors] Ride not in UnAssigned status", ride.status)
        return;
    }

    // TODO: Move these back to telegram-bot-processor.js for the case `command == TelegramCommand.ApproveRide` once 
    //       most moved to V2         
    // Send to ride announce
    let telegramPromise1 = telegramClient.sendRideMessage({
            ride: ride, //Kevalamana hack... should have sent the entire ride object
            from: ride.bookingDetails.pickup.fullAddress, 
            to: ride.bookingDetails.drop.fullAddress,
            tripType: ride.bookingDetails.tripType,
            carType: ride.bookingDetails.cabType == "xylo" ? "Any SUV" : ride.bookingDetails.cabType,
            time: moment(ride.bookingDetails.tripStartTime).utcOffset("+0530").format('MMM Do YYYY, h:mm a'),
            amount: ride.priceDetails.rateCard.total,
            commissionFee: ride.priceDetails.cabVendor.transactionFee 
        })
    let fireBasePromise =  firebaseClient.publishNewRide({
        from: ride.bookingDetails.pickup.shortName, 
        to: ride.bookingDetails.drop.shortName,
        carType: ride.bookingDetails.cabType == "xylo" ? "Any SUV" : ride.bookingDetails.cabType, 
        amount: ride.priceDetails.rateCard.total, 
        tripType: ride.bookingDetails.tripType
    })
    await Promise.all([telegramPromise1, fireBasePromise])
}

module.exports.generalProcessor = async function (event, context) {
    let body
    try {
        for (let record of event.Records) {
            body = record.body
            console.log(body)
            let {type, payload} = JSON.parse(record.body)
            if(type == Constants.SQSMsgType.DeviceConfigUpdate){
                await deviceRegistration(payload)
            } else if(type == Constants.SQSMsgType.VendorApprovalPendingNotification) {    
                await vendorMgmtService.initiateVendorApprovalFlow(payload.cabVendorId, payload)
            } else if(type == Constants.SQSMsgType.RideAssigned) {
                await rideStatusUpdationService.processAssignedRide(payload)
            } else if(type == Constants.SQSMsgType.CabVendorCreated) {
                // SQS sent only for v2+ cab vendor not v1
                await vendorMgmtService.cabVendorCreated(payload.cabVendorId, payload.version)
            } else if(type == Constants.SQSMsgType.ReCreateVendorUPI) {
                await vendorMgmtService.createPaymentUrl(payload.cabVendorId, null/*cabVendor*/,{forceCreate: payload.forceCreate})
            } else if(type == Constants.SQSMsgType.RideClosed) {
                await rideStatusUpdationService.processRideClosedEvent(payload.bookingId)
            }
            // TODO: Remove this once majority of vendors moved to V2
            else if(type == Constants.SQSMsgType.PublishRideForV1Vendors) {
                await publishRideForV1Vendors(payload.bookingId)
            } else {
                throw "Unknown payload type"
            }
        }
        return "OK"
    } catch (err) {
        console.error("Error processing sqs event", err)
        await telegramClient.sendDevMessage({err: err, body})
        return "ERRORED"
    } 
}