const dynamoDBClient = require('../clients/dynamo-db-client')
const telegramClient = require('../clients/telegram-bot')
const mailClient = require('../clients/mail-sender')
const pktClient = require('../clients/pkt-client')
const sqsClient = require('../clients/sqs-client');
const smsClient = require('../clients/digimile-client')
const firebaseClient = require('../clients/firebase-admin-client')
const whatsappClient = require('../clients/whatsapp-client');
const axios = require('axios')
const moment = require("moment")

const Schema = require('./schema')

const locationService = require('../service/googleLocationService')
const rateCardService = require('../service/rateCardService')
const rideStatusUpdaterService = require('../service/rideStatusUpdationService')

const { customAlphabet } = require('nanoid')
const nanoid = customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', 6)
const setCookie = require('set-cookie-parser');

const { func } = require('joi');
const invoiceGeneratorClient = require('../clients/invoice-generator-client');
const Constants = require('../service/constants');
const telegramBotProcessor = require('./telegram-bot-processor')

/*
    TODO: 
      - Find whether request is from App or not
      - Find whether its internal user
      - Check with calculating Rate cards is taking > 1s
*/
async function calculateRateCardsHandler(requestBody, requestContext = {}) {
    requestBody.isApp = requestContext.isGogoApp || false
    const dtResult = await locationService.calcDistance(requestBody.src, requestBody.dst)
    Object.assign(requestBody, dtResult)
    const response = rateCardService.getRateCards(requestBody)
    return {
        "headers": {
            "Cache-Control": "max-age=3600, public, must-revalidate"
        },
        "body": response
    }
}

async function bookRide(requestBody, requestContext = {}) {

    let rideDetails = Object.assign({}, requestBody, {
        createdAt: new Date().toISOString(),
        bookingId: nanoid()
    })

    await sqsClient.sendBookingConfirmation(rideDetails, requestContext)
    return {
        body: {
            message: "Booking Confirmed",
            rideDetails
        }
    }
}

async function apiclubProxy(requestBody, requestContext = {}) {
    console.log(requestBody)
    const response = await axios.post(`http://ec2-98-130-27-210.ap-south-2.compute.amazonaws.com:3000/proxy`, requestBody);
    console.log(response.data)
    return  {
        "statusCode" : 200,
        "body" : response.data,
        "headers" : { "Content-Type": "application/json" }
    };
}

const HANDLER_MAP = {
    "POST:/api/cust/rateCards": {
        handler: calculateRateCardsHandler,
        schema: Schema.CalculateRateCards
    },
    "POST:/api/cust/bookRide": {
        handler: bookRide
    },
    "POST:/api/apiclub/proxy": {
       handler:   apiclubProxy
    },
    "POST:/locations/distance": {
        handler: calculateRateCardsHandler,
        schema: Schema.CalculateRateCards
    },
    "POST:/publish": {
        handler: bookRide
    },
    "POST:/elastic": {
        handler: elasticProxy
    },
    "GET:/api/cust/warmup": {
        handler: () => { return { body: { "msg": "OK" } } }
    },
    "POST:/sendAlerts":{
        handler: sendAlertsToAllVendors
    },
    "POST:/generate-invoice":{
        handler: generateInvoice
    },
    "POST:/admin-action": {
        handler: adminAction
    }
}

function requestContextGather(event) {
    const cookies = setCookie.parse(event, { map: true, decodeValues: true })
    return {
        isInternalUser: cookies["isInternal"] == 'true' || cookies["isinternal"] == 'true' || event.headers["isInternal"] == 'true',
        isGogoApp: false
    }
}
async function elasticProxy(requestBody) {
    try {
        console.log(requestBody)
        const response = await axios.post(`http://ec2-98-130-27-210.ap-south-2.compute.amazonaws.com:9200/${requestBody.urlPath}`, requestBody.body, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic ZWxhc3RpYzptaWVQcjhOckxUQUpsT3dYZDZjWg=="
            },
            maxBodyLength: Infinity
        });
        console.log(response.data)
        return  {
            "statusCode" : 200,
            "body" : response.data,
            "headers" : { "Content-Type": "application/json" }
        };
    } catch (error) {
        throw new Error(error.response ? error.response.data : error.message);
    }
}

async function adminAction(requestBody) {
    const { action, payload } = requestBody
    if (action == Constants.AdminAction.ApproveRide) {
        let bookingId = payload.bookingId;
        let response = await telegramBotProcessor.answerCallbackQuery({data: `${Constants.TelegramCommand.ApproveRide} ${bookingId}`});
        return {
            "statusCode" : 200,
            "body" : {msg: "Approved"},
            "headers" : { "Content-Type": "application/json" }
        }
    }
    if (action == Constants.AdminAction.RemoveRide) {
        let bookingId = payload.bookingId;
        let response = await telegramBotProcessor.answerCallbackQuery({data: `${Constants.TelegramCommand.CancelRideByAdmin} ${bookingId}`});
        return {
            "statusCode" : 200,
            "body" : {msg: "Ride Removed"},
            "headers" : { "Content-Type": "application/json" }
        }
    }
    return {
        "statusCode" : 404,
            "body" : {msg: "Ride action not found"},
            "headers" : { "Content-Type": "application/json" }
    }
}

async function generateInvoice(params) {
    // send to customer
    // let bookingId = params.bookingId;
    // if (!bookingId) {
    //     console.log("Received empty booking Id", bookingId)
    //     throw "booking Id found empty"
    // }
    // try {
    //     let ride = await dynamoDBClient.getRideByBookingId(bookingId)
    //     if(!ride) {
    //         console.log("Found empty ride in the db", bookingId)
    //         throw "Ride not found"
    //     }
    //     // apply the overriden parameters on the ride payload
    //     await invoiceGeneratorClient.generateInvoice(ride)
    // } catch(err) {
    //     console.log("Error while generating invoice", params, err)
    //     throw err
    // }
}

async function sendAlertsToAllVendors() {
    try {
        await firebaseClient.sendMessageToTopicUsingNotifee({
            title : "New Ride Arrived",
            body : "Please check the new rides"
        });
        return {
            "statusCode" : 200,
            "body" : "okay",
            "headers" : { "Content-Type": "application/json" }
        }
    } catch(err) {
        console.error("Error while sending alerts to all vendors ", err);
        await telegramClient.sendDevMessage("Error while sending alerts to all vendors", err)
        return {
            "statusCode" : 400,
            "body" : err
        }
    }
}

async function publishRideV2(payload) {
    try{
        let bookingId = payload.bookingId;
        let ride = await dynamoDBClient.getRideByBookingId(bookingId)
        if(! ride){
            console.error("Ride not found")
            throw `Ride ${bookingId} not found`
        }
        let pickup = ride.bookingDetails.pickup;
        let drop = ride.bookingDetails.drop;
        let priceDetails = ride.priceDetails
        let placesInfo = await Promise.all([locationService.getPlaceInfo(pickup.placeId), locationService.getPlaceInfo(drop.placeId)])
        
        pickup = {
            ...pickup,
            shortName: placesInfo[0].shortName,
            fullAddress: placesInfo[0].fullAddress
        }
        drop = {
            ...drop,
            shortName: placesInfo[1].shortName,
            fullAddress: placesInfo[1].fullAddress
        }
        let cabVendorPriceDetails = {
            transactionFee: Math.round((priceDetails.rateCard?.totalBeforeEstCharges || priceDetails.rateCard?.total) * 0.1 * 100)/100
        }
        
        ride = await dynamoDBClient.updateRideDetailFromLambda({bookingId, pickup, drop, cabVendorPriceDetails})

        let telegramNotfPromise = telegramClient.sendRideForApproval(ride)
        let mailPromise = mailClient.sendBookingConfirmationEmail(ride.customerDetails.email, bookingId, ride)
        let whatsappClientPromise = whatsappClient.sendBookingConfirmation(bookingId, ride)
        let smsPromise = smsClient.sendConfirmationMessage(ride)
        await Promise.all([telegramNotfPromise, mailPromise, whatsappClientPromise, smsPromise])
    } catch(err){
        console.error("Error publishV2 " +  err);
        await telegramClient.sendDevMessage("Error process publishV2", err)
    }
}


async function publishUpdatedRideV2(payload) {
    let bookingId = payload.bookingId;
    let ride = await dynamoDBClient.getRideByBookingId(bookingId)
    let whatsappClientPromise = whatsappClient.sendBookingUpdate(bookingId, ride)
    let telegramNotfPromise = telegramClient.sendRideForApproval(ride)
    await Promise.all([telegramNotfPromise, whatsappClientPromise])
}

module.exports.SQSBookingProcessor = async (event, context) => {
    try {
        for (let record of event.Records) {
            let {version, updatedRide, payload} = JSON.parse(record.body);
            console.log("Processing payload ", record.body)
            if(version == "v2" && updatedRide == true) {
                await publishUpdatedRideV2(payload)
            } else if(version == "v2"){
                await publishRideV2(payload)   
            } else {
                console.error("Unsupported ride version")
                throw "Unsupported ride version"
            }
        }
    } catch (err) {
        console.error("Error processing SQS message", err)
        await telegramClient.sendDevMessage({ msg: "booking publish  failure", err })
    }
    return "ok";
}

module.exports.rideStatusUpdaterCron = async (event, context) => {
    try {
        let onGoingRideUpdate = await rideStatusUpdaterService.updateOnGoingRides();
        let assignedRideUpdate =  await rideStatusUpdaterService.updateAssingedRideStatus();
        let unApprovedRidesExpiry = await rideStatusUpdaterService.updateUnApprovedRides();
       // await Promise.all([assignedRideUpdate, onGoingRideUpdate])
        return "ok"
    } catch (err) {
        console.error("Error updating Ride status during cron job", err);
        await telegramClient.sendDevMessage({msg: "ride status updater failed", err})
        return "err"
    }
}

module.exports.customerEndpointHandler = async (event, context) => {
    try {
        const path = event.rawPath
        const method = event.requestContext.http.method

        if (!HANDLER_MAP[`${method}:${path}`])
            return {
                statusCode: 404,
                body: JSON.stringify({ message: `No handler found for ${method}:${path}` })
            }
        const endpointHandler = HANDLER_MAP[`${method}:${path}`]
        const requestContext = requestContextGather(event)

        const { body, statusCode = 200, headers = {} } = await endpointHandler.handler(JSON.parse(event.body || '{}'), requestContext, event)

        return {
            statusCode,
            body: JSON.stringify(body),
            headers: Object.assign(headers, {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Expose-Headers": "*",
             //   "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
                "Access-Control-Max-Age": "31536000",
                "Content-type": "application/json",
                "test": "test"
            })
        };

    } catch (err) {
        console.error(err);
        telegramClient.sendDevMessage({ msg: `Error processing ${event.rawPath}`, err })

        return {
            statusCode: 500,
            body: JSON.stringify({ messge: "Unable to process. Retry later / Call customer care for enquiry" })
        }
    }
}
