
const axios = require('axios');
const telegramBot = require('./telegram-bot');
const _ = require('lodash')
let Config = require('../config').whatsapp
const FormData = require('form-data');

// Config = Object.assign(Config, {
//     "phoneNoId": "113309415062016",
//     "phoneNoAccountId": "105480608958088",
//     "template": { "bookingConfirmation": "booking_template_2", "rideComplete": "booking_complete" }
// })
const isProduction = require('../config').isProduction

class WhatsAppClient {
  constructor() {
    this.url = `https://graph.facebook.com/v16.0/${Config.phoneNoId}/messages`
  }

  async sendTextMessage(toPhoneNumber, text) {
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify({
        "messaging_product": "whatsapp",
        "recipient_type": "individual",
        "to": `91${this.removeCountryCode(toPhoneNumber)}`,
        "type": "text",
        "text": { // the text object
          "preview_url": true,
          "body": text
        }
      })
    };
    await this.makeRequest(config)
  }

  async sendBookingConfirmation(bookingId, ride) {
    let payload = this.constructPayload("booking_confirmation", ride)
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify(payload)
    };

    await this.makeRequest(config)
  }

  async sendBookingUpdate(bookingId, ride) {
    let rideUpdateText = "Note: The price of the ride has been updated"
    let payload = this.constructPayload(Config.template.bookingConfirmation, ride, rideUpdateText)
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify(payload)
    };

    await this.makeRequest(config)
  }

  async sendRideCompletedWithInvoice(bookingId, ride, invoiceBuffer) {
    let mediaResponse = await this.uploadInvoicePdf(bookingId, invoiceBuffer);
    console.log("Invoice whatsapp  media id ", mediaResponse)
    let payload = this.constructPayload(Config.template.rideComplete, ride, mediaResponse)
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify(payload)
    };

    await this.makeRequest(config)
  }

  async sendDriverApproved(cabVendorId, cabVendor) {
    let payload = this.constructPayload(Config.template.driverApproved, cabVendorId, cabVendor)
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify(payload)
    };
    await this.makeRequest(config)
  }

  async sendDriverAssignedMessage(ride, cabVendor) {
    if (ride?.cabVendor?.cabVendorId != cabVendor.cabVendorId) {
      console.error("[sendDriverAssignedMessage] CabVendor id mismatch with ride object")
      throw `CabVendor id mismatch with ride object - ${ride.bookingId} - ${cabVendor.cabVendorId}`
    }

    let payload = this.constructPayload("driver_assigned", ride, cabVendor)
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify(payload)
    };

    await this.makeRequest(config)
  }

  async sendOtp(otp, phoneNo) {
    let payload = this.constructPayload('phone_verification', otp, phoneNo)
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Config.token}`
      },
      data: JSON.stringify(payload)
    };

    await this.makeRequest(config)
  }

  async uploadInvoicePdf(bookingId, buffer) {
    let data = new FormData();
    data.append('messaging_product', 'whatsapp');
    data.append('file', buffer, `invoice-${bookingId}.pdf`);
    data.append('type', 'application/pdf');
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `https://graph.facebook.com/v16.0/${Config.phoneNoId}/media`,
      headers: {
        'Authorization': `Bearer ${Config.token}`,
        ...data.getHeaders()
      },
      data
    }
    let mediaResponse = await this.makeRequest(config)
    return mediaResponse
  }

  async makeRequest(config) {
    try {
      if (isProduction == false) {
        console.debug("Not sending whatsapp notification")
         return true;
      }
      let response = await axios.request(config)
      console.log(JSON.stringify(response.data))
      return response.data
    } catch (err) {
      if (!err.response) {
        console.error(err)
        await telegramBot.sendDevMessageText(err)
      } else {
        let statusCode = err.response.status
        let headerErrorMessage = err.response.headers["www-authenticate"]
        let errorMsgToLog = `[sendBookingConfirmation] ${statusCode} - ${headerErrorMessage}`
        console.log(err.response.data.error)
        console.error(errorMsgToLog)
        await telegramBot.sendDevMessageText(errorMsgToLog)
      }
    }
  }

  constructPayload(templateName, ride, arg1) {
    if (templateName == "booking_confirmation") // I gave template names different in prod and test account.. my bad
      return this.payloadForBookingTemplate(ride, "booking_confirmation2")
    else if (templateName == "booking_complete")
      return this.payloadForBookingComplete(ride, arg1)
    else if (templateName == "driver_assigned")
      return this.payloadForDriverAssigned(ride, arg1)
    else if (templateName == "driver_approved")
      return this.payloadForDriverApproved(ride, arg1)
    else if(templateName == "phone_verification")
        return this.payloadForOTP(ride, arg1)
    else {
      console.error("Template not found")
      throw `Template ${templateName} not found`
    }
  }

  toHoursAndMinutes(totalMinutes) {
    const hours = Math.floor(totalMinutes / 60);
    const minutes = Math.floor(totalMinutes % 60);
    let string = ""
    if (hours == 1) {
      string += hours + " Hour"
    }
    else if (hours > 1) {
      string += hours + " Hours "
    }

    if (minutes > 1)
      string += `${minutes} Minutes`

    return string;
  }

  removeCountryCode(phoneNumber) {
    phoneNumber = phoneNumber + ''
    const countryCode = "+91";
    const countryCode2 = "91";
    const formattedNumber = phoneNumber.trim().replace(/\s+/g, ""); // remove whitespace and format number

    if (formattedNumber.startsWith(countryCode)) {
      return formattedNumber.substr(countryCode.length);
    } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode2)) {
      return formattedNumber.substr(countryCode2.length);
    } else {
      return formattedNumber;
    }
  }

  payloadForDriverAssigned(ride, cabVendor) {
    let tripStartTime = new Date(ride.bookingDetails.tripStartTime)
    let payload = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to":  `91${this.removeCountryCode(ride.customerDetails.phoneNumber)}`,
      "type": "template",
      "template": {
        "name": "driver_assigned",
        "language": {
          "code": "en_US"
        },
        "components": [
          {
            "type": "body",
            "parameters": [{
              "type": "text",
              "text": ride.bookingId
            },
            {
              "type": "text",
              "text": ride.cabVendor?.driver?.driverPhoneNo || cabVendor.profile.phoneNo
            },
            {
              "type": "text",
              "text": ride.cabVendor?.vehicle?.vehicleRegNo || Object.values(cabVendor.vehicles)[0].vehicleRegNo
            },
            {
              "type": "text",
              "text": tripStartTime.toLocaleDateString("en-IN", { day: 'numeric', month: 'short', year: "2-digit", timeZone: 'Asia/Kolkata' })
            }, {
              "type": "text",
              "text": tripStartTime.toLocaleTimeString("en-IN", {hour: '2-digit', minute:'2-digit', timeZone: 'Asia/Kolkata'})
            },
            {
              "type": "text",
              "text": ride.bookingDetails.pickup.fullAddress
            },
            {
              "type": "text",
              "text": ride.bookingDetails.drop.fullAddress
            }
            ]
          }]
      }
    }
    return payload
  }

  payloadForDriverApproved(cabVendorId, cabVendor) {
    let payload = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to":   `91${this.removeCountryCode(cabVendor.profile.phoneNo)}`,
      "type": "template",
      "template": {
        "name": "driver_approved",
        "language": {
          "code": "en_GB"
        },
        "components": [
          {
            "type": "body",
            "parameters": [{
              "type": "text",
              "text": cabVendor.profile.name
            }, {
              "type": "text",
              "text": "You can start taking the rides"
            }]
          }]
      }
    }

    return payload;
  }

  payloadForBookingComplete(ride, mediaResponse) {
    let payload = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to": `91${this.removeCountryCode(ride.customerDetails.phoneNumber)}`,
      "type": "template",
      "template": {
        "name": "booking_complete",
        "language": {
          "code": "en_US"
        },
        "components": [
          {
            "type": "header",
            "parameters": [
              {
                "type": "document",
                "document": {
                  "id": mediaResponse.id,
                  "filename": `invoice-${ride.bookingId}.pdf`
                }
              }
            ]
          },
          {
            "type": "body",
            "parameters": [
              {
                "type": "text",
                "text": ride.bookingId
              }
            ]
          }]
      }
    }
    return payload
  }

  payloadForBookingTemplate(ride, templateName) {
    let tripStartTime = new Date(ride.bookingDetails.tripStartTime)
    let bookingId = ride.bookingId
    let payload = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to": `91${this.removeCountryCode(ride.customerDetails.phoneNumber)}`,
      "type": "template",
      "template": {
        "name": "booking_confirmation3",
        "language": {
          "code": "en"
        },
        "components": [{
            "type": "header",
            "parameters": [{
                "type": "text",
                "parameter_name": "booking_id",
                "text": bookingId
            }]
        }, {
            "type": "body",
            "parameters": [ {
                "type": "text",
                "parameter_name": "booking_id",
                "text": bookingId
              },
              {
                "type": "text",
                "parameter_name": "name",
                "text": ride.customerDetails.name.charAt(0).toUpperCase() + ride.customerDetails.name.slice(1)
              },
              {
                "type": "text",
                "parameter_name": "phone_no",
                "text": `+91${this.removeCountryCode(ride.customerDetails.phoneNumber)}`
              },
              {
                "type": "text",
                "parameter_name": "pickup_loc",
                "text": ride.bookingDetails.pickup.fullAddress
              },
              {
                "type": "text",
                "parameter_name": "drop_loc",
                "text": ride.bookingDetails.drop.fullAddress
              },
              {
                "type": "date_time",
                "parameter_name": "pickup_time",
                "date_time": {
                  "fallback_value": tripStartTime.toLocaleString("en-IN", { timeZone: "Asia/Kolkata" }),
                  // "day_of_week": 2,
                  // "year": 2023,
                  // "month": 4,
                  // "day_of_month": 10,
                  // "hour": 15,
                  // "minute": 33,
                  // "calendar": "GREGORIAN"
                }
              },
              {
                "type": "text",
                "parameter_name": "car_type",
                "text": ride.bookingDetails.cabType
              },
              {
                "type": "text",
                "parameter_name": "trip_type",
                "text": ride.bookingDetails.tripType
              },
              {
                "type": "text",
                "parameter_name": "est_distance",
                "text": ride.bookingDetails.estDistance
              },
              {
                "type": "text",
                "parameter_name": "rate_perkm",
                "text": ride.priceDetails.rateCard.rate
              },
              {
                "type": "text",
                "parameter_name": "state_permit",
                "text": ride.priceDetails.rateCard.permitAmount
              },
              {
                "type": "text",
                "parameter_name": "est_toll",
                "text": ride.priceDetails.rateCard.estTollAmount
              },
              {
                "type": "text",
                "parameter_name": "driver_bata",
                "text": ride.priceDetails.rateCard.bata
              },
              {
                "type": "text",
                "parameter_name": "key1",
                "text": "Coupon"
              },
              {
                "type": "text",
                "parameter_name": "value1",
                "text": "NA"
              },
              {
                "type": "text",
                "parameter_name": "total",
                "text": ride.priceDetails.rateCard.total
              }, {
                "type": "text",
                "parameter_name": "footer",
                "text": "Private Parking, Root top carrier extra, Hill charges applicable for hill stations"
              }]
        }]
      }
    }
    return payload
  }

  payloadForOTP(otp, phoneNumber) {
    let payload = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to": `91${this.removeCountryCode(phoneNumber)}`,
      "type": "template",
      "template": {
        "name": "phone_verification",
        "language": {
          "code": "en"
        },
        "components": [
          {
            "type": "body",
            "parameters": [{
              "type": "text",
              "text": otp
            }]
          }, {
            "type": "button",
            "sub_type": "url",
            "index": 0,
            "parameters": [
              {
                "type": "text",
                "text": otp
              }
            ]
          }]
      }
    }
    return payload
  }
}

module.exports = new WhatsAppClient()