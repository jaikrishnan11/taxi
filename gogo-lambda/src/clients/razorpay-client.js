const Razorpay = require('razorpay');
var Jimp = require("jimp");
const axios = require('axios')
const jsQR = require("jsqr");


const Config = require('../config').razorpay

var instance = new Razorpay({
  key_id: Config.keyId,
  key_secret: Config.keySecret,
});

const razorpayClient = {
    async createQrCode(cabVendorId){
        if(cabVendorId == null)
            throw "cabVendorId should not be null"
        const qrCodeInfo = await instance.qrCode.create({
            "type": "upi_qr",
            "name": `CabVendorId#${cabVendorId}`,
            "usage": "multiple_use",
            "fixed_amount": false,
            "description": `CabVendorId ${cabVendorId}`,
            "notes": {
              "cabVendorId": cabVendorId
            }
        })
        //console.log(qrCodeInfo)
        qrCodeInfo.paymentUrl = await this.convertQrCodeToUrl(qrCodeInfo.image_url)
        return qrCodeInfo;
    },

    async convertQrCodeToUrl(imageUrl){
        // sample qr image test mode : https://rzp.io/i/AX2OP4P
        const response = await axios.get(imageUrl,  { responseType: 'arraybuffer' })
        const buffer = Buffer.from(response.data, "utf-8")
        const image = await Jimp.read(buffer)
        const code = jsQR(image.bitmap.data, image.bitmap.width, image.bitmap.height);
        if(!code)
            throw "Unable read qr code from the image in  " + imageUrl 
        return code.data
    }
}

//razorpayClient.createQrCode("test").then(console.log, console.log)
//razorpayClient.convertQrCodeToUrl("https://rzp.io/i/AX2OP4P").then(console.log, console.log)

module.exports = razorpayClient