const axios = require('axios');
const { invicibleOcean: CONFIG } = require('../config');
const utils = require('../commons/utils');

class Client {
    constructor() {
        this.baseUrl = "https://api.invincibleocean.com/invincible";
        this.vehicleUrl = `${this.baseUrl}/vehicleRcV6`;
        this.stolenVehicleUrl = `${this.baseUrl}/vehicleStolen`;
        this.licenceUrl = "https://api.emptra.com/drivingLicence";
        this.headers = {
            'Content-Type': 'application/json',
            'secretKey': CONFIG.secretKey,
            'clientId': CONFIG.clientId
        };
    }

    async makePostRequest(url, data) {
        try {
            const response = await axios.post(url, data, {
                headers: this.headers,
                maxBodyLength: Infinity
            });
            return response;
        } catch (error) {
            throw new Error(error.response ? error.response.data : error.message);
        }
    }

    async getVehicleInfoV6(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }
        return await this.makePostRequest(this.vehicleUrl, {
            "vehicleNumber": parsedVehicleNumber
        });
    }

    async stolenVehicleDetails(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }
        return await this.makePostRequest(this.stolenVehicleUrl, {
            "vehicleNumber": parsedVehicleNumber
        });
    }

    async getDrivingLicense(licenseNumber, dateOfBirth) {
        return await this.makePostRequest(this.licenceUrl, {
            "number": licenseNumber,
            "dob": dateOfBirth
        });
    }
}

// module.exports = new Client();
console.log(new Client().getVehicleInfoV6("TN07CJ1050").then((data) => {
    console.log(data.result)
}).catch((err) => {
    console.log(err)
}))