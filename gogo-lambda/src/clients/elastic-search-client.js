const {Client: ElasticClient} = require('@elastic/elasticsearch')
const CONFIG = require('../config');
const esConfig = CONFIG.elasticSearchConfig

const auth = `${esConfig.credentials.username}:${esConfig.credentials.password}`

const elasticClient = new ElasticClient({
    node:  `${esConfig.protocol}://${auth}@${esConfig.host}`
})

class Client {
    async bulkPush(body, indexName) {
        const { body: bulkResponse } = await elasticClient.bulk({ refresh: false, body })
        console.log("errors: " + bulkResponse.errors + " Time: " + bulkResponse.took)
        if (bulkResponse.errors) {
            const erroredDocuments = []
            // The items array has the same order of the dataset we just indexed.
            // The presence of the `error` key indicates that the operation
            // that we did for the document has failed.
            bulkResponse.items.forEach((action, i) => {
              const operation = Object.keys(action)[0]
              if (action[operation].error) {
                erroredDocuments.push({
                  // If the status is 429 it means that you can retry the document,
                  // otherwise it's very likely a mapping error, and you should
                  // fix the document before to try it again.
                  status: action[operation].status,
                  error: action[operation].error,
                  operation: body[i * 2],
                  document: body[i * 2 + 1]
                })
              }
            })
            console.log(erroredDocuments)
            throw "ES update failed " + erroredDocuments.map(err => err.error).join(" , ")
          }
        
          if (indexName) {
            const { body: count } = await elasticClient.count({ index: indexName })
            console.log("Index count " + indexName +" : ", count)
          }
    }

    async bulkIndexDocuments(documents, indexName) {
        let bulkRequestBody = documents.map(doc => {
            //doc.document._id = doc.documentId;
            return doc
        })
        .flatMap(doc => [{ index: { _index: doc.index, _id: doc.documentId } }, doc.document])
        await this.bulkPush(bulkRequestBody, indexName)
    }

    async getCabVendorByExternalIds(externalIds) {
      let response = await elasticClient.search({
            index: "table-cab-vendors",
            body: {
              "_source": ["externalId"],
              "query": {
                "terms": {
                  "externalId.keyword": externalIds
                }
              }
            }
          })
      return response.body.hits.hits.map(hit => hit._source.externalId)
    }

    _getEsClient() {
      return elasticClient
    }
}

module.exports = new Client()