const https = require("https");
const fs = require("fs");
const _ = require("lodash")
const moment = require("moment")

class InvoiceClient {
    constructor() {

    }

    // Function to fetch PDF file as a buffer using POST request
    async fetchPdfAsBuffer(options,  data) {
        return new Promise((resolve, reject) => {
            const req = https.request(options, (res) => {
                let pdfBuffer = Buffer.from('');

                res.on('data', (chunk) => {
                    pdfBuffer = Buffer.concat([pdfBuffer, chunk]);
                });

                res.on('end', () => {
                    resolve(pdfBuffer);
                });

                res.on('error', (err) => {
                    reject(err);
                });
            });

            req.write(JSON.stringify(data));
            req.end();
        });
    };

    async generateInvoice(ride) {
        let amountBeforeTax = _.round((ride.priceDetails.rateCard.total * 20)/21, 2)

        var invoice = {
            logo: "https://mail-templates.pages.dev/assets/img/logo.jpeg",
            date: moment(ride.bookingDetails.tripStartTime).utcOffset("+0530").format('DD MM YYYY'),
            date_title: "Invoice Date",
            from: `GOGO Cabs,\nChennai`,
            to: `${_.capitalize(ride.customerDetails.name)}\n${ride.customerDetails.phoneNumber}`,
            currency: "INR",
            number: ride.bookingId,
            payment_terms: "Amount Paid via Cash",
            balance_title: "Amount Paid",
            custom_fields: [
                {
                    "name": "GSTIN",
                    "value": "33LMDPS7497D1ZG"
                },
                {
                    "name": "HSN/SAC",
                    "value": "996412"
                }
            ],
            items: [
                {
                    name: `${ride.bookingDetails.tripType} - ${ride.bookingDetails.cabType}\n${ride.bookingDetails.pickup.fullAddress}\n${ride.bookingDetails.drop.fullAddress}`,
                    quantity: 1,
                    unit_cost: `${amountBeforeTax}`
                }
            ],
            fields: {
                tax: "%"
            },
            tax: 5,
            notes: "Thanks for being an awesome GoGo customer! www.gogocabs.com"
        };

        var postData = JSON.stringify(invoice);
        var options = {
            hostname: "invoice-generator.com",
            port: 443,
            path: "/",
            method: "POST",
            headers: {
                "Authorization" : "Bearer sk_ODef52YYku8bhwCkowmhP15pQLG00vmU",
                "Content-Type": "application/json",
                "Content-Length": Buffer.byteLength(postData)
            }
        };

        var buffer = await this.fetchPdfAsBuffer(options, invoice);
        return buffer
    }


}

module.exports = new InvoiceClient()