
const CONFIG = require('../config').dynamoDb
const Constants = require('../service/constants')
const _ = require('lodash')
// Create a DocumentClient that represents the query to add an item
const dynamodb = require('aws-sdk/clients/dynamodb');
const ddbClient = new dynamodb.DocumentClient(Object.assign({
  "marshallOptions": {
      "convertEmptyValues": false
  }
},CONFIG.clientConfig));


const { customAlphabet } = require('nanoid')

CURRENT_TRIP_VERSION = "v1.0"
CURRENT_CUSTOMER_VERSION = "v1.0"

TABLES = {
    "Trip": "Trip", // Old .. Will be deprecated soon
    "Rides": "rides", // New 
    "Vendor": "cab-vendors",
    "WalletLedger": "wallet-book-keeping"
}

const customerIdGenerator = customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', 6)

class Client {
    constructor(){
      this._client = ddbClient
    }
    
    async getRideByBookingId(bookingId, consistentRead = true) {
      let response = await ddbClient.get({
        TableName: TABLES.Rides,
        Key: { bookingId },
        ConsistentRead: consistentRead
      }).promise()
      return response.Item
    }
    
    async getRideAndVendor(bookingId, cabVendorId) {
      let response = await ddbClient.batchGet({
        RequestItems: {
          [TABLES.Rides]: {
            Keys: [{bookingId}]
          },
          [TABLES.Vendor]: {
            Keys: [{cabVendorId}]
          }
        }
      }).promise()
      if(!_.isEmpty(response.UnprocessedKeys))
        throw "[getRideAndVendor] Error batchGet " + response.UnprocessedKeys + " ";
      return {ride: response.Responses[TABLES.Rides][0], cabVendor: response.Responses[TABLES.Vendor][0]}
    }

    async getVendorById(cabVendorId) {
      let response = await ddbClient.get({
        TableName: TABLES.Vendor,
        Key: { cabVendorId },
      }).promise()
      return response.Item
    }

    async updateRideDetailFromLambda({bookingId, drop, pickup, cabVendorPriceDetails}){
      let response = await ddbClient.update({
        TableName: TABLES.Rides,
        Key: {
          bookingId
        },
        ConditionExpression: "rideStatus = :curRideStatus",
        ReturnValues: "ALL_NEW",
        UpdateExpression: "SET bookingDetails.pickup = :pickup, bookingDetails.#drop = :drop, priceDetails.cabVendor = :cabVendorPriceDetails, rideStatus = :rideStatus",
        ExpressionAttributeValues: {
          ":pickup": pickup,
          ":drop": drop,
          ":cabVendorPriceDetails": cabVendorPriceDetails,
          ":rideStatus": Constants.RideStatus.UnApproved,
          ":curRideStatus": Constants.RideStatus.UnPublished
        },
        ExpressionAttributeNames: {
          "#drop": "drop"
        }
      }).promise()
      return response.Attributes;
    }

    async updateRazorpayQrDetails(cabVendorId, razorpayQr, {forceUpdate=false}){
      let response = await ddbClient.update({
        TableName: TABLES.Vendor,
        Key: {cabVendorId},
        ConditionExpression: forceUpdate ? null : "attribute_not_exists(wallet.razorpay.paymentUrl)",
        UpdateExpression: "SET wallet.razorpay = :razorpayQr",
        ExpressionAttributeValues: {
          ":razorpayQr": razorpayQr
        },
        ReturnValues: "NONE"
      }).promise()
      return response.Attributes;
    }

    async insertTrip(tripInfo, requestInfo = {}) {
        if(requestInfo.isInternalUser){
          return;
        }
        Object.assign(tripInfo, {"version": CURRENT_TRIP_VERSION})
        await this.put(TABLES.Trip, tripInfo)
    }

    async insertCustomer(customerInfo) {
        Object.assign(customerInfo, {"version": CURRENT_CUSTOMER_VERSION})
        if(customerInfo.customerId != true)
            customerInfo.customerId = customerIdGenerator()
        await this.put(DDBConfig.table.customer, customerInfo)
    }

    async put(table, data) {
        if(CONFIG.dryRun)
            return;
          try {
            console.time("DB Put")
            await ddbClient.put({ TableName: table, Item: data}).promise()
            console.timeEnd("DB Put")
          } catch (err) {
            console.error("Failed to insert item to table " + table, err);
            throw err
          }
    }

    async queryRideStatusAndFilterByTripEndTime(rideStatus, tripEndTime) {
      try {
        let response = await ddbClient.scan({
          TableName: TABLES.Rides,
          FilterExpression: "rideStatus = :rideStatus AND bookingDetails.#estRideEndTime <= :tripEndTime",
          ExpressionAttributeValues: {
              ":rideStatus": rideStatus,
              ":tripEndTime": tripEndTime
          },
          ExpressionAttributeNames: {
            "#estRideEndTime": "estRideEndTime"
          }
        }).promise()
        if(response.LastEvaluatedKey){
          console.warn("LastEvaluatedKey is present... No of assigned rides is way higher !!! ", response.Count)
        }
        return response.Items
      } catch (err) {
        console.error("queryRideStatusAndFilterByTripEndTime failed", err);
        throw err
      }
    }

    async queryAssignedRides(rideStatus) {
      try {
        let response = await ddbClient.query({
          TableName: TABLES.Rides,
          IndexName: "booking-status-index",
          KeyConditionExpression: "rideStatus = :rideStatus",
          ExpressionAttributeValues: {
              ":rideStatus": rideStatus
          }
        }).promise()
        if(response.LastEvaluatedKey){
          console.warn("LastEvaluatedKey is present... No of assigned rides is way higher !!! ", response.Count)
        }
        return response.Items
      } catch (err) {
        console.error("queryAssignedRides failed", err);
        throw err
      }
    }

    async repairCabVendor(cabVendorId, {updateExpression, expressionAttrName, expressionAttrValue}) {
      let response = await ddbClient.update({
        TableName: TABLES.Vendor,
        Key: {cabVendorId},
        UpdateExpression: updateExpression, 
        ExpressionAttributeNames: expressionAttrName,
        ExpressionAttributeValues: expressionAttrValue,
        ReturnValues: "ALL_NEW"
      }).promise()
      return response.Attributes
    }

    async updateRideStatusToOngoing(bookingId, cabVendorId) {
      let response = await ddbClient.transactWrite({
        TransactItems:[{
          Update: this.constructRideStatusUpdatePayload(bookingId, Constants.RideStatus.OnGoing)
        }, {
          Update: {
              TableName: TABLES.Vendor,
              ReturnValuesOnConditionCheckFailure: "ALL_OLD",
              Key: {
                  cabVendorId
              },
              UpdateExpression: "ADD stats.#rides.#onGoing :onGoingIncrement, stats.#rides.#pending :pendingIncrement, #rides.#onGoing :rideSet DELETE #rides.#confirmed :rideSet",
              ExpressionAttributeValues: {
                  ":onGoingIncrement": 1,
                  ":pendingIncrement": -1,
                  ":requiredOnPendingCount": 1,
                  ":rideSet": ddbClient.createSet(bookingId)
              },
              ConditionExpression: "stats.#rides.#pending >= :requiredOnPendingCount",
              ExpressionAttributeNames: {
                 "#onGoing": "onGoing",
                  "#rides": "rides",
                  "#pending": "pending",
                  "#confirmed": "confirmed"
              }
          }
      }]
      }).promise()
      return response
    }

    async changeRideStatus(bookingId, curRideStatus, newRideStatus){
      try{
        let payload = this.constructRideStatusUpdatePayload(bookingId, newRideStatus, curRideStatus);
        let response = await ddbClient.update(payload).promise(); 
        return response.Attributes;
      }  catch(err){
        console.error(err)
        throw err
      }
    }


    async changeRideStatusIfAnyOf(bookingId, curRideStatuses, newRideStatus){
      
      try {
        let response = await ddbClient.update({
          TableName: TABLES.Rides,
          Key: {
              bookingId
          },
          UpdateExpression: "Set rideStatus = :newRideStatus",
          ReturnValues: "ALL_NEW",
          ConditionExpression: "contains(:curRideStatuses, rideStatus)",
          ExpressionAttributeValues: {
              ":curRideStatuses": ddbClient.createSet(curRideStatuses),
              ":newRideStatus": newRideStatus
          }
        }).promise()
        return response.Attributes
      } catch(err){
        if(err.code == "ConditionalCheckFailedException")
          throw {message: "Ride is assigned already"}
        throw err;
      }
    }

    async changeVendorStatus(cabVendorId, curVendorStatus, newVendorStatus){
      try{
        let payload = this.constructVendorStatusUpdatePayload(cabVendorId, newVendorStatus, curVendorStatus);
        let response = await ddbClient.update(payload).promise(); 
        return response.Attributes;
      }  catch(err){
        console.error(err)
        throw err
      }
    }

    constructVendorStatusUpdatePayload(cabVendorId, newVendorStatus, curVendorStatus){
      let payload =  {
        TableName: TABLES.Vendor,
        Key: {
            cabVendorId
        },
        UpdateExpression: "Set vendorStatus = :newVendorStatus",
        ReturnValues: "ALL_NEW",
        ConditionExpression: curVendorStatus ? `vendorStatus = :curVendorStatus` : null,
        ExpressionAttributeValues: {
            ":newVendorStatus": newVendorStatus
        }
      }
      if(curVendorStatus){
        payload.ExpressionAttributeValues[":curVendorStatus"] = curVendorStatus
      }
      return payload
    }

    constructRideStatusUpdatePayload(bookingId, rideStatus, oldRideStatus){
      let payload =  {
        TableName: TABLES.Rides,
        Key: {
            bookingId
        },
        UpdateExpression: "Set rideStatus = :rideStatus",
        ReturnValues: "ALL_NEW",
        ConditionExpression: oldRideStatus ? `rideStatus = :oldRideStatus` : null,
        ExpressionAttributeValues: {
            ":rideStatus": rideStatus
        }
      }
      if(oldRideStatus){
        payload.ExpressionAttributeValues[":oldRideStatus"] = oldRideStatus
      }
      return payload
    }

    async completeRide(bookingId, cabVendorId, driverEarnings) {
      let reponse = await ddbClient.transactWrite({
        TransactItems:[{
          Update: this.constructRideStatusUpdatePayload(bookingId, Constants.RideStatus.Completed)
        }, {
          Update: {
              TableName: TABLES.Vendor,
              Key: {
                  cabVendorId
              },

              //TODO: Add condition bookingId should be present in onGoing set before deleting from it

              UpdateExpression: " ADD stats.#rides.#onGoing :onGoingIncrement, stats.#rides.#completed :completedIncrement, stats.#totalEarning :driverEarnings, rides.completed :rideSet DELETE rides.onGoing :rideSet",
              ExpressionAttributeValues: {
                  ":onGoingIncrement": -1,
                  ":completedIncrement": 1,
                  ":driverEarnings": driverEarnings,
                  ":requiredOnGoingCount": 1,
                  ":rideSet": ddbClient.createSet(bookingId)
              },
              ConditionExpression: "stats.#rides.#onGoing >= :requiredOnGoingCount",
              ReturnValuesOnConditionCheckFailure: "ALL_OLD",
              ExpressionAttributeNames: {
                 "#onGoing": "onGoing",
                  "#rides": "rides",
                  "#completed": "completed",
                  "#totalEarning": "totalEarning"
              }
          }
      }]
      }).promise()
      console.log(reponse)
    }
    /*
      !!!!! DANGER ZONE !!!!! 
    */

      async captureRazorapyQRPayment(cabVendorId, creditDetails) {
        let response = await ddbClient.transactWrite({
          TransactItems: [{
            Put: {
              TableName: TABLES.WalletLedger,
              Item: {
                cabVendorId: cabVendorId,
                // Converting unix timestamp to Date object. need to convert to milli seconds
                createdDate: creditDetails.payment.createdDate,
                action: Constants.WallerLedgerAction.AddMoney,
                opDetails: {
                  status: Constants.AddMoneyStatus.COMPLETED,
                  paymentGateway: Constants.PaymentGateway.Razorpay,
                  isViaQR: true,
                  paymentId: creditDetails.payment.paymentId,
                  amount: creditDetails.payment.amountInPaise,
                  qrDetails: creditDetails.qr
                }
              }
            }
          }, {
            Update: {
              TableName: TABLES.Vendor,
              Key: {
                cabVendorId
              },
              UpdateExpression: "ADD wallet.#walletTotal :amount",
              ExpressionAttributeValues: {
                ":amount": creditDetails.payment.amountInPaise
              },
              ExpressionAttributeNames: {
                "#walletTotal": "total"
              }
            }
          }]
        }).promise()
      }
}

module.exports = new Client()