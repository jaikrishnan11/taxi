const {Client: OpenSearchClient }  =  require("@opensearch-project/opensearch");
const CONFIG = require('../config');

const opensearchConfig = CONFIG.opensearch
const auth = `${opensearchConfig.credentials.username}:${opensearchConfig.credentials.password}`

const openSearchClient = new OpenSearchClient({
    node: `${opensearchConfig.protocol}://${auth}@${opensearchConfig.host}`
})


class Client {
    async insertDocument(indexName, documentId, document) {
        if(indexName == null || documentId == null) {
            throw "[insertDocument] IndexName or DocumentId is null"
        }
        console.log("Inserting document " + indexName +"/" + documentId)
        let result = await openSearchClient.index({
            id: documentId,
            index: indexName,
            body: document,
            refresh: false // Set to True if you want to reflect the operation in search immediately
        })
        console.log("Inserted Document", result.body)
        return this.throwIfError(result)
    }

    async deleteDocument(indexName, documentId) {
        let result = await openSearchClient.delete({
            index: indexName,
            id: documentId
        })
        console.log(result.body)
        return this.throwIfError(result)
    }

    throwIfError(responseObject) {
        if (responseObject.statusCode >= 400) {
            throw "Ingestion failed " + responseObject.statusCode
        }
        return responseObject.body
    }
}


module.exports = new Client()