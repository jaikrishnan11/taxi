const S3 = require('aws-sdk/clients/s3');
const fs = require('fs')

class Client {
    constructor() {
        this.client = new S3({region: "ap-south-2", credentials: {accessKeyId: "access", secretAccessKey: "secret"}, signatureVersion: "v4"})
    }

    async listObjects(bucketName) {
        const input = {
            "Bucket": bucketName
          };
        let response = await this.client.listObjects(input).promise()
        console.log(response.Contents)
        return response;
    }


    async uploadFile(bucketName, key, fileStream, metadata = {}) {
        let response = await this.client.upload({
            Bucket: bucketName,
            Key: key,
            Body: fileStream,
            Metadata: metadata
        }).promise()
        console.log(response)
        return response
    }

    async getPreSignedUrlForCabVendorUpload(cabVendorId) {
        //https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTConstructPolicy.html
        let response = this.client.createPresignedPost({
            Bucket: "cabvendors",
            Expires: 1 * 60 * 60,
            Conditions: [
                ["starts-with", "$key", `${cabVendorId}/`],
                ["content-length-range", 	1, 3*1024*1024], // Size between  1 Byte to 3MB 
                ["starts-with", "x-amz-meta-type", ""]
            ]
        })
        //TODO: Fix Uploding meta fields is not being set in S3. 
        //console.debug(response)
        return {
            uploadUrl: response.url,
            requiredFields: response.fields
        }
    }
}


//let cl  = new Client()
//cl.getPreSignedUrlForCabVendorUpload("dummy")
//cl.uploadFile("cabvendors", "dummy/dummy.jpeg", fs.readFileSync("/Users/varsanka/images.jpeg"), {cabVendor: "dummy"}).then(console.log, console.log)
//cl.listObjects("cabvendors", "dummy")

module.exports = new Client()