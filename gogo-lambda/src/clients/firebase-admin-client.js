const serviceAccount = require("../config/firebase.json");
const Config = require('../config') 

const AllVendorTopic = Config.fireBase.topics.allCabVendors

class FireBaseAdmin {
    constructor(){
        this.isEnabled = Config.fireBase.isEnabled
        this.client = require("firebase-admin");
        this.client.initializeApp({
            credential: this.client.credential.cert(serviceAccount)
          });
          
    }

    async subscribeDeviceToTopic(deviceToken, topicName = AllVendorTopic) {
        let response = await this.client.messaging().subscribeToTopic(deviceToken, topicName)
        console.log("Subscribed to topic " + topicName, response)
        // await this.sendMessageToToken({
        //     title: "Welcome to GogoCabs",
        //     body: "Stay tuned for new rides !!" 
        // }, deviceToken)
    }

    async sendMessageToToken({title, body, sendLocation={}}, deviceToken){
        if(this.isEnabled == false) return;
        return await this.client.messaging().send({
            token: deviceToken,
            data: {
                sendLocation: JSON.stringify(Object.assign(sendLocation, {
                    fetchDurationMillis: 60000
                })),
                notifee: JSON.stringify({
                    title,
                    body,
                    android: {
                        channelId: "ride-alerts",
                        sound: 'gogo_cabs',
                        pressAction: {
                            id: "default",
                          }
                    }
                })
            }
        })
    }

    async sendMessageToTopic({title, body}, topicName = AllVendorTopic) {
        const options = {
            priority: "high"
        }
        let response = await this.client.messaging().sendToTopic(topicName,
            {
                notification: {
                    body,
                    title: title,
                    sound: "doorbell"
                }
            }, options)
        console.log("Message sent to topic", response)
    }

    async sendMessageToTopicUsingNotifee({title, body}, topicName = AllVendorTopic) {
        if(!this.isEnabled) return;
        let response = await this.client.messaging().send({
            topic: topicName,
            android: {
                priority: "high"
            },
            data: {
                sendLocation: JSON.stringify({
                    fetchDurationMillis: 60000
                }),
                notifee: JSON.stringify({
                    body,
                    title,
                    android: {
                        channelId: "ride-alerts",
                        sound: 'doorbell',
                        pressAction: {
                            id: "default",
                          }
                    }
                })
            }
        });
        console.debug("Message sent to topic", response)
    }

    async publishNewRide({from, to, tripType, carType, amount, topicName = AllVendorTopic}){
        const removeText = [", Tamil Nadu,", "India"]
        removeText.forEach(rmTxt => {
            from = from.replace(rmTxt, "")
            to = to.replace(rmTxt, "")
        })

        let body =  `${from} To ${to}, Car: ${carType}`;
        await this.sendMessageToTopicUsingNotifee({
            title: `New Ride - ₹${amount}`,
            body
        }, topicName)
    }
}

module.exports = new FireBaseAdmin()