const SQS = require('aws-sdk/clients/sqs')
const _ = require('lodash')
const crypto = require('crypto')
const sqsClient = new SQS()
const constants = require('../service/constants');
const GeneralSQS  = "https://sqs.ap-south-1.amazonaws.com/849398159878/GeneralSQS"
const Config = require('../config')

module.exports = {
    
    async sendBookingConfirmation(rideDetails, requestContext) {
        console.time("sqs-push")
        var params = {
            MessageDeduplicationId: this.generateDeDupHash(rideDetails),
            MessageGroupId: "booking", // TOOD : Will impact anything ?
            MessageBody: JSON.stringify({rideDetails, requestContext}),
            QueueUrl: "https://sqs.ap-south-1.amazonaws.com/849398159878/gogo-ride-confirm.fifo"
          };
        let result = await sqsClient.sendMessage(params).promise()
        console.timeEnd("sqs-push")
        return result;
    },

    async sendRideNotficationForVendorsV1(bookingId) {
        let result = await this.sendMessage({
            DelaySeconds: 5,
            QueueUrl: GeneralSQS,
            MessageBody: JSON.stringify({
                type: constants.SQSMsgType.PublishRideForV1Vendors,
                payload: {
                    bookingId,
                }
            })
        })
        console.log("Sent SQS ", result)
        return result
    },

    async sendMessage(messagePayload) {
        if(Config.isProduction == false) {
            console.log("Not sending any SQS message. Not production")
            return;
        }
        return await sqsClient.sendMessage(messagePayload).promise()
    },

    generateDeDupHash(rideDetails) {
        const payload = _.omit(rideDetails, ['bookingId', 'createdAt'])
        return crypto.createHash('md5').update(JSON.stringify(payload)).digest("hex");
    }
}