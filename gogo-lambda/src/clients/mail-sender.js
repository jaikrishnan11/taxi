var nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const _ = require('lodash')
const telegramBot = require('./telegram-bot')
const invoiceClient = require('./invoice-generator-client')

const { isProduction } = require('../config');
const { string } = require('joi');

const createSESTransporter = async () => {
    return nodemailer.createTransport({
        host: "email-smtp.ap-south-1.amazonaws.com",
        port: 587,
        auth: {
            user: "AKIA4LRATLIDIN3PJEP2",
            pass: "BG/2iNbA9AW1FzGvcSzlx9TRuoXaLD5UtC4PQkdpd8RU",
        },
    });

}
const sendEmail = async (emailOptions) => {
    let emailTransporter = await createSESTransporter();
    let response = await emailTransporter.sendMail(emailOptions);
    return response
};

class MailSender {
    constructor() {

    }

    getRideTemplateData(bookingId, ride){
        return {
            customer_name:  ride.customerDetails.name.charAt(0).toUpperCase() + ride.customerDetails.name.slice(1),
            booking_id: bookingId,
            from: ride.bookingDetails.pickup.fullAddress,
            to: ride.bookingDetails.drop.fullAddress,
            estimated_km: ride.bookingDetails.estDistance + "Km",
            vehicle: ride.bookingDetails.cabType,
            trip_type: ride.bookingDetails.tripType,
            pick_up_date: new Date(ride.bookingDetails.tripStartTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"}),
            return_date: _.isEmpty(ride.bookingDetails.returnStartTime) ? "-" : new Date(ride.bookingDetails.returnStartTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"}),
            estimated_price: ride.priceDetails.rateCard.total,
            contact_number: ride.customerDetails.phoneNumber
        }
    }

    async sendRideCompletedEmail(mail, bookingId, ride, invoicePdf) {
        try {
            if (this.shouldSendEmail(mail, mailOptions) == false){
                console.debug("Not sending ride completed email " + mail + " isProduction: " + isProduction)
                return;
            }
            let templateData = this.getRideTemplateData(bookingId, ride)

            if(!invoicePdf) {
                invoicePdf = await invoiceClient.generateInvoice(ride)
            }

            var mailOptions = {
                from: {
                    name: "Gogo Cabs",
                    address: "no_reply@gogocabs.com"
                },
                to: mail,
                replyTo: "gogocabmail@gmail.com",
                subject: `Ride completed - ${bookingId}`,
                text: `Text version of the email. \n Ride completed - ${bookingId} \n Please contact +91-638-55-888-06 incase of queries`,
                html: await this.renderTemplate(path.join(__dirname, "../../mail-templates/RideCompleted.html"), templateData),
                attachments: [{
                    filename: `invoice-${bookingId}.pdf`,
                    content: invoicePdf
                }]
            };
            let response = await sendEmail(mailOptions)
            console.debug("Ride completed mail sent ", response)
            return response

        } catch (err){
            console.error("[sendRideCompletedEmail] Unable to send ride completed email", err)
            await telegramBot.sendDevMessage({err, message: "Failed to send ride completed email", mail})
        }
    }
    async sendBookingConfirmationEmail(mail, bookingId, ride, mailOptions = {}) {
        try {
            if (this.shouldSendEmail(mail, mailOptions) == false){
                console.debug("Not sending email " + mail + " isProduction: " + isProduction)
                return;
            }
            let templateData = this.getRideTemplateData(bookingId, ride)
            var mailOptions = {
                from: {
                    name: "Gogo Cabs",
                    address: "no_reply@gogocabs.com"
                },
                to: mail,
                replyTo: "gogocabmail@gmail.com",
                subject: `Booking confirmed - ${bookingId}`,
                text: `Text version of the email. \n Booking confirmed - ${bookingId} \n Please contact +91-638-55-888-06`,
                html: await this.renderTemplate(path.join(__dirname, "../../mail-templates/BookingConfirmation.html"), templateData)
            };
            console.info("Mail sending ")

            let response = await sendEmail(mailOptions)
            console.debug("Mail sent ", response)
            return response
        } catch (ex) {
            console.error("Failed to send email ", mail, ex)
            await telegramBot.sendDevMessage({ex, message: "Failed to send email", mail})
        }
    }

    async renderTemplate(templateFilePath, templateData = {}) {
        let template = fs.readFileSync(templateFilePath, 'utf8');
        
        for(let [key, value] of Object.entries(templateData)){
            let snakeCasedKey = _.snakeCase(key) + "_value"
            if(template.indexOf(snakeCasedKey) != -1)
                template = _.replace(template, snakeCasedKey, value)
        }
        return template
    }

    shouldSendEmail(mail, mailOptions={}) {
        return _.isEmpty(mail) == false &&  mail.indexOf("test") == -1 && isProduction == true && !mailOptions.isInternalUser
    }
}

module.exports = new MailSender();


