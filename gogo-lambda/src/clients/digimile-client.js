const CONFIG = require('../config')
const axios = require('axios')

class DigiMileClient {
    constructor() { }

    async makeGetRequest(url) {
        try {
            if (CONFIG.isProduction == false) {
                // console.log(url)
            	console.debug("Not sending SMS in non Prod env ")
            	return true;
            }
            url = encodeURI(url); 
            const responseObj = await axios.get(url)
            console.log("digi mile client", responseObj.data)
            return responseObj
        } catch (error) {
            console.error(error)
            //throw new Error(error.response ? error.response.data : error.message);
        }
    }

    removeCountryCode(phoneNumber) {
        const countryCode = "+91";
        const countryCode2 = "91";
        const formattedNumber = phoneNumber.trim().replace(/\s+/g, ""); // remove whitespace and format number

        if (formattedNumber.startsWith(countryCode)) {
            return formattedNumber.substr(countryCode.length);
        } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode2)) {
            return formattedNumber.substr(countryCode2.length);
        } else {
            return formattedNumber;
        }
    }

    async sendOtp(phoneNumber, otp) {
        let msgType = "VENDOR REGISTRATION"
        if (isNaN(parseInt(otp))) {
            throw new Error("OTP is not a proper integer");
        }
        if (msgType == "" || msgType == undefined) {
            throw new Error("Message type should not be empty");
        }
        let parsePhoneNumber = this.removeCountryCode(phoneNumber)
        console.log("sending OTP ...")
        return await this.makeGetRequest(`http://route.digimiles.in/bulksms/bulksms?username=DG35-gogocb&password=digimile&type=0&dlr=1&destination=${parsePhoneNumber}&source=GOGOCB&message=OTP for ${msgType} on GOGO CABS is ${otp} and valid till 10 minutes.Do not share this OTP to anyone for security reasons - -GOGOCABS&entityid=1101721220000072338&tempid=1107169367301920885&tmid=1101721220000072338,1602100000000009244`)
    }

    async sendConfirmationMessage(ride) {
        console.log("sending ride received sms...")
        let pickupTime = new Date(ride.bookingDetails.tripStartTime)
        pickupTime = pickupTime = pickupTime.toLocaleString("en-IN", { day: 'numeric', month: 'short', year: "2-digit", timeZone: 'Asia/Kolkata', hour: '2-digit', minute:'2-digit'})
        return await this.makeGetRequest(`http://route.digimiles.in/bulksms/bulksms?username=DG35-gogocb&password=digimile&type=0&dlr=1&destination=${ride.customerDetails.phoneNumber}&source=GOGOCB&message=We're connecting with our awesome drivers to find you the perfect match. Once a driver confirms, we'll ping you right away! 

Booking details

Booking ID: ${ride.bookingId}
Name: ${ride.customerDetails.name}
Pickup location: ${ride.bookingDetails.pickup.shortName}
Drop location: ${ride.bookingDetails.drop.shortName}
Pickup time: ${pickupTime}
Car type: ${ride.bookingDetails.cabType == "xylo" ? "Any SUV" : ride.bookingDetails.cabType}
Trip type: ${ride.bookingDetails.tripType}
Rate per KM: ${ride.priceDetails.rateCard.rate}
MinBaseFare: ${ride.priceDetails.rateCard.fare}
Toll: ${ride.priceDetails.rateCard.estTollAmount}
Permit: ${ride.priceDetails.rateCard.permitAmount}
Driver bata: ${ride.priceDetails.rateCard.bata}

Total fare:  ${ride.priceDetails.rateCard.total}

Contact: 6385588806
Total price can increase or decrease based on the distance travelled

Booking Link: https://gogocabs.com/
Private Parking, Root top carrier extra, Hill charges applicable for hill stations

Wishing you a safe journey !!! -GOGOCABS -GOGOCABS&entityid=1101721220000072338&tempid=1107173420351703899&tmid=1101721220000072338,1602100000000009244`)
    }

    async sendDriverMessage(ride, cabVendor) {
        console.log("sending sendDriverMessage sms...")
        let pickupTime = new Date(ride.bookingDetails.tripStartTime)
        pickupTime = pickupTime.toLocaleString("en-IN", { day: 'numeric', month: 'short', year: "2-digit", timeZone: 'Asia/Kolkata', hour: '2-digit', minute:'2-digit'})
        return await this.makeGetRequest(`http://route.digimiles.in/bulksms/bulksms?username=DG35-gogocb&password=digimile&type=0&dlr=1&destination=${ride.customerDetails.phoneNumber}&source=GOGOCB&message=Dear ${ride.customerDetails.name},

Thank you for choosing GOGO Cabs! Your booking has been confirmed.

Booking Details:

Booking ID: ${ride.bookingId}
Pickup Location: ${ride.bookingDetails.pickup.shortName}
Drop-off Location: ${ride.bookingDetails.drop.shortName}
Time: ${pickupTime}
Cab Type: ${ride.bookingDetails.cabType == "xylo" ? "Any SUV" : ride.bookingDetails.cabType}

Driver Details:

Vehicle reg no: ${ride.cabVendor?.vehicle?.vehicleRegNo || Object.values(cabVendor.vehicles)[0].vehicleRegNo}
Driver Contact: ${ride.cabVendor?.driver?.driverPhoneNo || cabVendor.profile.phoneNo}

For any assistance or changes, feel free to reach us at 6385588806.
https://gogocabs.com/

Have a safe journey !!!

GOGO Cabs Team -GOGOCABS&entityid=1101721220000072338&tempid=1107171104584242323&tmid=1101721220000072338,1602100000000009244`)
    }

}





let digimileClient = new DigiMileClient()
module.exports = digimileClient
// digimileClient.sendConfirmationMessage()