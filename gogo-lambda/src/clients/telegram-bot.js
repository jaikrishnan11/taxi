const TelegramBot = require('node-telegram-bot-api');
const moment = require('moment')
const _ = require('lodash')
// replace the value below with the Telegram token you receive from @BotFather
const token = '5143210951:AAHdTsRzC516Q-6oX3xWqcR55AVNXobBdRY';
const Constants = require('../service/constants')
const Config = require('../config').telegram
const locationService = require('../service/googleLocationService');
const constants = require('../service/constants');

//Emoji https://apps.timwhitlock.info/emoji/tables/unicode
class Telegram {
  constructor() {
    // Create a bot that uses 'polling' to fetch new updates
    this.bot = new TelegramBot(token, {polling: false});
  }

  maskString(actualString, revealFirstNchar=2, maskString='*'){
    actualString = actualString + ''
    let stringToBeReplaced = actualString.substring(revealFirstNchar+1)
                                         .replace(/[a-z0-9]/gi, maskString);
    return actualString.substring(0, revealFirstNchar) + stringToBeReplaced;
  }

  async sendMessageInternal(roomId, message, options = {}){
    if(Config.isDryRun)
      return Promise.resolve()
    return await this.bot.sendMessage(roomId, message, options)
  }

  async postRide(msg, isInternal = false) {
    //Formatting reference https://core.telegram.org/bots/api#formatting-options
    let roomId = isInternal ? Config.internalUser : Config.rideDetailsNew;
    await this.bot.sendMessage(roomId, `<b>bold</b>, <strong>bold</strong>
    <i>italic</i>, <em>italic</em>
    <u>underline</u>, <ins>underline</ins>
    <s>strikethrough</s>, <strike>strikethrough</strike>, <del>strikethrough</del>
    <span class="tg-spoiler">spoiler</span>, <tg-spoiler>spoiler</tg-spoiler>
    <b>bold <i>italic bold <s>italic bold strikethrough <span class="tg-spoiler">italic bold strikethrough spoiler</span></s> <u>underline italic bold</u></i> bold</b>
    <a href="http://www.example.com/">inline URL</a>
    <a href="tg://user?id=123456789">inline mention of a user</a>
    <code>inline fixed-width code</code>
    <pre>pre-formatted fixed-width code block</pre>
    <pre><code class="language-python">pre-formatted fixed-width code block written in the Python programming language</code></pre>"`, {
      parse_mode: "HTML"
    })
  }

  async sendMessage(msg, isInternal = false) {
    let roomId = isInternal ? Config.internalUser : Config.rideDetails;
    await this.sendMessageInternal(roomId, JSON.stringify(msg, null, 2))
  }

  async sendFeedbackMessage(ride, isInternal = false) {
    let roomId = isInternal ? Config.internalUser : Config.gogoFeedbacks;
    await this.bot.sendMessage(roomId, `
    <b> \u{1F695} \u{1F695} GoGo Cabs \u{1F695} \u{1F695} </b>
    <b>Booking Id : </b> ${ride.bookingId}
    <b>From : </b> ${ride.bookingDetails.pickup.fullAddress}
    <b>To : </b> ${ride.bookingDetails.drop.fullAddress}
    <b>Trip Type : </b> ${ride.bookingDetails.tripType}
    <b>Vehicle: </b> ${ride.bookingDetails.cabType}
    <b>Pick up Time : </b> ${moment(ride.bookingDetails.tripStartTime).utcOffset("+0530").format('MMM Do YYYY, h:mm a')}
    <b>Amount : </b> ${ride.priceDetails.rateCard.total}
    <b>Customer Name : </b> ${ride.customerDetails.name}
    <b>Customer Phone Number : </b> ${ride.customerDetails.phoneNumber}
    <b>Driver Name : </b> ${ride.cabVendor?.driver?.driverName}
    <b>Driver Number : </b> ${ride.cabVendor?.driver?.driverPhoneNo}
    <b>Driver Account Number : </b> ${ride.cabVendor?.driver?.driverAccountNo}
    <b> Vehicle Numner :</b> ${ride.cabVendor?.vehicle?.vehicleRegNo}
    `, {
      parse_mode: "HTML",
      disable_web_page_preview: true
    })
  }

  async sendDevMessage(msg, isInternal = false) {
    let roomId = isInternal ? Config.internalUser : Config.devMessages;
    await this.sendMessageInternal(roomId, JSON.stringify(msg, null, 2))
  }

  async sendDevMessageText(msg, isInternal = false) {
    let roomId = isInternal ? Config.internalUser : Config.devMessages;
    await this.sendMessageInternal(roomId, msg)
  }

  async sendWhatsappMessage(msg, isInternal = false){
    let roomId = isInternal ? Config.internalUser : Config.rideAnnounce;
    await this.sendMessageInternal(roomId, msg)
  }

  async sendRideForApproval(ride) {
    let adminChannelId = Config.gogoAdminRideApproval;
    let {bookingDetails, customerDetails, priceDetails} = ride
    let options = {
      parse_mode: "HTML",
      reply_markup: {
        inline_keyboard:[[{
            text: "Approve",
            callback_data: `${Constants.TelegramCommand.ApproveRide} ${ride.bookingId}`
        },{
            text: "Remove",
            callback_data: `${Constants.TelegramCommand.CancelRideByAdmin} ${ride.bookingId}`
        }]]
    },
    };
    let googleMapUrls = locationService.getGoogleMapUrls(bookingDetails.pickup, bookingDetails.drop, bookingDetails.intermediates)
    let message = `
    <i class="fab fa-telegram"></i>
    <b>BookingId : </b> ${ride.bookingId}
    <b>From : </b> <a href="${googleMapUrls.srcUrl}"> ${bookingDetails.pickup.fullAddress} </a>
    `
    if (bookingDetails.intermediates && bookingDetails.intermediates.length > 0) {
      message+= `<b> IntermediateStops: </b> ${bookingDetails.intermediates.map(i=> i.fullAddress).join("\n")}\n` 
    }

    message += `<b>To : </b>   <a href="${googleMapUrls.dstUrl}">   ${bookingDetails.drop.fullAddress} </a>
    <b> Direction: </b> <a href="${googleMapUrls.directionUrl}"> Open in Google maps </a>
    <b>Trip Type : </b> ${bookingDetails["tripType"]} -  ${bookingDetails["cabType"]}
    <b>Pick up Time : </b> ${moment(bookingDetails.tripStartTime).utcOffset("+0530").format('MMM Do YYYY, h:mm a')}
    `
    if(bookingDetails.returnStartTime){
      message += `<b>Return start Time : </b> ${moment(bookingDetails.returnStartTime).utcOffset("+0530").format('MMM Do YYYY, h:mm a')}
      `
    }
    if(bookingDetails["tripType"] === constants.ONE_WAY) {
      message += `<b>Total Km : </b> ${bookingDetails.estDistance} km`
    } else if (bookingDetails["tripType"] === constants.ROUND_TRIP) {
      message += `<b>Total Km : </b> ${bookingDetails.estDistance} km (Per day 250km)`
    }
    message += 
    `\n<b>Amount : </b> ${priceDetails.rateCard.total},
    <b>Toll Amount : </b> ${priceDetails.rateCard.estTollAmount},
    <b>Permit Amount : </b> ${priceDetails.rateCard.permitAmount},
    <b>Driver Bata : </b> ${priceDetails.rateCard?.priceSplit?.DriverBata?.value},
    <b>CustomerName : </b> ${customerDetails.name}
    <b>CustomerPhoneNumber : </b> ${customerDetails.phoneNumber}
    `
    await this.sendMessageInternal(adminChannelId, message, options)
  }

  async sendVendorDetailsForApproval(cabVendorId, cabVendorPayload) {
    let adminChannelId = Config.gogoAdminChannel;
    let options = {
      parse_mode: "HTML",
      reply_markup: {
        inline_keyboard:[[{
            text: "ApproveVendor",
            callback_data: `${Constants.TelegramCommand.ApproveVendor} ${cabVendorId}`
        }]]
    },
    };
    let message = `<b> !!!! New Vendor !!!! </b>\n\n`
    _.forOwn(cabVendorPayload, (value, key) => message+= `<b>${key} : </b> ${value}\n`)
    await this.sendMessageInternal(adminChannelId, message, options)
  }

  async sendGogoAdminMessage(title, keyValuePair) {
    let adminChannelId = Config.gogoAdminChannel;
    let options = {
      parse_mode: "HTML"
    }
    let message = `<b> !!! ${title} !!! </b>`
    _.forOwn(keyValuePair, (value, key) => message+= `<b>${key} : </b> ${value}\n`)
    await this.sendMessageInternal(adminChannelId, message, options)
  }

  async sendRideMessage(msg, isInternal = false) {
    let ride = msg.ride
    let googleMapUrls = locationService.getGoogleMapUrls(ride.bookingDetails.pickup, ride.bookingDetails.drop)
    let rideStartTime = ride.bookingDetails.returnStartTime ? moment(ride.bookingDetails.returnStartTime).utcOffset("+0530").format('MMM Do YYYY, h:mm a') : "";
    let roomId = isInternal ? Config.internalUser : Config.rideAnnounce;
    let intermediateString = undefined
    if (ride.bookingDetails.intermediates && ride.bookingDetails.intermediates.length > 0) {
      intermediateString = `<b> IntermediateStops: </b> ${ride.bookingDetails.intermediates.map(i=> i.fullAddress).join("\n")}` 
    }
    let message = `
    <b> \u{1F69C} \u{1F69C} Gogo Cabs \u{1F69C} \u{1F69C} </b>
    <b>From : </b> <a href="${googleMapUrls.directionUrl}"> ${msg["from"]}  </a> ${intermediateString ? "\n" + intermediateString + "\n": ""}
    <b>To : </b> <a href="${googleMapUrls.directionUrl}"> ${msg["to"]}  </a>
    <b>Trip Type : </b> ${msg["tripType"]}
    <b>Vehicle: </b> ${msg["carType"]}
    <b>Pick up Time : </b> ${msg["time"]}
    ${rideStartTime && msg["tripType"] != "ONE_WAY" ? "<b>Return Time : </b> " + rideStartTime : "" }
    <b>Amount : </b> ${msg["amount"]}
    <b>Commission : </b> ${msg["commissionFee"]}

    <b>Free Registration for Driver !!! </b>
    GoGo driver App - https://play.google.com/store/apps/details?id=com.gogodriverapp&hl=en_IN
    
    <b>Join in Whatsapp Channel !!! </b>
    Channel Link - https://whatsapp.com/channel/0029Va9FntK4dTnNrltbpP0v

    <b>Join in Telegram Group for live updates !!! </b>
    Telegram Group Link - https://t.me/gogo_ride_announce

    <a href="tel:9962772400"> Call 9962772400 CMBT Baskar </a>
    
    <i> Toll/Permit included </i>

    <i> Carrier, hill charges only extra </i>

    <b> !!! Booking ONLY USING APP !!! </b>
    `

    if(Config.isDryRun)
      return Promise.resolve()
    this.bot.sendMessage(Config.publicRideAnnounce, message , {
      parse_mode: "HTML",
      disable_web_page_preview: true
    }).catch(console.error)

    //TODO: Stop sending to this group 
    await this.bot.sendMessage(roomId, message , {
      parse_mode: "HTML",
      disable_web_page_preview: true
    })
  }

  async sendRideAssignedMessage(ride, cabVendor) {
    let roomId = Config.rideAnnounce
    let googleMapUrls = locationService.getGoogleMapUrls(ride.bookingDetails.pickup, ride.bookingDetails.drop)
    let tripReturnTime = ride.bookingDetails.returnStartTime ? new Date(ride.bookingDetails.returnStartTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"}) : undefined
    let vehicle = cabVendor.vehicles[ride.cabVendor?.vehicle?.vehicleRegNo] || Object.values(cabVendor.vehicles)[0]
    await this.sendMessageInternal(roomId,`
    \u{1F64F} \u{1F64F} Ride Assigned !!! \u{1F64F} \u{1F64F}
    <b> BookingId : </b> ${ride.bookingId}
    <b> CabVendorId: </b> ${cabVendor.externalId}
    <b> CabVendorInternalId : </b> ${cabVendor.cabVendorId} -- ${cabVendor.version}
    ----------
    <b> From : </b> <a href="${googleMapUrls.srcUrl}"> ${ride.bookingDetails.pickup.shortName} </a>
    <b> To : </b> <a href="${googleMapUrls.dstUrl}">   ${ride.bookingDetails.drop.shortName} </a>
    <b> TripStartTime: </b> ${new Date(ride.bookingDetails.tripStartTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"})}
    ${tripReturnTime && ride.bookingDetails.tripType != "ONE_WAY" ? "<b> TripReturnTime: </b> " + tripReturnTime : ""}
    <b> Commission: </b> \u{1F4B0} ${ride.priceDetails.cabVendor.transactionFee}
    ----------
    <b> CabVendorName: </b> ${cabVendor.profile.name}
    <b> CabVendorPhone: </b> ${cabVendor.profile.phoneNo}
    <b> VehicleRegNo: </b> ${vehicle.vehicleRegNo}
    <b> VehicleType: </b> ${vehicle.type} 
    `, {
     parse_mode: "HTML"
    })
  }

  async sendRideAssignedToDriversGroup(ride, cabVendor) {
    let roomId = Config.publicRideAnnounce
    let googleMapUrls = locationService.getGoogleMapUrls(ride.bookingDetails.pickup, ride.bookingDetails.drop)
    let tripReturnTime = ride.bookingDetails.returnStartTime ? new Date(ride.bookingDetails.returnStartTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"}) : undefined
    let vehicle = cabVendor.vehicles[ride.cabVendor?.vehicle?.vehicleRegNo] || Object.values(cabVendor.vehicles)[0]
    await this.sendMessageInternal(roomId,`
    \u{1F696} \u{1F696} Ride Assigned  \u{1F696} \u{1F696}
    <b> BookingId : </b> ${ride.bookingId}
    ----------
    <b> From : </b> <a href="${googleMapUrls.srcUrl}"> ${ride.bookingDetails.pickup.shortName} </a>
    <b> To : </b> <a href="${googleMapUrls.dstUrl}">   ${ride.bookingDetails.drop.shortName} </a>
    <b> TripStartTime: </b> ${new Date(ride.bookingDetails.tripStartTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"})}
    ${tripReturnTime ? "<b> TripReturnTime: </b> " + tripReturnTime : ""}
    ----------
    <b> CabVendorName: </b> ${cabVendor.profile.name}
    <b> CabVendorPhone: </b> ${this.maskString(cabVendor.profile.phoneNo, 7, '*')}
    <b> VehicleRegNo: </b> ${vehicle.vehicleRegNo}
    `, {
     parse_mode: "HTML"
    })
  }

  async sendRideClosedMessage(ride) {
    let roomId = Config.gogoAdminChannel;
    let bookingDetails = ride.bookingDetails
    await this.sendMessageInternal(roomId,`
    \u{26A1} \u{26A1} Ride Closed  \u{26A1} \u{26A1}
    <b> BookingId : </b> ${ride.bookingId}
    ----------
    <b> CabVendorId: </b> ${ride.cabVendor.cabVendorId}
    <b> Est Ride end time: </b> ${new Date(ride.bookingDetails.estRideEndTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"})}
    <b> Actual Ride close time: </b>${new Date(ride.bookingDetails.rideCloseTime).toLocaleString("en-IN", {timeZone: "Asia/Kolkata"})}
    ----------
    <b> Rate per km:</b> ${ride.priceDetails.rateCard.rate}
    <b> Estimated Distance: </b> ${bookingDetails.estDistance}
    <b> Distance closed: </b> ${bookingDetails.actualDistanceCovered}
    <b> Other charges: </b> ${JSON.stringify(_.pick(ride.priceDetails.rateCard, ['hillCharge', 'tollAmount', 'waitingCharge', 'petCharge', 'parkingCharge']))}
    <b> Estimated total:</b> ${ride.priceDetails.rateCard.previousEstimatedFare}
    <b> New total: </b> ${ride.priceDetails.rateCard.total}
    `, {
     parse_mode: "HTML"
    })
  }

}

module.exports = new Telegram()