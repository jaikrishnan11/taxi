const axios = require('axios');
var qs = require('qs');
const telegramBot = require('./telegram-bot')
const Config = require('../config').pktApp
TRIP_TYPE_MAP = {
    "one way": 1,
    "round trip": 2
}

VEHICLE_TYPE_MAP = {
    sedan: 2,
    etios: 3,
    innova: 5,
    xylo: 4
}

class Client {

    async queryCustomer(nameOrPhoneNumber) {
        const URL = "https://pktcalltaxi.com/securehost/_userlist"
        let response = await axios.get(URL, {
            params: { query: nameOrPhoneNumber },
            headers: {
                Cookie: Config.cookie
            }
        })
        return response?.data[0]?.id
    }

    async createCustomer(customerInfo) {
        const URL = "https://pktcalltaxi.com/securehost/_customeradd"
        var data = qs.stringify({
            'User[user_name]': customerInfo.name,
            'User[user_contactno]': customerInfo.phoneNumber,
            'User[user_state]': '1',
            'User[user_city]': '449'
        });
        var config = {
            method: 'post',
            url: URL,
            headers: {
                'Cookie': Config.cookie,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data
        };

        let response = await axios(config)
        let [responseCode, __] = response.data.split("|||")
        console.log(responseCode, __)

    }

    async getDurationBetweenPlaces(pickup, drop) {
        const URL = "https://pktcalltaxi.com/securehost/_getdisduration"
        let response = await axios.get(URL, {
            params: { pickup, drop },
            headers: {
                Cookie: Config.cookie
            }
        })
        let [distance, duration] = response.data.split("|||")
        //console.log(distance + " " + duration)
        return duration
    }

    async createAndGetCustomerId(customerInfo) {
        await this.createCustomer(customerInfo)
        return await this.queryCustomer(customerInfo.phoneNumber)
    }

    async postRide(rideDetails, requestInfo) {
        try {
            let payload = {
                "uid": await this.createAndGetCustomerId(rideDetails),
                "vehicle_type": VEHICLE_TYPE_MAP[rideDetails.vehicle] || 3,
                "trip_type": TRIP_TYPE_MAP[rideDetails.tripType] || 1,
                "drop_latitude": "10.4232271",
                "drop_longitude": "79.3199985",
                "pickup_latitude": rideDetails.latitude,
                "pickup_longitude": rideDetails.longitude,
                "pickup_location_name": rideDetails.from,
                "drop_location_name": rideDetails.to,
                "time": rideDetails.pickupTime,
                "date": rideDetails.pickupDate,
                "km": rideDetails.estimatedKm,
                "returndate": rideDetails.returnDate,
                "returntime": "",
                "remark": "G",
                "travel_duration": await this.getDurationBetweenPlaces(rideDetails.from, rideDetails.to),
                "from_admin": "1",
                "basefare": rideDetails.price,
                "addhr": "0",
                "addkm": "12",
            }
            var config = {
                method: 'post',
                url: "https://pktcalltaxi.com/securehostpanel/clapi/bookingupt",
                headers: {
                    'Cookie': Config.cookie,
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: qs.stringify(payload)
            };

            if (Config.dontPostRides || requestInfo.isInternalUser) {
                console.log("Didnt post to Pkt app: ", payload)
                return;
            }

            let response = await axios(config)
            console.log("Posted to pkt app : ", JSON.stringify(response.data))
            if (response.data.error) {
                console.error("Error booking failed", response.data.error)
                telegramBot.sendDevMessage({ msg: "Posting ride to pkt app failed", ex: response.data.error })
                throw response.data.error
            }
        } catch (err) {
            console.error(err)
            telegramBot.sendDevMessage({ msg: "Posting ride to pkt app failed", ex: err })
        }
    }
}
//new Client().queryCustomer("sa").then(console.log, console.log)

module.exports = new Client()