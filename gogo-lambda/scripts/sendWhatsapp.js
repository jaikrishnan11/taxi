const whatsappClient = require('../src/clients/whatsapp-client')
const invoiceClient = require('../src/clients/invoice-generator-client')
const ddbClient = require('./ddbClient')

async function sendBookingConfirmationMessage(bookingId, phoneNumber) {
    let ride = await ddbClient.get({
        TableName: "rides",
        Key: {
            bookingId
        }
    }).promise();
    ride = ride.Item
    if(phoneNumber){
        ride.customerDetails.phoneNumber = phoneNumber
    }
    await whatsappClient.sendBookingConfirmation(bookingId, ride).then(console.log, console.log)

}

async function sendRideCompletedMessage(bookingId, phoneNumber) {
    let ride = await ddbClient.get({ TableName: "rides", Key: { bookingId } }).promise();
    ride = ride.Item
    let buffer = await invoiceClient.generateInvoice(ride)
    if(phoneNumber){
        ride.customerDetails.phoneNumber = phoneNumber
    }
    await whatsappClient.sendRideCompletedWithInvoice(ride.bookingId, ride, buffer)
}

async function sendDriverAssignedMessage(bookingId, cabVendorId, phoneNumber) {
    let ride = await ddbClient.get({ TableName: "rides", Key: { bookingId } }).promise();
    ride = ride.Item
    let cabVendor = await ddbClient.getCabVendorById(cabVendorId);
    
    ride.customerDetails.phoneNumber = phoneNumber
    await whatsappClient.sendDriverAssignedMessage(ride, cabVendor)
}

async function sendDriverApprovedMessage(cabVendorId) {
    let cabVendor = await ddbClient.internalClient.getVendorById(cabVendorId)
    await whatsappClient.sendDriverApproved(cabVendorId, cabVendor)
}
async function sendOtp(otp, phoneNo) {
    await whatsappClient.sendOtp(otp, phoneNo)
}

//whatsappClient.sendTextMessage("919443272489", "This is a automated account for sending ride updates. Please contact our customer support number(+916385588806) for queries.")

//sendRideCompletedMessage("DMQF53", "7338838089") // confirmd
//sendBookingConfirmationMessage("DMQF53", "7338838089")
//sendDriverApprovedMessage("114205603249694360772")
//sendOtp("1234", "7338838089") // confirmed
//sendDriverAssignedMessage("7UOOT1", "107129062810685009663", "7338838089")