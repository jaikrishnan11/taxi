const axios = require('axios');

// Elasticsearch URL and index
const ES_URL = 'http://ec2-18-61-216-82.ap-south-2.compute.amazonaws.com:9200/table-cab-vendors/_search';

// Custom headers (e.g., Authorization token or API key)
const headers = {
    'Authorization': 'Basic ZWxhc3RpYzptaWVQcjhOckxUQUpsT3dYZDZjWg==', // Replace with your actual token
    'Content-Type': 'application/json'
};

// Initial search body to start the scroll
const searchBody = {
    "query": {
        "match": {
            "version": "v2"
        }
    },
    "scroll": "1m", // Time the scroll remains open on the server (1 minute)
    "size": 100 // Number of documents per batch
};

// Function to fetch all data using scroll
const fetchAllDocuments = async () => {
    try {
        // Initial search request to start the scroll
        const initialResponse = await axios.post(ES_URL, searchBody, { headers });

        let scrollId = initialResponse.data._scroll_id;
        let documents = initialResponse.data.hits.hits;

        console.log(`Fetched ${documents.length} documents in the first batch.`);

        // Continue fetching documents while scroll has results
        while (true) {
            const scrollResponse = await axios.post('http://ec2-18-61-216-82.ap-south-2.compute.amazonaws.com:9200/_search/scroll', {
                scroll: '1m', // Same scroll timeout
                scroll_id: scrollId
            }, { headers });

            const hits = scrollResponse.data.hits.hits;
            if (hits.length === 0) {
                console.log('No more documents to fetch.');
                break;
            }

            console.log(`Fetched ${hits.length} more documents.`);
            documents = documents.concat(hits); // Append new documents

            scrollId = scrollResponse.data._scroll_id; // Update the scroll ID for the next request
        }

        console.log(`Total documents fetched: ${documents.length}`);
        // Do something with the documents (e.g., save to a file or database)
    } catch (error) {
        console.error('Error fetching documents:', error);
    }
};

// Start fetching
fetchAllDocuments();
