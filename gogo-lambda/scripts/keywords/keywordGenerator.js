const fs = require("fs")
let _ = require("lodash")

let cities = JSON.parse(fs.readFileSync("gogo-lambda/scripts/data/important_cities.json", { encoding: 'utf-8'}))

let generatedKeywordList = []

let prefixCabTypes = ["Book a cab from", "Book a taxi from", "Book a outstation cab", "Intercity cab"]
let suffixCabTypes = ["cab", "taxi", "outstation", "ride"]
let cityWiseKeywords = ["outstation cabs", "intercity cabs"]

// Finding unique cities
let cityList = new Set()
_.forEach(cities, (value, key) => {
    cityList.add(value["city1"])
    cityList.add(value["city2"])
})

let list = Array.from(cityList)

let fromToList = new Set()
_.each(list, (s1) => {
    _.each(list, (s2) => {
        if(s1 != s2) {
            fromToList.add(`${s1} to ${s2}`)
        }
    })
})

_.each(prefixCabTypes, (value)=> {
    _.each(Array.from(fromToList), (city) => {
        generatedKeywordList.push(`${value} ${city}`)
    })
})

_.each(suffixCabTypes, (value)=> {
    _.each(Array.from(fromToList), (city) => {
        generatedKeywordList.push(`${city} ${value}`)
    })
})

_.each(Array.from(cityList), (city)=> {
    _.each(cityWiseKeywords, (value) => {
        generatedKeywordList.push(`${city} ${value}`)
    })
})

let output = _.map(generatedKeywordList, (value) => {
    return `"${value}"`
})

fs.writeFileSync("./keywords", _.join(output, "\n"))


