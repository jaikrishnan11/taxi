let data = {
    regNo: 'TN63BE4114',
    vehicleClass: 'Motor Cab(LPV)',
    chassis: 'MBJB49BTX00194649~0219',
    engine: '1ND1B00336',
    vehicleManufacturerName: 'TOYOTA KIRLOSKAR MOTOR PVT LTD',
    model: 'TOYOTA ETIOS GD',
    vehicleColour: 'WHITE',
    type: 'DIESEL',
    normsType: 'BHARAT STAGE IV',
    bodyType: 'SEDAN',
    ownerCount: '1',
    owner: 'SELVARAJ.A',
    ownerFatherName: 'ARUMUGAM',
    mobileNumber: null,
    status: 'ACTIVE',
    statusAsOn: '14-Nov-2023',
    regAuthority: 'SIVAGANGAI RTO, Tamil Nadu',
    regDate: '08-May-2019',
    vehicleManufacturingMonthYear: '2/2019',
    rcExpiryDate: '09-May-2025',
    vehicleTaxUpto: 'LTT',
    vehicleInsuranceCompanyName: 'The New India Assurance Company Limited',
    vehicleInsuranceUpto: '30-Jan-2024',
    vehicleInsurancePolicyNumber: '67140031220350042767',
    rcFinancer: 'UNION BANK OF INDIA',
    presentAddress: 'NO 2/999 KOTTAGUDI , SIVAGANGAI,SIVAGANGAI, Sivaganga -630561',
    permanentAddress: 'NO 2/999 KOTTAGUDI , SIVAGANGAI,SIVAGANGAI, Sivaganga -630561',
    vehicleCubicCapacity: '1364.0',
    grossVehicleWeight: '1440',
    unladenWeight: '1010',
    vehicleCategory: 'LPV',
    rcStandardCap: '0',
    vehicleCylindersNo: '4',
    vehicleSeatCapacity: '5',
    vehicleSleeperCapacity: '0',
    vehicleStandingCapacity: '0',
    wheelbase: '2550',
    vehicleNumber: 'TN63BE4114',
    puccNumber: 'TN02200030023476',
    puccUpto: '09-Feb-2024',
    blacklistStatus: 'NA',
    blacklistDetails: [Array],
    permitIssueDate: '29-May-2019',
    permitNumber: 'TN/63/CC/MOTO/2019/122',
    permitType: 'Contract Carriage Permit[Motor Cab Permit]',
    permitValidFrom: '29-May-2019',
    permitValidUpto: '28-May-2024',
    nonUseStatus: 'N',
    nonUseFrom: null,
    nonUseTo: null,
    nationalPermitNumber: null,
    nationalPermitUpto: null,
    nationalPermitIssuedBy: null,
    isCommercial: true,
    nocDetails: 'NA',
    dbResult: false,
    partialData: false,
    mmvResponse: null,
    financed: true
  }