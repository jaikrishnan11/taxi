const firebaseClient = require('../src/clients/firebase-admin-client')
const fs = require("fs");
const _ = require("lodash")

//Vehicle Expired Notice


main = async () => {
    let cabVendors = JSON.parse(fs.readFileSync("./cab-vendors.json", "utf-8")).items
    for (let i = 0; i < cabVendors.length; i++) {
        if (cabVendors[i].version == "v2" && _.size(_.keys(cabVendors[i].vehicles)) > 0) {
            let vehicleNos = _.keys(cabVendors[i].vehicles)
            for (let j = 0; j < vehicleNos.length; j++) {
                if(cabVendors[i].vehicles[vehicleNos[j]]?.pendingActions?.length > 0) {
                    let deviceToken = cabVendors[i]?.devices?.cabVendorDevice?.deviceToken
                    if(deviceToken == undefined) {
                        console.log("No device token found", deviceToken, cabVendors[i])
                        continue
                    }
                    let bodyMessage = `Vehicle ${vehicleNos[j]} not approved, contact gogo cabs in driver app whatsapp`
                    let firebaseResponse = await firebaseClient.sendMessageToToken({title: "Vehicle not approved notice", body: bodyMessage, sendLocation: {}}, deviceToken)
                    console.log(firebaseResponse)
                }
            }
        }
    }
    console.log("Completed ...")
}

main()