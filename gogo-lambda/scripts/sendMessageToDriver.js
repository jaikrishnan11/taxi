const ddbClient = require('./ddbClient')

const firebaseClient = require('../src/clients/firebase-admin-client')
// uvsankar94@gmail.com = 106201177605008703346
// velu1995@gmail.com   = 100146709221843723535
// gogocabmail          = 114205603249694360772
const CAB_VENDOR_ID = "100146709221843723535";
const NOTF_MESSAGE = 'New rides available';

async function sendNotification(cabVendorId, notfMessage){
    let cabVendor = await ddbClient.getCabVendorById(cabVendorId);
    let deviceToken = cabVendor.devices.cabVendorDevice.deviceToken;
    if(!deviceToken)
        throw 'deviceToken not found'
    console.log("Using token of " + cabVendor.profile.name, deviceToken)
    let firebaseResponse = await firebaseClient.sendMessageToToken({title: "Hello from gogo cabs", body: notfMessage, sendLocation: {}}, deviceToken)

    console.log(firebaseResponse)
}

async function sendNotificationToken(){
    let deviceToken = "dFxJOtEsThGHepbWYMpija:APA91bEhgKrscgjDiLJySnVAAmQO7nKkDWE6gtYLtCU8VJvqGYgo3s8E-VOsLXlNhjfgxfyficYx8yh6mrAo3jlQP68slzSWRAtMPYpl4YCdgaSrK-hvL_MAhStX2qPTR2402JIFVLL0";
    let firebaseResponse = await firebaseClient.sendMessageToToken({title: "Testghgvh title", body: "test bodyh", sendLocation: {}}, deviceToken)
    console.log(firebaseResponse)
}

//sendNotificationToken()
sendNotification(CAB_VENDOR_ID, NOTF_MESSAGE)

