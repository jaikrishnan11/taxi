const axios = require('axios');
const fs = require("fs");
const _ = require("lodash")

const refreshVehicle = async (cabVendorId, vehicleNumber) => {
    let data = JSON.stringify({
        "cabVendorId": cabVendorId,
        "vehicleNumber": vehicleNumber,
        "fromCron": true,
        "isDashboard" : false,
        "isDeductMoney" : false,
        "isParivahanCall" : false
    });

    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: 'https://api.gogocab.in/cab-vendors/refresh-vehicle',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };

    try {
        const response = await axios.request(config);
        // console.log(JSON.stringify(response.data));
    } catch (error) {
        console.log(error);
    }
};

function sleepSync(milliseconds) {
    const start = Date.now();
    while (Date.now() - start < milliseconds) {
        // Busy-wait loop to block the event loop
    }
}

main = async () => {
    let cabVendors = JSON.parse(fs.readFileSync("./cab-vendors.json", "utf-8")).items
    for (let i = 0; i < cabVendors.length; i++) {
        if (cabVendors[i].version == "v2" && _.size(_.keys(cabVendors[i].vehicles)) > 0) {
            let vehicleNos = _.keys(cabVendors[i].vehicles)
            for (let j = 0; j < vehicleNos.length; j++) {
                console.log("Refreshing for", cabVendors[i].cabVendorId, vehicleNos[j])
                await refreshVehicle(cabVendors[i].cabVendorId, vehicleNos[j])
            }
        }
    }
    console.log("Completed ...")
}

main()
