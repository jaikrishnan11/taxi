const moment = require('moment')
const ddbClient = require('./ddbClient')
const _ = require('lodash')


const AMOUNT_IN_PAISE = -39400; // Amount will be added to the existing money.. 
const CABVENDORID = "112238029055657023961";
const BOOKINGID = "BookingId :  PX1540";
const REASON = "Deducting for the ride, which was manually moved" // Should be string explaining the details for future reference

updateWallet(CABVENDORID, AMOUNT_IN_PAISE, BOOKINGID, REASON)

async function updateWallet(cabVendorId, amountInPaise, bookingId, reason) {
    if(_.isEmpty(reason))
        throw "Reason string is not defined"
    let walletAction = "DEDUCT_MONEY"
    if(amountInPaise < 0)
        console.log("Deducting money... ")
    else {
        console.log("Adding Money !!! \n Hope you know what are you doing ....")
        walletAction = "REFUND_MONEY"
    }
    let response = await ddbClient.transactWrite({
        TransactItems: [{
            Update: {
                TableName: "cab-vendors",
                Key: {
                    cabVendorId
                },
                UpdateExpression: "ADD wallet.#walletTotal :changeInAmountInPaise",
                ExpressionAttributeValues: {
                    ":changeInAmountInPaise": amountInPaise,
                    ":changeInAmountInPaisePositive": Math.abs(amountInPaise)
                },
                ReturnValuesOnConditionCheckFailure: "ALL_OLD",
                ConditionExpression: "wallet.#walletTotal >= :changeInAmountInPaisePositive",
                ExpressionAttributeNames: {
                    "#walletTotal": "total"
                }
            }
        }, {
            Put: {
                TableName: "wallet-book-keeping",
                Item: {
                    cabVendorId,
                    createdDate: new Date().toISOString(),
                    action: walletAction,
                    opDetails: {
                        amount: Math.abs(amountInPaise),
                        bookingId,
                        reason: reason
                    }
                }
            }
        }]
    }).promise().then(()=> "Success", err => {
        console.log(err)

    })
    console.log(response)
}