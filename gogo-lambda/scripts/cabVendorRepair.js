const dynamoClient = require("./ddbClient");
const itemRepairer = require('../src/service/ItemRepairer')

let externalIdMap = {}
let externalIdsR = {}
async function scanTable() {
    let items = [];
    let params = {
      TableName: "cab-vendors"
    };
  
    do {
      const data = await dynamoClient.scan(params).promise();
      items = data.Items
      params.ExclusiveStartKey = data.LastEvaluatedKey;
      
      for (let i=0; i< items.length; i++) {

        if (externalIdMap[items[i].externalId]) {
            externalIdMap[items[i].externalId].push(items[i].cabVendorId)
            externalIdsR[items[i].externalId] = externalIdMap[items[i].externalId]
        } else {
            externalIdMap[items[i].externalId] = [items[i].cabVendorId]
        }
      }
    
    } while (params.ExclusiveStartKey);

    console.log(externalIdsR)
  }

scanTable().then(console.log, console.log)