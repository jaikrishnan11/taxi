const axios = require('axios');
const fs = require('fs');

let data = JSON.stringify({
  "googleUserId": "114205603249694360772",
  "requestType": "DUMP_TABLE",
  "payload": {
    "tableName": "cab-vendors",
    "inFull": false
  },
  "googleIdToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjI4YTQyMWNhZmJlM2RkODg5MjcxZGY5MDBmNGJiZjE2ZGI1YzI0ZDQiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI1MTY0Njg3NDU1MjUtMHNqdDBoaHNoNzhvN2lxampwdjVnbGxwcmU0dXMwajYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI1MTY0Njg3NDU1MjUtMHNqdDBoaHNoNzhvN2lxampwdjVnbGxwcmU0dXMwajYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTQyMDU2MDMyNDk2OTQzNjA3NzIiLCJlbWFpbCI6ImdvZ29jYWJtYWlsQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYmYiOjE3MjgwMzc0MzEsIm5hbWUiOiJHb2dvIENhYnMiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUNnOG9jS0JlVEV2VzNmN0tlV2JWOTZCOGNFT053S2dPNTJhYTNnYkk3X2ptOUtzWGVpUE94TTU9czk2LWMiLCJnaXZlbl9uYW1lIjoiR29nbyIsImZhbWlseV9uYW1lIjoiQ2FicyIsImlhdCI6MTcyODAzNzczMSwiZXhwIjoxNzI4MDQxMzMxLCJqdGkiOiI3YzgzOTQ4OGFiMGMyMDI5Zjg0N2I2MTMyZTRkZTIxYzg1ZTQzYzc5In0.piStwMyRxvSsQ22koxIurbgdiRUOR0ZIkxh7OmqrOHpyDz0VegwZDfPGNGkGuqoVntACBSOYSZjcs44FTRsN7UDNt9yn_Yf5XEoSIE_-VUUTJoiEwMDCPsAHMl1j-woBEB17I5KT0gAL1ZZ13fMUau5mSXm1UEnmdWtBEUV_N2nsfkyAZX0EsPpHSWyUZIrMafDBotgI9lLPaUhzipPhoJ86aw8Dm22U-DapNTKzi_xN2FXjpBV27zeIbR4Vo_94Ve6HwhtQrCqLYS10dS6_pysLQ9sa6WXr1n4xhuJaHzmBMq2OIrojyYRWeD5aLRGNLirgryQMf9PXowPip4rVIQ"
});

let config = {
  method: 'post',
  maxBodyLength: Infinity,
  url: 'https://api.gogocab.in/admin/peformAction',
  headers: { 
    'Content-Type': 'application/json'
  },
  data : data
};

axios.request(config)
.then((response) => {
  fs.writeFileSync("./cab-vendors.json", JSON.stringify(response.data.items))
})
.catch((error) => {
  console.log(error);
});
