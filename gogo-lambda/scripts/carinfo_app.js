const axios = require('axios');

async function getVehicleInfo(vehicleRegNo) {
    let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://web.cuvora.com/car/web/details/list?vehicle_num=TN28AF1234',
        headers: {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/115.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'src': 'car-info_web',
            'appVersion': '251',
            'deviceId': '487edb3-1234-c183-f61-4e607ed5ad8',
            'userId': '650608b43272f54110c9d6cc',
            'Connection': 'keep-alive'
        }
    };

    let response = await axios.request(config);
    return response.data
}

getVehicleInfo("TN28AD1234").then(console.log)