const ddbClient = require('./ddbClient')

const smsClient = require('../src/clients/digimile-client')

async function sendSMS(phoneNumber, cabVendorId, bookingId) {
    let cabVendor = await ddbClient.getCabVendorById(cabVendorId);
    let ride = await ddbClient.get({
        TableName: "rides",
        Key: {
            bookingId
        }
    }).promise();
    ride = ride.Item;
    ride.customerDetails.phoneNumber = phoneNumber
    let response = await smsClient.sendDriverMessage(ride, cabVendor)
}

//sendSMS("7418268186", "113750849297076489811", "UZOG2Z").then(console.log, console.log)