const moment = require('moment')
const ddbClient = require('./ddbClient')
const _ = require('lodash')


const AMOUNT_IN_PAISE = 0; // Amount will be added to the existing money.. 
const CABVENDORID = "";

updateWallet(CABVENDORID, AMOUNT_IN_PAISE)

async function updateWallet(cabVendorId, amountInPaise) {
    if(amountInPaise < 0)
        console.log("Deducting money... ")
    else {
        console.log("Adding Money !!! \n Hope you know what are you doing ....")
        walletAction = "ADD_MONEY"
    }
    let response = await ddbClient.transactWrite({
        TransactItems: [{
            Update: {
                TableName: "cab-vendors",
                Key: {
                    cabVendorId
                },
                UpdateExpression: "ADD wallet.#walletTotal :changeInAmountInPaise",
                ExpressionAttributeValues: {
                    ":changeInAmountInPaise": amountInPaise,
                },
                ReturnValuesOnConditionCheckFailure: "ALL_OLD",
                ExpressionAttributeNames: {
                    "#walletTotal": "total"
                }
            }
        }]
    }).promise().then(()=> "Success", err => {
        console.log(err)

    })
    console.log(response)
}