const moment = require('moment')
const dynamoClient = require("./ddbClient");
const utils = require("../src/commons/utils")
const _ = require('lodash')

async function fetchCustomersData() {
    // let client = new DYNAMODB_CLIENT();
    let response = (await dynamoClient.dumpTable("rides", false)).items
    for (let i = 0; i < response.length; i++) {
        let customerData = response[i].customerDetails;
        if(_.isEmpty(customerData) || customerData.phoneNumber == "9962772100") {
            continue
        }
        customerData.phoneNumber = utils.removeCountryCode(customerData.phoneNumber)
        await dynamoClient.insertCustomer(customerData);
    }
}

fetchCustomersData()