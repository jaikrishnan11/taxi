const ddbClient = require('./ddbClient')
const vendorMgmtService  = require('../src/service/vendorMgmtService')
const _ = require('lodash')


async function approveVendors(lastEvaluatedKey){
    let response = await ddbClient.scan({
        TableName: "cab-vendors",
        FilterExpression: "vendorStatus = :vendorStatus",
        ExclusiveStartKey: lastEvaluatedKey,
        ExpressionAttributeValues: {
          ":vendorStatus": "UNAPPROVED"
        }
      }).promise()
    
    //console.log("Total record to be processed in this run" + response.Count)
    for(const cabVendors of _.chunk(response.Items, 2)){
        try {
            let promises = []
            for(const cabVendor of cabVendors) {
                cabVendor.id
                console.log(cabVendor.cabVendorId +"," + cabVendor.vendorStatus +","+ cabVendor.profile.email+","+cabVendor.profile.phoneNo)
                promises.push(vendorMgmtService.approveVendor(cabVendor.cabVendorId))
            }
            await Promise.all(promises)
        } catch(ex) {
            console.error(ex)
            return
        }
    }
    if(response.LastEvaluatedKey != null) {
        await approveVendors(response.LastEvaluatedKey)
    }
}

approveVendors()