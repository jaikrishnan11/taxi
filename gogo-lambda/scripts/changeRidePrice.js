const moment = require('moment')
const ddbClient = require('./ddbClient')
const _ = require('lodash')


const TRANSACTION_FEE = 210; // Amount will be added to the existing money.. 
const OLD_TRANSACTION_FEE = 209;
const BOOKINGID = "";

updateRideDetails(BOOKINGID, TRANSACTION_FEE, OLD_TRANSACTION_FEE)

async function updateRideDetails(bookingId, transactionFee, oldTransactionFee) {
    if (_.isEmpty(bookingId))
        throw "Booking Id is not defined"
    if (transactionFee < 0)
        throw "Transaction fee is not defined"
    let response = await ddbClient.transactWrite({
        TransactItems: [{
            Update: {
                TableName: "rides",
                Key: {
                    bookingId
                },
                UpdateExpression: "SET priceDetails.cabVendor.transactionFee =:newTransactionFee",
                ExpressionAttributeValues: {
                    ":newTransactionFee": 255.7
                },
                ReturnValuesOnConditionCheckFailure: "ALL_OLD"
            }
        }]
    }).promise().then(() => "Success", err => {
        console.log(err)

    })
    console.log(response)
}