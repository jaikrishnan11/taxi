const ddbClient = require('./ddbClient')
const elasticSearchClient = require('../src/clients/elastic-search-client')
const {OpensearchIndex, GenericTableNamespace}  = require('../src/service/constants')
const _ = require('lodash')
const {transformCabVendor, transformLedger, transformGeneric, transformRide} = require('../src/handlers/opensearch-ingestor')


async function scanAndIngest(tableName, lastEvaluatedKey) {
    let indexName;
    let transformFunction;

    if (tableName == "wallet-book-keeping") {
        indexName = OpensearchIndex.WalletLedger
        transformFunction = transformLedger
    } else if (tableName == "rides") {
        indexName = OpensearchIndex.Rides
        transformFunction = transformRide
    } else if (tableName == "cab-vendors") {
        indexName = OpensearchIndex.CabVendors
        transformFunction = transformCabVendor
    } else if (tableName == "generic-table") {
        indexName = OpensearchIndex.GenericTable
        transformFunction = transformGeneric
    } else {
        throw "not supported"
    }

    let response = await ddbClient.scan({
        TableName: tableName,
        ExclusiveStartKey: lastEvaluatedKey
      }).promise()
      lastEvaluatedKey = response.LastEvaluatedKey

      let dataSet = response.Items;
      const documents = dataSet.flatMap(transformFunction)
      if (!_.isEmpty(documents)){
        await elasticSearchClient.bulkIndexDocuments(documents, indexName)
      }
      if (!lastEvaluatedKey) {
        console.info("Reached end of scan - " + tableName)
      } else {
        console.info("Scanning next page - " + tableName)
        await scanAndIngest(tableName, lastEvaluatedKey)
      }
}

// scanAndIngest("generic-table").catch(err => {
//     console.error("Fail aaguthu da eruma.. ")
//     console.error(err)
// })
// scanAndIngest("rides").catch(err => {
//     console.error("Fail aaguthu da eruma.. ")
//     console.error(err)
// })
// scanAndIngest("cab-vendors").catch(err => {
//     console.error("Fail aaguthu da eruma.. ")
//     console.error(err)
// })
// scanAndIngest("wallet-book-keeping").catch(err => {
//     console.error("Fail aaguthu da eruma.. ")
//     console.error(err)
// })