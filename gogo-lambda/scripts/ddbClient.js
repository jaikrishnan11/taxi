if (process.env.NODE_ENV == "Prod") {
    const AWS = require('aws-sdk');
    const credentialsFromProfile = new AWS.SharedIniFileCredentials({profile: 'gogo-admin'});
    AWS.config.credentials = credentialsFromProfile;
}

const dynamodb = require('aws-sdk/clients/dynamodb');
const srcDynamodbClient = require('../src/clients/dynamo-db-client')

const testConfig = {
    "endpoint": "http://localhost:8000",
    "region": "ap-south-1",
    "credentials": {
        "accessKeyId": "AKIA4LRATLIDMYNER5N4",
        "secretAccessKey": "DONT_CHECK_IN"
    }
}
const prodConfig = {
    "region": "ap-south-1",
    // "credentials": {
    //     "accessKeyId": "AKIA4LRATLIDI6VWHUON",
    //     "secretAccessKey": "DONT_CHECK_IN"
    // }
}


const ddbClient = new dynamodb.DocumentClient(Object.assign({
    "marshallOptions": {
        "convertEmptyValues": false
    }
}, process.env.NODE_ENV == 'Prod' ? prodConfig : testConfig));

Object.assign(ddbClient, {
    getCabVendorById: async function(cabVendorId){
        let result = await ddbClient.get({
            TableName: "cab-vendors",
            Key: {
                cabVendorId
            }
        }).promise()
        if(!result.Item)
            throw "CabVendor " + cabVendorId + " not found";
        return result.Item;
    },
    getAllCabVendors: async function({filterExpression=null,projectionExpression=null}, lastEvaluatedKey){
        let response = await ddbClient.scan({
            TableName: "cab-vendors",
            FilterExpression: filterExpression,
            ProjectionExpression: projectionExpression,
            ExclusiveStartKey: lastEvaluatedKey
          }).promise()
          let items = response.Items
          if(response.LastEvaluatedKey){
            let restOfItems = await this.getAllCabVendors({filterExpression,projectionExpression}, response.LastEvaluatedKey)
            return items.concat(restOfItems)
          }
          return items || [];
    }
})
ddbClient.internalClient = srcDynamodbClient
module.exports = ddbClient