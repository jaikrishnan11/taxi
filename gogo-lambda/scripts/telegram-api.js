const path = require('path');
const MTProto = require('@mtproto/core');
const { sleep } = require('@mtproto/core/src/utils/common');
const _ = require('lodash');
// const { removeCountryCode } = require('../src/clients/whatsapp-client');

class API {
    constructor() {
        this.mtproto = new MTProto({
            api_id: '28947512',
            api_hash: 'a98b86fc801eeec5546627aa7c335aa9',

            storageOptions: {
                path: path.resolve(__dirname, './data/1.json'),
            },
        });
    }

    /* Authorization methods START */
    async sendCode(phone) {
        let result = await this.call('auth.sendCode', {
            phone_number: phone,
            settings: {
                _: 'codeSettings',
            },
        });
        return { phoneHash: result.phone_code_hash, result }
    }

    async signIn({ phoneCode, phone, phoneCodeHash }) {
        let result = await this.call('auth.signIn', {
            phone_code: phoneCode,
            phone_number: phone,
            phone_code_hash: phoneCodeHash,
        });
        return result;
    }
    //contacts: phoneDetails.map(phone => Object.assign(phone, {  _: 'inputPhoneContact'}))
    /* Authorization methods END */

    async importContact(phoneDetails) {
        let result = await this.call('contacts.importContacts', {
            contacts: phoneDetails.map(phone => Object.assign(phone, {  _: 'inputPhoneContact', 
            client_id: Math.ceil(Math.random() * 0xffffff) + Math.ceil(Math.random() * 0xffffff)
        }))
              
        })
        return {
            users: result.users.map(user => { return {id: user.id, accessHash: user.access_hash, username: user.username, phone: user.phone} }),
            retryContacts: result.retry_contacts,
            result
        }
    }

    async addContact(contactDetail) {
        let result = await this.call('contacts.addContact', {
            id: "14353611",
            first_name: "Manogaran",
            last_name: "Manogaran",
            phone: "+916369536159"
        })
        return result
    }

    async getChannelInfo(channelId) {
        let result = await this.call('channels.getChannels', {
            id: [{
                _: 'inputChannel'
            }]
        })
    }

    async inviteUserToChannel(channelId, users) {
        let result = await this.call('channels.inviteToChannel', {
            channel: {
                _: "inputChannel",
                channel_id: "1879794235",
                access_hash: "15015231777569674720"
            },
            users: users.map(user => {
                return {
                    _: "inputUser",
                    user_id: user.id,
                    access_hash: user.accessHash
                }
            })
        })
        return result;
    }

    async resolveUserName(username) {
        return this.call('contacts.resolveUsername', { username })
    }

    async createChannel(){
        return await this.call('channels.createChannel', {
            broadcast: true,
            title: "Test1",
            about: "about",
        })
    }

    async sendMessage(userInfo, message){
        return await this.call('messages.sendMessage', {
            clear_draft: true,
          
            peer: {
              _: 'inputPeerUser',
              user_id: userInfo.userId,
              access_hash: userInfo.accessHash
            },
            message: 'Test message from API',
            entities: [
              {
                _: 'messageEntityBold',
                offset: 0,
                length: 13,
              },
            ],
          
            random_id: Math.ceil(Math.random() * 0xffffff) + Math.ceil(Math.random() * 0xffffff),
          });
    }

    async call(method, params, options = {}) {
        try {
            console.log("Call : ", method, params)
            const result = await this.mtproto.call(method, params, options);
            console.log("Call result ", result)
            return result;
        } catch (error) {
            console.log(`${method} error:`, error);

            const { error_code, error_message } = error;

            if (error_code === 420) {
                const seconds = Number(error_message.split('FLOOD_WAIT_')[1]);
                const ms = seconds * 1000;

                await sleep(ms);

                return this.call(method, params, options);
            }

            if (error_code === 303) {
                const [type, dcIdAsString] = error_message.split('_MIGRATE_');

                const dcId = Number(dcIdAsString);

                // If auth.sendCode call on incorrect DC need change default DC, because
                // call auth.signIn on incorrect DC return PHONE_CODE_EXPIRED error
                if (type === 'PHONE') {
                    await this.mtproto.setDefaultDc(dcId);
                } else {
                    Object.assign(options, { dcId });
                }

                return this.call(method, params, options);
            }

            return Promise.reject(error);
        }
    }
}

const api = new API();

function removeCountryCode(phoneNumber) {
    const countryCode = "+91";
    const countryCode2 = "91";
    const formattedNumber = phoneNumber.toString().trim().replace(/\s+/g, ""); // remove whitespace and format number

    if (formattedNumber.startsWith(countryCode)) {
      return formattedNumber.substr(countryCode.length);
    } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode2)) {
      return formattedNumber.substr(countryCode2.length);
    } else {
      return formattedNumber;
    }
  }

async function addVendorsToChannel(){
    let cabVendors = require('./data/updatedCabVendor.json')
    let count = 1;
    for(let cabVendorChuncks of _.chunk(cabVendors, 100)){
        console.log(`Processing chuck ${count++}`)
        let importContactRequest = cabVendorChuncks.map( cv => { return { 
            phone: `+91${removeCountryCode(cv.profile.phoneNo)}`, 
            first_name: cv.profile.name, 
            last_name: cv.vehicles.vehicleRegNo
        }})
        let importContactResponse = await api.importContact(importContactRequest)
        if (importContactResponse.users.length > 0) {
            console.log("adding to group")
            await api.inviteUserToChannel("dummy", importContactResponse.users)
        }
    }
    console.log("Completed")
}

// api.call('help.getNearestDc').then(result => {
//     console.log('country:', result.country);
//   });

/**
 *   Name               Id              username                acessHash
 * JaiKrishnan      1257270134        jk_vijay_1_vj         11580564498999983418
 * Sankar           709088445         thanos1_2             18197824571613948466
 * Poorna           1523212171         1523212171                              4369502002820475000
 * 
 * 
 * gogo_ride_announce            1879794235                              15015231777569674720
 */

// gogo number 65238 e0932b56a8cdc2788a

phoneCodeHash = 'd59a0b308505193527'
phoneCode = '50884'
phone = "+916385588806"

// phoneCodeHash = 'fb763f86e94d26a83e'
// phoneCode = '22484'
// phone = "+919842975789"
// api.sendCode(phone).then(console.log, console.log)
// api.signIn({phoneCodeHash, phoneCode, phone}).then(console.log, console.log)
//{"name":{"S":"Manogaran Manogaran"},"phoneNo":{"N":"6369536159"},"email":{"S":"manosiva1404@gmail.com"}}
// api.importContact([{phone: "+919944298920", first_name: "Mohamed", last_name: "TN10AY2995"}]).then(console.debug)
addVendorsToChannel()
//api.addContact()
//api.inviteUserToChannel()
// api.resolveUserName("gogo_ride_announce")
//api.createChannel()
//api.sendMessage({userId: '1257270134', accessHash: "11580564498999983418"})
//api.mtproto.destroy()


module.exports = api;