const SQS = require('aws-sdk/clients/sqs')
const Constants = require('../src/service/constants')
const ddbClient = require('./ddbClient')
const _ = require('lodash')
const GeneralSQS  = "https://sqs.ap-south-1.amazonaws.com/849398159878/GeneralSQS"

let sqsClient = new SQS({
    "region": "ap-south-1",
    "credentials": {
        "accessKeyId": "AKIA4LRATLIDI6VWHUON",
        "secretAccessKey": "DONT_CHECK_IN"
    }
})


async function reCreateUPIURL(cabVendorId) {
    try {
        await sendSQSMessage({
            type: Constants.SQSMsgType.ReCreateVendorUPI,
            payload: {
                cabVendorId,
                forceCreate: true
            }
        }, GeneralSQS, {})
    } catch (err) {
        console.error(`Failed for ${cabVendorId}`, err)
    }
}

async function cabVendorV2Created(cabVendorId) {
    try {
        await sendSQSMessage({
            type: Constants.SQSMsgType.CabVendorCreated,
            payload: {
                cabVendorId,
                version: "v2"
            }
        }, GeneralSQS, {})
    } catch(err) {
        console.error(`Push failed for ${cabVendorId}`)
    }
}

async function sendSQSMessage(messageJson, queueUrl, {delaySeconds = 0}){
    let response = await sqsClient.sendMessage({
        MessageBody: JSON.stringify(messageJson),
        QueueUrl: queueUrl,
        DelaySeconds: delaySeconds
    }).promise()
    //console.debug("send message", response)
    return response
}



// ddbClient.getAllCabVendors({projectionExpression:"cabVendorId"}).then(cabVendorList => {
//     cabVendorList = _.map(cabVendorList, cabVendor => cabVendor.cabVendorId)
//     require('fs').writeFileSync('./cabVendorId.json', JSON.stringify(cabVendorList))
// })

async function readFromFileAndPushToSQS() {
    let cabVendorIds = require('fs').readFileSync('./cabVendorId.json')
    cabVendorIds = JSON.parse(cabVendorIds)
    let cabVendorIdChunks = _.chunk(cabVendorIds, 50)
    for (let itr = 1; itr <= cabVendorIdChunks.length; itr++) {
        for(let chunkItr=0; chunkItr < cabVendorIdChunks[itr].length; chunkItr++) {
            await reCreateUPIURL(cabVendorIdChunks[itr][chunkItr])
        }
        console.log(`Batch ${itr+1}/${cabVendorIdChunks.length} completed`)
    }
}

//reCreateUPIURL("114205603249694360772").then(console.log, console.log)
//readFromFileAndPushToSQS()
//cabVendorV2Created("test").then(console.log)
//sendSQSMessage({type: "test", delay: 5*60}, GeneralSQS, {delaySeconds: 5*60}).then(console.log, console.log)
