const mailSender = require('../src/clients/mail-sender')
const invoiceClient = require('../src/clients/invoice-generator-client')

const ddbClient = require('./ddbClient')
const fs = require('fs');

async function sendBookingConfirmationEmail(bookingId, toEmail) {
    let ride = await ddbClient.get({
        TableName: "rides",
        Key: {
            bookingId
        }
    }).promise();
    ride = ride.Item
    let buffer = await invoiceClient.generateInvoice(ride)
    fs.writeFileSync("invoice.pdf", buffer)
    //await mailSender.sendBookingConfirmationEmail(toEmail, bookingId, ride).then(console.log, console.log)
    //await mailSender.sendRideCompletedEmail(toEmail, bookingId, ride).catch(console.log, console.log)

}

sendBookingConfirmationEmail("BI8KCE", "gogocabmail@gmail.com")
