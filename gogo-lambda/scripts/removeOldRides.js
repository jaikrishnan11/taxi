const moment = require('moment')
const dynamodb = require('aws-sdk/clients/dynamodb');
const _ = require('lodash')

const testConfig = {
    "endpoint": "http://localhost:8000",
    "region": "ap-south-1",
    "credentials": {
        "accessKeyId": "test",
        "secretAccessKey": "test"
    }
}
const prodConfig = {
    "region": "ap-south-1",
    "credentials": {
        "accessKeyId": "DONT CHECKIN PROD KEYS",
        "secretAccessKey": "DONT CHECKIN PROD KEYS"
    }
}

const ddbClient = new dynamodb.DocumentClient(Object.assign({
    "marshallOptions": {
        "convertEmptyValues": false
    }
}, testConfig));

const deleteRidesCreatedBefore = moment('2023-03-11T23:05:31.000Z')

async function deleteRides() {
    let records = await ddbClient.scan({
        TableName: "rides",
        FilterExpression: "bookingDetails.estRideEndTime < :filterCreatedDate",
        ExpressionAttributeValues: {
            ":filterCreatedDate": deleteRidesCreatedBefore.toISOString(),

        }
    }).promise()
    let items = _.sortBy(records.Items, i => i.createdDate)
    items.forEach(item => {
        console.log(item.bookingId + "  " + item.createdDate + "  " + item.bookingDetails.estRideEndTime + "  " + item.rideStatus)
    })

    _.chunk(items, 25).forEach(async (items) => {
        let response = await ddbClient.batchWrite({
            "RequestItems": {
                "rides": items.map(item => {
                    return {
                        "DeleteRequest": {
                            "Key": { bookingId: item.bookingId }
                        }
                    }
                })
            }
        }).promise()
        console.log(response)
        console.log(response.UnprocessedItems)
    })

}

async function deleteWalletLedger() {
    let records = await ddbClient.scan({
        TableName: "wallet-book-keeping",
        FilterExpression: "cabVendorId IN (:c1, :c2, :c3) AND  createdDate <= :filterCreatedDate", 
        ExpressionAttributeValues: {
            ":filterCreatedDate": deleteRidesCreatedBefore.toISOString(),
            ":c1": "100146709221843723535",
            ":c2": "106201177605008703346",
            ":c3": "114205603249694360772"
        }
    }).promise()
    let items = _.sortBy(records.Items, i => i.createdDate)
    items.forEach(item => console.log(item.createdDate))
    console.log(records.Count)
    _.chunk(items, 25).forEach(async (items) => {
        let response = await ddbClient.batchWrite({
            "RequestItems": {
                "wallet-book-keeping": items.map(item => {
                    return {
                        "DeleteRequest": {
                            "Key": { cabVendorId: item.cabVendorId, createdDate: item.createdDate }
                        }
                    }
                })
            }
        }).promise()
        console.log(response)
        console.log(response.UnprocessedItems)
    })
}

//deleteWalletLedger()