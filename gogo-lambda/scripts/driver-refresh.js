const axios = require('axios');
const fs = require("fs");
const _ = require("lodash")

const refreshDriver = async (cabVendorId, licenseNumber) => {
    let data = JSON.stringify({
        "cabVendorId": cabVendorId,
        "licenseNumber": licenseNumber,
        "fromCron": true,
        "isDashboard" : false,
        "isDeductMoney" : false,
        "isParivahanCall" : false
    });

    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: 'https://api.gogocab.in/cab-vendors/refresh-driver',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };

    try {
        const response = await axios.request(config);
        // console.log(JSON.stringify(response.data));
    } catch (error) {
        console.log(error);
    }
};

function sleepSync(milliseconds) {
    const start = Date.now();
    while (Date.now() - start < milliseconds) {
        // Busy-wait loop to block the event loop
    }
}

main = async () => {
    let cabVendors = JSON.parse(fs.readFileSync("./cab-vendors.json", "utf-8")).items
    for (let i = 0; i < cabVendors.length; i++) {
        if (cabVendors[i].version == "v2" && _.size(_.keys(cabVendors[i].drivers)) > 0) {
            let licenseNos = _.keys(cabVendors[i].drivers)
            for (let j = 0; j < licenseNos.length; j++) {
                console.log("Refreshing for", cabVendors[i].cabVendorId, licenseNos[j])
                await refreshDriver(cabVendors[i].cabVendorId, licenseNos[j])
            }
        }
    }
    console.log("Completed ...")
}

main()