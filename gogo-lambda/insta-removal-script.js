$x('/html/body/div[7]/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[3]/div[1]/div/div[i]/div/div/div/div[3]/div/div')[0].click()
$x('/html/body/div[7]/div[2]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[3]/button[1]')[0].click()


function unfollowWithDelay() {
  const delayBetweenActions = 3000; // Adjust the delay in milliseconds as needed
  
  const totalItems = 5; // Replace with the total number of items to iterate over
  
  for (let i = 1; i <= totalItems; i++) {
    setTimeout(() => {
      // Replace the 'i' in your xpaths with the current iteration value (i)
      const firstXPath = `/html/body/div[7]/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[3]/div[1]/div/div[${i}]/div/div/div/div[3]/div/div`;
      const secondXPath = '/html/body/div[7]/div[2]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[3]/button[1]';
      
      // Perform actions using the modified xpaths
      document.evaluate(firstXPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
      document.evaluate(secondXPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
    }, i * delayBetweenActions);
  }
}

// Call the function to start unfollowing with a delay
unfollowWithDelay();
