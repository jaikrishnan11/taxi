const _ = require('lodash')
const moment = require('moment')

const Config = {
    "minDistancePerDay" : 250,
    "oneWayMinDistance": 130,
    "interStateRateCardBanner": "Toll and State permit charges extra",
    "withinStateRateCardBanner": "Only Toll charges extra",
    "appDiscount": 200
}

const RATE_CARDS =  {
    "oneway": [
        {
            "id": 101,
            "type": "sedan",
            "rate": 15,
            "bata": 400
        },
        {
            "id": 104,
            "type": "etios",
            "rate": 16,
            "bata": 400
        },
        {
            "id": 102,
            "type": "xylo",
            "rate": 20,
            "bata": 400
        },
        {
            "id": 103,
            "type": "innova",
            "rate": 21,
            "bata": 500
        }
    ],
    "roundTrip": [
        {
            "id": 101,
            "type": "sedan",
            "rate": 14,
            "bata": 400
        },
        {
            "id": 104,
            "type": "etios",
            "rate": 15,
            "bata": 400
        },
        {
            "id": 102,
            "type": "xylo",
            "rate": 19,
            "bata": 500
        },
        {
            "id": 103,
            "type": "innova",
            "rate": 20,
            "bata": 500
        }
    ]
}

class RateCardService {
  constructor() {
    this.rateCardTemplate = RATE_CARDS
  }

  getOneWayRateCards(distance) {
    let rateCards = _.cloneDeep(this.rateCardTemplate)

    let remainingDistanceToCover = Math.max(Config.oneWayMinDistance - distance, 0)
    _.forEach(rateCards.oneway, tripType => {
      tripType.fare = _.round(distance * tripType.rate)
      tripType.minDailyCharge = remainingDistanceToCover * tripType.rate
      tripType.total = Math.round(tripType.fare + tripType.bata + tripType.minDailyCharge)
      tripType.fareSplitText = this.getFareSplit(tripType, distance , Config.oneWayMinDistance)
    })
    
    return {
      distance,
      rateCards
    }
  }

  getRateCards(searchCabsRequest){
    let result;
    if(searchCabsRequest.tripType == "ROUND_TRIP"){
      result= this.getRTRateCards(searchCabsRequest)
    }
    else{
    result= this.getOneWayRateCards(searchCabsRequest.distance);
    }

    //Move this logic somewhere. Chain of Responsibilty pattern ;P
    result.bannerText = searchCabsRequest.src.address.stateCode != searchCabsRequest.dst.address.stateCode ? Config.interStateRateCardBanner : Config.withinStateRateCardBanner
    this.applyAppDiscount(result, searchCabsRequest.isApp)
    return result;
  }
  
  applyAppDiscount(result, isApp){
    if(!isApp)
      return result;
    let discountApplier = (rateCards) => {
      rateCards.forEach(rateCard => {
        rateCard.appDiscount = Config.appDiscount
        if(rateCard.total)
          rateCard.discountedPrice = rateCard.total - rateCard.appDiscount
      })
    }

    discountApplier(result.rateCards.oneway)
    discountApplier(result.rateCards.roundTrip)
  }

  getRTRateCards(searchCabsRequest){
    const {pickUpDate, dropDate, distance} = searchCabsRequest
    let duration = moment.duration(moment(dropDate).diff(moment(pickUpDate)));


    // if pick and drop is strictly less than 24hrs then days = 0; meaning no extra charges
    let days = duration.days();
    let minDistanceToCover = days * Config.minDistancePerDay
    // if days ==  0 then pickup and drop within same day

    let remainingDistanceToCover = minDistanceToCover - searchCabsRequest.distance * 2  // -ve means no extra charges

    if(duration.hours() <= 24){
      if(distance * 2 < Config.minDistancePerDay){
        let result =  this.getOneWayRateCards(distance * 2);
        result.rateCards.roundTrip = result.rateCards.oneway
        return result;
      }
    }
   
    
    
    let rateCards = _.cloneDeep(this.rateCardTemplate)
    delete rateCards.oneway
    _.forEach(rateCards.roundTrip, tripType => {
      tripType.fare = _.round(distance * tripType.rate * 2)
      tripType.minDailyCharge  = remainingDistanceToCover <= 0 ? 0 : remainingDistanceToCover * tripType.rate
      tripType.total = Math.round(tripType.fare + tripType.bata + tripType.minDailyCharge),
      tripType.fareSplitText = this.getFareSplit(tripType, distance * 2, minDistanceToCover)
    })

    return {
      distance : searchCabsRequest.distance,
      rateCards
    }
  }
  
  getFareSplit(tripDetails, actualDistance, minDistance){
    let split = []
    if(tripDetails.minDailyCharge <= 0){
      split.push(['BaseFare', `${actualDistance} km X ₹${tripDetails.rate}`, `₹${tripDetails.fare}`])
    }
    else {
      split.push(['MinBaseFare', `${minDistance} km X ₹${tripDetails.rate}`,  `₹${Math.round(tripDetails.fare + tripDetails.minDailyCharge)}`])
    }
    split.push(['Driver bata', `₹${tripDetails.bata}`])
    return split;
  }
}


export default new RateCardService();