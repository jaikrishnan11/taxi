import { getAssetFromKV, mapRequestToAsset } from '@cloudflare/kv-asset-handler'
import { UAParser } from 'ua-parser-js'
/**
 * The DEBUG flag will do two things that help during development:
 * 1. we will skip caching on the edge, which makes it easier to
 *    debug.
 * 2. we will return an error message on exception in your Response rather
 *    than the default 404.html page.
 */
const DEBUG = false

addEventListener('fetch', event => {
  try {
    event.respondWith(handleEvent(event))
  } catch (e) {
    if (DEBUG) {
      return event.respondWith(
        new Response(e.message || e.toString(), {
          status: 500,
        }),
      )
    }
    event.respondWith(new Response('Internal Error', { status: 500 }))
  }
})


const corsHeaders = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "GET, HEAD, POST, OPTIONS",
  "Access-Control-Allow-Headers": "Content-Type",
  'Access-Control-Max-Age': '86400',
}

function handleOptions(request) {
  if (request.headers.get("Origin") !== null &&
      request.headers.get("Access-Control-Request-Method") !== null &&
      request.headers.get("Access-Control-Request-Headers") !== null) {
    // Handle CORS pre-flight request.
    return new Response(null, {
      headers: corsHeaders
    })
  } else {
    // Handle standard OPTIONS request.
    return new Response(null, {
      headers: {
        "Allow": "GET, HEAD, POST, OPTIONS",
      }
    })
  }
}

async function handleEvent(event) {
  let request = event.request;
  if (event.request.method === "OPTIONS") {
    return handleOptions(event.request)
  }
  else if(event.request.url.indexOf("locations/distance") != -1){
    let cache = caches.default;
    let response = await cache.match(request, {ignoreMethod: true})
    if(response)
      return response

    response =  await calculateRateCards(event.request)
    event.waitUntil(cache.put(request, response.clone()))
    return response
  }
  let options = {}

  /**
   * You can add custom logic to how we fetch your assets
   * by configuring the function `mapRequestToAsset`
   */
  options.mapRequestToAsset = handlePrefix(/^\/mobile/)

  try {
    if (DEBUG) {
      // customize caching
      options.cacheControl = {
        bypassCache: true,
      };
    }
    const page = await getAssetFromKV(event, options);

    // allow headers to be altered
    const response = new Response(page.body, page);

    response.headers.set("X-XSS-Protection", "1; mode=block");
    response.headers.set("X-Content-Type-Options", "nosniff");
    response.headers.set("X-Frame-Options", "DENY");
    response.headers.set("Referrer-Policy", "unsafe-url");
    response.headers.set("Feature-Policy", "none");
    response.headers.set("Cache-Control", "max-age=3600");

    return response;

  } catch (e) {
    // if an error is thrown try to serve the asset at 404.html
    if (!DEBUG) {
      try {
        let notFoundResponse = await getAssetFromKV(event, {
          mapRequestToAsset: req => new Request(`${new URL(req.url).origin}/404.html`, req),
        })

        return new Response(notFoundResponse.body, { ...notFoundResponse, status: 404 })
      } catch (e) {}
    }

    return new Response(e.message || e.toString(), { status: 500 })
  }
}

/**
 * Here's one example of how to modify a request to
 * remove a specific prefix, in this case `/docs` from
 * the url. This can be useful if you are deploying to a
 * route on a zone, or if you only want your static content
 * to exist at a specific path.
 */
function handlePrefix(prefix) {
  return request => {
    // compute the default (e.g. / -> index.html)
    let url = new URL(request.url)
    if (["/", "/oneway", "/roundtrip"].indexOf(url.pathname) != -1) {
      url.pathname = '/index.html'
    }
    url.pathname = addHtmlExtensionIfNeeded(url.pathname)
    url.pathname = url.pathname.replace(/^\/homepage.html/, '/index.html')

    const { browser, cpu, device } = UAParser(request.headers.get('user-agent'));
    if (!device.type && !url.pathname.startsWith("/desktop")) {
      url.pathname = "/new-desktop" + url.pathname
    } else if (!url.pathname.startsWith("msite")) {
      url.pathname = "/msite" + url.pathname
    }
    // inherit all other props from the default request
    return new Request(url.toString(), request)
  }
}

function addHtmlExtensionIfNeeded(pathname) {
  // Check if the pathname already has an extension
  if (pathname.endsWith("/") || pathname.match(/\.\w+$/)) {
    return pathname; // Return as is
  }
  // If it already has an extension, return as is
  return `${pathname}.html`;
}