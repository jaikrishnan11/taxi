function emptyPickupLocationData() {
    return {
        "heading": "Enter your pick location",
        "subHeading": "or select your current location",
        "isEmpty" : true
    }
}

function emptyDropLocationData() {
    return {
        "heading": "Enter your drop location",
        "subHeading": "or select your current location",
        "isEmpty" : true
    }
}

function pickupLocationHandler() {
    let pickupLocationData;
    
    try {
        const sessionData = sessionStorage.getItem('pickupLocationObject');
        pickupLocationData = sessionData ? JSON.parse(sessionData) : emptyPickupLocationData();
    } catch (error) {
        console.error("Error parsing JSON from session storage:", error);
        pickupLocationData = emptyPickupLocationData();
    }
    
    return { pickupLocationData };
}

function savePickupInSession(pickupLocationData) {
    sessionStorage.setItem('pickupLocationObject', JSON.stringify(pickupLocationData));
}

function dropLocationHandler() {
    let dropLocationData;
    
    try {
        const sessionData = sessionStorage.getItem('dropLocationObject');
        dropLocationData = sessionData ? JSON.parse(sessionData) : emptyDropLocationData();
    } catch (error) {
        console.error("Error parsing JSON from session storage:", error);
        dropLocationData = emptyDropLocationData();
    }
    
    return { dropLocationData };
}

function saveDropInSession(dropLocationData) {
    sessionStorage.setItem('dropLocationObject', JSON.stringify(dropLocationData));
}