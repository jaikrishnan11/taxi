

// AIzaSyA-O1_PezYhTQc0CKm3qDwc0IxsDJQ2zSI

var uberGreyStyle = [
    { "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }] },
    { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
    { "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
    { "elementType": "labels.text.stroke", "stylers": [{ "color": "#f5f5f5" }] },
    { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#bdbdbd" }] },
    { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
    { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
    { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
    { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
    { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }] },
    { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
    { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#dadada" }] },
    { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
    { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
    { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
    { "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
    { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#c9c9c9" }] },
    { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] }
];

function initMap() {
    // Create a map centered on a specific location
    var map = new google.maps.Map(document.getElementById('vehicle_selection_map'), {
        center: { lat: 10.7905, lng: 78.7047 }, // Centered at Trichy
        zoom: 6,
        disableDefaultUI: true, // Disable all default UI elements
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: uberGreyStyle
    });

    // Define the start and end points
    var start = 'Chennai, India';
    var end = 'Trichy, India';

    // Create a directions service and renderer
    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers: true, // Suppress default markers
        polylineOptions: {
            strokeColor: '#000000', // Custom color for the route
            strokeOpacity: 0.8,
            strokeWeight: 5
        }
    });

    // Request directions
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.WALKING
    }, function(response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsRenderer.setDirections(response);

            // Place custom start marker
            new google.maps.Marker({
                position: response.routes[0].legs[0].start_location,
                map: map,
                icon: 'start_map_marker.svg',
                title: "Start: Chennai"
            });

            // Place custom end marker
            new google.maps.Marker({
                position: response.routes[0].legs[0].end_location,
                map: map,
                icon: 'stop_map_marker.svg',
                title: "End: Trichy"
            });
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}