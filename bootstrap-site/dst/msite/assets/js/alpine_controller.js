function setPickupPlaceObject(locationObject) {
    savePickupInSession(locationObject)
    window.location.href = "homepage.html";
}

function setDropPlaceObject(locationObject) {
    saveDropInSession(locationObject);
    window.location.href = "homepage.html";
}

document.addEventListener('alpine:init', () => {
    console.log("Alpine got registered")
    Alpine.store('locationResults', []);
    
    let todayDate = getTodayDate();
    let customTodayDate = getCustomDateFormat(todayDate);
    storePickupDate({
        formattedDate : todayDate,
        customDate : customTodayDate
    })

    let tmrwDate = getTmrwDate();
    let customTmrwDate = getCustomDateFormat(tmrwDate);
    storeReturnDate({
        formattedDate: tmrwDate,
        customDate: customTmrwDate
    })
    // Alpine.store("pickup_date", {
    //     formattedDate : "",
    //     customDate : ""
    // })
    // Alpine.store("return_date", {
    //     formattedDate : "",
    //     customDate : ""
    // })
    Alpine.store('components', {
        pickupLocationComponent: null,
        dropLocationComponent: null,
    });
    Alpine.data('pickupLocationComponent', () => ({
        pickupLocationData: {},

        init() {
            this.loadInitialData();
            this.$store.components.pickupLocationComponent = this;
        },

        loadInitialData() {
            try {
                const sessionData = sessionStorage.getItem('pickupLocationObject');
                this.pickupLocationData = sessionData ? JSON.parse(sessionData) : this.emptyPickupLocationData();
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.pickupLocationData = this.emptyPickupLocationData();
            }
        },

        emptyPickupLocationData() {
            return {
                "heading": "Enter your pickup location",
                "subHeading": "or select your current location",
                "isEmpty": true
            };
        },

        update(locationResult, refreshRequired) {
            try {
                sessionStorage.setItem('pickupLocationObject', JSON.stringify(locationResult));
                const sessionData = sessionStorage.getItem('pickupLocationObject');
                this.pickupLocationData = sessionData ? JSON.parse(sessionData) : this.emptyPickupLocationData();
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.pickupLocationData = this.emptyPickupLocationData();
            }
            if (refreshRequired) {
                window.location.href = "homepage.html";
            }
        }
    }));

    Alpine.data('dropLocationComponent', () => ({
        dropLocationData: {},

        init() {
            this.loadInitialData();
            this.$store.components.dropLocationComponent = this;
        },

        loadInitialData() {
            try {
                const sessionData = sessionStorage.getItem('dropLocationObject');
                this.dropLocationData = sessionData ? JSON.parse(sessionData) : this.emptyDropLocationData();
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.dropLocationData = this.emptyDropLocationData();
            }
        },

        emptyDropLocationData() {
            return {
                "heading": "Enter your drop location",
                "subHeading": "or select your current location",
                "isEmpty": true
            };
        },

        update(locationResult, refreshRequired) {
            try {
                sessionStorage.setItem('dropLocationObject', JSON.stringify(locationResult));
                const sessionData = sessionStorage.getItem('dropLocationObject');
                this.dropLocationData = sessionData ? JSON.parse(sessionData) : this.emptyDropLocationData();
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.dropLocationData = this.emptyDropLocationData();
            }

            if (refreshRequired) {
                window.location.href = "homepage.html";
            }
        }
    }));
    Alpine.store("vehicleDetails", {
        sedan: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        },
        etios: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        },
        xylo: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        },
        innova: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        }
      })
      Alpine.store("reviewDetails", {
        "cabType": "",
        "vehicleText": "",
        "peopleCount": "",
        "distance": ``,
        "totalCost": ``,
        "rateCardId": "",
        "carImage": ""
    })
    Alpine.store("rateCardInfo", {
        "distance": "",
        "oneWayDistance": "",
        "rateCards": {
            "oneway": []
        },
        "latitude": "",
        "longitude": "",
        "durationText": "",
        "durationInMinutes": "",
        "estTollAmount": "",
        "coOrd": {
            "src": {
                "lat": "",
                "lon": ""
            },
            "dst": {
                "lat": "",
                "lon": ""
            }
        }
    }
    )

    Alpine.store('mapSelectedLocation', {
        "shortAddress" : "",
        "longAddress" : "",
        "pickupPlaceId": ""
    })
})
