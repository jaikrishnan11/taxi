// pickup_selection_map

var uberGreyStyle = [
    { "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }] },
    { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
    { "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
    { "elementType": "labels.text.stroke", "stylers": [{ "color": "#f5f5f5" }] },
    { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#bdbdbd" }] },
    { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
    { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
    { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
    { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
    { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }] },
    { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
    { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#dadada" }] },
    { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
    { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
    { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
    { "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
    { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#c9c9c9" }] },
    { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] }
];

let map;
let marker;
let timeoutId;

function initPickupLocationMap() {
    // Check if Geolocation is supported
    if (navigator.geolocation) {
        console.log("Geo location is supported...")
        // Get the user's current position
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("Fetching current location...")
                // Get latitude and longitude from the geolocation API
                const userLocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                // Initialize the map centered at the user's location
                map = new google.maps.Map(document.getElementById('pickup_selection_map'), {
                    center: userLocation,
                    zoom: 17,
                    disableDefaultUI: true,
                    styles: uberGreyStyle
                });

                // Set custom icon for the fixed marker
                const customIcon = {
                    url: 'location_marker.svg', // URL to your custom icon
                    scaledSize: new google.maps.Size(50, 50), // scaled size of the icon
                };

                // Set a marker at the user's location
                marker = new google.maps.Marker({
                    position: userLocation,
                    icon: customIcon,
                    map: map,
                    title: "You are here!"
                });

                map.addListener('idle', () => {
                    // getLocation()
                    // Clear previous timeout if it exists
                    if (timeoutId) clearTimeout(timeoutId);

                    // Set timeout to fetch location after 2 seconds of idle
                    timeoutId = setTimeout(getLocation, 5000);
                });
                map.addListener('center_changed', () => {
                    // setMarkerToCenter()
                    // Clear previous timeout if it exists
                    if (timeoutId) clearTimeout(timeoutId);

                    // Set timeout to fetch location after 2 seconds of idle
                    timeoutId = setTimeout(getLocation, 5000);
                });

            },
            (error) => {
                // Handle errors (if user denies permission, etc.)
                console.error("Error getting location: ", error);
                // handleLocationError(true, map.getCenter());
            }
        );
    } else {
        // Browser doesn't support Geolocation
        // handleLocationError(false, map.getCenter());
        alert("Geo location support not found, please select the location manually")
    }
}

function handleLocationError(browserHasGeolocation, pos) {
    // Handle error based on whether geolocation is supported or not
    alert(browserHasGeolocation ?
        "Error: The Geolocation service failed." :
        "Error: Your browser doesn't support geolocation.");
}

function loadMap() {
    // Initialize the map
    map = new google.maps.Map(document.getElementById('pickup_selection_map'), {
        center: { lat: 13.0843, lng: 80.2705 },
        zoom: 17,
        disableDefaultUI: true,
        styles: uberGreyStyle
    });

    // Set custom icon for the fixed marker
    const customIcon = {
        url: 'location_marker.svg', // URL to your custom icon
        scaledSize: new google.maps.Size(50, 50), // scaled size of the icon
    };

    // Create a marker object with a custom icon, initially centered
    marker = new google.maps.Marker({
        map: map,
        icon: customIcon,
        position: map.getCenter(),
        draggable: false, // Keep the marker fixed
    });
    map.addListener('center_changed', () => {
        setMarkerToCenter()
        // Clear previous timeout if it exists
        // if (timeoutId) clearTimeout(timeoutId);

        // Set timeout to fetch location after 2 seconds of idle
        // timeoutId = setTimeout(getLocation, 400);
    });
    // Add event listener to detect map idle (when the user stops moving the map)
    map.addListener('idle', () => {
        // getLocation()
        // Clear previous timeout if it exists
        if (timeoutId) clearTimeout(timeoutId);

        // Set timeout to fetch location after 2 seconds of idle
        timeoutId = setTimeout(getLocation, 2000);
    });
}


function setMarkerToCenter() {
    const center = map.getCenter();
    const lat = center.lat();
    const lng = center.lng();

    // Update the marker position
    marker.setPosition(center);
}

function getAddressInfoFromPlaceId(placeId) {
    const service = new google.maps.places.PlacesService(map);

    service.getDetails({ placeId: placeId }, (place, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            const shortAddress = place.address_components
                .filter(component => component.types.includes("political"))
                .map(component => component.short_name)
                .join(", ");
            const longAddress = place.formatted_address;
            console.log(`short address ${shortAddress}, \n Long address ${longAddress}`)
            Alpine.store('mapSelectedLocation', {
                "shortAddress" : shortAddress,
                "longAddress" : longAddress,
                "pickupPlaceId": placeId
            })
        }
    });
}

// Function to fetch and display the marker's location details
function getLocation() {
    // Get the center of the map, which is the marker's position
    const center = map.getCenter();
    const lat = center.lat();
    const lng = center.lng();

    // Update the marker position
    marker.setPosition(center);

    // Display the coordinates or other information
    // document.getElementById('info').innerText = `Latitude: ${lat}, Longitude: ${lng}`;

    // Optionally, get place details using a service like Geocoding or Places API
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ location: { lat: lat, lng: lng } }, (results, status) => {
        if (status === 'OK' && results[0]) {
            console.log(`, Place ID: ${results[0].place_id}`)
            getAddressInfoFromPlaceId(results[0].place_id)
            // document.getElementById('info').innerText += `, Place ID: ${results[0].place_id}`;
        }
    });
}