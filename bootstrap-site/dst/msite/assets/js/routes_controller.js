$(document).ready(function () {
    $("#routes_outstation_cities").hide()
    $("#routes_outstation_cities_header").click(function () {
        var img = $("#routes_outstation_cities_header_image");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/acc_up.svg") {
            img.attr("src", "assets/img/acc_down.svg");
        } else {
            img.attr("src", "assets/img/acc_up.svg");
        }
        $("#routes_outstation_cities").toggle(175);
    })

    $("#routes_popular_routes").hide()
    $("#routes_popular_routes_header").click(function () {
        var img = $("#routes_popular_routes_header_image");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/acc_up.svg") {
            img.attr("src", "assets/img/acc_down.svg");
        } else {
            img.attr("src", "assets/img/acc_up.svg");
        }
        $("#routes_popular_routes").toggle(175);
    })

    // // FAQ
    $("#faq_1_content").hide()
    $("#faq_1_header").click(function () {
        var img = $("#faq_1_header_image");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/faq_up.svg") {
            img.attr("src", "assets/img/faq_down.svg");
        } else {
            img.attr("src", "assets/img/faq_up.svg");
        }
        $("#faq_1_content").toggle(175);
    })

    $("#faq_2_content").hide()
    $("#faq_2_header").click(function () {
        var img = $("#faq_2_header_image");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/faq_up.svg") {
            img.attr("src", "assets/img/faq_down.svg");
        } else {
            img.attr("src", "assets/img/faq_up.svg");
        }
        $("#faq_2_content").toggle(175);
    })

    $("#faq_3_content").hide()
    $("#faq_3_header").click(function () {
        var img = $("#faq_3_header_image");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/faq_up.svg") {
            img.attr("src", "assets/img/faq_down.svg");
        } else {
            img.attr("src", "assets/img/faq_up.svg");
        }
        $("#faq_3_content").toggle(175);
    })

    // vehicle selection
    $("#sedan_more_details_expand").hide()
    $("#sedan_more_details").click(function () {
        var img = $("#sedan_more_details_img");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/acc_up.svg") {
            img.attr("src", "assets/img/acc_down.svg");
        } else {
            img.attr("src", "assets/img/acc_up.svg");
        }
        $("#sedan_more_details_expand").toggle(100);
    })


    $("#etios_more_details_expand").hide()
    $("#etios_more_details").click(function () {
        var img = $("#etios_more_details_img");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/acc_up.svg") {
            img.attr("src", "assets/img/acc_down.svg");
        } else {
            img.attr("src", "assets/img/acc_up.svg");
        }
        $("#etios_more_details_expand").toggle(100);
    })


    $("#xylo_more_details_expand").hide()
    $("#xylo_more_details").click(function () {
        var img = $("#xylo_more_details_img");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/acc_up.svg") {
            img.attr("src", "assets/img/acc_down.svg");
        } else {
            img.attr("src", "assets/img/acc_up.svg");
        }
        $("#xylo_more_details_expand").toggle(100);
    })


    $("#innova_more_details_expand").hide()
    $("#innova_more_details").click(function () {
        var img = $("#innova_more_details_img");
        var currentSrc = img.attr("src");

        if (currentSrc === "assets/img/acc_up.svg") {
            img.attr("src", "assets/img/acc_down.svg");
        } else {
            img.attr("src", "assets/img/acc_up.svg");
        }
        $("#innova_more_details_expand").toggle(100);
    })

})