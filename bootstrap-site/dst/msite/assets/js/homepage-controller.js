function storePickupDate(dateObject) {
    Alpine.store("pickup_date", dateObject)
}

function storeReturnDate(dateObject) {
    Alpine.store("return_date", dateObject)
}

function getCustomDateFormat(inputDate) {
    const date = new Date(inputDate);

    const formattedDate = date.toLocaleDateString('en-GB', {
        day: 'numeric',      // '9'
        month: 'short',      // 'Aug'
        year: '2-digit'      // '24'
    });

    const weekday = date.toLocaleDateString('en-US', {
        weekday: 'long'      // 'Wednesday'
    });

    return {
        formattedDate: formattedDate,
        weekday: weekday
    };
}

function getTodayDate() {
    const today = new Date();
    return formatDate(today);
}

function getTmrwDate() {
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    return formatDate(tomorrow);
}

function formatDate(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
}

function isFutureOrToday(dateString) {
    const inputDate = new Date(dateString);
    const today = new Date();

    // Remove the time part from the comparison by setting hours, minutes, seconds, and milliseconds to zero
    today.setHours(0, 0, 0, 0);
    inputDate.setHours(0, 0, 0, 0);

    // Return true if the input date is today or in the future
    return inputDate >= today;
}

function isValidDateRange(pickupDateString, returnDateString) {
    const pickupDate = new Date(pickupDateString);
    const returnDate = new Date(returnDateString);

    // Remove the time part from the comparison by setting hours, minutes, seconds, and milliseconds to zero
    pickupDate.setHours(0, 0, 0, 0);
    returnDate.setHours(0, 0, 0, 0);

    // Return true if the pickup date is earlier than or equal to the return date
    return pickupDate <= returnDate;
}

function loadButton(buttonId, textToAppearWhileLoading) {
    var $button = $(`#${buttonId}`);
    $button.html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ${textToAppearWhileLoading}..`
    );
    $button.attr('disabled', true);
}

function clearLoading(buttonId, textToRevert) {
    var $button = $(`#${buttonId}`);
    $button.html(textToRevert); // Reset the button content after the action is done
    $button.attr('disabled', false);
}

document.addEventListener("DOMContentLoaded", function () {
    console.log("dom loaded")
    // function isHomePage() {
    //     return window.location.href.includes("homepage.html")
    // }

    function isOneWay() {
        return window.location.href.includes("#oneway")
    }

    function isRoundTrip() {
        return window.location.href.includes("#roundtrip")
    }
    let savedTab = sessionStorage.getItem('tripType')
    if (savedTab == undefined && document.getElementById("oneway") != undefined) {
        document.getElementById("oneway").click()
    }
    if ((isOneWay() || savedTab == "oneway") && document.getElementById("oneway") != undefined) {
        document.getElementById("oneway").click()
    }
    if ((isRoundTrip() || savedTab == "roundtrip") && document.getElementById("roundtrip") != undefined) {
        document.getElementById("roundtrip").click()
    }
})

$(document).ready(function () {
    $("#vehicle_selection").hide()
    $("#booking_page").hide()
    $("#header_call_btn").click(function () {
        window.open("tel:+919092890928")
    })
    console.log("jquery loaded")
    // function isOneWay() {
    //     return window.location.href.includes("#oneway")
    // }

    // function isRoundTrip() {
    //     return window.location.href.includes("#roundtrip")
    // }
    // let savedTab = sessionStorage.getItem('tripType')
    // if ((isOneWay() || savedTab == "oneway") && document.getElementById("oneway") != undefined) {
    //     document.getElementById("oneway").click()
    // }
    // if ((isRoundTrip() || savedTab == "roundtrip") && document.getElementById("roundtrip") != undefined) {
    //     document.getElementById("roundtrip").click()
    // }

    function getActiveTab() {
        var activeTab = $('#trip_type_tabs .nav-link.active').attr('id');  // Get the ID of the active tab
        console.log('Active Tab ID:', activeTab);
        return activeTab
    }


    function fixReturnDate() {
        let activeTabResult = getActiveTab()
        if (activeTabResult == "oneway") {
            document.getElementById("return_date_div").style.display = "none";
            // Persisting the tab value in session handler
            sessionStorage.setItem('tripType', activeTabResult);
        }
        if (activeTabResult == "roundtrip") {
            document.getElementById("return_date_div").style.display = "block";
            // Persisting the tab value in session handler
            sessionStorage.setItem('tripType', activeTabResult);

            // Handle the today tmrw button for round trip
            // const dateInput = document.getElementById('return_date_picker');
            // const today = new Date().toISOString().split('T')[0];
            // dateInput.min = today;
        }
    }

    // Call the function whenever a tab is shown (Bootstrap 5)
    $('#trip_type_tabs a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
        fixReturnDate()
    });

    fixReturnDate()

    $("#swap_pickup_drop").click(function () {
        // console.log(Alpine.store('components').pickupLocationComponent.pickupLocationData)
        let pickupFinalResponse = {}
        let pickupLocationData = Alpine.store('components').pickupLocationComponent.pickupLocationData
        if (pickupLocationData?.isEmpty) {
            pickupFinalResponse = Alpine.store('components').pickupLocationComponent.emptyPickupLocationData()
        } else {
            pickupFinalResponse = pickupLocationData
        }

        let dropFinalResponse = {}
        let dropLocationData = Alpine.store('components').dropLocationComponent.dropLocationData
        if (dropLocationData?.isEmpty) {
            dropFinalResponse = Alpine.store('components').dropLocationComponent.emptyDropLocationData()
        } else {
            dropFinalResponse = dropLocationData
        }

        console.log(pickupFinalResponse)
        console.log(dropFinalResponse)
        if (pickupLocationData?.isEmpty && dropLocationData?.isEmpty) {
            return
        }

        if (!(pickupLocationData?.isEmpty) && !(dropLocationData?.isEmpty)) {
            Alpine.store('components').dropLocationComponent.update(pickupFinalResponse, false)
            Alpine.store('components').pickupLocationComponent.update(dropFinalResponse, false)
            return
        }

        if (pickupLocationData?.isEmpty) {
            Alpine.store('components').pickupLocationComponent.update(dropFinalResponse, false)
            Alpine.store('components').dropLocationComponent.update(Alpine.store('components').dropLocationComponent.emptyDropLocationData(), false)
        } else {
            Alpine.store('components').dropLocationComponent.update(pickupFinalResponse, false)
            Alpine.store('components').pickupLocationComponent.update(Alpine.store('components').pickupLocationComponent.emptyPickupLocationData(), false)
        }
    })

    $('#pickup_date_picker').on('change', function () {
        let formattedDate = $('#pickup_date_picker').val()
        if (formattedDate == "") {
            return
        }
        //    console.log()
        storePickupDate({
            formattedDate: formattedDate,
            customDate: getCustomDateFormat(formattedDate)
        })
        //    console.log(getCustomDateFormat(formattedDate))
    });

    $('#return_date_picker').on('change', function () {
        let formattedDate = $('#return_date_picker').val()
        if (formattedDate == "") {
            return
        }
        //    console.log()
        storeReturnDate({
            formattedDate: formattedDate,
            customDate: getCustomDateFormat(formattedDate)
        })
        //    console.log(getCustomDateFormat(formattedDate))
    });

    $("#pickup_today_btn").click(function () {
        let currentDate = getTodayDate()
        $('#pickup_date_picker').val(currentDate)
        storePickupDate({
            formattedDate: currentDate,
            customDate: getCustomDateFormat(currentDate)
        })
    })

    $("#pickup_tmrw_btn").click(function () {
        let tmrwDate = getTmrwDate()
        $('#pickup_date_picker').val(tmrwDate)
        storePickupDate({
            formattedDate: tmrwDate,
            customDate: getCustomDateFormat(tmrwDate)
        })
    })

    $("#return_today_btn").click(function () {
        let currentDate = getTodayDate()
        $('#return_date_picker').val(currentDate)
        storeReturnDate({
            formattedDate: currentDate,
            customDate: getCustomDateFormat(currentDate)
        })
    })

    $("#return_tmrw_btn").click(function () {
        let tmrwDate = getTmrwDate()
        $('#return_date_picker').val(tmrwDate)
        storeReturnDate({
            formattedDate: tmrwDate,
            customDate: getCustomDateFormat(tmrwDate)
        })
    })

    $("#search_btn").click(function () {
        // alert("Button clicked")
        let pickupLocation = sessionStorage.getItem('pickupLocationObject') ? JSON.parse(sessionStorage.getItem('pickupLocationObject')) : sessionStorage.getItem('pickupLocationObject');
        let dropLocation = sessionStorage.getItem('dropLocationObject') ? JSON.parse(sessionStorage.getItem('dropLocationObject')) : sessionStorage.getItem('dropLocationObject');
        if (pickupLocation?.isEmpty) {
            alert("Please fill the pickup location !!!");
            return
        }

        if (dropLocation?.isEmpty) {
            alert("Please fill the drop location !!!");
            return
        }

        if (pickupLocation.placeId == dropLocation.placeId) {
            alert("Pickup and drop locations are same, Kindly select different pickup / drop location !!!");
            return
        }

        let tripType = sessionStorage.getItem('tripType');
        let pickupDate = Alpine.store('pickup_date');
        let returnDate = Alpine.store('return_date');

        if (pickupDate != undefined && !isFutureOrToday(pickupDate.formattedDate)) {
            alert("Please select the correct pickup date")
            return
        }

        if (tripType == "oneway") {
            let cpPayload = {
                tripType: "ONE_WAY",
                pickupPlaceId: pickupLocation.placeId,
                dropPlaceId: dropLocation.placeId,
                pickupDate: pickupDate.formattedDate
            }
            loadButton("search_btn", "Searching");
            window.gogoService.getRateCards(cpPayload).then(function (data) {
                populateRateCards(data);
                clearLoading("search_btn", "Search");

                Alpine.store("selectedTripType", tripType)
                Alpine.store("tripTypeString", "One-way trip ");
                Alpine.store("reviewBookingHeading", `${pickupLocation?.heading} - ${dropLocation?.heading} | One-way trip`)
                Alpine.store("reviewBookingPickup", `${pickupLocation?.heading}`)
                Alpine.store("reviewBookingDrop", `${dropLocation?.heading}`)
                Alpine.store("pickupPlaceId", pickupLocation.placeId)
                Alpine.store("dropPlaceId", dropLocation.placeId)
                Alpine.store("selectedPickupObject", pickupLocation)
                Alpine.store("selectedDropObject", dropLocation)
                Alpine.store("selectedPickupDate", pickupDate.formattedDate)

                $("#booking_page").hide()
                $("#vehicle_selection").show(200);
                $('html, body').animate({
                    scrollTop: $("#vehicle_selection").offset().top
                }, 400);
                console.log(data);
            }).catch((err) => {
                if (err?.errorCode == "SERVICE_UNAVAILABLE") {
                    clearLoading("search_btn", "Search");
                    $("#location-blocker-modal").modal('show')
                    if (err?.message) {
                        $("#location-blocker-display-text").val(err?.message)
                    } else {
                        $("#location-blocker-display-text").val("This service is currently unavailable in your location.")
                    }
                }
                console.log(err)
                return
            })
        }

        if (tripType == "roundtrip") {
            if (returnDate != undefined && (!isFutureOrToday(returnDate.formattedDate) || !isValidDateRange(pickupDate.formattedDate, returnDate.formattedDate))) {
                alert("Please select the correct return date")
                return
            }
            let cpPayload = {
                tripType: "ROUND_TRIP",
                pickupPlaceId: pickupLocation.placeId,
                dropPlaceId: dropLocation.placeId,
                pickupDate: pickupDate.formattedDate,
                returnDate: returnDate.formattedDate,
                keepCab: true
            }
            loadButton("search_btn", "Searching");
            window.gogoService.getRateCards(cpPayload).then(function (data) {
                populateRateCards(data);
                clearLoading("search_btn", "Search");

                Alpine.store("selectedTripType", tripType);
                Alpine.store("tripTypeString", "Round trip ");
                Alpine.store("reviewBookingHeading", `${pickupLocation?.heading} - ${dropLocation?.heading} | Round trip`)
                Alpine.store("reviewBookingPickup", `${pickupLocation?.heading}`)
                Alpine.store("reviewBookingDrop", `${dropLocation?.heading}`)
                Alpine.store("pickupPlaceId", pickupLocation.placeId)
                Alpine.store("dropPlaceId", dropLocation.placeId)
                Alpine.store("selectedPickupObject", pickupLocation)
                Alpine.store("selectedDropObject", dropLocation)
                Alpine.store("selectedPickupDate", pickupDate.formattedDate)
                Alpine.store("selectedDropDate", returnDate.formattedDate)

                $("#booking_page").hide()
                $("#vehicle_selection").show(200);
                $('html, body').animate({
                    scrollTop: $("#vehicle_selection").offset().top
                }, 400);
            }).catch((err) => {
                if (err?.errorCode == "SERVICE_UNAVAILABLE") {
                    clearLoading("search_btn", "Search");
                    $("#location-blocker-modal").modal('show')
                    if (err?.message) {
                        $("#location-blocker-display-text").val(err?.message)
                    } else {
                        $("#location-blocker-display-text").val("This service is currently unavailable in your location.")
                    }
                }
                console.log(err)
                return
            })

        }


    })


    // vehicle selection 

    $("#sedan_select").click(function () {
        let distance = Alpine.store("rateCardInfo")?.["distance"]
        const totalCost = Alpine.store("vehicleDetails")?.["sedan"]?.["total"]
        const rateCardId = Alpine.store("vehicleDetails")?.["sedan"]?.["rateCardId"]
        const imageName = "assets/img/car_sedan.svg"
        let reviewDetails = {
            "cabType": "sedan",
            "vehicleText": "Swift Dzire / Ford Aspire / Tata zest or Similar",
            "peopleCount": "4 people + 1 driver",
            "distance": `${distance} km`,
            "totalCost": `${totalCost}`,
            "rateCardId": rateCardId,
            "carImage": imageName,
            "priceDetails": Alpine.store("vehicleDetails")?.["sedan"]
        }
        Alpine.store("reviewDetails", reviewDetails)
        $("#rb_car_image").attr("src", imageName);
        $("#booking_page").show();
        $('html, body').animate({
            scrollTop: $("#booking_page").offset().top
        }, 400);
        sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
    });

    $("#etios_select").click(function () {
        let distance = Alpine.store("rateCardInfo")?.["distance"]
        const totalCost = Alpine.store("vehicleDetails")?.["etios"]?.["total"]
        const rateCardId = Alpine.store("vehicleDetails")?.["etios"]?.["rateCardId"]
        const imageName = "assets/img/car_etios.svg"
        let reviewDetails = {
            "cabType": "etios",
            "vehicleText": "Etios (sedan type)",
            "peopleCount": "4 people + 1 driver",
            "distance": `${distance} km`,
            "totalCost": `${totalCost}`,
            "rateCardId": rateCardId,
            "carImage": imageName,
            "priceDetails": Alpine.store("vehicleDetails")?.["etios"]
        }
        Alpine.store("reviewDetails", reviewDetails)
        $("#rb_car_image").attr("src", imageName);
        $("#booking_page").show();
        $('html, body').animate({
            scrollTop: $("#booking_page").offset().top
        }, 400);
        sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
    });

    $("#any_suv_select").click(function () {
        let distance = Alpine.store("rateCardInfo")?.["distance"]
        const totalCost = Alpine.store("vehicleDetails")?.["xylo"]?.["total"]
        const rateCardId = Alpine.store("vehicleDetails")?.["xylo"]?.["rateCardId"]
        const imageName = "assets/img/car_suv.svg"
        let reviewDetails = {
            "cabType": "xylo",
            "vehicleText": "Ertiga / Enjoy / Xylo / Tavera / Marazzo / Renault Lodgy / Kia Carens or Similar",
            "peopleCount": "7 people + 1 driver",
            "distance": `${distance} km`,
            "totalCost": `${totalCost}`,
            "rateCardId": rateCardId,
            "carImage": imageName,
            "priceDetails": Alpine.store("vehicleDetails")?.["xylo"]
        }
        Alpine.store("reviewDetails", reviewDetails)
        $("#rb_car_image").attr("src", imageName);
        $("#booking_page").show();
        $('html, body').animate({
            scrollTop: $("#booking_page").offset().top
        }, 400);
        sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
    });

    $("#innova_select").click(function () {
        let distance = Alpine.store("rateCardInfo")?.["distance"]
        const totalCost = Alpine.store("vehicleDetails")?.["innova"]?.["total"]
        const rateCardId = Alpine.store("vehicleDetails")?.["innova"]?.["rateCardId"]
        const imageName = "assets/img/car_innova.svg"
        let reviewDetails = {
            "cabType": "innova",
            "vehicleText": "Innova",
            "peopleCount": "7 people + 1 driver",
            "distance": `${distance} km`,
            "totalCost": `${totalCost}`,
            "rateCardId": rateCardId,
            "carImage": imageName,
            "priceDetails": Alpine.store("vehicleDetails")?.["innova"]
        }
        Alpine.store("reviewDetails", reviewDetails);
        $("#rb_car_image").attr("src", imageName);
        $("#booking_page").show();
        $('html, body').animate({
            scrollTop: $("#booking_page").offset().top
        }, 400);
        sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
    });


    $('#only_whatsapp').change(function () {
        if ($(this).is(':checked')) {
            // Code to execute when the checkbox is checked
            console.log('WhatsApp checkbox is checked');
            document.getElementById("phone_number_div").style.display = "none";
            document.getElementById("same_whatsapp_phone_number_div").style.display = "none";
        } else {
            // Code to execute when the checkbox is unchecked
            console.log('WhatsApp checkbox is unchecked');
            document.getElementById("phone_number_div").style.display = "block";
            document.getElementById("same_whatsapp_phone_number_div").style.display = "block";
        }
    });


    $('#same_no_whatsapp_checkbox').change(function () {
        if ($(this).is(':checked')) {
            // Code to execute when the checkbox is checked
            document.getElementById("whatsapp_number_div").style.display = "block";
        } else {
            // Code to execute when the checkbox is unchecked
            document.getElementById("whatsapp_number_div").style.display = "none";
        }
    });

    $('#confirm_booking_phone_no').on('input', function () {
        // Get the value of the text box
        let value = $(this).val();

        // Remove any non-numeric characters
        value = value.replace(/\D/g, '');

        // Limit the value to 10 digits
        if (value.length > 10) {
            value = value.substring(0, 10);
        }

        // Set the modified value back to the text box
        $(this).val(value);
    });

    // confirm booking validation and calls
    $("#confirm_booking").click(function () {
        // alert("Confirm button clicked")  

        // name validation
        let customerName = $("#confirm_booking_name").val()
        if (isEmpty(customerName)) {
            alert("Please enter your full name");
            return
        }
        if (customerName.length < 3 || customerName.length > 25) {
            alert("Please enter your name length greater than 3 and less than 25 characters");
            return
        }

        //confirm_booking_mail
        // mail validation
        let customerMail = $("#confirm_booking_mail").val()
        // If the mail string is not empty, because mail is not mandatory
        if (!isEmpty(customerMail)) {
            let mailValidationResult = validateEmail(customerMail)
            if (!isEmpty(mailValidationResult)) {
                alert(mailValidationResult)
                return
            }
        }

        // if ($('#only-whatsapp').is(":checked")) {

        // } else {

        // }

        // should skip if need only whatsapp number
        //confirm_booking_phone_no
        let customerPrimaryPhoneNumber = $("#confirm_booking_phone_no").val()
        if (isEmpty(customerPrimaryPhoneNumber)) {
            alert("Please enter your phone number !!!")
            return
        }

        if (customerPrimaryPhoneNumber.length != 10) {
            alert("Please enter only 10 digits, International customers contact 90928 90928 whatsapp !!!")
            return
        }

        let bookingTime = $("#confirm_booking_pickup_time").val()
        if (isEmpty(bookingTime)) {
            alert("Please select your pickup time !!!")
            return
        }

        let returnBookingTime = ""

        // API call begins
        // disable the button once the call is prepared
        let tripType = "ONE_WAY"
        if (Alpine.store("selectedTripType") == "roundtrip") {
            tripType = "ROUND_TRIP"
            let parsedReturnBookingTime = $("#confirm_booking_return_time").val()
            if (isEmpty(parsedReturnBookingTime)) {
                alert("Please select your return time !!!")
                return
            }
            returnBookingTime = parsedReturnBookingTime
        }
        let cabType = Alpine.store("reviewDetails")?.["cabType"]
        let rateCardId = Alpine.store("reviewDetails")?.["rateCardId"]
        let selectedPickupDate = Alpine.store("selectedPickupDate")
        let selectedDropDate = Alpine.store("selectedDropDate");
        let customerDetails = {
            "phoneNumber": customerPrimaryPhoneNumber,
            "name": customerName,
            "email": customerMail
        }
        loadButton("confirm_booking", "Loading")
        $("#confirm_booking").attr("disabled", true)
        new GogoService().bookCabV2({
            "bookingDetails": {
                "tripType": tripType,
                "pickupPlaceId": Alpine.store("pickupPlaceId"),
                "dropPlaceId": Alpine.store("dropPlaceId"),
                "pickUpDate": new Date(selectedPickupDate + " " + bookingTime).toISOString(),
                "dropTime": tripType == "ROUND_TRIP" ? new Date(selectedDropDate + " " + returnBookingTime).toISOString() : "",
                "dropDate": tripType == "ROUND_TRIP" ? new Date(selectedDropDate + " " + returnBookingTime).toISOString() : new Date(selectedPickupDate + " " + bookingTime).toISOString(),
                "keepCab": "true",
                "cabType": cabType
            },
            "customerDetails": customerDetails,
            "rateCardId": rateCardId
        }).then(function (data) {
            // $("#details_booking_id").text(`Booking Id : ${data.bookingId}`); $("#details_booked_on").text(`Booked on ${moment(data.bookingDetails["tripStartTime"]).format('MMMM Do YYYY, h:mm A')}`);
            // $("#booking_dialog").modal('show');
            console.log(data);
            clearLoading("confirm_booking", "Confirm Booking")
            data["bookingDetails"]["pickup"] = Alpine.store("selectedPickupObject")
            data["bookingDetails"]["drop"] = Alpine.store("selectedDropObject")
            data["customerDetails"] = customerDetails
            sessionStorage.setItem("final_booking_response", JSON.stringify(data))
            window.location.href = "booking_confirmed.html"
        }).catch((err) => {
            clearLoading("confirm_booking", "Confirm Booking")
            alert("Error occurred while booking the cab " + JSON.stringify(err));
            console.log(err);
        })
        //confirm_booking_whatsapp_phone_no
        //confirm_booking_pickup_time
    });

    $("#link_tariff_plans").click(function () {
        $('html, body').animate({
            scrollTop: $("#fleets_and_tarrifs").offset().top
        }, 200);
    })

    $("#link_popular_routes").click(function () {
        $('html, body').animate({
            scrollTop: $("#fav_routes").offset().top
        }, 200);
    })

    $("#link_stats").click(function () {
        $('html, body').animate({
            scrollTop: $("#gogo_stats").offset().top
        }, 200);
    })

    $("#link_faq").click(function () {
        $('html, body').animate({
            scrollTop: $("#faq").offset().top
        }, 200);
    })
    /////////////////
    $("#contact_us").click(function () {
        window.location.href = "contact-us"
    })

    $("#privacy_policy").click(function () {
        window.location.href = "privacy-policy"
    })

    $("#terms_and_conditions").click(function () {
        window.location.href = "terms-and-conditions"
    })

    $("#cancellation_policies").click(function () {
        window.location.href = "cancellation-and-refund-policy"
    })

});

