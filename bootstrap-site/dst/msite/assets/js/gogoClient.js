class GogoService {
    constructor(options ={}){
        this.options = Object.assign({}, options);
        this.baseUrl =  "https://api.gogocab.in"
        // this.baseUrl =  "http://localhost:8787"
    }

    async postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          
          headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'error', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

   async getRateCards(body){
        //return this.postData(`${this.baseUrl}/locations/distance`, body)
        return await this.newGetRateCards(body);
    }

    async newGetRateCards(params){
        let response =  await fetch(`${this.baseUrl}/rateCards?` + new URLSearchParams({
            tripType: params.tripType,
            pickupPlaceId: params.pickupPlaceId,
            dropPlaceId: params.dropPlaceId,
            pickUpDate: params.pickupDate != undefined ? new Date(params.pickupDate).toISOString() : new Date().toString(),
            dropDate: params.returnDate ? new Date(params.returnDate).toISOString() : "",
            keepCab: params.keepCab,
            latitude: params.latitude,
            longitude: params.longitude
        }))
        if (response.status == 400) {
            throw await response.json()
        }
        if(response.status>400)
            throw response
        return response.json()
    }

    async bookCab(body){
        return this.postData(`${this.baseUrl}/publish`, body)
    }
    
    async bookCabV2(body){
        console.log(body)
       // return this.postData("http://127.0.0.1:8787/rides", body)
        return this.postData(`${this.baseUrl}/rides`, body)
    }

    async reverseLookUp(coOrd){
        let response = await fetch(`${this.baseUrl}/locations/reverseLookUp`,{params: coOrd} )
        if(response.status < 400)
            return response.data
        throw response
    }

    async autoComplete(location){
        let response = await fetch(`${this.baseUrl}/locations/autocomplete`, {params: {loc: location}})
        if(response.status > 400)
            throw response
        return response.data
    }

}
window.gogoService = new GogoService()