function populateRateCards(rateCardInfo) {
    let tripType = ""
    let rateCards = []
    if(rateCardInfo?.rateCards.hasOwnProperty('oneway')) {
        tripType = "ONE_WAY"
        rateCards = rateCardInfo?.rateCards?.oneway
    }
    if(rateCardInfo?.rateCards.hasOwnProperty('roundTrip')) {
        tripType = "ROUND_TRIP"
        rateCards = rateCardInfo?.rateCards?.roundTrip
    }
    let rateCardVehicleBased = {}
    for (let i = 0; i < rateCards.length; i++) {
        let permitValueString = ""
        let permitValue = rateCardInfo?.permitAmount?.[rateCards[i]?.type]
        if (permitValue == undefined || permitValue == "") {
            permitValueString = `Not Applicable`
        } else {
            permitValueString = `₹ ${permitValue}`
        }
        rateCardVehicleBased[rateCards[i]?.type] = {
            "baseKm": `${rateCardInfo?.distance} km`,
            "baseFarePerKm": `₹ ${rateCards[i]?.rate}`,
            "driverAllowance": `₹ ${rateCards[i]?.priceSplit?.DriverBata?.value}`,
            "tollAmount": `₹ ${rateCardInfo?.estTollAmount}`,
            "permit": permitValueString,
            "total": `₹ ${rateCards[i].total}`,
            "rateCardId": rateCards[i]?.id
        }
        if (rateCardInfo?.estTollAmount > 0)  {
            rateCardVehicleBased[rateCards[i]?.type]["tollText"] = `+ Toll charge ₹ ${rateCardInfo?.estTollAmount} approx`
        } else {
            rateCardVehicleBased[rateCards[i]?.type]["tollText"] = `No Toll charge in this route`
        }
        if (permitValue == undefined || permitValue == "") {
            rateCardVehicleBased[rateCards[i]?.type]["permitText"] = `No permit in this route`
        } else {
            rateCardVehicleBased[rateCards[i]?.type]["permitText"] = `+ State permit ₹ ${permitValue}`
        }
        if(rateCards[i]?.priceSplit?.DriverBata?.calculation) {
            rateCardVehicleBased[rateCards[i]?.type]["driverAllowance"] = `₹ ${rateCards[i]?.priceSplit?.DriverBata?.value} (${rateCards[i]?.priceSplit?.DriverBata?.calculation})`
        }
        if(rateCards[i]?.priceSplit?.BaseKm?.value) {
            rateCardVehicleBased[rateCards[i]?.type]["baseKm"] = `${rateCards[i]?.priceSplit?.BaseKm?.value} km (${rateCards[i]?.priceSplit?.BaseKm?.calculation})`
        }
        if(rateCards[i]?.priceSplit?.MinBaseFare?.value) {
            rateCardVehicleBased[rateCards[i]?.type]["totalBaseFare"] = `${rateCards[i]?.priceSplit?.MinBaseFare?.value} (${rateCards[i]?.priceSplit?.MinBaseFare?.calculation})`
        }
        if(rateCards[i]?.priceSplit?.BaseFare?.value) {
            rateCardVehicleBased[rateCards[i]?.type]["totalBaseFare"] = `${rateCards[i]?.priceSplit?.BaseFare?.value} (${rateCards[i]?.priceSplit?.BaseFare?.calculation})`
        }

    }
    console.log(rateCardVehicleBased)
    Alpine.store("vehicleDetails", rateCardVehicleBased)
    Alpine.store("rateCardInfo", rateCardInfo)
}