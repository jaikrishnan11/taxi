// {
//     "bookingId": "CSTHNA",
//     "bookingDetails": {
//       "pickup": {
//         "heading": "Chennai International Airport (MAA)",
//         "subHeading": "Airport Road, Meenambakkam, Chennai, Tamil Nadu, India",
//         "placeId": "ChIJl2OoXR9eUjoRR27ibiEvCSE"
//       },
//       "drop": {
//         "heading": "Bangalore Cantonment Railway Station",
//         "subHeading": "Cantonment Railway Quarters, Shivaji Nagar, Bengaluru, Karnataka, India",
//         "placeId": "ChIJk9jf61wWrjsRuP7dRNhe4fY"
//       },
//       "intermediates": [],
//       "tripStartTime": "2024-10-02T05:41:00.000Z",
//       "returnStartTime": "",
//       "keepCab": "true",
//       "tripType": "ONE_WAY",
//       "cabType": "sedan",
//       "estDistance": 339.12,
//       "estTripDuration": 412.82,
//       "estRideEndTime": "2024-10-02T12:33:49.200Z"
//     }
//   }

function addOffsetWithDateAndTime(date, offsetHours, offsetMinutes) {
    // Convert offset to milliseconds
    const offsetMilliseconds = (offsetHours * 60 + offsetMinutes) * 60 * 1000;
    
    // Add the offset to the date in milliseconds
    const adjustedDate = new Date(date.getTime() + offsetMilliseconds);
    
    // Extract the date parts
    const day = adjustedDate.getUTCDate();
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const month = monthNames[adjustedDate.getUTCMonth()];
    const year = adjustedDate.getUTCFullYear();
  
    // Extract time and convert to 12-hour AM/PM format
    const hours24 = adjustedDate.getUTCHours();
    const minutes = adjustedDate.getUTCMinutes();
    
    let hours12 = hours24 % 12 || 12; // Convert to 12-hour format
    const period = hours24 >= 12 ? "PM" : "AM";
  
    // Ensure minutes are always two digits
    const formattedMinutes = minutes.toString().padStart(2, "0");
  
    // Add ordinal suffix to the day
    const daySuffix = (day) => {
      if (day > 3 && day < 21) return 'th'; // Covers 11th - 20th
      switch (day % 10) {
        case 1: return 'st';
        case 2: return 'nd';
        case 3: return 'rd';
        default: return 'th';
      }
    };
  
    // Return formatted date and time
    return `${month} ${day}${daySuffix(day)}, ${year} ${hours12}:${formattedMinutes} ${period}`;
  }

function isBookingConfirmedPage() {
    return window.location.href.includes("booking_confirmed.html")
}

$(document).ready(function () {
    $("#call_for_assistance").click(function () {
        window.open("tel:+916385588806")
    })
    if(!isBookingConfirmedPage()) {
        return
    }
    let bookingResponse = JSON.parse(sessionStorage.getItem("final_booking_response"));
    if(bookingResponse == undefined) {
        alert("Something went wrong, Please contact gogo cabs")
        return
    }
    Alpine.store("bc_pickup_heading", bookingResponse?.bookingDetails?.pickup?.heading)
    Alpine.store("bc_drop_heading", bookingResponse?.bookingDetails?.drop?.heading)
    Alpine.store("bc_pickup_sub_heading", bookingResponse?.bookingDetails?.pickup?.subHeading)
    Alpine.store("bc_drop_sub_heading", bookingResponse?.bookingDetails?.drop?.heading)
    Alpine.store("bc_booking_id", bookingResponse?.bookingId)
    Alpine.store("bc_customer_details_name", bookingResponse?.customerDetails?.name)
    Alpine.store("bc_customer_details_mail", bookingResponse?.customerDetails?.email)
    Alpine.store("bc_customer_details_phone_number", bookingResponse?.customerDetails?.phoneNumber)

    Alpine.store("bc_trip_start_time", addOffsetWithDateAndTime(new Date(bookingResponse?.bookingDetails?.tripStartTime), 5, 30))


    let vehicleDetails = JSON.parse(sessionStorage.getItem("vehicleDetails"));
    if(vehicleDetails == undefined) {
        alert("Something went wrong while updating vehicle details")
        return
    }

    Alpine.store("bc_vehicle_name", vehicleDetails?.vehicleText)
    Alpine.store("bc_vehicle_people_count", vehicleDetails?.peopleCount)
    Alpine.store("bc_vehicle_distance", vehicleDetails?.distance)
    Alpine.store("bc_total", vehicleDetails?.totalCost)
    $("#bc_car_image").attr("src", vehicleDetails?.carImage);
    Alpine.store("bc_toll_text", vehicleDetails?.priceDetails?.tollText);
    Alpine.store("bc_permit_text", vehicleDetails?.priceDetails?.permitText);

})