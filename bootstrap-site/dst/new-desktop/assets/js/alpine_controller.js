function storePickupDate(dateObject) {
    Alpine.store("pickup_date", dateObject)
}

function storeReturnDate(dateObject) {
    Alpine.store("return_date", dateObject)
}

function getCustomDateFormat(inputDate) {
    const date = new Date(inputDate);

    const formattedDate = date.toLocaleDateString('en-GB', {
        day: 'numeric',      // '9'
        month: 'short',      // 'Aug'
        year: '2-digit'      // '24'
    });

    const weekday = date.toLocaleDateString('en-US', {
        weekday: 'long'      // 'Wednesday'
    });

    return {
        formattedDate: formattedDate,
        weekday: weekday
    };
}

function getTodayDate() {
    const today = new Date();
    return formatDate(today);
}

function getTmrwDate() {
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    return formatDate(tomorrow);
}

function formatDate(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
}

function isFutureOrToday(dateString) {
    const inputDate = new Date(dateString);
    const today = new Date();

    // Remove the time part from the comparison by setting hours, minutes, seconds, and milliseconds to zero
    today.setHours(0, 0, 0, 0);
    inputDate.setHours(0, 0, 0, 0);

    // Return true if the input date is today or in the future
    return inputDate >= today;
}

function isValidDateRange(pickupDateString, returnDateString) {
    const pickupDate = new Date(pickupDateString);
    const returnDate = new Date(returnDateString);

    // Remove the time part from the comparison by setting hours, minutes, seconds, and milliseconds to zero
    pickupDate.setHours(0, 0, 0, 0);
    returnDate.setHours(0, 0, 0, 0);

    // Return true if the pickup date is earlier than or equal to the return date
    return pickupDate <= returnDate;
}

function setPickupPlaceObject(locationObject) {
    savePickupInSession(locationObject)
}

function setDropPlaceObject(locationObject) {
    saveDropInSession(locationObject);
}

document.addEventListener('alpine:init', () => {
    console.log("Alpine got registered")
    Alpine.store('locationResults', []);
    
    let todayDate = getTodayDate();
    let customTodayDate = getCustomDateFormat(todayDate);
    storePickupDate({
        formattedDate : todayDate,
        customDate : customTodayDate
    })

    let tmrwDate = getTmrwDate();
    let customTmrwDate = getCustomDateFormat(tmrwDate);
    storeReturnDate({
        formattedDate: tmrwDate,
        customDate: customTmrwDate
    })
    // Alpine.store("pickup_date", {
    //     formattedDate : "",
    //     customDate : ""
    // })
    // Alpine.store("return_date", {
    //     formattedDate : "",
    //     customDate : ""
    // })
    Alpine.store('components', {
        pickupLocationComponent: null,
        dropLocationComponent: null,
    });
    Alpine.data('pickupLocationComponent', () => ({
        pickupLocationData: {},

        init() {
            this.loadInitialData();
            this.$store.components.pickupLocationComponent = this;
        },

        loadInitialData() {
            try {
                const sessionData = sessionStorage.getItem('pickupLocationObject');
                this.pickupLocationData = sessionData ? JSON.parse(sessionData) : this.emptyPickupLocationData();
                Alpine.store("pickupLocationData", this.pickupLocationData)
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.pickupLocationData = this.emptyPickupLocationData();
                Alpine.store("pickupLocationData", this.pickupLocationData)
            }
        },

        emptyPickupLocationData() {
            return {
                "heading": "Enter your pickup location",
                "subHeading": "or select your current location",
                "isEmpty": true
            };
        },

        update(locationResult) {
            try {
                sessionStorage.setItem('pickupLocationObject', JSON.stringify(locationResult));
                const sessionData = sessionStorage.getItem('pickupLocationObject');
                this.pickupLocationData = sessionData ? JSON.parse(sessionData) : this.emptyPickupLocationData();
                Alpine.store("pickupLocationData", this.pickupLocationData);
                document.getElementById("pickup_select_dropdown").style.display = "none";
                Alpine.store('locationResults', []);
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.pickupLocationData = this.emptyPickupLocationData();
                Alpine.store("pickupLocationData", this.pickupLocationData)
                Alpine.store('locationResults', []);
            }
        }
    }));

    Alpine.data('dropLocationComponent', () => ({
        dropLocationData: {},

        init() {
            this.loadInitialData();
            this.$store.components.dropLocationComponent = this;
        },

        loadInitialData() {
            try {
                const sessionData = sessionStorage.getItem('dropLocationObject');
                this.dropLocationData = sessionData ? JSON.parse(sessionData) : this.emptyDropLocationData();
                Alpine.store("dropLocationData", this.dropLocationData)
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.dropLocationData = this.emptyDropLocationData();
                Alpine.store("dropLocationData", this.dropLocationData)
            }
        },

        emptyDropLocationData() {
            return {
                "heading": "Enter your drop location",
                "subHeading": "or select your current location",
                "isEmpty": true
            };
        },

        update(locationResult) {
            try {
                sessionStorage.setItem('dropLocationObject', JSON.stringify(locationResult));
                const sessionData = sessionStorage.getItem('dropLocationObject');
                this.dropLocationData = sessionData ? JSON.parse(sessionData) : this.emptyDropLocationData();
                Alpine.store("dropLocationData", this.dropLocationData);
                document.getElementById("drop_select_dropdown").style.display = "none";
                Alpine.store('locationResults', []);
            } catch (error) {
                console.error("Error parsing JSON from session storage:", error);
                this.dropLocationData = this.emptyDropLocationData();
                Alpine.store("dropLocationData", this.dropLocationData);
            }
        }
    }));
    Alpine.store("vehicleDetails", {
        sedan: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        },
        etios: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        },
        xylo: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        },
        innova: {
          baseKm: '',
          baseFarePerKm: '',
          totalBaseFare: '',
          driverAllowance: '',
          tollAmount: '',
          permit: '',
          total: ''
        }
      })
      Alpine.store("reviewDetails", {
        "cabType": "",
        "vehicleText": "",
        "peopleCount": "",
        "distance": ``,
        "totalCost": ``,
        "rateCardId": "",
        "carImage": ""
    })
    Alpine.store("rateCardInfo", {
        "distance": "",
        "oneWayDistance": "",
        "rateCards": {
            "oneway": []
        },
        "latitude": "",
        "longitude": "",
        "durationText": "",
        "durationInMinutes": "",
        "estTollAmount": "",
        "coOrd": {
            "src": {
                "lat": "",
                "lon": ""
            },
            "dst": {
                "lat": "",
                "lon": ""
            }
        }
    }
    )

    Alpine.store('mapSelectedLocation', {
        "shortAddress" : "",
        "longAddress" : "",
        "pickupPlaceId": ""
    })
})
