function isEmpty(str) {
    return (!str || str.trim() === "");
}

function validateEmail(email) {
    // Define the regular expression for validating an email address
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    // Check if the email is empty
    if (!email || email.trim() === "") {
        return "Email is required.";
    }

    // Check if the email contains any spaces
    if (/\s/.test(email)) {
        return "Email should not contain spaces.";
    }

    // Check if the email contains an '@' symbol
    if (!email.includes("@")) {
        return "Email must contain '@'.";
    }

    // Check if the email contains a domain after '@'
    const domain = email.split("@")[1];
    if (!domain || !domain.includes(".")) {
        return "Email must contain a valid domain (e.g., example.com).";
    }

    // Validate the email format
    if (!emailRegex.test(email)) {
        return "Invalid email format.";
    }

    // If all validations pass, return null (no error)
    return null;
}