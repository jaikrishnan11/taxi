// pickup_selection_map

var uberGreyStyle = [
    { "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }] },
    { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
    { "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
    { "elementType": "labels.text.stroke", "stylers": [{ "color": "#f5f5f5" }] },
    { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#bdbdbd" }] },
    { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
    { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
    { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
    { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
    { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }] },
    { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "color": "#757575" }] },
    { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#dadada" }] },
    { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{ "color": "#616161" }] },
    { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] },
    { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#e5e5e5" }] },
    { "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#eeeeee" }] },
    { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#c9c9c9" }] },
    { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#9e9e9e" }] }
];

function loadMap(locationCoordinates) {
    // Define source and destination points
    const { src, dst } = locationCoordinates
    src["lng"] = src["lon"]
    dst["lng"] = dst["lon"]
    
    // const centerLat = (src.lat + dst.lat) / 2;
    // const centerLng = (src.lng + dst.lng) / 2;

    // Initialize the map without center and zoom
    const map = new google.maps.Map(document.getElementById('selection_map'), {
        // centre: { lat:  9.9252025, lng: 78.1197758 },
        zoom : 20,
        disableDefaultUI: true,
        styles: uberGreyStyle
    });

    // Create a DirectionsService instance to handle route requests
    const directionsService = new google.maps.DirectionsService();

    // Create a DirectionsRenderer instance to display the route
    const directionsRenderer = new google.maps.DirectionsRenderer();
    directionsRenderer.setMap(map);

    // Request route
    const request = {
        origin: src,
        destination: dst,
        travelMode: google.maps.TravelMode.DRIVING, // Choose 'DRIVING', 'WALKING', 'BICYCLING', or 'TRANSIT'
    };

    directionsService.route(request, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsRenderer.setDirections(result); // Display the route on the map
        } else {
            console.error('Directions request failed due to ' + status);
        }
    });
}