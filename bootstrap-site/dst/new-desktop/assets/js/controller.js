function isFutureOrToday(dateString) {
  const inputDate = new Date(dateString);
  const today = new Date();

  // Remove the time part from the comparison by setting hours, minutes, seconds, and milliseconds to zero
  today.setHours(0, 0, 0, 0);
  inputDate.setHours(0, 0, 0, 0);

  // Return true if the input date is today or in the future
  return inputDate >= today;
}

function isValidDateRange(pickupDateString, returnDateString) {
  const pickupDate = new Date(pickupDateString);
  const returnDate = new Date(returnDateString);

  // Remove the time part from the comparison by setting hours, minutes, seconds, and milliseconds to zero
  pickupDate.setHours(0, 0, 0, 0);
  returnDate.setHours(0, 0, 0, 0);

  // Return true if the pickup date is earlier than or equal to the return date
  return pickupDate <= returnDate;
}

function loadButton(buttonId, textToAppearWhileLoading) {
  var $button = $(`#${buttonId}`);
  $button.html(
    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ${textToAppearWhileLoading}..`
  );
  $button.attr('disabled', true);
}

function clearLoading(buttonId, textToRevert) {
  var $button = $(`#${buttonId}`);
  $button.html(textToRevert); // Reset the button content after the action is done
  $button.attr('disabled', false);
}

document.addEventListener("DOMContentLoaded", function () {
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  tooltipTriggerList.forEach(function (tooltipTriggerEl) {
    new bootstrap.Tooltip(tooltipTriggerEl)
  })
});

$(document).ready(function () {
  $("#vehicle_selection").hide();
  $("#booking_page").hide();

  const path = window.location.pathname;

  // Check the path and set the radio button accordingly
  if (path.includes('oneway')) {
    $("input[name='tripType'][value='oneway']").prop('checked', true);
  } else if (path.includes('roundtrip')) {
    $("input[name='tripType'][value='roundtrip']").prop('checked', true);
  }

  let tripType = $('input[name="tripType"]:checked').val();
  if (tripType == "oneway") {
    $("#return_div").css('display', 'none');
  }
  if (tripType == "roundtrip") {
    $("#return_div").css('display', 'block');
  }

  $('input[name="tripType"]').change(function () {
    var selectedValue = $('input[name="tripType"]:checked').val();

    if (selectedValue) {
      console.log('Selected value: ' + selectedValue);
      if (selectedValue == "oneway") {
        $("#oneway_radio_box").css("background-color", "#FFEFF2");
        $("#roundtrip_radio_box").css("background-color", "#FFFFFF");
        $("#return_div").css('display', 'none');
      }
      if (selectedValue == "roundtrip") {
        $("#roundtrip_radio_box").css("background-color", "#FFEFF2");
        $("#oneway_radio_box").css("background-color", "#FFFFFF");
        $("#return_div").css('display', 'block');
      }

    } else {
      console.log('No option selected');
    }
  })

  // Nav button 
  document.addEventListener("scroll", function () {
    const header = document.getElementById("header_nav");
    if (window.scrollY > 50) {
      header.classList.add("scrolled");
    } else {
      header.classList.remove("scrolled");
    }
  });

  $("#logo_nav").click(function () {
    window.location.href = "/"
    return
  })

  $("#stats_btn").click(function () {
    $('html, body').animate({
      scrollTop: $("#stats_section").offset().top
    }, 200);
  })

  $("#faq_btn").click(function () {
    $('html, body').animate({
      scrollTop: $("#faq").offset().top
    }, 200);
  })

  $("#contact_us_btn").click(function () {
    $('html, body').animate({
      scrollTop: $("#footer_div").offset().top
    }, 200);
  })


  $(document).click(function (event) {
    if (!$(event.target).closest("#from_div").length) {
      document.getElementById("pickup_select_dropdown").style.display = "none";
    }
    if (!$(event.target).closest("#to_div").length) {
      document.getElementById("drop_select_dropdown").style.display = "none";
    }
  });


  // pickup drop text box changes
  $("#pickup_select_textbox").click(function () {
    document.getElementById("drop_select_dropdown").style.display = "none";
    document.getElementById("pickup_select_dropdown").style.display = "block";
    document.getElementById("ch_location_textbox").focus()
  });

  $("#drop_select_textbox").click(function () {
    document.getElementById("pickup_select_dropdown").style.display = "none";
    document.getElementById("drop_select_dropdown").style.display = "block";
    document.getElementById("drop_location_textbox").focus()
  });

  // date pickers
  // Function to set current month and year as static text in the calendar header
  function updateCalendarHeader(picker) {
    const currentMonthYear = picker.startDate.format('MMMM YYYY');
    console.log(currentMonthYear)
    $(picker.container).find('.drp-calendar .calendar-table th.month').text(currentMonthYear);
  }

  function reinitializeReturnDate(receivedDate) {
    if (receivedDate != undefined) {
      let returnDate = moment(receivedDate, "YYYY-MM-DD")
      let nextDay = returnDate.add(1, "days").format("YYYY-MM-DD")
      storeReturnDate({
        formattedDate: nextDay,
        customDate: getCustomDateFormat(nextDay)
      })
    }
    //return_date_range_picker
    $('#return_date_range_picker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: false, // Disable month/year dropdowns
      autoApply: true,
      minDate: moment(Alpine.store("pickup_date")?.formattedDate, "YYYY-MM-DD"),
      startDate: receivedDate != undefined ? moment(receivedDate, "YYYY-MM-DD").add(1, 'days') : moment().add(1, 'days')
    }, function (start) {
      updateCalendarHeader(this);
      let convertedDate = start.format('YYYY-MM-DD')
      storeReturnDate({
        formattedDate: convertedDate,
        customDate: getCustomDateFormat(convertedDate)
      })
    });
  }

  // Initialize Single Date Picker with auto-apply option
  $('#departure_date_range_picker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: false, // Disable month/year dropdowns
    autoApply: true,
    minDate: moment(),
    startDate: moment()
  }, function (start) {
    updateCalendarHeader(this);
    let convertedDate = start.format('YYYY-MM-DD')
    storePickupDate({
      formattedDate: convertedDate,
      customDate: getCustomDateFormat(convertedDate)
    })
    reinitializeReturnDate(convertedDate);
    console.log("Selected Date: ", convertedDate);
  });

  //return_date_range_picker
  reinitializeReturnDate();

  // Handling swap
  $("#swap_pickup_drop").click(function () {
    // console.log(Alpine.store('components').pickupLocationComponent.pickupLocationData)
    let pickupFinalResponse = {}
    let pickupLocationData = Alpine.store('components').pickupLocationComponent.pickupLocationData
    if (pickupLocationData?.isEmpty) {
      pickupFinalResponse = Alpine.store('components').pickupLocationComponent.emptyPickupLocationData()
    } else {
      pickupFinalResponse = pickupLocationData
    }

    let dropFinalResponse = {}
    let dropLocationData = Alpine.store('components').dropLocationComponent.dropLocationData
    if (dropLocationData?.isEmpty) {
      dropFinalResponse = Alpine.store('components').dropLocationComponent.emptyDropLocationData()
    } else {
      dropFinalResponse = dropLocationData
    }

    console.log(pickupFinalResponse)
    console.log(dropFinalResponse)
    if (pickupLocationData?.isEmpty && dropLocationData?.isEmpty) {
      return
    }

    if (!(pickupLocationData?.isEmpty) && !(dropLocationData?.isEmpty)) {
      Alpine.store('components').dropLocationComponent.update(pickupFinalResponse, false)
      Alpine.store('components').pickupLocationComponent.update(dropFinalResponse, false)
      return
    }

    if (pickupLocationData?.isEmpty) {
      Alpine.store('components').pickupLocationComponent.update(dropFinalResponse, false)
      Alpine.store('components').dropLocationComponent.update(Alpine.store('components').dropLocationComponent.emptyDropLocationData(), false)
    } else {
      Alpine.store('components').dropLocationComponent.update(pickupFinalResponse, false)
      Alpine.store('components').pickupLocationComponent.update(Alpine.store('components').pickupLocationComponent.emptyPickupLocationData(), false)
    }
  })



  // searching cab prices
  $("#search_btn").click(function () {
    let pickupLocation = sessionStorage.getItem('pickupLocationObject') ? JSON.parse(sessionStorage.getItem('pickupLocationObject')) : sessionStorage.getItem('pickupLocationObject');
    let dropLocation = sessionStorage.getItem('dropLocationObject') ? JSON.parse(sessionStorage.getItem('dropLocationObject')) : sessionStorage.getItem('dropLocationObject');
    if (pickupLocation?.isEmpty) {
      alert("Please fill the pickup location !!!");
      return
    }

    if (dropLocation?.isEmpty) {
      alert("Please fill the drop location !!!");
      return
    }

    if (pickupLocation.placeId == dropLocation.placeId) {
      alert("Pickup and drop locations are same, Kindly select different pickup / drop location !!!");
      return
    }

    let tripType = $('input[name="tripType"]:checked').val();
    let pickupDate = Alpine.store('pickup_date');
    let returnDate = Alpine.store('return_date');

    if (pickupDate != undefined && !isFutureOrToday(pickupDate.formattedDate)) {
      alert("Please select the correct pickup date")
      return
    }

    if (tripType == "oneway") {
      let cpPayload = {
        tripType: "ONE_WAY",
        pickupPlaceId: pickupLocation.placeId,
        dropPlaceId: dropLocation.placeId,
        pickupDate: pickupDate.formattedDate
      }
      loadButton("search_btn", "Searching");
      window.gogoService.getRateCards(cpPayload).then(function (data) {
        populateRateCards(data);
        clearLoading("search_btn", "Search");

        Alpine.store("selectedTripType", tripType)
        Alpine.store("tripTypeString", "One-way trip - you can take 1 hr halt and enjoy tea breaks between the journey ");
        Alpine.store("reviewBookingHeading", `${pickupLocation?.heading} - ${dropLocation?.heading} | One-way trip`)
        Alpine.store("reviewBookingPickup", `${pickupLocation?.heading}`)
        Alpine.store("reviewBookingDrop", `${dropLocation?.heading}`)
        Alpine.store("pickupPlaceId", pickupLocation.placeId)
        Alpine.store("dropPlaceId", dropLocation.placeId)
        Alpine.store("selectedPickupObject", pickupLocation)
        Alpine.store("selectedDropObject", dropLocation)
        Alpine.store("selectedPickupDate", pickupDate.formattedDate)

        $("#booking_page").hide()
        $("#vehicle_selection").show(200);
        $('html, body').animate({
          scrollTop: $("#vehicle_selection").offset().top
        }, 400);
        loadMap(data?.coOrd)
        console.log(data);
      }).catch((err) => {
        if(err?.errorCode == "SERVICE_UNAVAILABLE") {
          clearLoading("search_btn", "Search");
          $("#location-blocker-modal").modal('show')
          if (err?.message) {
            $("#location-blocker-display-text").val(err?.message)
          } else {
            $("#location-blocker-display-text").val("This service is currently unavailable in your location.")
          }
        }
        console.log(err)
        return
      })
    }

    if (tripType == "roundtrip") {
      if (returnDate != undefined && (!isFutureOrToday(returnDate.formattedDate) || !isValidDateRange(pickupDate.formattedDate, returnDate.formattedDate))) {
        alert("Please select the correct return date")
        return
      }
      let cpPayload = {
        tripType: "ROUND_TRIP",
        pickupPlaceId: pickupLocation.placeId,
        dropPlaceId: dropLocation.placeId,
        pickupDate: pickupDate.formattedDate,
        returnDate: returnDate.formattedDate,
        keepCab: true
      }
      loadButton("search_btn", "Searching");
      window.gogoService.getRateCards(cpPayload).then(function (data) {
        populateRateCards(data);
        clearLoading("search_btn", "Search");

        Alpine.store("selectedTripType", tripType);
        Alpine.store("tripTypeString", "Round trip - Multiple pickups & drops. Unlimited halts & sight-seeing included ");
        Alpine.store("reviewBookingHeading", `${pickupLocation?.heading} - ${dropLocation?.heading} | Round trip`)
        Alpine.store("reviewBookingPickup", `${pickupLocation?.heading}`)
        Alpine.store("reviewBookingDrop", `${dropLocation?.heading}`)
        Alpine.store("pickupPlaceId", pickupLocation.placeId)
        Alpine.store("dropPlaceId", dropLocation.placeId)
        Alpine.store("selectedPickupObject", pickupLocation)
        Alpine.store("selectedDropObject", dropLocation)
        Alpine.store("selectedPickupDate", pickupDate.formattedDate)
        Alpine.store("selectedDropDate", returnDate.formattedDate)

        $("#booking_page").hide()
        $("#vehicle_selection").show(200);
        $('html, body').animate({
          scrollTop: $("#vehicle_selection").offset().top
        }, 400);
        loadMap(data?.coOrd)
      }).catch((err) => {
        if(err?.errorCode == "SERVICE_UNAVAILABLE") {
          clearLoading("search_btn", "Search");
          $("#location-blocker-modal").modal('show')
          if (err?.message) {
            $("#location-blocker-display-text").val(err?.message)
          } else {
            $("#location-blocker-display-text").val("This service is currently unavailable in your location.")
          }
        }
        console.log(err)
        return
      })

    }
  })

  $("#sedan_more_details").click(function () {
    // Toggle the div with a faster transition effect (e.g., 200ms)
    $("#sedan_more_details_div").fadeToggle(200);

    // Get the current image source
    const img = $("#sedan_more_details_img");
    const currentSrc = img.attr("src");

    // Toggle the image source
    img.attr("src", currentSrc.includes("more-details-down.svg") ? "assets/img/more-details-up.svg" : "assets/img/more-details-down.svg");
  });

  $("#etios_more_details").click(function () {
    // Toggle the div with a faster transition effect (e.g., 200ms)
    $("#etios_more_details_div").fadeToggle(200);

    // Get the current image source
    const img = $("#etios_more_details_img");
    const currentSrc = img.attr("src");

    // Toggle the image source
    img.attr("src", currentSrc === "/more-details-down.svg" ? "/more-details-up.svg" : "/more-details-down.svg");
  });

  $("#xylo_more_details").click(function () {
    // Toggle the div with a faster transition effect (e.g., 200ms)
    $("#xylo_more_details_div").fadeToggle(200);

    // Get the current image source
    const img = $("#xylo_more_details_img");
    const currentSrc = img.attr("src");

    // Toggle the image source
    img.attr("src", currentSrc === "/more-details-down.svg" ? "/more-details-up.svg" : "/more-details-down.svg");
  });


  $("#innova_more_details").click(function () {
    // Toggle the div with a faster transition effect (e.g., 200ms)
    $("#innova_more_details_div").fadeToggle(200);

    // Get the current image source
    const img = $("#innova_more_details_img");
    const currentSrc = img.attr("src");

    // Toggle the image source
    img.attr("src", currentSrc === "/more-details-down.svg" ? "assets/img/more-details-up.svg" : "assets/img/more-details-down.svg");
  });

  // vehicle selection 

  $("#sedan_select").click(function () {
    let distance = Alpine.store("rateCardInfo")?.["distance"]
    const totalCost = Alpine.store("vehicleDetails")?.["sedan"]?.["total"]
    const rateCardId = Alpine.store("vehicleDetails")?.["sedan"]?.["rateCardId"]
    const imageName = "assets/img/car_sedan.svg"
    let reviewDetails = {
      "cabType": "sedan",
      "vehicleText": "Swift Dzire / Ford Aspire / Tata zest or Similar",
      "peopleCount": "4 people + 1 driver",
      "distance": `${distance} km`,
      "totalCost": `${totalCost}`,
      "rateCardId": rateCardId,
      "carImage": imageName,
      "priceDetails": Alpine.store("vehicleDetails")?.["sedan"]
    }
    Alpine.store("reviewDetails", reviewDetails)
    $("#rb_car_image").attr("src", imageName);
    $("#booking_page").show();
    $('html, body').animate({
      scrollTop: $("#booking_page").offset().top
    }, 400);
    sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
  });

  $("#etios_select").click(function () {
    let distance = Alpine.store("rateCardInfo")?.["distance"]
    const totalCost = Alpine.store("vehicleDetails")?.["etios"]?.["total"]
    const rateCardId = Alpine.store("vehicleDetails")?.["etios"]?.["rateCardId"]
    const imageName = "assets/img/car_etios.svg"
    let reviewDetails = {
      "cabType": "etios",
      "vehicleText": "Etios (sedan type)",
      "peopleCount": "4 people + 1 driver",
      "distance": `${distance} km`,
      "totalCost": `${totalCost}`,
      "rateCardId": rateCardId,
      "carImage": imageName,
      "priceDetails": Alpine.store("vehicleDetails")?.["etios"]
    }
    Alpine.store("reviewDetails", reviewDetails)
    $("#rb_car_image").attr("src", imageName);
    $("#booking_page").show();
    $('html, body').animate({
      scrollTop: $("#booking_page").offset().top
    }, 400);
    sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
  });

  $("#any_suv_select").click(function () {
    let distance = Alpine.store("rateCardInfo")?.["distance"]
    const totalCost = Alpine.store("vehicleDetails")?.["xylo"]?.["total"]
    const rateCardId = Alpine.store("vehicleDetails")?.["xylo"]?.["rateCardId"]
    const imageName = "assets/img/car_suv.svg"
    let reviewDetails = {
      "cabType": "xylo",
      "vehicleText": "Ertiga / Enjoy / Xylo / Tavera / Marazzo / Renault Lodgy / Kia Carens or Similar",
      "peopleCount": "7 people + 1 driver",
      "distance": `${distance} km`,
      "totalCost": `${totalCost}`,
      "rateCardId": rateCardId,
      "carImage": imageName,
      "priceDetails": Alpine.store("vehicleDetails")?.["xylo"]
    }
    Alpine.store("reviewDetails", reviewDetails)
    $("#rb_car_image").attr("src", imageName);
    $("#booking_page").show();
    $('html, body').animate({
      scrollTop: $("#booking_page").offset().top
    }, 400);
    sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
  });

  $("#innova_select").click(function () {
    let distance = Alpine.store("rateCardInfo")?.["distance"]
    const totalCost = Alpine.store("vehicleDetails")?.["innova"]?.["total"]
    const rateCardId = Alpine.store("vehicleDetails")?.["innova"]?.["rateCardId"]
    const imageName = "assets/img/car_innova.svg"
    let reviewDetails = {
      "cabType": "innova",
      "vehicleText": "Innova",
      "peopleCount": "7 people + 1 driver",
      "distance": `${distance} km`,
      "totalCost": `${totalCost}`,
      "rateCardId": rateCardId,
      "carImage": imageName,
      "priceDetails": Alpine.store("vehicleDetails")?.["innova"]
    }
    Alpine.store("reviewDetails", reviewDetails);
    $("#rb_car_image").attr("src", imageName);
    $("#booking_page").show();
    $('html, body').animate({
      scrollTop: $("#booking_page").offset().top
    }, 400);
    sessionStorage.setItem("vehicleDetails", JSON.stringify(reviewDetails))
  });

  $('#confirm_booking_phone_no').on('input', function () {
    // Get the value of the text box
    let value = $(this).val();

    // Remove any non-numeric characters
    value = value.replace(/\D/g, '');

    // Limit the value to 10 digits
    if (value.length > 10) {
      value = value.substring(0, 10);
    }

    // Set the modified value back to the text box
    $(this).val(value);
  });

  // confirm booking validation and calls
  $("#confirm_booking").click(function () {
    // alert("Confirm button clicked")  

    // name validation
    let customerName = $("#confirm_booking_name").val()
    if (isEmpty(customerName)) {
      alert("Please enter your full name");
      return
    }
    if (customerName.length < 3 || customerName.length > 25) {
      alert("Please enter your name length greater than 3 and less than 25 characters");
      return
    }

    //confirm_booking_mail
    // mail validation
    let customerMail = $("#confirm_booking_mail").val()
    // If the mail string is not empty, because mail is not mandatory
    if (!isEmpty(customerMail)) {
      let mailValidationResult = validateEmail(customerMail)
      if (!isEmpty(mailValidationResult)) {
        alert(mailValidationResult)
        return
      }
    }

    // if ($('#only-whatsapp').is(":checked")) {

    // } else {

    // }

    // should skip if need only whatsapp number
    //confirm_booking_phone_no
    let customerPrimaryPhoneNumber = $("#confirm_booking_phone_no").val()
    if (isEmpty(customerPrimaryPhoneNumber)) {
      alert("Please enter your phone number !!!")
      return
    }

    if (customerPrimaryPhoneNumber.length != 10) {
      alert("Please enter only 10 digits, International customers contact 638-55-888-06 whatsapp !!!")
      return
    }

    let bookingTime = $("#confirm_booking_pickup_time").val()
    if (isEmpty(bookingTime)) {
      alert("Please select your pickup time !!!")
      return
    }

    let returnBookingTime = ""

    // API call begins
    // disable the button once the call is prepared
    let tripType = "ONE_WAY"
    if (Alpine.store("selectedTripType") == "roundtrip") {
      tripType = "ROUND_TRIP"
      let parsedReturnBookingTime = $("#confirm_booking_return_time").val()
      if (isEmpty(parsedReturnBookingTime)) {
        alert("Please select your return time !!!")
        return
      }
      returnBookingTime = parsedReturnBookingTime
    }
    let cabType = Alpine.store("reviewDetails")?.["cabType"]
    let rateCardId = Alpine.store("reviewDetails")?.["rateCardId"]
    let selectedPickupDate = Alpine.store("selectedPickupDate")
    let selectedDropDate = Alpine.store("selectedDropDate");
    let pickupDateTime = new Date(selectedPickupDate + " " + bookingTime).toISOString();
    let roundTripDateTIme = tripType == "ROUND_TRIP" ? new Date(selectedDropDate + " " + returnBookingTime).toISOString() : new Date(selectedPickupDate + " " + bookingTime).toISOString();

    if (moment(roundTripDateTIme).isBefore(moment(pickupDateTime))) {
      alert("Pickup date/time should be ahead of Return journey time !!!")
      return
    }

    let customerDetails = {
      "phoneNumber": customerPrimaryPhoneNumber,
      "name": customerName,
      "email": customerMail
    }
    loadButton("confirm_booking", "Loading")
    $("#confirm_booking").attr("disabled", true)
    new GogoService().bookCabV2({
      "bookingDetails": {
        "tripType": tripType,
        "pickupPlaceId": Alpine.store("pickupPlaceId"),
        "dropPlaceId": Alpine.store("dropPlaceId"),
        "pickUpDate": pickupDateTime,
        "dropTime": tripType == "ROUND_TRIP" ? roundTripDateTIme : "",
        "dropDate": tripType == "ROUND_TRIP" ? roundTripDateTIme : pickupDateTime,
        "keepCab": "true",
        "cabType": cabType
      },
      "customerDetails": customerDetails,
      "rateCardId": rateCardId
    }).then(function (data) {
      // $("#details_booking_id").text(`Booking Id : ${data.bookingId}`); $("#details_booked_on").text(`Booked on ${moment(data.bookingDetails["tripStartTime"]).format('MMMM Do YYYY, h:mm A')}`);
      // $("#booking_dialog").modal('show');
      console.log(data);
      clearLoading("confirm_booking", "Confirm Booking")
      data["bookingDetails"]["pickup"] = Alpine.store("selectedPickupObject")
      data["bookingDetails"]["drop"] = Alpine.store("selectedDropObject")
      data["customerDetails"] = customerDetails
      sessionStorage.setItem("final_booking_response", JSON.stringify(data))
      window.location.href = "booking_confirmed.html"
    }).catch((err) => {
      clearLoading("confirm_booking", "Confirm Booking")
      alert("Error occurred while booking the cab " + JSON.stringify(err));
      console.log(err);
    })
    //confirm_booking_whatsapp_phone_no
    //confirm_booking_pickup_time
  });

  $("#contact_us").click(function () {
    window.location.href = "contact-us"
    return
  })

  $("#privacy_policy").click(function () {
    window.location.href = "privacy-policy"
    return
  })

  $("#terms_and_conditions").click(function () {
    window.location.href = "terms-and-conditions"
    return
  })

  $("#cancellation_policies").click(function () {
    window.location.href = "cancellation-and-refund-policy"
    return
  })

  // Footer icon redirection

  $("#footer_instagram_icon").click(function () {
    window.open("https://www.instagram.com/gogo_cabs/", "_blank");
    return
  })

  $("#footer_facebook_icon").click(function () {
    window.open("https://www.facebook.com/gogocabsoutstation/", "_blank");
    return
  })

  $("#footer_twitter_icon").click(function () {
    window.open("https://x.com/gogo_outstation", "_blank");
    return
  })

  $("#footer_linkedin_icon").click(function () {
    window.open("https://www.linkedin.com/in/gogo-cabs-b78b9a26a/", "_blank");
    return
  })


})

