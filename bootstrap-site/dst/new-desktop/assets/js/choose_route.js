let timeout;

function debounce(func, delay) {
    return function (...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(this, args), delay);
    };
}


async function getAutocompleteResults(query) {
    // Create a new AutocompleteService instance
    const autocompleteService = new google.maps.places.AutocompleteService();
    const center = { lat: 11.79292, lng: 77.79081 };

    // Create a bounding box with sides ~600km away from the center point
    const defaultBounds = {
        north: center.lat + 8,
        south: center.lat - 8,
        east: center.lng + 8,
        west: center.lng - 8,
    };
    // Define the request object
    const request = {
        bounds: defaultBounds,
        input: query,
        strictBounds: true,
        componentRestrictions: { country: 'in' } // Optional: restrict results to a specific country
    };
    let autocompleteResponse = []
    // Call the getPlacePredictions method
    return new Promise((resolve, reject) => {
        if(request.input != "") {
            // document.getElementById("route_spinner").style.visibility = "visible";
        }
        
        autocompleteService.getPlacePredictions(request, (predictions, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                // console.log('Predictions:', predictions);
                predictions.forEach(prediction => {
                    autocompleteResponse.push({
                        "heading": prediction?.structured_formatting?.main_text,
                        "subHeading": prediction?.structured_formatting?.secondary_text,
                        "placeId": prediction?.place_id
                    })
                });
                console.log("Got the results", autocompleteResponse)
                if (autocompleteResponse.length > 0) {
                    Alpine.store("locationResults", autocompleteResponse)
                }
                resolve(autocompleteResponse)
                // document.getElementById("route_spinner").style.visibility = "hidden";
            } else {
                console.error('Error fetching autocomplete predictions:', status);
                reject(status)
                // document.getElementById("route_spinner").style.visibility = "hidden";
            }
        });
    })

}

const debouncedFetchCities = debounce(getAutocompleteResults, 400);

async function getAutocompleteResultsDummy(query) {
    // Replace this with the actual call to Google Autocomplete API
    return new Promise(resolve => {
        setTimeout(() => {
            resolve([{
                "heading": "Chennai Airport",
                "subHeading": "Tamilnadu"
            }, {
                "heading": "Bangalore Airpot",
                "subHeading": "Karnataka"
            },
            {
                "heading": "Bangalore Airpot",
                "subHeading": "Karnataka"
            },
            {
                "heading": "Bangalore Airpot",
                "subHeading": "Karnataka"
            }
            ]);
        }, 500);
    });
}

{/* <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-O1_PezYhTQc0CKm3qDwc0IxsDJQ2zSI&callback=loadMap&libraries=places"></script> */ }

$(document).ready(function () {
    // fetchPlaces("pudukkottai")
    let lastValue = '';
    $("#ch_location_textbox").on('change keyup paste mouseup', async function () {
        if ($(this).val() != lastValue) {
            lastValue = $(this).val();
            // console.log('The text box really changed this time');
            debouncedFetchCities(lastValue)
        }
    });
    $("#drop_location_textbox").on('change keyup paste mouseup', async function () {
        if ($(this).val() != lastValue) {
            lastValue = $(this).val();
            // console.log('The text box really changed this time');
            debouncedFetchCities(lastValue)
        }
    });
})
