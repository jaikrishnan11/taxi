function returnCurrentDate() {
  let today = new Date();
  const yyyy = today.getFullYear();
  let mm = today.getMonth() + 1; // Months start at 0!
  let dd = today.getDate();

  if (dd < 10) dd = '0' + dd;
  if (mm < 10) mm = '0' + mm;

  // today = dd + '/' + mm + '/' + yyyy;
  today = `${yyyy}-${mm}-${dd}`;
  return today;
}

function returnTmrwDate() {
  const tomorrow = moment().add(1, 'days');
  const formattedTomorrow = tomorrow.format('YYYY-MM-DD');
  return formattedTomorrow;
}