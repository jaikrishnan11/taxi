function onSignIn(googleUserCredential) {
    let credentials = JSON.parse(atob(googleUserCredential.credential.split('.')[1]));
    $("#user_mail").val(credentials.email)
    $("#user_name").val(credentials.given_name + " " + credentials.family_name)
    // Another analytics integration
    if (mixpanel) {
        mixpanel.identify(credentials.sub);
        mixpanel.people.set_once({ "$name": credentials.name, "$email": credentials.email })
    }
}


function isOneWay() {
    return window.location.href.includes("#oneway")
}

function isRoundTrip() {
    return window.location.href.includes("#roundtrip")
}

const center = { lat: 11.79292, lng: 77.79081 };

// Create a bounding box with sides ~600km away from the center point
const defaultBounds = {
    north: center.lat + 8,
    south: center.lat - 8,
    east: center.lng + 8,
    west: center.lng - 8,
};

function getUUID() {
    try {
        return crypto.randomUUID()
    } catch (err) {
        return "toaken-" + Math.random()
    }
}
const options = {
    bounds: defaultBounds,
    componentRestrictions: { country: "in" },
    strictBounds: true,
    fields: ["place_id", "name"]
};


let pickupOneWay = document.getElementById("pickup_oneway_text")
let dropOneway = document.getElementById("drop_oneway_text")
let pickupRoundTrip = document.getElementById("pickup_roundtrip_text")
let dropRoundTrip = document.getElementById("drop_roundtrip_text")

let distance = 0;
let latitude = 0;
let longitude = 0;

// Function to populate the text box with place details
function populateTextBox(placeId, textBoxId) {
    var service = new google.maps.places.PlacesService(document.createElement('div'));
    service.getDetails({
        placeId: placeId
    }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            // Create a PlaceResult object
            var placeResult = {
                place_id: place.place_id,
                formatted_address: place.formatted_address,
                name: place.name,
                // Include additional details if needed
            };
            // Set the PlaceResult object to the text box data
            $('#' + textBoxId).data('autoComplete', { 'place': placeResult });
            // Update the text box value
            $('#' + textBoxId).val(place.formatted_address);
        }
    });
}

function populateAutocomplete(placeId, textId) {
    var autocompleteService = new google.maps.places.AutocompleteService();
    var autocompleteInput = document.getElementById(textId);
    autocompleteService.getPlacePredictions({
        placeId: placeId
    }, function(predictions, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK && predictions && predictions.length > 0) {
            autocompleteInput.value = predictions[0].description;
        }
    });
}



$(document).ready(function () {
    if(isOneWay()) {
        document.getElementById("oneway").click()
    }
    if(isRoundTrip()) {
        document.getElementById("roundtrip").click()
        // $("#poda_venna").click()
    }
    // *******************
    // var api_key = "AIzaSyDaaf3IpE8PdgC4YHTXrm17hHNkyTcHJNI";
    // var test_place_id = "ChIJYTN9T-plUjoRM9RjaAunYW4"
    // function getPlaceDetails(placeId, callback) {
    //     const url = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&key=${api_key}`;
      
    //     fetch(url)
    //       .then(response => response.json())
    //       .then(data => {
    //         callback(data.result);
    //       })
    //       .catch(error => {
    //         console.error('Error fetching place details:', error);
    //       });
    //   }
      
    //   // Function to set the fullName value in the textbox
    //   function setFullNameValue(placeId) {
    //     getPlaceDetails(placeId, result => {
    //       const fullName = result.formatted_address;
    //       const fullNameTextbox = document.getElementById('pickup_oneway_text');
    //       fullNameTextbox.value = fullName;
    //     });
    //   }
      
    //   setFullNameValue(test_place_id);

    // *********************
    var standalone = window.navigator.standalone,
        userAgent = window.navigator.userAgent.toLowerCase(),
        safari = /safari/.test(userAgent),
        ios = /iphone|ipod|ipad/.test(userAgent);

    if (userAgent != undefined && userAgent.startsWith("gogocabs")) {
        $("#app_link").css("visibility", "hidden");
    }

    // $("#gogo_container").css("visibility", "hidden");
    // setTimeout(function(){
    //     $("#splash_screen").css('visibility','hidden');
    //     $("#gogo_container").css("visibility", "visible");
    // }, 1200)

    $("#app_download").click(function () {
        window.open("https://play.google.com/store/apps/details?id=com.gogoapp&hl=en&gl=IN");
    });

    let cpPayload = {

    }

    //setting default values
    $("#oneway_pickup_date_textbox").attr({ "min": new Date().toISOString().split("T")[0] })
    $("#roundtrip_pickup_date_textbox").attr({ "min": new Date().toISOString().split("T")[0] })

    $("#oneway_pickup_date_textbox").val(returnCurrentDate());
    $("#oneway_pickup_time").val();

    // hiding vehicle selection at the beginning  
    $("#vehicle_selection").hide();
    $("#confirmation_page").hide();
    $("#spinner").hide();

    //auto suggestions
    $.data(pickupOneWay, "autoComplete", new google.maps.places.Autocomplete(pickupOneWay, options));
    $.data(dropOneway, "autoComplete", new google.maps.places.Autocomplete(dropOneway, options));
    $.data(pickupRoundTrip, "autoComplete", new google.maps.places.Autocomplete(pickupRoundTrip, options));
    $.data(dropRoundTrip, "autoComplete", new google.maps.places.Autocomplete(dropRoundTrip, options));


    // call and whatsapp
    $("#call-us-now, #details_call").click(function () {
        window.open("tel:6385588806");
    });

    $("#whatsapp_msg, #nav_whatsapp_msg").click(function () {
        window.open("https://wa.me/+916385588806");
    });

    function populateOneWay(payload) {
        new GogoService().getRateCards(payload).then((rates) => {
            $("#spinner").hide();
            distance = _.get(rates, "distance")
            Alpine.store("esTollAmount", _.get(rates, "estTollAmount", 0))
            // latitude = $.data(pickupOneWay, "autoComplete").getPlace().geometry.location.lat()
            // longitude = $.data(pickupOneWay, "autoComplete").getPlace().geometry.location.lng()
            console.log(rates);
            //sedan one way
            let sedanData = _.get(rates, "rateCards.oneway.0");
            if (!_.isEmpty(sedanData)) {
                $("body").data("#sedan_rateCardId", sedanData.id)
                $("#sedan_price").text(sedanData["total"]);
                $("#sedan_route_details").html(`One way <i class="fa fa-arrow-up" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;14 / km`);
                //First row
                $("#oneway_sedan_basekm").text(`${rates.distance} km`);
                $("#oneway_sedan_basefare").text(`${_.get(rates, "rateCards.oneway.0.rate")} km`);
                $("#oneway_sedan_totalbasefare").text(`₹ ${_.get(rates, "rateCards.oneway.0.fare")}`);
                $("#oneway_sedan_driver_bata").text(`₹ ${_.get(rates, "rateCards.oneway.0.bata")}`);
                $("#oneway_sedan_total").text(`₹ ${_.get(rates, "rateCards.oneway.0.total")}`);
                // $("#toll_amount").text(`₹ ${_.get(rates, "estTollAmount", 0)}`)
                // $("#toll_amount").text(`₹ ${Alpine.store("esTollAmount")}`)
            }
            //etios one way
            let etiosData = _.get(rates, "rateCards.oneway.1");
            if (!_.isEmpty(etiosData)) {
                $("body").data("#etios_rateCardId", etiosData.id)
                $("#etios_price").text(etiosData["total"]);
                $("#etios_route_details").html(`One way <i class="fa fa-arrow-up" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;15 / km`);
                //First row
                $("#oneway_etios_basekm").text(`${rates.distance} km`);
                $("#oneway_etios_basefare").text(`${_.get(rates, "rateCards.oneway.1.rate")} km`);
                $("#oneway_etios_totalbasefare").text(`₹ ${_.get(rates, "rateCards.oneway.1.fare")}`);
                $("#oneway_etios_driver_bata").text(`₹ ${_.get(rates, "rateCards.oneway.1.bata")}`);
                $("#oneway_etios_total").text(`₹ ${_.get(rates, "rateCards.oneway.1.total")}`);
            }
            //innova one way
            let innovaData = _.get(rates, "rateCards.oneway.3");
            if (!_.isEmpty(innovaData)) {
                $("body").data("#innova_rateCardId", innovaData.id)
                $("#innova_price").text(innovaData["total"]);
                $("#innova_route_details").html(`One way <i class="fa fa-arrow-up" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;20 / km`);
                //First row
                $("#oneway_innova_basekm").text(`${rates.distance} km`);
                $("#oneway_innova_basefare").text(`${_.get(rates, "rateCards.oneway.3.rate")} km`);
                $("#oneway_innova_totalbasefare").text(`₹ ${_.get(rates, "rateCards.oneway.3.fare")}`);
                $("#oneway_innova_driver_bata").text(`₹ ${_.get(rates, "rateCards.oneway.3.bata")}`);
                $("#oneway_innova_total").text(`₹ ${_.get(rates, "rateCards.oneway.3.total")}`);
            }
            //xylo one way
            let xyloData = _.get(rates, "rateCards.oneway.2");
            if (!_.isEmpty(xyloData)) {
                $("body").data("#xylo_rateCardId", xyloData.id)
                $("#xylo_price").text(xyloData["total"]);
                $("#xylo_route_details").html(`One way <i class="fa fa-arrow-up" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;19 / km`);
                //First row
                $("#oneway_xylo_basekm").text(`${rates.distance} km`);
                $("#oneway_xylo_basefare").text(`${_.get(rates, "rateCards.oneway.2.rate")} km`);
                $("#oneway_xylo_totalbasefare").text(`₹ ${_.get(rates, "rateCards.oneway.2.fare")}`);
                $("#oneway_xylo_driver_bata").text(`₹ ${_.get(rates, "rateCards.oneway.2.bata")}`);
                $("#oneway_xylo_total").text(`₹ ${_.get(rates, "rateCards.oneway.2.total")}`);
            }

            $("#vehicle_selection").show(200);
            $('html, body').animate({
                scrollTop: $("#vehicle_selection").offset().top
            }, 400);
        }, (err) => {
            $("#spinner").hide();
            console.log(err);
            alert("[one way] Something went wrong try again later" + JSON.stringify(err));
        })
    }

    // search one way cabs
    $("#search_oneway").click(function (e) {
        $("#confirm_pickup_date_div").show()
        $("#confirm_pickup_time_div").show()
        e.preventDefault();
        $("#confirmation_page").hide();
        
        let payload = {
            tripType: "ONE_WAY",
            src: {
                id: $.data(pickupOneWay, "autoComplete").getPlace().place_id,
                address: {}
            },
            dst: {
                id: $.data(dropOneway, "autoComplete").getPlace().place_id,
                address: {}
            }
        }
        if (_.isEmpty(payload.src.id)) {
            alert("Please select the pick up place !!! ");
            return;
        }
        if (_.isEmpty(payload.dst.id)) {
            alert("Please select the drop place !!! ");
            return;
        }
        cpPayload = {
            tripType: _.lowerCase(payload["tripType"]),
            from: $("#pickup_oneway_text").val(),
            to: $("#drop_oneway_text").val(),
            src: payload.src.id,
            dst: payload.dst.id
        }
        $("#spinner").show();
        //payload["latitude"] = $.data(pickupOneWay, "autoComplete").getPlace().geometry.location.lat()
        //payload["longitude"] = $.data(pickupOneWay, "autoComplete").getPlace().geometry.location.lng()
        populateOneWay(payload)
    });

    //search round trip cabs
    $("#search_roundtrip").click(function (e) {
        $("#confirm_pickup_date_div").hide()
        $("#confirm_pickup_time_div").hide()
        e.preventDefault();
        $("#confirmation_page").hide();
        let roundPickupTime = $("#roundtrip_pickup_time").val();
        if (!roundPickupTime) {
            alert("Please enter the pickup time");
            return;
        }
        console.log("round trip clicked !!");
        $("#spinner").show();
        // var pickupValue = $("#pickup_roundtrip_text").val();
        // console.log(pickupValue);
        // var dropValue = $("#drop_roundtrip_text").val();
        // console.log(dropValue);
        var pickupDate = $("#roundtrip_pickup_date_textbox").val();
        console.log(pickupDate);
        var pickupTime = $("#roundtrip_pickup_time").val();
        console.log(pickupTime);
        var returnDate = $("#roundtrip_drop_date_textbox").val();

        if (!returnDate) {
            alert("Please enter the trip end date");
            return;
        }
        console.log(returnDate);
        var keepCab = $("#keep_cab").is(":checked");
        console.log(keepCab);

        let payload = {
            tripType: "ROUND_TRIP",
            pickupTime: pickupTime,
            pickupDate: pickupDate,
            returnDate: returnDate,
            keepCab: keepCab,
            src: {
                id: $.data(pickupRoundTrip, "autoComplete").getPlace().place_id,
                address: {}
            },
            dst: {
                id: $.data(dropRoundTrip, "autoComplete").getPlace().place_id,
                address: {}
            }
        }
        if (_.isEmpty(payload.src.id)) {
            alert("Please select the pick up place !!! ");
            return;
        }
        if (_.isEmpty(payload.dst.id)) {
            alert("Please select the drop place !!! ");
            return;
        }
        cpPayload = {
            tripType: _.lowerCase(payload["tripType"]),
            pickupDate: `${payload["pickupDate"]}`,
            pickupTime: `${payload["pickupTime"]}`,
            returnDetails: `${payload["returnDate"]}`,
            from: $("#pickup_roundtrip_text").val(),
            to: $("#drop_roundtrip_text").val(),
            src: payload.src.id,
            dst: payload.dst.id
        }
        // console.log(payload);

        new GogoService().getRateCards(payload).then((rates) => {
            console.log(rates);
           // latitude = $.data(pickupRoundTrip, "autoComplete").getPlace().geometry.location.lat()
           // longitude = $.data(dropRoundTrip, "autoComplete").getPlace().geometry.location.lng()
            distance = _.multiply(_.get(rates, "distance"), 2)
            Alpine.store("esTollAmount", _.get(rates, "estTollAmount", 0))
            //sedan round trip
            let sedanData = _.get(rates, "rateCards.roundTrip.0");
            if (!_.isEmpty(sedanData)) {
                $("body").data("#sedan_rateCardId", sedanData.id)
                $("#sedan_price").text(sedanData["total"]);
                $("#sedan_route_details").html(`Round trip <i class="fa fa-arrows-v" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;13 / km`);
                $("#sedan_price").text(sedanData["total"]);
                // $("#sedan_route_details").html(`One way <i class="fa fa-arrow-up" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;13 km/hr`);
                //First row
                //First row
                $("#oneway_sedan_basekm").text(`${rates.distance} km`);
                $("#oneway_sedan_basefare").text(`${_.get(rates, "rateCards.roundTrip.0.rate")} km`);
                $("#oneway_sedan_totalbasefare").text(`₹ ${_.get(rates, "rateCards.roundTrip.0.fare")}`);
                $("#oneway_sedan_driver_bata").text(`₹ ${_.get(rates, "rateCards.roundTrip.0.priceSplit.DriverBata.calculation")}`);
                $("#oneway_sedan_total").text(`₹ ${_.get(rates, "rateCards.roundTrip.0.total")}`);
            }
            //etios round trip
            let etiosData = _.get(rates, "rateCards.roundTrip.1");
            if (!_.isEmpty(etiosData)) {
                $("#etios_price").text(etiosData["total"]);
                $("body").data("#etios_rateCardId", etiosData.id)
                $("#etios_route_details").html(`Round trip <i class="fa fa-arrows-v" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;13 / km`);
                //First row
                $("#oneway_etios_basekm").text(`${rates.distance} km`);
                $("#oneway_etios_basefare").text(`${_.get(rates, "rateCards.roundTrip.1.rate")} km`);
                $("#oneway_etios_totalbasefare").text(`₹ ${_.get(rates, "rateCards.roundTrip.1.fare")}`);
                $("#oneway_etios_driver_bata").text(`₹ ${_.get(rates, "rateCards.roundTrip.1.priceSplit.DriverBata.calculation")}`);
                $("#oneway_etios_total").text(`₹ ${_.get(rates, "rateCards.roundTrip.1.total")}`);
            }
            //innova round trip
            let innovaData = _.get(rates, "rateCards.roundTrip.3");
            if (!_.isEmpty(innovaData)) {
                $("body").data("#innova_rateCardId", innovaData.id)
                $("#innova_price").text(innovaData["total"]);
                $("#innova_route_details").html(`Round trip <i class="fa fa-arrows-v" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;18 / km`);
                //First row
                $("#oneway_innova_basekm").text(`${rates.distance} km`);
                $("#oneway_innova_basefare").text(`${_.get(rates, "rateCards.roundTrip.3.rate")} km`);
                $("#oneway_innova_totalbasefare").text(`₹ ${_.get(rates, "rateCards.roundTrip.3.fare")}`);
                $("#oneway_innova_driver_bata").text(`₹ ${_.get(rates, "rateCards.roundTrip.3.priceSplit.DriverBata.calculation")}`);
                $("#oneway_innova_total").text(`₹ ${_.get(rates, "rateCards.roundTrip.3.total")}`);
            }
            //xylo round trip
            let xyloData = _.get(rates, "rateCards.roundTrip.2");
            if (!_.isEmpty(xyloData)) {
                $("body").data("#xylo_rateCardId", xyloData.id)
                $("#xylo_price").text(xyloData["total"]);
                $("#xylo_route_details").html(`Round trip <i class="fa fa-arrows-v" style="color: #fe3e66;"></i><br><i class="fa fa-rupee" style="color: #fe3e66;"></i>&nbsp;17 / km`);
                //First row
                $("#oneway_xylo_basekm").text(`${rates.distance} km`);
                $("#oneway_xylo_basefare").text(`${_.get(rates, "rateCards.roundTrip.2.rate")} km`);
                $("#oneway_xylo_totalbasefare").text(`₹ ${_.get(rates, "rateCards.roundTrip.2.fare")}`);
                $("#oneway_xylo_driver_bata").text(`₹ ${_.get(rates, "rateCards.roundTrip.2.priceSplit.DriverBata.calculation")}`);
                $("#oneway_xylo_total").text(`₹ ${_.get(rates, "rateCards.roundTrip.2.total")}`);
            }
            $("#spinner").hide();
            $("#vehicle_selection").show(200);
            $('html, body').animate({
                scrollTop: $("#vehicle_selection").offset().top
            }, 400);
        }, (err) => {
            $("#spinner").hide();
            console.log(err);
            alert("[round trip]Something went wrong gogoService APIs" + JSON.stringify(err));
            throw err;
        });

    });

    $("#chennai_bangalore").click(function(e) {
        populateOneWay({
            tripType: "ONE_WAY",
            src : {
                id : "ChIJCykE-VgprTsRpPEUgyF0nps"
            },
            dst : {
                id : "ChIJd3hK6VMTBzsRIheYU_34K5Y"
            },
            latitude : "12.6680884",
            longitude: "79.2847091"
        })
    })

    // $("#populate_1").click(function(e){
    //     e.preventDefault();
    //     populateAutocomplete("ChIJYTN9T-plUjoRM9RjaAunYW4", 'pickup_oneway_text');
    // })

    // $("#populate_2").click(function(e){
    //     e.preventDefault();
    //     populateAutocomplete("ChIJbU60yXAWrjsR4E9-UejD3_g", 'drop_oneway_text');
    // })
    

    //confirmation page formation
    $("#sedan_select").click(function () {
        cpPayload["price"] = $("#sedan_price").text();
        cpPayload["vehicle"] = "sedan";
        $("#confirmation_page").show(200);

        $("#cp_pickup_date_details").text(_.toString(cpPayload["pickupDetails"]));
        $("#cp_trip_type").text(`(${cpPayload["tripType"]})`);
        $("#cp_from").text(cpPayload["from"]);
        $("#cp_to").text(cpPayload["to"]);
        $("body").data("#cp_rateCardId", $("body").data("#sedan_rateCardId"))
        $("#cp_car_type").text("Car type - Sedan (4 people + 1 driver)");
        $("#cp_price").text($("#sedan_price").text());

        $('html, body').animate({
            scrollTop: $("#confirmation_page").offset().top
        }, 300);
    });

    $("#etios_select").click(function () {
        cpPayload["price"] = $("#etios_price").text();
        cpPayload["vehicle"] = "etios";
        $("#confirmation_page").show(200);
        $("#cp_pickup_date_details").text(_.toString(cpPayload["pickupDetails"]));
        $("#cp_trip_type").text(`(${cpPayload["tripType"]})`);
        $("#cp_from").text(cpPayload["from"]);
        $("#cp_to").text(cpPayload["to"]);
        $("body").data("#cp_rateCardId", $("body").data("#etios_rateCardId"))
        $("#cp_car_type").text("Car type - Etios (4 people + 1 driver)");
        $("#cp_price").text($("#etios_price").text());

        $('html, body').animate({
            scrollTop: $("#confirmation_page").offset().top
        }, 300);
    });

    $("#innova_select").click(function () {
        cpPayload["price"] = $("#innova_price").text();
        cpPayload["vehicle"] = "innova";
        $("#confirmation_page").show(200);
        $("#cp_pickup_date_details").text(_.toString(cpPayload["pickupDetails"]));
        $("#cp_trip_type").text(`(${cpPayload["tripType"]})`);
        $("#cp_from").text(cpPayload["from"]);
        $("#cp_to").text(cpPayload["to"]);

        $("#cp_car_type").text("Car type - Innova (7 people + 1 driver)");
        $("#cp_price").text($("#innova_price").text());
        $("body").data("#cp_rateCardId", $("body").data("#innova_rateCardId"))

        $('html, body').animate({
            scrollTop: $("#confirmation_page").offset().top
        }, 300);
    });

    $("#xylo_select").click(function () {
        cpPayload["price"] = $("#xylo_price").text();
        cpPayload["vehicle"] = "xylo";
        $("#confirmation_page").show(200);
        $("#cp_pickup_date_details").text(_.toString(cpPayload["pickupDetails"]));
        $("#cp_trip_type").text(`(${cpPayload["tripType"]})`);
        $("#cp_from").text(cpPayload["from"]);
        $("#cp_to").text(cpPayload["to"]);

        $("#cp_car_type").text("Car type - Xylo (7 people + 1 driver)");
        $("#cp_price").text($("#xylo_price").text());
        $("body").data("#cp_rateCardId", $("body").data("#xylo_rateCardId"))

        $('html, body').animate({
            scrollTop: $("#confirmation_page").offset().top
        }, 300);
    });

    //confirm booking
    $("#confirm_booking").click(function (e) {
        e.preventDefault();
        if(cpPayload["tripType"] == "one way") {
            let pickupDate = $("#oneway_pickup_date_textbox").val()
            let pickupTime = $("#oneway_pickup_time").val();
            if (!pickupTime) {
                alert("Please enter the pickup time");
                return;
            }
            cpPayload["pickupDate"] = pickupDate
            cpPayload["pickupTime"] = pickupTime
        }
        var username = $("#user_name").val();
        var mail = $("#user_mail").val();
        var phoneNumber = $("#user_phone_no").val();
        if (username == "") {
            alert("Please fill the name");
            return
        }
        if (phoneNumber == "") {
            alert("Please fill the phone number");
            return
        }
        if (phoneNumber.length > 10 || phoneNumber.length < 10) {
            alert("Phone number should be of only 10 digits, Ex:9812398123");
            return
        }

        Object.assign(cpPayload, {
            name: username,
            mailId: mail,
            phoneNumber: phoneNumber,
            estimatedKm: distance,
           // latitude: latitude,
           // longitude: longitude
        });
        $("#confirm_booking").attr("disabled", true)
        // new GogoService().bookCab(cpPayload).then(function (data) {
        //     $("#details_booking_id").text(`Booking Id : ${data.rideDetails.bookingId}`); $("#details_booked_on").text(`Booked on ${moment(data.rideDetails["createdAt"]).format('MMMM Do YYYY, h:mm A')}`);
        //     $("#booking_dialog").modal('show');
        //     console.log(data);
        //     setTimeout(() => $("#confirm_booking").attr("disabled", false), 5000)
        // }).catch((err) => {
        //     $("#confirm_booking").attr("disabled", false)
        //     alert("Error occurred while booking the cab " + JSON.stringify(err));
        //     console.log(err);
        // })

        new GogoService().bookCabV2({
            "bookingDetails": {
                "tripType": cpPayload.tripType == "one way" ? "ONE_WAY" : "ROUND_TRIP",
                "pickupPlaceId": cpPayload.src,
                "dropPlaceId": cpPayload.dst,
                "pickUpDate": new Date(cpPayload.pickupDate + " " + cpPayload.pickupTime).toISOString(),
                "dropTime": cpPayload.returnDetails ? new Date(cpPayload.returnDetails).toISOString() : "",
                "dropDate": cpPayload.returnDetails ? new Date(cpPayload.returnDetails).toISOString() : "",
                "keepCab": "true",
                "cabType": cpPayload.vehicle
            },
            "customerDetails": {
                "phoneNumber": phoneNumber,
                "name": username,
                "email": mail
            },
            "rateCardId": parseInt($("body").data("#cp_rateCardId"))

        }).then(function (data) {
            $("#details_booking_id").text(`Booking Id : ${data.bookingId}`); $("#details_booked_on").text(`Booked on ${moment(data.bookingDetails["tripStartTime"]).format('MMMM Do YYYY, h:mm A')}`);
            $("#booking_dialog").modal('show');
            console.log(data);
            setTimeout(() => $("#confirm_booking").attr("disabled", false), 5000)
        }).catch((err) => {
            $("#confirm_booking").attr("disabled", false)
            alert("Error occurred while booking the cab " + JSON.stringify(err));
            console.log(err);
        })
    });


    // Nav bar and front page transition 
    $("#begin, #begin_drop").click(function () {
        $('html, body').animate({
            scrollTop: $("#booking_form").offset().top
        }, 400);
    });


    $("#nav_tariff").click(function () {
        $('html, body').animate({
            scrollTop: $("#tariff_div").offset().top
        }, 200);
    });

    $("#nav_stats").click(function () {
        $('html, body').animate({
            scrollTop: $("#stats").offset().top
        }, 200);
    });

    $("#nav_faq").click(function () {
        $('html, body').animate({
            scrollTop: $("#faq").offset().top
        }, 200);
    });

    //form level functions
    $("#oneway").click(function () {
        // $("#confirm_pickup_date_div").show()
        // $("#confirm_pickup_time_div").show()
        $("#pickup_oneway_text").val($("#pickup_roundtrip_text").val());
        $("#drop_oneway_text").val($("#drop_roundtrip_text").val());
        $("#oneway_pickup_date_textbox").val($("#roundtrip_pickup_date_textbox").val());
    });

    // $("#roundtrip").click(function () {
    //     $("#confirm_pickup_date_div").hide()
    //     $("#confirm_pickup_time_div").hide()
    // })


    // $("#roundtrip").click(function () {
    //     $("#pickup_roundtrip_text").val($("#pickup_oneway_text").val())
    //     $("#drop_roundtrip_text").val($("#drop_oneway_text").val());
    //     $("#roundtrip_pickup_date_textbox").val($("#oneway_pickup_date_textbox").val());
    // });

    //one way functions
    $("#oneway_pickup_date_today").click(function () {
        $("#oneway_pickup_date_textbox").val(returnCurrentDate);
    });
    $("#oneway_pickup_date_tmrw").click(function () {
        $("#oneway_pickup_date_textbox").val(returnTmrwDate);
    });

    $("#oneway_swap").click(function () {
        var pickupValue = $("#pickup_oneway_text").val();
        $("#pickup_oneway_text").val($("#drop_oneway_text").val());
        $("#drop_oneway_text").val(pickupValue);
    });

    // $("#oneway_swap").click(function () {
    //     var pickupValue = $("#pickup_oneway_text").clone();
    //     // $("#pickup_oneway_text").val($("#drop_oneway_text").val());
    //     $("#drop_oneway_text").append(pickupValue);
    // });


    //round trip functions
    $('#roundtrip_pickup_date_textbox').on('change', function () {
        $("#roundtrip_drop_date_textbox").val($('#roundtrip_pickup_date_textbox').val())
        $("#roundtrip_drop_date_textbox").attr({ "min": $('#roundtrip_pickup_date_textbox').val() })
    });

    $("#roundtrip_pickup_date_today").click(function () {
        $("#roundtrip_pickup_date_textbox").val(returnCurrentDate);
        $("#roundtrip_drop_date_textbox").val($('#roundtrip_pickup_date_textbox').val())
        $("#roundtrip_drop_date_textbox").attr({ "min": $('#roundtrip_pickup_date_textbox').val() })
    });
    $("#roundtrip_pickup_date_tmrw").click(function () {
        $("#roundtrip_pickup_date_textbox").val(returnTmrwDate);
        $("#roundtrip_drop_date_textbox").val($('#roundtrip_pickup_date_textbox').val())
        $("#roundtrip_drop_date_textbox").attr({ "min": $('#roundtrip_pickup_date_textbox').val() })
    });

    // check the pick up date, if its empty throw error. if not empty set the min date as the pickup date
    $('#roundtrip_drop_date_textbox').on('change', function () {
        let roundTripPickupDate = $('#roundtrip_pickup_date_textbox').val();
        if (roundTripPickupDate == "" || roundTripPickupDate == undefined) {
            $('#roundtrip_drop_date_textbox').val("");
            alert("Please fill the pickup date");
        }
    });

    $("#roundtrip_swap").click(function () {
        var pickupValue = $("#pickup_roundtrip_text").val();
        $("#pickup_roundtrip_text").val($("#drop_roundtrip_text").val());
        $("#drop_roundtrip_text").val(pickupValue);
    });

    // same day and next day date changes
    // $("#roundtrip_drop_date_today").click(function () {
    //     let selectedDate = $("#roundtrip_pickup_date_textbox").val()
    //     $("#roundtrip_drop_date_textbox").val(selectedDate);
    // });
    $("#roundtrip_drop_date_tmrw").click(function () {
        let selectedDate = $("#roundtrip_drop_date_textbox").val()
        $("#roundtrip_drop_date_textbox").val(returnTmrwDate);
    });

    // window.gogoService.warmupLambda()
});