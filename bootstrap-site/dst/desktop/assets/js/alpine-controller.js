const favRoutes = {
    Chennai: [
      {
        formattedText: 'Outstation Cabs from Chennai to Bangalore',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Salem',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Trichy',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Karaikudi',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Madurai',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Coimbatore',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Karur',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Pondicherry',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Rameshwaram',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Vellore',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Erode',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Kanyakumari',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Tirupati',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Thanjavur',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Thoothukudi',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Tirunelveli',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Chennai Airport',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Coimbatore Airport',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Madurai Airport',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Trichy Airport',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Bangalore Airport',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      },
      {
        formattedText: 'Outstation Cabs from Chennai to Thoothukudi Airport',
        sourcePlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '13.0826802',
        longitude: '80.2707184',
        section: 'Chennai'
      }
    ],
    Bangalore: [
      {
        formattedText: 'Outstation Cabs from Bangalore to Chennai',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Salem',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Trichy',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Karaikudi',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Madurai',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Coimbatore',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Karur',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Pondicherry',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Rameshwaram',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Vellore',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Erode',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Kanyakumari',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Tirupati',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Thanjavur',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Thoothukudi',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Tirunelveli',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Chennai Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Coimbatore Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Madurai Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Trichy Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Bangalore Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore to Thoothukudi Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore'
      }
    ],
    Salem: [
      {
        formattedText: 'Outstation Cabs from Salem to Chennai',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Bangalore',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Trichy',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Karaikudi',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Madurai',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Coimbatore',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Karur',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Pondicherry',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Rameshwaram',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Vellore',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Erode',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Kanyakumari',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Tirupati',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Thanjavur',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Thoothukudi',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Tirunelveli',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Chennai Airport',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Coimbatore Airport',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Madurai Airport',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Trichy Airport',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Bangalore Airport',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      },
      {
        formattedText: 'Outstation Cabs from Salem to Thoothukudi Airport',
        sourcePlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '11.664325',
        longitude: '78.1460142',
        section: 'Salem'
      }
    ],
    Trichy: [
      {
        formattedText: 'Outstation Cabs from Trichy to Chennai',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Bangalore',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Salem',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Karaikudi',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Madurai',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Coimbatore',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Karur',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Pondicherry',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Rameshwaram',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Vellore',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Erode',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Kanyakumari',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Tirupati',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Thanjavur',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Thoothukudi',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Tirunelveli',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Chennai Airport',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Coimbatore Airport',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Madurai Airport',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Trichy Airport',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Bangalore Airport',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      },
      {
        formattedText: 'Outstation Cabs from Trichy to Thoothukudi Airport',
        sourcePlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '10.7904833',
        longitude: '78.7046725',
        section: 'Trichy'
      }
    ],
    Karaikudi: [
      {
        formattedText: 'Outstation Cabs from Karaikudi to Chennai',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Bangalore',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Salem',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Trichy',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Madurai',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Coimbatore',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Karur',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Pondicherry',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Rameshwaram',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Vellore',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Erode',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Kanyakumari',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Tirupati',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Thanjavur',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Thoothukudi',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Tirunelveli',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Chennai Airport',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Coimbatore Airport',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Madurai Airport',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Trichy Airport',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Bangalore Airport',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      },
      {
        formattedText: 'Outstation Cabs from Karaikudi to Thoothukudi Airport',
        sourcePlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '10.07354',
        longitude: '78.773198',
        section: 'Karaikudi'
      }
    ],
    Madurai: [
      {
        formattedText: 'Outstation Cabs from Madurai to Chennai',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Bangalore',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Salem',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Trichy',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Karaikudi',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Coimbatore',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Karur',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Pondicherry',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Rameshwaram',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Vellore',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Erode',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Kanyakumari',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Tirupati',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Thanjavur',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Thoothukudi',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Tirunelveli',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Chennai Airport',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Coimbatore Airport',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Madurai Airport',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Trichy Airport',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Bangalore Airport',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      },
      {
        formattedText: 'Outstation Cabs from Madurai to Thoothukudi Airport',
        sourcePlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '9.9252007',
        longitude: '78.1197754',
        section: 'Madurai'
      }
    ],
    Coimbatore: [
      {
        formattedText: 'Outstation Cabs from Coimbatore to Chennai',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Bangalore',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Salem',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Trichy',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Karaikudi',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Madurai',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Karur',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Pondicherry',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Rameshwaram',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Vellore',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Erode',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Kanyakumari',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Tirupati',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Thanjavur',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Thoothukudi',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Tirunelveli',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Chennai Airport',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Coimbatore Airport',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Madurai Airport',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Trichy Airport',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Bangalore Airport',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore to Thoothukudi Airport',
        sourcePlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '11.0168445',
        longitude: '76.9558321',
        section: 'Coimbatore'
      }
    ],
    Karur: [
      {
        formattedText: 'Outstation Cabs from Karur to Chennai',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Bangalore',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Salem',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Trichy',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Karaikudi',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Madurai',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Coimbatore',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Pondicherry',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Rameshwaram',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Vellore',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Erode',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Kanyakumari',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Tirupati',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Thanjavur',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Thoothukudi',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Tirunelveli',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Chennai Airport',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Coimbatore Airport',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Madurai Airport',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Trichy Airport',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Bangalore Airport',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      },
      {
        formattedText: 'Outstation Cabs from Karur to Thoothukudi Airport',
        sourcePlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '10.9600778',
        longitude: '78.07660360000001',
        section: 'Karur'
      }
    ],
    Pondicherry: [
      {
        formattedText: 'Outstation Cabs from Pondicherry to Chennai',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Bangalore',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Salem',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Trichy',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Karaikudi',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Madurai',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Coimbatore',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Karur',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Rameshwaram',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Vellore',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Erode',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Kanyakumari',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Tirupati',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Thanjavur',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Thoothukudi',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Tirunelveli',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Chennai Airport',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Coimbatore Airport',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Madurai Airport',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Trichy Airport',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Bangalore Airport',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      },
      {
        formattedText: 'Outstation Cabs from Pondicherry to Thoothukudi Airport',
        sourcePlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '11.9415915',
        longitude: '79.8083133',
        section: 'Pondicherry'
      }
    ],
    Rameshwaram: [
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Chennai',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Bangalore',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Salem',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Trichy',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Karaikudi',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Madurai',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Coimbatore',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Karur',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Pondicherry',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Vellore',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Erode',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Kanyakumari',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Tirupati',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Thanjavur',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Thoothukudi',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Tirunelveli',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Chennai Airport',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Coimbatore Airport',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Madurai Airport',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Trichy Airport',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Bangalore Airport',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      },
      {
        formattedText: 'Outstation Cabs from Rameshwaram to Thoothukudi Airport',
        sourcePlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '9.287583699999999',
        longitude: '79.3129488',
        section: 'Rameshwaram'
      }
    ],
    Vellore: [
      {
        formattedText: 'Outstation Cabs from Vellore to Chennai',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Bangalore',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Salem',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Trichy',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Karaikudi',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Madurai',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Coimbatore',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Karur',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Pondicherry',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Rameshwaram',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Erode',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Kanyakumari',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Tirupati',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Thanjavur',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Thoothukudi',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Tirunelveli',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Chennai Airport',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Coimbatore Airport',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Madurai Airport',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Trichy Airport',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Bangalore Airport',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      },
      {
        formattedText: 'Outstation Cabs from Vellore to Thoothukudi Airport',
        sourcePlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '12.9165167',
        longitude: '79.13249859999999',
        section: 'Vellore'
      }
    ],
    Erode: [
      {
        formattedText: 'Outstation Cabs from Erode to Chennai',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Bangalore',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Salem',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Trichy',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Karaikudi',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Madurai',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Coimbatore',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Karur',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Pondicherry',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Rameshwaram',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Vellore',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Kanyakumari',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Tirupati',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Thanjavur',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Thoothukudi',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Tirunelveli',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Chennai Airport',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Coimbatore Airport',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Madurai Airport',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Trichy Airport',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Bangalore Airport',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      },
      {
        formattedText: 'Outstation Cabs from Erode to Thoothukudi Airport',
        sourcePlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '11.3410364',
        longitude: '77.7171642',
        section: 'Erode'
      }
    ],
    Kanyakumari: [
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Chennai',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Bangalore',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Salem',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Trichy',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Karaikudi',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Madurai',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Coimbatore',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Karur',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Pondicherry',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Rameshwaram',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Vellore',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Erode',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Tirupati',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Thanjavur',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Thoothukudi',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Tirunelveli',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Chennai Airport',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Coimbatore Airport',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Madurai Airport',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Trichy Airport',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Bangalore Airport',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      },
      {
        formattedText: 'Outstation Cabs from Kanyakumari to Thoothukudi Airport',
        sourcePlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '8.0843512',
        longitude: '77.54950190000001',
        section: 'Kanyakumari'
      }
    ],
    Tirupati: [
      {
        formattedText: 'Outstation Cabs from Tirupati to Chennai',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Bangalore',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Salem',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Trichy',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Karaikudi',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Madurai',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Coimbatore',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Karur',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Pondicherry',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Rameshwaram',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Vellore',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Erode',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Kanyakumari',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Thanjavur',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Thoothukudi',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Tirunelveli',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Chennai Airport',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Coimbatore Airport',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Madurai Airport',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Trichy Airport',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Bangalore Airport',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      },
      {
        formattedText: 'Outstation Cabs from Tirupati to Thoothukudi Airport',
        sourcePlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '13.6287557',
        longitude: '79.4191795',
        section: 'Tirupati'
      }
    ],
    Thanjavur: [
      {
        formattedText: 'Outstation Cabs from Thanjavur to Chennai',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Bangalore',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Salem',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Trichy',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Karaikudi',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Madurai',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Coimbatore',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Karur',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Pondicherry',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Rameshwaram',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Vellore',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Erode',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Kanyakumari',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Tirupati',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Thoothukudi',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Tirunelveli',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Chennai Airport',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Coimbatore Airport',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Madurai Airport',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Trichy Airport',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Bangalore Airport',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      },
      {
        formattedText: 'Outstation Cabs from Thanjavur to Thoothukudi Airport',
        sourcePlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '10.7869994',
        longitude: '79.13782739999999',
        section: 'Thanjavur'
      }
    ],
    Thoothukudi: [
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Chennai',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Bangalore',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Salem',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Trichy',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Karaikudi',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Madurai',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Coimbatore',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Karur',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Pondicherry',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Rameshwaram',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Vellore',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Erode',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Kanyakumari',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Tirupati',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Thanjavur',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Tirunelveli',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Chennai Airport',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Coimbatore Airport',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Madurai Airport',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Trichy Airport',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Bangalore Airport',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi to Thoothukudi Airport',
        sourcePlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '8.764166099999999',
        longitude: '78.1348361',
        section: 'Thoothukudi'
      }
    ],
    Tirunelveli: [
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Chennai',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Bangalore',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Salem',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Trichy',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Karaikudi',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Madurai',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Coimbatore',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Karur',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Pondicherry',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Rameshwaram',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Vellore',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Erode',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Kanyakumari',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Tirupati',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Thanjavur',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Thoothukudi',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Chennai Airport',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Coimbatore Airport',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Madurai Airport',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Trichy Airport',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Bangalore Airport',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      },
      {
        formattedText: 'Outstation Cabs from Tirunelveli to Thoothukudi Airport',
        sourcePlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '8.715017999999999',
        longitude: '77.765628',
        section: 'Tirunelveli'
      }
    ],
    'Chennai Airport': [
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Bangalore',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Salem',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Trichy',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Karaikudi',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Madurai',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Coimbatore',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Karur',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Pondicherry',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Rameshwaram',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Vellore',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Erode',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Kanyakumari',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Tirupati',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Thanjavur',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Thoothukudi',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Tirunelveli',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Coimbatore Airport',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Madurai Airport',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Trichy Airport',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Bangalore Airport',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Chennai Airport to Thoothukudi Airport',
        sourcePlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '12.9811068',
        longitude: '80.159623',
        section: 'Chennai Airport'
      }
    ],
    'Coimbatore Airport': [
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Chennai',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Bangalore',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Salem',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Trichy',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Karaikudi',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Madurai',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Karur',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Pondicherry',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Rameshwaram',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Vellore',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Erode',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Kanyakumari',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Tirupati',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Thanjavur',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Thoothukudi',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Tirunelveli',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Chennai Airport',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Madurai Airport',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Trichy Airport',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Bangalore Airport',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Coimbatore Airport to Thoothukudi Airport',
        sourcePlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '11.0304324',
        longitude: '77.03909279999999',
        section: 'Coimbatore Airport'
      }
    ],
    'Madurai Airport': [
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Chennai',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Bangalore',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Salem',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Trichy',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Karaikudi',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Coimbatore',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Karur',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Pondicherry',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Rameshwaram',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Vellore',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Erode',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Kanyakumari',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Tirupati',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Thanjavur',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Thoothukudi',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Tirunelveli',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Chennai Airport',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Coimbatore Airport',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Trichy Airport',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Bangalore Airport',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      },
      {
        formattedText: 'Outstation Cabs from Madurai Airport to Thoothukudi Airport',
        sourcePlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '9.838228199999998',
        longitude: '78.0894782',
        section: 'Madurai Airport'
      }
    ],
    'Trichy Airport': [
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Chennai',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Bangalore',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Salem',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Karaikudi',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Madurai',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Coimbatore',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Karur',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Pondicherry',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Rameshwaram',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Vellore',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Erode',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Kanyakumari',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Tirupati',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Thanjavur',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Thoothukudi',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Tirunelveli',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Chennai Airport',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Coimbatore Airport',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Madurai Airport',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Bangalore Airport',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      },
      {
        formattedText: 'Outstation Cabs from Trichy Airport to Thoothukudi Airport',
        sourcePlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '10.7594989',
        longitude: '78.7139806',
        section: 'Trichy Airport'
      }
    ],
    'Bangalore Airport': [
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Chennai',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Salem',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Trichy',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Karaikudi',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Madurai',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Coimbatore',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Karur',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Pondicherry',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Rameshwaram',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Vellore',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Erode',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Kanyakumari',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Tirupati',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Thanjavur',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Thoothukudi',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJT3attGfuAzsR0-cOyW0-RAI',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Tirunelveli',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Chennai Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Coimbatore Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Madurai Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Trichy Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      },
      {
        formattedText: 'Outstation Cabs from Bangalore Airport to Thoothukudi Airport',
        sourcePlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        destinationPlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        latitude: '12.9715987',
        longitude: '77.5945627',
        section: 'Bangalore Airport'
      }
    ],
    'Thoothukudi Airport': [
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Chennai',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJYTN9T-plUjoRM9RjaAunYW4',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Bangalore',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Salem',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJC7os9czxqzsRR8pocACJme4',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Trichy',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJrc2u8g_1qjsRpq677Tss4G0',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Karaikudi',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJO0_eCItdADsR-IRFuAr8JB8',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Madurai',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJM5YYsYLFADsRMzn2ZHJbldw',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Coimbatore',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJtRyXL69ZqDsRgtI-GB7IwS8',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Karur',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJjzmVJAcvqjsRG599H3r8fJQ',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Pondicherry',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJz89JjqthUzoRTrDw0ibTa8w',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Rameshwaram',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJs_Ic5sTjATsRoWO9i7n5Z9Y',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Vellore',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJ-4-mH-Y4rTsRXismfZGm3b4',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Erode',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJcUYvdkZvqTsRXvfH2eOmfdk',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Kanyakumari',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJYXgIKj3tBDsRoPrvaokOeR4',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Tirupati',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJJwRiiA9LTToRrAzK2tFSQc8',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Thanjavur',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJOTBF6py4qjsR5itjH5vaE-E',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Tirunelveli',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJGcVTUGIRBDsRTZo1GzYbea0',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Chennai Airport',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJl2OoXR9eUjoRR27ibiEvCSE',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Coimbatore Airport',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJr6lem7xXqDsReEaM9A2hfjE',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Madurai Airport',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJFSAYuGnQADsRF3_75hRYz5A',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Trichy Airport',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJ7YNhAGqLqjsRZaisy_iqN3U',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      },
      {
        formattedText: 'Outstation Cabs from Thoothukudi Airport to Bangalore Airport',
        sourcePlaceId: 'ChIJ73YRaYTzAzsRdD7okC8sNk8',
        destinationPlaceId: 'ChIJbU60yXAWrjsR4E9-UejD3_g',
        latitude: '8.7260824',
        longitude: '78.0268992',
        section: 'Thoothukudi Airport'
      }
    ]
  }
  


document.addEventListener('alpine:init', () => {
    console.log("Alpine registered")
    Alpine.store("favRoutes", favRoutes)
})