const data = require("./places_data.json")
const _ = require("lodash")

let result = {}

_.each(_.keys(data), (key1) => {
    _.each(_.keys(data), (key2) => {
        if(key1 != key2 && !data[key1].skipPlaces.includes(key2)) {
            result[`${key1}_to_${key2}`] = {
                "formattedText" : `Outstation Cabs from ${key1} to ${key2}`,
                "sourcePlaceId" : data[key1].placeId,
                "destinationPlaceId" : data[key2].placeId,
                "latitude" : data[key1].latitude,
                "longitude" : data[key1].longitude,
                "section" : key1
            }
            console.log(`[Outstation Cabs from ${key1} to ${key2}]`);
            console.log(`[${key1} to ${key2} cabs]`);
            console.log(`[${key1} to ${key2} intercity cabs]`);
            console.log(`[${key1} to ${key2} drop taxi]`);
            console.log(`[${key1} to ${key2} taxi]`);
            console.log(`[${key1} to ${key2} one way drop taxi]`);
            console.log(`[${key1} to ${key2} round trip]`);
        }
    })  
})

// console.log(_.groupBy(result, "section"))
// _.each(result, (value) => {
//     console.log(value.formattedText)
// })
