
import { CONFIG } from "../config";

const TOKEN = '5143210951:AAHdTsRzC516Q-6oX3xWqcR55AVNXobBdRY'

function constructURL( action = "sendMessage") {
    return `https://api.telegram.org/bot${TOKEN}/${action}`
}

async function makeRequest(chat_id, text, action = "sendMessage") {

    let response = await fetch(constructURL(action), {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ chat_id, text })
    })
    response = await response.json()
    if (!response.ok) {
        console.error("Telegram api failure", response)
        throw response
    }
    return response.result
}

// !!!! Expensive Operation in cloudflare !!!!!
export async function sendDevMessage(msg, isInternal = false) {
    let roomId = isInternal ? Config.internalUser : CONFIG.telegram.devMessages;
    await makeRequest(roomId, JSON.stringify(msg, null))
}

export async function sendTextMessageToDev(msg) {
    await makeRequest(CONFIG.telegram.devMessages, msg)
}
