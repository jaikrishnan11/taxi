
import _ from 'lodash'

function getShortName(addressComponents) {
    let addressComponent = addressComponents.find(cmp => _.intersection(cmp.types, ["locality", "administrative_area_level_3"]).length > 0)
    if(!addressComponent){
      addressComponent = addressComponents[0]
    }
    return addressComponent.short_name
  }

function getStateCode(addressComponents) {
    let addressComponent = addressComponents.find(cmp => _.intersection(cmp.types, ["administrative_area_level_1"]).length > 0)
    if (!addressComponent) {
        addressComponent = addressComponents[0]
    }
    return addressComponent.short_name
}

export async function getPlaceDetails(placeIds = []) {
    let promises = [];
    let placeMap = {}
    for (let placeId of placeIds) {
        let requestData = new URLSearchParams({
            key: "AIzaSyDaaf3IpE8PdgC4YHTXrm17hHNkyTcHJNI",
            place_id: placeId,
            fields: "place_id,formatted_address,name,address_component,type,geometry"
          })
        promises.push(fetch("https://maps.googleapis.com/maps/api/place/details/json?"+requestData.toString()))
    }
    let responses = await Promise.all(promises)
    for (let idx = 0 ; idx < responses.length; idx++) {
        let response = responses[idx]
        if (response.status >= 400) {
            console.error("placeId fetch failed", await response.json())
            return;
        } else {
            let data = await response.json()
            let result = {
                stateCode: getStateCode(data.result.address_components),
                shortName: getShortName(data.result.address_components),
                fullAddress: data.result.formatted_address,
                placeId: data.result.place_id,
                geo: {
                    lat: data.result.geometry.location.lat, 
                    lng: data.result.geometry.location.lng, // For backwards compatibility not sure where we are using
                    lon: data.result.geometry.location.lng}
            }
            // Sometimes the placeId in the response might be different.
            placeMap[placeIds[idx]] = result
        }
    }
    return placeMap
}
