import { CONFIG } from '../config/index.js'
import utils from '../commons/utils.js'
import invicibleTestClient from '../../test/invicible-ocean-test-client.js'
import moment from 'moment';
import _ from 'lodash'


class Client {
    constructor() {
        this.baseUrl = "https://api.invincibleocean.com/invincible";
        this.vehicleUrl = `${this.baseUrl}/vehicleRcV6`;
        //this.apiSevaVehicleUrl = `https://api.apiseva.co.in/api/verification_pv2/rc_verify\?apikey\=KD4Y6ZANL5T72ISCGMP839H47F3J1D206BE0CXV1UBR58OQ9WA\&agent_code\=APISV10067\&client_order_id\=0000\&vehicle_number\=`;
        this.apiSevaVehicleUrl = `https://tjhw3ezqjdbsfuhsoegwsttsvq0marlu.lambda-url.ap-south-1.on.aws?vehicle_number\=`
        this.stolenVehicleUrl = `${this.baseUrl}/vehicleStolen`;
        this.licenceUrl = "https://api.invincibleocean.com/invincible/dlFastest";
        this.headers = {
            'Content-Type': 'application/json',
            'accept': 'application/json',
            // 'Referer': 'gogo cabs',
            // 'x-api-key': 'apclb_XDBwzEzFOekdz3eh6albeOp1ca6940c3'
            // 'secretKey': CONFIG.invicibleOcean.secretKey,
            // 'clientId': CONFIG.invicibleOcean.clientId
        };
        this.apiClubProxy = "https://jv63qxazxunn2pa6dgu4fv7vkm0xcuhc.lambda-url.ap-south-1.on.aws/api/apiclub/proxy"
        // this.apiClubVehicleUrl = "https://prod.apiclub.in/api/v1/rc_info"
        // this.apiClubLicenseUrl = "https://prod.apiclub.in/api/v1/fetch_dl"
        // this.apiClubLicenseUrl = "http://13.233.164.35:5000/forward"
    }

    async makePostRequest(url, data, requestHeaders) {
        try {
            console.log(url, data, requestHeaders)

            let response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(data),
                headers: _.isEmpty(requestHeaders) ? this.headers : requestHeaders
            })
            let status = response.status
            console.log("status", status);
            response = await response.json()
            console.log("received response", response)
            return response;
        } catch (error) {
            console.log(error)
            throw error;
        }
    }

    async makeGetRequest(url) {
        try {
            console.log(url)
            let response = await fetch(url, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                }

            })
            let status = response.status
            console.log("status", status);
            response = await response.json()
            console.log("received response", response)
            return response;
        } catch (error) {
            console.log(error)
            throw error;
        }
    }

    async getVehicleInfoV6(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }
        return await this.makePostRequest(this.vehicleUrl, {
            "vehicleNumber": parsedVehicleNumber
        });
    }

    async getVehicleDetailsFromAPIseva(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }

        let response = await this.makeGetRequest(`${this.apiSevaVehicleUrl}${parsedVehicleNumber}`);
        if (response.error_code != "SPC-200") {
            throw new Error("Error in fetching vehicle data from api seva")
        }
        return this.apiSevaToV6Converter(response)
    }

    async stolenVehicleDetails(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }
        return await this.makePostRequest(this.stolenVehicleUrl, {
            "vehicleNumber": parsedVehicleNumber
        });
    }

    async getDrivingLicense(licenseNumber, dateOfBirth) {
        return await this.makePostRequest(this.licenceUrl, {
            "dlNumber": licenseNumber,
            "dob": dateOfBirth
        });
    }

    async getLicenseFromApiClub(licenseNumber, dateOfBirth) {
        let payload = {
            "url": "https://prod.apiclub.in/api/v1/fetch_dl",
            "headers": {
                "accept": "application/json",
                "Content-Type": "application/json",
                "x-api-key": "apclb_XDBwzEzFOekdz3eh6albeOp1ca6940c3",
                "Referer": "gogo cabs"
            },
            "method": "POST",
            "data": {
                "dl_no": licenseNumber,
                "dob": moment(dateOfBirth, "YYYY-MM-DD").format("DD-MM-YYYY")
            }
        }
        let response = await this.makePostRequest(this.apiClubProxy, payload);
        // console.log(response)
        if (!_.isEmpty(response) && response.code == 200) {
            return this.apiClubToLicenseResponse(response)
        } else {
            throw new Error("Error in fetching license data from Parivahan")
        }
    }

    async getVehicleInfoFromApiClub(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }
        let payload = {
            "url": "https://prod.apiclub.in/api/v1/rc_info",
            "headers": {
                "accept": "application/json",
                "Content-Type": "application/json",
                "x-api-key": "apclb_XDBwzEzFOekdz3eh6albeOp1ca6940c3",
                "Referer": "gogo cabs"
            },
            "method": "POST",
            "data": {
                "vehicleId": vehicleNumber
            }
        }
        let response = await this.makePostRequest(this.apiClubProxy, payload);
        if (response.code != "200") {
            throw new Error("Error in fetching vehicle data from api club")
        }
        return this.apiClubToVehicleResponse(response)
    }

    apiClubToLicenseResponse(apiResponse) {
        return {
            "result": {
                "data": {
                    "gender": apiResponse?.response?.gender,
                    "temporary_address": apiResponse?.response?.temporary_address,
                    "vehicle_classes": _.map(apiResponse?.response?.vehicle_class, (vc) => {
                        return vc.cov
                    }),
                    "initial_doi": apiResponse?.response?.issue_date,
                    "permanent_address": apiResponse?.response?.permanent_address,
                    "client_id": "license_ErsmSqjtwxRtCqgvtRbN",
                    "father_or_husband_name": apiResponse?.response?.father_or_husband_name,
                    "profile_image": apiResponse?.response?.image,
                    "has_image": !_.isEmpty(apiResponse?.response?.image),
                    "ola_code": apiResponse?.response?.rto_code,
                    "state": apiResponse?.response?.state,
                    "permanent_zip": apiResponse?.response?.permanent_zip,
                    "less_info": false,
                    "citizenship": "",
                    "additional_check": [],
                    "license_number": apiResponse?.response?.license_number,
                    "temporary_zip": apiResponse?.response?.temporary_zip,
                    "ola_name": apiResponse?.response?.rto,
                    "transport_doi": "2021-12-14",
                    "dob": moment(apiResponse?.response?.dob, "DD-MM-YYYY").format("YYYY-MM-DD"),
                    "name": apiResponse?.response?.holder_name,
                    "transport_doe": "",
                    "blood_group": apiResponse?.response?.blood_group,
                    "current_status": null,
                    "doe": moment(apiResponse?.response?.valid_upto, "DD-MM-YYYY").format("YYYY-MM-DD"),
                    "doi": moment(apiResponse?.response?.valid_from, "DD-MM-YYYY").format("YYYY-MM-DD"),
                },
                "message": null,
                "message_code": apiResponse["status"],
                "success": true
            },
            "code": apiResponse["code"]
        }
    }

    apiClubToVehicleResponse(apiResponse) {
        return {
            "result": {
                "data": {
                    "regNo": apiResponse?.response?.license_plate,
                    "bodyType": apiResponse?.response?.body_type,
                    "vehicleTaxUpto": "",
                    "mobileNumber": null,
                    "vehicleManufacturingMonthYear": moment(apiResponse?.response?.manufacturing_date_formatted, "YYYY-MM").format("MM/YYYY"),
                    "type": apiResponse?.response?.fuel_type,
                    "grossVehicleWeight": apiResponse?.response?.gross_weight,
                    "puccNumber": apiResponse?.response?.pucc_number,
                    "vehicleNumber": apiResponse?.response?.license_plate,
                    "model": apiResponse?.response?.brand_model,
                    "normsType": apiResponse?.response?.norms,
                    "vehicleManufacturerName": apiResponse?.response?.brand_name,
                    "vehicleInsuranceUpto": moment(apiResponse?.response?.insurance_expiry, "YYYY-MM-DD").format("DD-MMM-YYYY"),
                    "nonUseFrom": null,
                    "partialData": true,
                    "rcExpiryDate": moment(apiResponse?.response?.fit_up_to, "YYYY-MM-DD").format("DD-MMM-YYYY"),
                    "permitValidFrom": "NA",
                    "isCommercial": "true", // TODO
                    "vehicleSeatCapacity": apiResponse?.response?.seating_capacity,
                    "blacklistDetails": "NA",
                    "chassis": apiResponse?.response?.chassis_number,
                    "vehicleInsuranceCompanyName": apiResponse?.response?.insurance_company,
                    "nationalPermitNumber": "NA",
                    "financed": apiResponse?.response?.is_financed,
                    "permitIssueDate": apiResponse?.response?.permit_issue_date,
                    "wheelbase": apiResponse?.response?.wheelbase,
                    "ownerCount": apiResponse?.response?.owner_count,
                    "nonUseTo": null,
                    "nationalPermitIssuedBy": apiResponse?.response?.national_permit_issued_by,
                    "nationalPermitUpto": apiResponse?.response?.national_permit_upto,
                    "status": apiResponse?.response?.rc_status,
                    "nocDetails": apiResponse?.response?.noc_details,
                    "permitValidUpto": apiResponse?.response?.permit_valid_upto,
                    "rcFinancer": apiResponse?.response?.financer,
                    "regAuthority": apiResponse?.response?.rto_name,
                    "regDate":  moment(apiResponse?.response?.registration_date, "YYYY-MM-DD").format("DD-MMM-YYYY"),
                    "vehicleCategory": apiResponse?.response?.category,
                    "vehicleStandingCapacity": apiResponse?.response?.standing_capacity,
                    "permitType": apiResponse?.response?.permit_type,
                    "ownerFatherName": apiResponse?.response?.father_name,
                    "presentAddress": apiResponse?.response?.present_address,
                    "engine": apiResponse?.response?.engine_number,
                    "vehicleInsurancePolicyNumber": apiResponse?.response?.insurance_policy,
                    "blacklistStatus": "NA",
                    "vehicleClass": apiResponse.class,
                    "vehicleSleeperCapacity": apiResponse?.response?.sleeper_capacity,
                    "permanentAddress": apiResponse?.response?.permanent_address,
                    "puccUpto": moment(apiResponse?.response?.pucc_upto, "YYYY-MM-DD").format("DD-MMM-YYYY"),
                    "owner": apiResponse?.response?.owner_name,
                    "vehicleColour": apiResponse?.response?.color,
                    "unladenWeight": apiResponse?.response?.unladen_weight,
                    "vehicleCubicCapacity": apiResponse?.response?.cubic_capacity,
                    "dbResult": true,
                    "permitNumber": apiResponse?.response?.permit_number,
                    "vehicleCylindersNo": apiResponse?.response?.cylinders,
                    "rcStandardCap": "NA",
                    "mmvResponse": null,
                    "nonUseStatus": null,
                    "statusAsOn": moment(apiResponse?.response?.latest_by, "YYYY-MM-DD").format("DD-MMM-YYYY")
                }
            },
            "code": apiResponse["code"]
        }
    }

    apiSevaToV6Converter(response) {
        let data = response.vehicleDetails.Data.result
        return {
            "code": 200,
            "result": {
                "data": {
                    "regNo": data?.rc_regn_no,
                    "vehicleClass": "",
                    "chassis": data?.rc_chasi_no,
                    "engine": data?.rc_eng_no,
                    "vehicleManufacturerName": data?.rc_maker_desc,
                    "model": data?.rc_maker_model,
                    "vehicleColour": "",
                    "type": data?.bancs_Fuel_Type,
                    "normsType": "",
                    "bodyType": data?.bancs_Body_Type,
                    "ownerCount": "",
                    "owner": data?.rc_owner_name,
                    "ownerFatherName": "",
                    "mobileNumber": null,
                    "status": data?.rc_status,
                    "statusAsOn": "",
                    "regAuthority": data?.rc_registered_at,
                    "regDate": data?.rc_regn_dt,
                    "vehicleManufacturingMonthYear": data?.rc_manu_month_yr,
                    "rcExpiryDate": moment(data?.rc_fit_upto).format("DD-MMM-YYYY"),
                    "vehicleTaxUpto": moment(data?.rc_fit_upto).format("DD-MMM-YYYY"),
                    "vehicleInsuranceCompanyName": data?.rc_insurance_comp,
                    "vehicleInsuranceUpto": moment(data?.rc_insurance_upto).format("DD-MMM-YYYY"),
                    "vehicleInsurancePolicyNumber": "",
                    "rcFinancer": "",
                    "presentAddress": "",
                    "permanentAddress": data?.rc_permanent_address,
                    "vehicleCubicCapacity": data?.rc_cubic_cap,
                    "grossVehicleWeight": data?.rc_gvw,
                    "unladenWeight": "",
                    "vehicleCategory": data?.rc_vh_class_desc,
                    "rcStandardCap": "",
                    "vehicleCylindersNo": "",
                    "vehicleSeatCapacity": data?.rc_seat_cap,
                    "vehicleSleeperCapacity": "",
                    "vehicleStandingCapacity": "",
                    "wheelbase": "",
                    "vehicleNumber": data?.rc_regn_no,
                    "puccNumber": "",
                    "puccUpto": "",
                    "blacklistStatus": false,
                    "blacklistDetails": data?.rc_blacklist_status,
                    "permitIssueDate": "",
                    "permitNumber": "",
                    "permitType": "",
                    "permitValidFrom": "",
                    "permitValidUpto": "",
                    "nonUseStatus": "",
                    "nonUseFrom": "",
                    "nonUseTo": "",
                    "nationalPermitNumber": "",
                    "nationalPermitUpto": "",
                    "nationalPermitIssuedBy": "",
                    "isCommercial": data?.rc_vehicle_type == "CV",
                    "nocDetails": "",
                    "dbResult": false,
                    "partialData": false,
                    "mmvResponse": null,
                    "financed": false
                }
            }
        }
    }
}

let invicibleClient = new Client();

export function initializeInvicible(config) {
    if (config.isProduction == false) {
        console.log("Replacing invicibleClient")
        invicibleClient = invicibleTestClient
    }
}

export { invicibleClient as default }

// console.log(new Client().getVehicleInfoV6("TN09CV6881").then((data) => {
//     console.log(data.result)
// }).catch((err) => {
//     console.log(err)
// }))