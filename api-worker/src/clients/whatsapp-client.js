import { CONFIG } from '../config'
import {sendDevMessage} from './telegram-client'

class WhatsappClient {
	constructor() {
	}

	async sendOtp(toPhoneNumber, otp) {
		let WhatappConfig = CONFIG.whatsapp
		this.url = `https://graph.facebook.com/v16.0/${WhatappConfig.phoneNoId}/messages`
		let payload = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to":  `91${this.removeCountryCode(toPhoneNumber)}`,
      "type": "template",
      "template": {
        "name": "phone_verification",
        "language": {
          "code": "en"
        },
        "components": [
          {
            "type": "body",
            "parameters": [{
              "type": "text",
              "text": otp
            }]
          }, {
            "type": "button",
            "sub_type": "url",
            "index": 0,
            "parameters": [
              {
                "type": "text",
                "text": otp
              }
            ]
          }]
      }
    }
    let config = {
      method: 'POST',
      maxBodyLength: Infinity,
      url: this.url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${WhatappConfig.token}`
      },
      data: JSON.stringify(payload)
    };

    await this.makeRequest(config)
	}

	async makeRequest(config) {
		try {
			if (CONFIG.isProduction == false) {
				console.debug("Not sending whatsapp notification")
				return true;
			}
			let responseObj = await fetch(config.url, {
				method: config.method,
				headers: config.headers,
				body: config.data
			})
			let response = await responseObj.json()
			console.log(JSON.stringify(response))
			let statusCode = responseObj.status
			if(statusCode != 200){
				let headerErrorMessage = responseObj.headers["www-authenticate"]
				let errorMsgToLog = `[makeRequest] ${statusCode} - ${headerErrorMessage}`
				console.error(errorMsgToLog)
				await sendDevMessage({errorMsgToLog})
			}
			return response.data
		} catch (err) {
			await sendDevMessage({msg: "error when sending OTP", err})
			throw err
		}
	}

	removeCountryCode(phoneNumber) {
		const countryCode = "+91";
		const countryCode2 = "91";
		const formattedNumber = phoneNumber.trim().replace(/\s+/g, ""); // remove whitespace and format number

		if (formattedNumber.startsWith(countryCode)) {
			return formattedNumber.substr(countryCode.length);
		} else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode2)) {
			return formattedNumber.substr(countryCode2.length);
		} else {
			return formattedNumber;
		}
	}
}

let whatsappClient = new WhatsappClient()
export { whatsappClient as default }