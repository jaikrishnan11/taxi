import { CONFIG } from '../config/index.js';

const INDICES = {
  Rides: "table-rides",
  CabVendors: "table-cab-vendors",
  Drivers: "table-cab-drivers",
  Vehicles: "table-cab-vehicles",
  GenericTableDrivers: "drivers_generic",
  GenericTable: "generic-table",
  WalletLedger: "wallet-ledger",
  Location: "driver-location"
};

class Client {
  async request(method, path, body = null) {
    const esConfig = CONFIG.elasticSearchConfig;
    const auth = `${esConfig.credentials.username}:${esConfig.credentials.password}`;
    const baseUrl = `${esConfig.protocol}://${auth}@${esConfig.host}`;
    console.log("baseUrl", baseUrl)
    const url = `${baseUrl}/${path}`;
    const options = {
      method,
      headers: {
        'Content-Type': 'application/json'
      }
    };
    if (body) {
      options.body = JSON.stringify(body);
    }

    const response = await fetch(url, options);
    if (!response.ok) {
      const errorText = await response.text();
      throw new Error(`Elasticsearch request failed: ${response.statusText}\n${errorText}`);
    }
    return response.json();
  }

  async updateLocation(documentId, document) {
    const path = `${INDICES.Location}/_doc/${documentId}?refresh=wait_for`;
    let response =  await this.request('PUT', path, document);
    console.log("response", response)
    return response
  }
}

const esClient = new Client();
export { esClient as default };
