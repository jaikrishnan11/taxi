import {SQSClient, SendMessageCommand} from "@aws-sdk/client-sqs"

import { CONFIG } from "../config";
import { SQSMessageType } from "../config/constants"

const GeneralSQS  = "https://sqs.ap-south-1.amazonaws.com/849398159878/GeneralSQS"
const RideProcessor = "https://sqs.ap-south-1.amazonaws.com/849398159878/gogo-ride-confirm.fifo"
const LocationUpdates = "https://sqs.ap-south-1.amazonaws.com/849398159878/location-updates"

class SqsClient {
    constructor(config) {
        this.client = new SQSClient(config)
    }

    async pushDeviceUpdateMsg(msg = {}) {
        var params = {
            MessageBody: JSON.stringify(msg),
            QueueUrl: GeneralSQS
          };
        //TODO : Move the duplicate below to a function
          if(! CONFIG.isProduction ){
            return Promise.resolve()
        }
          return await  this.client.send(new SendMessageCommand(params))
    }

    async sendLocationUpdates(document) {
        var params = {
            MessageBody: JSON.stringify({
                type: SQSMessageType.LocationUpdate,
                payload: document
            }),
            QueueUrl: LocationUpdates
        }
        return await this.send(params)
    }

    async publishRideAssignment(msg = {}){
        if(!msg.bookingId)
                throw "[publishRideAssignment] bookingId should not be null"
        var params = {
            MessageBody: JSON.stringify({
                type: SQSMessageType.RideAssigned,
                payload: msg
            }),
            QueueUrl: GeneralSQS
        }
        return await this.send(params)
    }

    async publishRide(msg) {
        var params = {
            MessageBody: JSON.stringify({
                "version": "v2",
                "payload": msg
            }),
            MessageGroupId: "bookinIdV2", 
            MessageDeduplicationId: msg.bookingId,
            QueueUrl: RideProcessor
        }
        if(! CONFIG.isProduction ){
            return Promise.resolve()
        }
        return await this.client.send(new SendMessageCommand(params))
    }

    async rideClosed(bookingId) {
        var params = {
            MessageBody: JSON.stringify({
                type: SQSMessageType.RideClosed,
                payload: {
                    bookingId
                }
            }),
            QueueUrl: GeneralSQS,
            DelaySeconds: 5*60
        }
        if(! CONFIG.isProduction ){
            return Promise.resolve()
        }
        return await this.client.send(new SendMessageCommand(params))
    }

    async cabVendorV2Created(cabVendorId, additionalDetails = {}) {
        var params = {
            MessageBody: JSON.stringify({
                type: SQSMessageType.CabVendorCreated,
                payload: {
                    cabVendorId,
                    ...additionalDetails
                }
            }),
            QueueUrl: GeneralSQS
        }
        if(! CONFIG.isProduction) {
            return Promise.resolve()
        }
        return await  this.client.send(new SendMessageCommand(params))
    }

    async publishUpdatedRideByAdmin(ride) {
        var params = {
            MessageBody: JSON.stringify({
                "version": "v2",
                "updatedRide": true,
                "payload": {
                    "bookingId": ride.bookingId
                }
            }),
            MessageGroupId: "bookinIdV2", 
            MessageDeduplicationId: "updatedRide" + ride.bookingId,
            QueueUrl: RideProcessor
        }
        if(! CONFIG.isProduction ){
            return Promise.resolve()
        }
        return await this.client.send(new SendMessageCommand(params))
    }

    async sendVendorApprovalNotification(cabVendorId, additionalDetails = {}) {
        var params = {
            MessageBody: JSON.stringify({
                type: SQSMessageType.VendorApprovalPendingNotification,
                payload: {
                    cabVendorId,
                    ...additionalDetails
                }
            }),
            QueueUrl: GeneralSQS
        }
        if(! CONFIG.isProduction ){
            return Promise.resolve()
        }
        return await  this.client.send(new SendMessageCommand(params))
    }

    async send(params){
        if(! CONFIG.isProduction ){
            return Promise.resolve()
        }
        return await this.client.send(new SendMessageCommand(params))
    }
}

let sqsClient = new SqsClient(CONFIG.sqs);

export { sqsClient as default }

export function initializeSqs(config) {
    sqsClient = new SqsClient(config)
}