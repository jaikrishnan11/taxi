import { DynamoDBClient, ConditionalCheckFailedException } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, PutCommand, GetCommand, BatchGetCommand, TransactWriteCommand, QueryCommand, UpdateCommand, ScanCommand, DeleteCommand } from "@aws-sdk/lib-dynamodb"
import _ from "lodash";

import { CONFIG } from '../config'
import { ErrorCodes, WallerLedgerAction, BookingSource, VendorStatus, VendorVehicleStatus, PaymentGateway, RideStatus } from "../config/constants";
import moment from "moment";

const ADD_MONEY_STATUS = {
  INITIATED: "INITIATED",
  COMPLETED: "COMPLETED",
  FAILED: "FAILED"
}

const NAMESPACES = {
  VEHICLES: "vehicles",
  DRIVERS: "drivers",
  CUSTOMER_ACCOUNT: "CUSTOMER_ACCOUNT",
  DRIVER_ADDITION_PHONE_NO_VERIFICATION: "DRIVER_ADDITION_PHONE_NO_VERIFICATION",
  VENDOR_CREATION_PHONE_NO_VERIFICATION: "VENDOR_CREATION_PHONE_NO_VERIFICATION"
}

class Dynamo {
  constructor(config) {
    let client = new DynamoDBClient({
      region: config.region,
      endpoint: config.endpoint,
      credentials: config.credentials,
      maxAttempts: 10,
      maxRetries: 10
    })
    this.ddbClient = DynamoDBDocumentClient.from(client, {
      "marshallOptions": {
        "convertEmptyValues": false,
        "removeUndefinedValues": true
      }
    });
  }

  async getNextBookingId() {
    let data = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.genericTable,
      Key: {
        namespace: "BOOKING_ID",
        primaryKey: "BOOKING_ID"
      },
      UpdateExpression: "ADD #counter :incr",
      ExpressionAttributeNames: {
        "#counter": "counter"
      },
      ExpressionAttributeValues: {
        ":incr": 1
      },
      ReturnValues: "UPDATED_NEW"
    }))
    return data.Attributes.counter.toString().padStart(6, '0');
  }

  async addAuditEntry(namespace, primaryKey, details) {
    let data = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.genericTable,
      Key: {
        namespace,
        primaryKey
      },
      UpdateExpression: `SET events = list_append(if_not_exists(events, :empty_list), :new_value)`,
      ExpressionAttributeValues: {
        ':new_value': [details],
        ':empty_list': []
      }
    }))
    return data;
  }

  async createCustomerAccount(phoneNo, customerDetails) {
    try {
      let data = await this.ddbClient.send(new PutCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Item: {
          namespace: NAMESPACES.CUSTOMER_ACCOUNT,
          primaryKey: phoneNo,
          ...customerDetails
        },
        ReturnValuesOnConditionCheckFailure: "ALL_OLD",
        ReturnValues: "ALL_OLD",
        ConditionExpression: "attribute_not_exists(primaryKey) and attribute_not_exists(namespace)"
      }))
      return data.Attributes
    } catch (err) {
      if (err instanceof ConditionalCheckFailedException) {
        console.log({msg: "Account already exists", phoneNo})
        return err.Item
      }
      throw err;
    }
  }

  async getCabVendorByPhoneNoV2(phoneNumber, lastEvaluatedKey) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      FilterExpression: "#version = :version",
      ExpressionAttributeNames: {
        "#version": "version"
      },
      ExpressionAttributeValues: {
        ":version": "v2"
      },
      ProjectionExpression: "profile",
      LastEvaluatedKey: lastEvaluatedKey
    }))
    console.log("request", phoneNumber, lastEvaluatedKey)
    console.log("items", JSON.stringify(data.Items))
    let items = data.Items.filter(cabVendor => {
      if (Object.keys(cabVendor).length === 0) {
        return false
      }
      return cabVendor.profile.phoneNo == phoneNumber
    })
    if (data.lastEvaluatedKey) {
      console.log("[getCabVendorByPhoneNoV2] Fetching next set")
      items.push(...(await this.getCabVendorByPhoneNoV2(phoneNumber, lastEvaluatedKey)))
    }
    return items
  }

  async getAllCabDriversWithLicenseOrPhoneNumber(license, phoneNumber, lastEvaluatedKey) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      FilterExpression: "#version = :version",
      ExpressionAttributeNames: {
        "#version": "version"
      },
      ExpressionAttributeValues: {
        ":version": "v2"
      },
      ProjectionExpression: "drivers",
      LastEvaluatedKey: lastEvaluatedKey
    }))
    console.log("request", license, phoneNumber, lastEvaluatedKey)
    console.log("items", JSON.stringify(data.Items))
    let items = data.Items.filter(cabVendor => {
      if (Object.keys(cabVendor).length === 0) {
        return false
      }
      else if (!_.isEmpty(license)) {
        return Object.values(cabVendor.drivers).find(driver => driver.licenseNumber == license)
      } else {
        return Object.values(cabVendor.drivers).find(driver => driver.phoneNumber == phoneNumber)
      }
    })
    if (data.lastEvaluatedKey) {
      console.log("[getAllCabDriversWithLicenseOrPhoneNumber] Fetching next set")
      items.push(...(await this.getAllCabDriversWithLicenseOrPhoneNumber(license, phoneNumber, lastEvaluatedKey)))
    }
    return items
  }

  async getAllCabVehiclesWithVehicleNumber(vehicleNumber, lastEvaluatedKey) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      ProjectionExpression: "vehicles",
      LastEvaluatedKey: lastEvaluatedKey
    }))
    // No need to check the status, even if its black listed we should not add it again.
    let items = data.Items.filter(cabVendor => {
      return Object.values(cabVendor.vehicles).find(driver => driver.vehicleRegNo == vehicleNumber)
    })
    if (data.lastEvaluatedKey) {
      console.log("[getAllCabVehiclesWithVehicleNumber] Fetching next set")
      items.push(...(await this.getAllCabVehiclesWithVehicleNumber(vehicleNumber, lastEvaluatedKey)))
    }
    return items
  }

  async putRazorpayOrder(cabVendorId, razorpayOrder) {
    let addMoneyOperation = {
      cabVendorId: cabVendorId,
      // Converting unix timestamp to Date object. need to convert to milli seconds
      createdDate: new Date(razorpayOrder.created_at * 1000).toISOString(),
      action: WallerLedgerAction.AddMoney,
      opDetails: {
        status: ADD_MONEY_STATUS.INITIATED,
        paymentGateway: PaymentGateway.Razorpay,
        orderId: razorpayOrder.id,
        amount: razorpayOrder.amount,
        ...razorpayOrder.notes
      }
    }
    await this.ddbClient.send(new PutCommand({
      TableName: CONFIG.dynamodb.tables.walletLedger,
      Item: addMoneyOperation
    }))
  }

  async putIppopayOrder(cabVendorId, ippopayOrder) {
    let addMoneyOperation = {
      cabVendorId: cabVendorId,
      // Converting unix timestamp to Date object. need to convert to milli seconds
      createdDate: ippopayOrder.orderCreatedDate,
      action: WallerLedgerAction.AddMoney,
      opDetails: {
        status: ADD_MONEY_STATUS.INITIATED,
        paymentGateway: PaymentGateway.Ippopay,
        orderId: ippopayOrder.orderId,
        amount: ippopayOrder.amount,
        ...ippopayOrder.notes
      }
    }
    await this.ddbClient.send(new PutCommand({
      TableName: CONFIG.dynamodb.tables.walletLedger,
      Item: addMoneyOperation
    }))
  }

  async getAddMoneyActionDetails(cabVendorId, createdDate) {
    let data = await this.ddbClient.send(new GetCommand({
      TableName: CONFIG.dynamodb.tables.walletLedger,
      Key: {
        cabVendorId,
        createdDate
      }
    }))
    return data.Item
  }

  async createRide(rideDetails) {
    rideDetails = {
      version: "v1",
      ...rideDetails
    }
    let ridePromise =  this.ddbClient.send(new PutCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      Item: rideDetails,
      //TODO: We need to regenerate the bookingId and try again 
      ConditionExpression: "attribute_not_exists(bookingId)"
    }))
    let customerAccountUpdatePromise;
    if (rideDetails?.bookingSource?.source == BookingSource.CustomerApp) {
      customerAccountUpdatePromise = this.ddbClient.send(new UpdateCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: NAMESPACES.CUSTOMER_ACCOUNT,
          primaryKey: rideDetails.bookingSource.sourceId
        },
        UpdateExpression: "ADD rides.#pending :bookingIdSet",
        ExpressionAttributeNames: {
          "#pending": "pending"
        },
        ExpressionAttributeValues: {
          ":bookingIdSet": new Set([rideDetails.bookingId])
        },
        ReturnValues: "ALL_NEW"
        }
      ))
    }
    await Promise.all([ridePromise, customerAccountUpdatePromise])
  } 

  async createCabVendor(cabVendor) {
    cabVendor = {
      version: "v1",
      rides: {

      },
      devices: {
        cabVendorDevice: {
          deviceToken: ""
        }
      },
      meta: {},
      stats: {
        totalEarning: 0,
        rides: {
          pending: 0,
          onGoing: 0,
          completed: 0,
          cancelledByVendor: 0,
          cancelledByCustomer: 0
        }
      },
      wallet: {
        total: 0 // VERY IMPORTANT !!! ==> this is in PAISE
      },
      createdDate: new Date().toISOString(),
      vendorStatus: VendorStatus.UnApproved,
      ...cabVendor
    }

    try {
      let data = await this.ddbClient.send(new PutCommand({
        TableName: CONFIG.dynamodb.tables.cabVendors,
        ConditionExpression: "attribute_not_exists(cabVendorId)",
        Item: cabVendor,
        ReturnValues: "ALL_OLD"
      }))
    } catch (err) {
      if (err.name == "ConditionalCheckFailedException") {
        throw { response: { errorCode: ErrorCodes.ResourceExists }, statusCode: 400 }
      }
      throw err;
    }
    return cabVendor
  }

  async createCabVendorV2(cabVendor) {
    cabVendor = {
      version: "v2",
      rides: {},
      drivers: {},
      vehicles: {},
      meta: {},
      devices: {
        cabVendorDevice: {
          deviceToken: ""
        }
      },
      stats: {
        totalEarning: 0,
        rides: {
          pending: 0,
          onGoing: 0,
          completed: 0,
          cancelledByVendor: 0,
          cancelledByCustomer: 0
        }
      },
      wallet: {
        total: 0 // VERY IMPORTANT !!! ==> this is in PAISE
      },
      createdDate: new Date().toISOString(),
      vendorStatus: VendorStatus.Approved,
      ...cabVendor
    }
    try {
      let data = await this.ddbClient.send(new PutCommand({
        TableName: CONFIG.dynamodb.tables.cabVendors,
        ConditionExpression: "attribute_not_exists(cabVendorId)",
        Item: cabVendor,
        ReturnValues: "ALL_OLD"
      }))
    } catch (err) {
      if (err.name == "ConditionalCheckFailedException") {
        throw { response: { errorCode: ErrorCodes.ResourceExists }, statusCode: 400 }
      }
      throw err;
    }
    return cabVendor
  }


  async updateCabVendorLocation(cabVendorId, locationOwner, locationData) {
    try {
      let templocationData = {}
      templocationData[locationOwner] = locationData
      console.log(templocationData)
      let data = await this.ddbClient.send(new UpdateCommand({
        TableName: CONFIG.dynamodb.tables.cabVendors,
        Key: {
          cabVendorId
        },
        UpdateExpression: "SET #location = :locationData",
        ExpressionAttributeNames: {
          "#location": "location",
          "#locationOwner": locationOwner,
          "#locationTime": "locationTime"
        },
        ExpressionAttributeValues: {
          ":locationData": templocationData,
          ":locationTime": locationData.locationTime
        },
        //ReturnValues: "ALL_NEW",
        ConditionExpression: "attribute_exists(cabVendorId) AND (attribute_not_exists(#location.#locationOwner.#locationTime) OR #location.#locationOwner.#locationTime < :locationTime)",
        ReturnValuesOnConditionCheckFailure: "ALL_OLD"
      }))
      //      console.log("Success", data.Attributes);
    } catch (err) {
      console.error(err.message)
      if (err.name == "ConditionalCheckFailedException") {
        throw { response: { errorCode: ErrorCodes.LocationInfoStale }, statusCode: 400 }
      }
      throw err
    }
  }

  // Note: This will overwrite the driver info. Merge all driver info before calling method !!!
  async updateDriverInfo(cabVendorId, drivers) {
    let ExpressionAttributeValues = { ":driverValue": drivers }
    let UpdateExpression = "SET drivers = :driverValue"

    let data = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      ReturnValues: "ALL_NEW",
      Key: {
        cabVendorId
      },
      UpdateExpression,
      ExpressionAttributeValues
    }))
    return data.Attributes;
  }

  async updateBookingDetails(bookingId, bookingDetails) {
    let ExpressionAttributeValues = { ":bookingDetails": bookingDetails }
    let UpdateExpression = "SET bookingDetails = :bookingDetails"

    let data = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      ReturnValues: "ALL_NEW",
      Key: {
        bookingId
      },
      UpdateExpression,
      ExpressionAttributeValues
    }))
    return data.Attributes;
  }  

  // Note: This will overwrite the vehicle info. Merge all vehicle info before calling method !!!
  async updateVehicleInfo(cabVendorId, vehicles) {
    let ExpressionAttributeValues = { ":vehicleValue": vehicles }
    let UpdateExpression = "SET vehicles = :vehicleValue"

    let data = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      ReturnValues: "ALL_NEW",
      Key: {
        cabVendorId
      },
      UpdateExpression,
      ExpressionAttributeValues
    }))
    return data.Attributes;
  }

  async getCabVendorsById(vendorId, projectionExpression, stronglyConsistentRead = false) {
    projectionExpression = projectionExpression || "profile, rides, wallet, vendorId, vehicles, stats, vendorStatus, drivers, version, meta, externalId"
    let data = await this.ddbClient.send(new GetCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      Key: {
        cabVendorId: vendorId
      },
      ProjectionExpression: projectionExpression,
      ConsistentRead: stronglyConsistentRead
    }))
    return data.Item
  }

  async getCabVendorAllData(cabVendorId) {
    let data = await this.ddbClient.send(new GetCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      Key: { cabVendorId },
      ConsistentRead: true
    }))
    return data.Item
  }

  async getCabVendorsByIds(vendorIds) {
    const params = {
      RequestItems: {
        [CONFIG.dynamodb.tables.cabVendors]: {
          Keys: vendorIds.map(id => { return { cabVendorId: id } }),
        },
      },
    };
    let data = await this.ddbClient.send(new BatchGetCommand(params))
    return data.Responses[CONFIG.dynamodb.tables.cabVendors]
  }

  async getWalletTransactionsById(vendorId) {
    const params = {
      TableName: CONFIG.dynamodb.tables.walletLedger,
      KeyConditionExpression: "cabVendorId = :pk",
      ExpressionAttributeValues: {
        ":pk": vendorId,
      },
      Limit: 20,
      ScanIndexForward: false,
    };

    const command = new QueryCommand(params);
    const response = await this.ddbClient.send(command);
    return response.Items;
  }

  async getRideByBookingIds(bookingIds = []) {
    const params = {
      RequestItems: {
        [CONFIG.dynamodb.tables.rides]: {
          Keys: bookingIds.map(id => { return { bookingId: id } }),
          //     ProjectionExpression: "ATTRIBUTE_NAME",
        },
      },
    };
    let data = await this.ddbClient.send(new BatchGetCommand(params))
    if (Object.keys(data.UnprocessedKeys).length > 0) {
      console.error("Unable to fetch few bookingIds ", data.UnprocessedKeys)
    }
    return data.Responses.rides
  }

  async getCabVendorByPhoneNos(phoneNumbers = [], lastEvaluatedKey) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      FilterExpression: "contains(:phoneNos, profile.#phoneNo)",
      LastEvaluatedKey: lastEvaluatedKey,
      ExpressionAttributeValues: {
        ":phoneNos": phoneNumbers
      },
      ExpressionAttributeNames: {
        "#phoneNo": "phoneNo"
      }
    }))
    let items = []
    items.push(...data.Items)
    if (data.lastEvaluatedKey) {
      console.log("[getCabVendorByPhoneNos] Fetching next set")
      items.push(...(await this.getCabVendorByPhoneNos(phoneNumbers, lastEvaluatedKey)))
    }
    return items
  }

  async getCabVendorByEmailIds(emailIds = [], lastEvaluatedKey) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      FilterExpression: "contains(:emailIds, profile.#email)",
      LastEvaluatedKey: lastEvaluatedKey,
      ExpressionAttributeValues: {
        ":emailIds": emailIds
      },
      ExpressionAttributeNames: {
        "#email": "email"
      }
    }))
    let items = []
    items.push(...data.Items)
    if (data.lastEvaluatedKey) {
      console.log("[getCabVendorByEmailIds] Fetching next set")
      items.push(...(await this.getCabVendorByEmailIds(emailIds, lastEvaluatedKey)))
    }
    return items
  }

  async getCabVendorByRegNo(regNo, lastEvaluatedKey) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      FilterExpression: "attribute_exists(vehicles.#regNo)",
      LastEvaluatedKey: lastEvaluatedKey,
      ExpressionAttributeNames: {
        "#regNo": regNo
      }
    }))
    let items = []
    items.push(...data.Items)
    if (data.lastEvaluatedKey) {
      console.log("[getCabVendorByRegNos] Fetching next set")
      items.push(...(await this.getCabVendorByRegNo(regNo, lastEvaluatedKey, lastEvaluatedKey)))
    }
    return items
  }

  async getRideByBookingId(bookingId, returnAllAttributes = false) {
    let projection = returnAllAttributes ? null : "bookingId, rideStatus, bookingDetails, priceDetails";
    let data = await this.ddbClient.send(new GetCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      Key: {
        bookingId
      },
      ProjectionExpression: projection
    }))
    return data.Item
  }

  async getVendorAndRideById(cabVendorId, bookingId) {
    const params = {
      RequestItems: {
        [CONFIG.dynamodb.tables.rides]: {
          Keys: [{ bookingId }],
          //  ProjectionExpression: "bookingId, rideStatus, bookingDetails, priceDetails" 
        },
        [CONFIG.dynamodb.tables.cabVendors]: { Keys: [{ cabVendorId }] }
      },
    };

    let data = await this.ddbClient.send(new BatchGetCommand(params))
    //TODO: Handle this gracefully. What if cabVendor / ride were not fetched ?
    return {
      cabVendor: data.Responses[CONFIG.dynamodb.tables.cabVendors][0],
      ride: data.Responses[CONFIG.dynamodb.tables.rides][0]
    }
  }

  async closeRideUpdate(bookingId, updatedRateCard, ride) {
    if (!updatedRateCard) {
      throw "UpdatedRateCard should not be null"
    }
    try {
      let response = await this.ddbClient.send(new UpdateCommand({
        TableName: CONFIG.dynamodb.tables.rides,
        Key: { bookingId },
        UpdateExpression: "SET priceDetails.rateCard = :updatedRateCard, bookingDetails.rideCloseTime = :rideCloseTime, bookingDetails.actualDistanceCovered = :actualDistanceCovered",
        ConditionExpression: "rideStatus = :rideStatus",
        ReturnValues: "ALL_NEW",
        ExpressionAttributeValues: {
          ":updatedRateCard": updatedRateCard,
          ":rideStatus": RideStatus.OnGoing,
          ":rideCloseTime": moment().toISOString(),
          ":actualDistanceCovered": ride.bookingDetails.actualDistanceCovered
        }
      }))
      return response.Attributes
    } catch (err) {
      if (isConditionExpressionFailed(err)) {
        throw { response: { errorCode: ErrorCodes.InvalidRideStatus, message: "Ride status is not ongoing" } }
      }
    }
  }

  async updateDeviceDetails(cabVendorId, deviceDetails) {
    try {
      await this.ddbClient.send(new UpdateCommand({
        TableName: CONFIG.dynamodb.tables.cabVendors,
        Key: {
          cabVendorId
        },
        UpdateExpression: "SET devices.cabVendorDevice.#deviceToken = :deviceToken",
        ConditionExpression: "cabVendorId = :cabVendorId",
        ExpressionAttributeValues: {
          ":deviceToken": deviceDetails.deviceToken,
          ":cabVendorId": cabVendorId
        },
        ExpressionAttributeNames: {
          "#deviceToken": "deviceToken"
        }
      }))
    } catch (err) {
      if (err.name == "ConditionalCheckFailedException") {
        throw { response: { errorCode: ErrorCodes.ResourceNotFound }, statusCode: 400 }
      }
      throw err
    }
  }

  async updateCabVendorMetaData(cabVendorId, meta) {
    try {
      let expressionAttributeValues = { ":cabVendorId": cabVendorId }
      let setExpression = []
      if (meta.forceV2Migration != undefined) {
        setExpression.push(`#meta.forceV2Migration=:forceV2Migration`)
        expressionAttributeValues[":forceV2Migration"] = meta.forceV2Migration
      }
      if (meta.shadowban) {
        setExpression.push(`#meta.shadowban=:shadowban`)
        expressionAttributeValues[":shadowban"] = meta.shadowban
      }
      let response = await this.ddbClient.send(new UpdateCommand({
        TableName: CONFIG.dynamodb.tables.cabVendors,
        Key: { cabVendorId },
        ConditionExpression: "cabVendorId = :cabVendorId",
        UpdateExpression: `SET ${setExpression.join(",")}`,
        ExpressionAttributeValues: expressionAttributeValues,
        ReturnValues: "ALL_NEW",
        ExpressionAttributeNames: {
          "#meta": "meta"
        }
      }))
      return response.Attributes
    } catch (err) {
      if (err.name == "ConditionalCheckFailedException") {
        throw { response: { errorCode: ErrorCodes.ResourceNotFound }, statusCode: 400 }
      }
      throw err
    }
  }

  async updateVendorStatus(cabVendorId, vendorStatus, vehicleStatus) {
    let query = [];
    let ExpressionAttributeValues = { ":cabVendorId": cabVendorId }
    if (_.isEmpty(vehicleStatus) == false && Object.keys(vehicleStatus).length > 0) {
      Object.keys(vehicleStatus).forEach((vehicleRegNo, idx) => {
        if (!Object.values(VendorVehicleStatus).includes(vehicleStatus[vehicleRegNo])) {
          throw "vehicle status invalid"
        }
        query.push(`vehicles.${vehicleRegNo}.vehicleStatus = :vehicleStatus${idx}`);
        ExpressionAttributeValues[`:vehicleStatus${idx}`] = vehicleStatus[vehicleRegNo]
      })
    }
    if (_.isEmpty(vendorStatus) == false) {
      if (!Object.values(VendorStatus).includes(vendorStatus)) {
        throw "vendorStatus invalid"
      }
      query.push(` vendorStatus = :vendorStatus`)
      ExpressionAttributeValues[":vendorStatus"] = vendorStatus;
    }

    console.log(query, ExpressionAttributeValues)
    let data = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      ReturnValues: "ALL_NEW",
      Key: {
        cabVendorId
      },
      UpdateExpression: `SET ${query.join(',')}`,
      ConditionExpression: "cabVendorId = :cabVendorId",
      ExpressionAttributeValues
    }))
    return data.Attributes;
  }

  //This is a expensive query
  async getCabVendorByStatus(vendorStatus, lastEvaluatedKey = null) {
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      FilterExpression: "vendorStatus = :vendorStatus",
      //   ExclusiveStartKey: lastEvaluatedKey,
      ExpressionAttributeValues: {
        ":vendorStatus": vendorStatus
      }
    }))
    return {
      cabVendors: data.Items,
      lastEvaluatedKey: data.LastEvaluatedKey
    }
  }


  async dumpTable(tableName, inFull = true, lastEvaluatedKey) {
    let items;
    let data = await this.ddbClient.send(new ScanCommand({
      TableName: tableName,
      ExclusiveStartKey: lastEvaluatedKey
    }))
    items = data.Items
    if (data.LastEvaluatedKey && inFull) {
      items.push(...((await this.dumpTable(tableName, inFull, data.LastEvaluatedKey)).items))
    }
    return { items, lastEvaluatedKey: data.LastEvaluatedKey };
  }

  async queryRidesByStatus(rideStatus, projectionExpression = null, lastEvaluatedKey = undefined) {
    let data = await this.ddbClient.send(new QueryCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      IndexName: CONFIG.dynamodb.tables.ridesStatusIndex,
      KeyConditionExpression: "rideStatus = :rideStatus",
      ProjectionExpression: projectionExpression,
      ExclusiveStartKey: lastEvaluatedKey,
      ExpressionAttributeValues: {
        ":rideStatus": rideStatus
      }
    }))
    //TODO: Bug Handle lastEvaluated key
    return { rides: data.Items, lastEvaluatedKey: data.LastEvaluatedKey }
  }

  async startRideByCabVendor(bookingId, cabVendorId) {
    try {
      let data = await this.ddbClient.send(new TransactWriteCommand({
        TransactItems: [{
          Update: {
            TableName: CONFIG.dynamodb.tables.rides,
            ReturnValuesOnConditionCheckFailure: "ALL_OLD",
            Key: { bookingId },
            UpdateExpression: "SET rideStatus=:newRideStatus",
            ConditionExpression: "rideStatus=:rideStatus AND cabVendor.cabVendorId=:cabVendorId",
            ExpressionAttributeValues: {
              ":rideStatus": RideStatus.Assigned,
              ":cabVendorId": cabVendorId,
              ":newRideStatus": RideStatus.OnGoing
            }
          }
        }, {
          Update: {
            TableName: CONFIG.dynamodb.tables.cabVendors,
            ReturnValuesOnConditionCheckFailure: "ALL_OLD",
            Key: {
              cabVendorId
            },
            UpdateExpression: "ADD stats.#rides.#onGoing :onGoingIncrement, stats.#rides.#pending :pendingIncrement, #rides.#onGoing :rideSet DELETE #rides.#confirmed :rideSet",
            ExpressionAttributeValues: {
              ":onGoingIncrement": 1,
              ":pendingIncrement": -1,
              ":requiredOnPendingCount": 1,
              ":rideSet": new Set([bookingId])
            },
            ConditionExpression: "stats.#rides.#pending >= :requiredOnPendingCount",
            ExpressionAttributeNames: {
              "#onGoing": "onGoing",
              "#rides": "rides",
              "#pending": "pending",
              "#confirmed": "confirmed"
            }
          }
        }]
      }))
      return data.Attributes;
    } catch (err) {
      if (err.name === 'ConditionalCheckFailedException') {
        throw { errorCode: ErrorCodes.CabVendorAndRideMismatch, message: "Condition check failed" }
      }
      throw err
    }
  }

  async insertOrGetOtpRecord(phoneNo, data, namespace, {lastSentThresholdHours}) {
    try {
      await this.ddbClient.send(new PutCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        ConditionExpression: "attribute_not_exists(primaryKey) OR #data.lastSentTime <= :lastSentThreshold ",
        ExpressionAttributeNames: {
          "#data": "data"
        },
        ExpressionAttributeValues: {
          ":lastSentThreshold": moment().subtract(lastSentThresholdHours, "hours").toISOString()
        },
        ReturnValuesOnConditionCheckFailure: "ALL_OLD",
        Item: {
          namespace,
          primaryKey: phoneNo + "",
          createdDate: new Date().toISOString(),
          data
        }
      }))
    } catch (error) {
      if (error instanceof ConditionalCheckFailedException) {
        console.log("CCF", error.$response, error.message, error.Item)
        return await this.getOtpRecord(phoneNo, namespace)
      }
      throw error
    }
  }

  async getVehicleInfo(vehicleNo) {
    try {
      let result = await this.ddbClient.send(new GetCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: NAMESPACES.VEHICLES,
          primaryKey: vehicleNo
        }
      }))
      return result.Item
    } catch (err) {
      console.log("Error while fetching the vehicle info", vehicleNo, err)
      throw err
    }
  }

  async saveVehicleInfo(vehicleNo, vehicleInfo) {
    try {
      await this.ddbClient.send(new PutCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Item: {
          namespace: NAMESPACES.VEHICLES,
          primaryKey: vehicleNo,
          createdDate: new Date().toISOString(),
          vehicleInfo
        }
      }))
    } catch (err) {
      console.log("Error while saving the vehicle info", vehicleNo, vehicleInfo, err)
      throw err
    }
  }

  async deleteVehicleInfo(vehicleNo) {
    try {
      let result = await this.ddbClient.send(new DeleteCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: NAMESPACES.VEHICLES,
          primaryKey: vehicleNo
        }
      }))
    } catch (err) {
      console.log("Error while deleting the vehicle info", vehicleNo, err)
      throw err
    }
  }

  async deleteGenericEnteries(namespace, primaryKey) {
    try {
      let result = await this.ddbClient.send(new DeleteCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: namespace,
          primaryKey: primaryKey
        }
      }))
    } catch (err) {
      console.log("Error while deleting the generic enteries", namespace, primaryKey, err)
      throw err
    }
  }

  async deleteDriverInfo(licenseNo) {
    try {
      let result = await this.ddbClient.send(new DeleteCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: NAMESPACES.DRIVERS,
          primaryKey: licenseNo
        }
      }))
    } catch (err) {
      console.log("Error while deleting the driver info", licenseNo, err)
      throw err
    }
  }

  async getDriverInfo(licenseNumber) {
    try {
      let result = await this.ddbClient.send(new GetCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: NAMESPACES.DRIVERS,
          primaryKey: licenseNumber
        }
      }))
      return result.Item
    } catch (err) {
      console.log("Error while fetching the driver info", licenseNumber, err)
      throw err
    }
  }

  async saveDriverInfo(licenseNumber, licenseInfo) {
    try {
      await this.ddbClient.send(new PutCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Item: {
          namespace: NAMESPACES.DRIVERS,
          primaryKey: licenseNumber,
          createdDate: new Date().toISOString(),
          licenseInfo
        }
      }))
    } catch (err) {
      console.log("Error while saving the vehicle info", licenseNumber, licenseInfo, err)
      throw err
    }
  }

  async getOtpRecord(phoneNo, namespace) {
    let result = await this.ddbClient.send(new GetCommand({
      TableName: CONFIG.dynamodb.tables.genericTable,
      Key: {
        namespace,
        primaryKey: phoneNo
      }
    }))
    return result.Item
  }

  async updateOtpRecord(phoneNo, data, namespace) {
    console.log(phoneNo, data, namespace)
    await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.genericTable,
      Key: {
        namespace,
        primaryKey: phoneNo
      },
      UpdateExpression: "SET #data = :data",
      ExpressionAttributeNames: {
        "#data": "data"
      },
      ExpressionAttributeValues: {
        ":data": data
      }
    }))
  }

  async getRidesByBookingIds(bookingIds = [], returnAllAttributes = false) {
    let projection = returnAllAttributes ? null : "bookingId, rideStatus, bookingDetails, priceDetails";
  }


  async updateCabVendorByAdmin(cabVendorId, cabVendor) {
    let updatedCabVendor = await this.ddbClient.send(new PutCommand({
      TableName: CONFIG.dynamodb.tables.cabVendors,
      ConditionExpression: "cabVendorId=:cabVendorId",
      ExpressionAttributeValues: {
        ":cabVendorId": cabVendorId
      },
      Item: cabVendor
    }))
    return updatedCabVendor.Attributes;
  }

  async updateRideCustomerInfoByAdmin(bookingId, customerInfo) {
    let updateExpression = "SET lastUpdatedDate=:lastUpdatedDate"
    let expressionAttrValues = { ":lastUpdatedDate": new Date().toISOString(), ":bookingId": bookingId }
    let expressionAttrNames = {}
    for (const key in customerInfo) {
      if (customerInfo[key] != null) {
        updateExpression += `, customerDetails.#${key} = :${key} `
        expressionAttrNames[`#${key}`] = key
        expressionAttrValues[`:${key}`] = customerInfo[key]
      }
    }
    let updatedRide = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      Key: { bookingId },
      UpdateExpression: updateExpression,
      ExpressionAttributeValues: expressionAttrValues,
      ExpressionAttributeNames: expressionAttrNames,
      ConditionExpression: "bookingId = :bookingId",
      ReturnValues: "ALL_NEW"
    }))
    return updatedRide.Attributes
  }

  async updateRideRateCard(bookingId, newRateCard, updatedTransactionFee) {
    let updatedRide = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      Key: { bookingId },
      UpdateExpression: "SET priceDetails.rateCard=:newRateCard, priceDetails.cabVendor.transactionFee=:updatedTransactionFee",
      ExpressionAttributeValues: {
        ":newRateCard": newRateCard,
        ":updatedTransactionFee": updatedTransactionFee,
        ":bookingId": bookingId
      },
      ConditionExpression: "bookingId = :bookingId",
      ReturnValues: "ALL_NEW"
    }))
    return updatedRide.Attributes;
  }

  /*
              !!!!! DANGER ZONE !!!!!
      
      The below methods update the wallet details of CabVendors. Be very mindful of the changes

  */

  // !!!  This adds money to the wallet
  async completePaymentAndAddMoney(cabVendorId, { orderCreatedDate, amountToBeAdded }) {
    try {
      let data = await this.ddbClient.send(new TransactWriteCommand({
        TransactItems: [{
          Update: {
            TableName: CONFIG.dynamodb.tables.walletLedger,
            Key: {
              cabVendorId,
              createdDate: orderCreatedDate
            },
            UpdateExpression: "SET opDetails.#opStatus = :newStatus",
            ExpressionAttributeValues: {
              ":newStatus": ADD_MONEY_STATUS.COMPLETED,
              ":curStatus": ADD_MONEY_STATUS.INITIATED
            },
            ExpressionAttributeNames: {
              "#opStatus": "status"
            },
            ConditionExpression: "opDetails.#opStatus = :curStatus"
          }
        }, {
          Update: {
            TableName: CONFIG.dynamodb.tables.cabVendors,
            Key: {
              cabVendorId
            },
            UpdateExpression: "ADD wallet.#walletTotal :amount",
            ExpressionAttributeValues: {
              ":amount": amountToBeAdded
            },
            ExpressionAttributeNames: {
              "#walletTotal": "total"
            }
          }
        }]
      }));
      data = await this.getCabVendorsById(cabVendorId, "wallet", true)
      return data;
    } catch (err) {
      // If conditionCheck failed, then amount is already added. Just return the wallet
      if (isConditionExpressionFailed(err)) {
        console.log("Condition check failed, Already added money to wallet")
        return await this.getCabVendorsById(cabVendorId, "wallet")
      }
      throw err
    }
  }

  async assignRideToCabVendor(bookingId, cabVendorId, { amountToBeDeductedInPaise, vendorDetails }) {
    try {
      if (isNaN(amountToBeDeductedInPaise) || amountToBeDeductedInPaise < 0)
        throw "amountToBeDeducted must be a positive number. Got " + amountToBeDeductedInPaise

      vendorDetails = {
        ...vendorDetails,
        cabVendorId,
        assignedDate: new Date().toISOString() // This date will be accurate due to Cloudflare runtime
      }
      let newRideSet = new Set()
      newRideSet.add(bookingId)
      /*
          1. Update rideStatus and cabVendor details in Rides table
          2. Deduct amount, add ride to the cabVendor item
      */
      let data = await this.ddbClient.send(new TransactWriteCommand({
        TransactItems: [{
          Update: {
            TableName: CONFIG.dynamodb.tables.rides,
            Key: {
              bookingId
            },
            UpdateExpression: "SET rideStatus = :newStatus, cabVendor = :vendorDetails",
            ReturnValuesOnConditionCheckFailure: "ALL_OLD",
            ExpressionAttributeValues: {
              ":curStatus": "UNASSIGNED",
              ":newStatus": "ASSIGNED",
              ":vendorDetails": vendorDetails
            },
            ConditionExpression: "rideStatus = :curStatus"
          }
        }, {
          Update: {
            TableName: CONFIG.dynamodb.tables.cabVendors,
            Key: {
              cabVendorId
            },
            UpdateExpression: "ADD wallet.#walletTotal :amountToBeDeductedInNegative, rides.confirmed :newRide, stats.#rides.#pending :updateValue ",
            ExpressionAttributeValues: {
              ":amountToBeDeducted": amountToBeDeductedInPaise,
              ":amountToBeDeductedInNegative": amountToBeDeductedInPaise * -1,
              ":newRide": newRideSet,
              ":updateValue": 1
            },
            ReturnValuesOnConditionCheckFailure: "ALL_OLD",
            ConditionExpression: "wallet.#walletTotal >= :amountToBeDeducted",
            ExpressionAttributeNames: {
              "#walletTotal": "total",
              "#rides": "rides",
              "#pending": "pending"
            }
          }
        }, {
          Put: {
            TableName: CONFIG.dynamodb.tables.walletLedger,
            Item: {
              cabVendorId,
              createdDate: vendorDetails.assignedDate,
              action: WallerLedgerAction.DeductMoney,
              opDetails: {
                amount: amountToBeDeductedInPaise,
                bookingId
              }
            }
          }
        }]
      }));
    } catch (err) {
      console.error("Error assigning rides", err)
      if (isConditionExpressionFailed(err)) {
        let index = isConditionExpressionFailed(err);
        throw {
          errorCode: index == 1 ? ErrorCodes.RideAlreadyAssinged : ErrorCodes.InsufficientBalance
        }
      }
      // else if(isTransactionConflict(err)) {
      //  throw {  errorCode: ErrorCodes.RetryBooking, message: "Please retry booking"  }
      // }
      throw err
    }
  }

  async cancelAssignedRide(ride, cabVendorId) {
    const amountToBeRefundedInPaise = ride.priceDetails.cabVendor.transactionFee * 100;
    const bookingId = ride.bookingId

    if (_.isFinite(amountToBeRefundedInPaise) == false) {
      throw `amountToBeRefunded is not a number: ${ride.bookingId}`
    }

    let cancelledRideSet = new Set()
    cancelledRideSet.add(bookingId)

    let curValidStatus = new Set()
    curValidStatus.add(RideStatus.Assigned)
    curValidStatus.add(RideStatus.OnGoing)
    curValidStatus.add(RideStatus.Completed)

    return await this.ddbClient.send(new TransactWriteCommand({
      TransactItems: [{
        Update: {
          TableName: CONFIG.dynamodb.tables.rides,
          Key: {
            bookingId: ride.bookingId
          },
          ConditionExpression: "contains(:curValidStatus, rideStatus)",
          UpdateExpression: "SET rideStatus = :newStatus",
          ExpressionAttributeValues: {
            ":newStatus": RideStatus.CancelledByCustomer,
            ":curValidStatus": curValidStatus
          }
        }
      }, {
        Update: {
          TableName: CONFIG.dynamodb.tables.cabVendors,
          Key: {
            cabVendorId
          },
          UpdateExpression: "ADD wallet.#walletTotal :amountToBeRefunded, rides.cancelled :cancelledRideSet, stats.#rides.#curStat :updateValueNegative, stats.#rides.#cancelledByCustomer :updateValue DELETE rides.#rideStatus :cancelledRideSet",
          ExpressionAttributeValues: {
            ":amountToBeRefunded": amountToBeRefundedInPaise,
            ":updateValue": 1,
            ":updateValueNegative": -1,
            ":cancelledRideSet": cancelledRideSet,
          },
          ReturnValuesOnConditionCheckFailure: "ALL_OLD",
          ConditionExpression: "stats.#rides.#curStat >= :updateValue",
          ExpressionAttributeNames: {
            "#walletTotal": "total",
            "#rides": "rides",
            "#cancelledByCustomer": "cancelledByCustomer",
            "#curStat": ride.rideStatus == RideStatus.Assigned ? "pending" : (ride.rideStatus == RideStatus.OnGoing ? "onGoing" : "completed"),
            "#rideStatus": ride.rideStatus == RideStatus.Assigned ? "confirmed" : (ride.rideStatus == RideStatus.OnGoing ? "onGoing" : "completed")
          }
        }
      }, {
        Put: {
          TableName: CONFIG.dynamodb.tables.walletLedger,
          Item: {
            cabVendorId,
            createdDate: new Date().toISOString(),
            action: WallerLedgerAction.RefundMoney,
            opDetails: {
              amount: amountToBeRefundedInPaise,
              bookingId,
              cabVendorId
            }
          }
        }
      }]
    })).then((res) => {
      return { statusCode: 200, response: { message: "CANCELLED RIDE.." } }
    }, err => {
      if (isConditionExpressionFailed(err)) {
        console.error(err)
        let index = isConditionExpressionFailed(err)
        return { statusCode: 400, response: { errorCode: "TOO_TIRED_TO_ADD_ERROR_CODE", message: "CCF check log... " } }
      }
      throw err
    })
  }

  async updateDriverInfoAndChargeMoney(cabVendorId, drivers, amountToBeDeductedNotInPaise, { newDriverLicenseNo, isRefreshDriver }) {
    if (isNaN(amountToBeDeductedNotInPaise) || amountToBeDeductedNotInPaise <= 0) {
      throw "Amount invalid"
    }

    try {
      return await this.ddbClient.send(new TransactWriteCommand({
        TransactItems: [{
          Update: {
            TableName: CONFIG.dynamodb.tables.cabVendors,
            ReturnValuesOnConditionCheckFailure: "ALL_OLD",
            Key: {
              cabVendorId
            },
            ConditionExpression: "wallet.#walletTotal >= :amountToBeDeductedInPaise",
            UpdateExpression: "SET drivers = :driverValue ADD wallet.#walletTotal :negativeAmountToBeAddedinPaise",
            ExpressionAttributeNames: {
              "#walletTotal": "total"
            },
            ExpressionAttributeValues: {
              ":driverValue": drivers,
              ":negativeAmountToBeAddedinPaise": amountToBeDeductedNotInPaise * 100 * -1,
              ":amountToBeDeductedInPaise": amountToBeDeductedNotInPaise * 100
            }
          }
        }, {
          Put: {
            TableName: CONFIG.dynamodb.tables.walletLedger,
            Item: {
              cabVendorId,
              createdDate: new Date().toISOString(),
              action: isRefreshDriver ? WallerLedgerAction.DriverRefreshMoney : WallerLedgerAction.DriverAddMoney,
              opDetails: {
                amount: amountToBeDeductedNotInPaise * 100,
                reason: isRefreshDriver ? `Refresh driver ${newDriverLicenseNo}` : `Add driver ${newDriverLicenseNo}`
              }
            }
          }
        }]
      }))
    } catch (err) {
      console.error("[updateDriverInfoAndChargeMoney] error ", err)
      if (isConditionExpressionFailed(err)) {
        //TODO: Get current wallet amount and add it in error message
        throw { response: { errorCode: ErrorCodes.InsufficientBalance, message: `Need atleast Rs.${amountToBeDeductedNotInPaise} in wallet` }, statusCode: 400 }
      }
    }
  }

  async updateVehicleInfoAndChargeMoney(cabVendorId, vehicles, amountToBeDeductedNotInPaise, { newVehicleRegNo, isRefreshVehicle }) {
    if (isNaN(amountToBeDeductedNotInPaise) || amountToBeDeductedNotInPaise <= 0) {
      throw "Amount invalid"
    }

    // if (isAdmin == true) {
    //   console.warn("Admin override... not deducting money")
    //   amountToBeDeductedNotInPaise = 0
    // }

    try {
      return await this.ddbClient.send(new TransactWriteCommand({
        TransactItems: [{
          Update: {
            TableName: CONFIG.dynamodb.tables.cabVendors,
            ReturnValuesOnConditionCheckFailure: "ALL_OLD",
            Key: {
              cabVendorId
            },
            ConditionExpression: "wallet.#walletTotal >= :amountToBeDeductedInPaise",
            UpdateExpression: "SET vehicles = :vehicleValue ADD wallet.#walletTotal :negativeAmountToBeAddedinPaise",
            ExpressionAttributeNames: {
              "#walletTotal": "total"
            },
            ExpressionAttributeValues: {
              ":vehicleValue": vehicles,
              ":negativeAmountToBeAddedinPaise": amountToBeDeductedNotInPaise * 100 * -1,
              ":amountToBeDeductedInPaise": amountToBeDeductedNotInPaise * 100
            }
          }
        }, {
          Put: {
            TableName: CONFIG.dynamodb.tables.walletLedger,
            Item: {
              cabVendorId,
              createdDate: new Date().toISOString(),
              action: isRefreshVehicle? WallerLedgerAction.VehicleRefreshMoney  : WallerLedgerAction.VehicleAddMoney,
              opDetails: {
                amount: amountToBeDeductedNotInPaise * 100,
                reason: isRefreshVehicle ? `Refresh vehicle ${newVehicleRegNo}` : `Add vehicle ${newVehicleRegNo}`
              }
            }
          }
        }]
      }))
    } catch (err) {
      console.error("[updateVehicleInfoAndChargeMoney] error ", err)
      if (isConditionExpressionFailed(err)) {
        //TODO: Get current wallet amount and add it in error message
        throw { response: { errorCode: ErrorCodes.InsufficientBalance, message: `Need atleast Rs.${amountToBeDeductedNotInPaise} in wallet` }, statusCode: 400 }
      }
    }
  }

  async updateCabVendorWallet(cabVendorId, amountInPaise, comment, adminEmail) {
      try {
        let data = await this.ddbClient.send(new TransactWriteCommand({
          TransactItems: [{
            Update: {
              TableName: CONFIG.dynamodb.tables.cabVendors,
              Key: {
                cabVendorId
              },
              UpdateExpression: "ADD wallet.#walletTotal :amountInPaise",
              ExpressionAttributeValues: {
                ":amountInPaise": amountInPaise,
              },
              ReturnValuesOnConditionCheckFailure: "ALL_OLD",
              ExpressionAttributeNames: {
                "#walletTotal": "total"
              }
            }
          }, {
            Put: {
              TableName: CONFIG.dynamodb.tables.walletLedger,
              Item: {
                cabVendorId,
                createdDate: new Date().toISOString(),
                action: amountInPaise > 0 ? WallerLedgerAction.AddMoney : WallerLedgerAction.DeductMoney,
                opDetails: {
                  amount: Math.abs(amountInPaise),
                  comment,
                  adminEmail
                }
              }
            }
          }]
        }))
        return data
    } catch(err) {
      console.error("[updateCabVendorWallet] error ", err)
      throw err
    }
  }

  // currently we have only driver notes use case
  async addOrUpdateNotes(bookingId, driverNotes) {
    let updateExpression = "SET driverNotes = :notes"
    let expressionAttrValues = { ":notes": driverNotes, ":bookingId": bookingId }
    let updatedRide = await this.ddbClient.send(new UpdateCommand({
      TableName: CONFIG.dynamodb.tables.rides,
      Key: { bookingId },
      UpdateExpression: updateExpression,
      ExpressionAttributeValues: expressionAttrValues,

      ConditionExpression: "bookingId = :bookingId",
      ReturnValues: "ALL_NEW"
    }))
    return updatedRide.Attributes
  }

  async getGenericEnteries(namespace, primaryKey) {
    try {
      let data = await this.ddbClient.send(new GetCommand({
        TableName: CONFIG.dynamodb.tables.genericTable,
        Key: {
          namespace: NAMESPACES[namespace],
          primaryKey
        }
      }))
      return data.Item
    } catch (err) {
      console.error("[getGenericEnteries] error", err)
      throw err
    }
  }

  async forceUpdateGenericEntry(data) {
    await this.ddbClient.send(new PutCommand({
      TableName: CONFIG.dynamodb.tables.genericTable,
      Item: data
    }))
  }

}

function isConditionExpressionFailed(err) {
  if (err.name == "ConditionalCheckFailedException") {
    return true;
  }
  let index = err?.CancellationReasons?.findIndex(cr => cr.Code == "ConditionalCheckFailed")
  return index == -1 ? false : index + 1
}

function isTransactionConflict(err) {
  let index = err?.CancellationReasons?.findIndex(cr => cr.Code == "TransactionConflict")
  return index == -1 ? false : index;
}

let dynamoInstance = null

export { dynamoInstance as default }

export function initializeDynamo(config) {
  dynamoInstance = new Dynamo(config)
}