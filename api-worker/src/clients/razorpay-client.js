import {CONFIG} from '../config'
import dynamodbClient from "./dynamodb-client"
import {sendDevMessage} from './telegram-client'

// https://razorpay.com/docs/api/orders
export async function createOrder(orderPayload) {
    const base64token = btoa(`${CONFIG.razorpay.keyId}:${CONFIG.razorpay.keySecret}`)
    let response = await fetch("https://api.razorpay.com/v1/orders", {
        method: "POST",
        body: JSON.stringify({
            "amount": orderPayload.amount,
            "currency": "INR",
            //"receipt": payload.receipt,
            "notes":  {
              ...orderPayload.notes,
              cabVendorId: orderPayload.cabVendorId
            } 
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Basic ${base64token}`
        }
    })
    let status = response.status
    response = await response.json()
    if(status != 200){
        console.error(`Create order failed`, response)
        throw response
    }

    return response
}

export async function verifyPayment(paymentDetails) {
    const cabVendorId = paymentDetails.cabVendorId;
    const orderCreatedDate = isNaN(paymentDetails.orderCreatedDate) ? paymentDetails.orderCreatedDate :  new Date(paymentDetails.orderCreatedDate*1000).toISOString() ;
    let addMoneyDetail = await dynamodbClient.getAddMoneyActionDetails(cabVendorId, orderCreatedDate)
    let generatedSignature = await hash(`${addMoneyDetail.opDetails.orderId}|${paymentDetails.paymentId}`)
    let isValidPayment = generatedSignature == paymentDetails.signature
    if(!isValidPayment){
        console.error("CRITICAL !!! signature not valid.. Is someone tampering .... !?")
        await sendDevMessage({msg: "!!! CRITICAL !!! payment signature not valid...", paymentDetails})
        return false
    }
    return addMoneyDetail
}

async function hash(string) {
    const encoder = new TextEncoder();
    const secretKeyData = encoder.encode(CONFIG.razorpay.keySecret);
    const key = await crypto.subtle.importKey(
        'raw',
        secretKeyData,
        { name: 'HMAC', hash: 'SHA-256' },
        false,
        ['sign']
    );
    const mac = await crypto.subtle.sign('HMAC', key, encoder.encode(string));
    return bufferToHex(mac)
  }

  function bufferToHex (buffer) {
    return [...new Uint8Array (buffer)]
        .map (b => b.toString (16).padStart (2, "0"))
        .join ("");
}