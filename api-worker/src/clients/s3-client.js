import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";
import { createPresignedPost } from "@aws-sdk/s3-presigned-post";

import { CONFIG } from '../config'
import { ErrorCodes, FileUploadCategory } from "../config/constants";


class Client {

    constructor(config) {
        this.client = new S3Client({
            region: config.region,
            credentials: config.credentials
        })
    }

    async getPreSignedUrlForCabVendorUpload(cabVendorId) {
       // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/modules/_aws_sdk_s3_presigned_post.html
        let response = await createPresignedPost(this.client, {
            Bucket: CONFIG.s3.buckets.cabVendors,
            Key: cabVendorId +"/${filename}",
            Expires: 1 * 60 * 60,
            Conditions: [
                ["starts-with", "$key", `${cabVendorId}/`],
                ["content-length-range", 	1, 3*1024*1024], // Size between  1 Byte to 3MB 
                ["starts-with", "x-amz-meta-type", ""]
            ]
        })
        
        //TODO: Fix Uploding meta fields is not being set in S3. 
        //console.debug(response)
        return {
            uploadUrl: response.url,
            requiredFields: response.fields
        }
    }

    async cabVendorFilesUpload(cabVendorId, files) {
        let fileUploadPromises = files.map(file => {
            this.validateFile(file)
            let key = cabVendorId + `/${file.category}/${file.categoryId}/${file.name}.${file.file.name.split('.').pop()}`
            
            if(file.category == FileUploadCategory.CabVendor) {
                key = cabVendorId + `/${file.category}/${file.name}.${file.file.name.split('.').pop()}`
            }

            return this.client.send(new PutObjectCommand({
                Bucket: CONFIG.s3.buckets.cabVendors,
                Key: key,
                Body: file.file,
                Metadata: {
                    cabVendorId,
                    category: file.category,
                    categoryId: file.categoryId
                }
            }))
        })

        let fileUploadResult = await Promise.all(fileUploadPromises)
        return fileUploadResult
    }

    validateFile(file) {
        // file.size is in bytes. Restricting each file size to be less than 5mb
        if (file.file.size > 5*1024*1024) {
            throw { response: {errorCode: ErrorCodes.FileSizeExceedLimit, fileName: file.name}, statusCode: 400 }
        }
    }
}


let s3Instance = null

export { s3Instance as default }

export function initializeS3(config) {
    s3Instance = new Client(config)
}