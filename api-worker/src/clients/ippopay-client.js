
import {CONFIG} from '../config';

// Prod - https://api.ippopay.com/v1
// Test - https://api.ippopay.com/apitest/v1/pg

export async function createOrder(orderPayload){
    let response = await fetch(`${CONFIG.ippopay.baseUrl}/order/create`, {
        method: "POST",
        body: JSON.stringify({
            amount: orderPayload.amount / 100, //amount sent from client is in paise. Ippopay requires in decimal format
            payment_modes: "cc,dc,nb,upi",
            customer: {
                name: orderPayload.notes.cabVendorName,
                email: orderPayload.notes.cabVendorEmail,
                phone: {
                    country_code: "91",
                    national_number:  orderPayload.notes.cabVendorNo
                }
            }
        }),
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Basic " + new Buffer(CONFIG.ippopay.keyId + ":" + CONFIG.ippopay.keySecret).toString("base64")
        }
    })

    let status = response.status;
    response = await response.json()
    console.log(response)
    if(status != 200 || response.success == false) {
        console.error("Create Ippopay order failed", response);
        throw response;
    }
    const order = response.data.order

    return {
        orderId: order.order_id,
        orderCreatedDate: order.createdAt,
        amount: order.amount * 100, // converting back to paise
        cabVendorId: orderPayload.cabVendorId
    }
}

export async function getIppopayOrder(orderId){
    let response = await fetch(`${CONFIG.ippopay.baseUrl}/order/${orderId}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Basic " + new Buffer(CONFIG.ippopay.keyId + ":" + CONFIG.ippopay.keySecret).toString("base64")
        }
    })

    let status = response.status;
    if(status != 200) {
        console.error("Create Ippopay order failed", response);
        throw response.json();
    }
    response = await response.json()
    return response.data.order
}

export async function verifyPayment(orderDetails){
    const order = await getIppopayOrder(orderDetails.orderId)
    if(order.trans_id != orderDetails.paymentId){
        console.error("Transaction id is different... How is this possible ?", order, orderDetails)
        return false;
    }
    return order.status == 'paid'
}