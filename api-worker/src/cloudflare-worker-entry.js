/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npx wrangler dev src/index.js` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npx wrangler publish src/index.js --name my-worker` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

import Handler from './route-handler'
import _ from "lodash"
import {CONFIG, configInit} from './config'
import { initializeDynamo } from './clients/dynamodb-client'
import { initializeSqs } from './clients/sqs-client'
import { initializeS3 } from './clients/s3-client'
import {sendDevMessage, sendTextMessageToDev} from './clients/telegram-client'
import { initializeInvicible } from './clients/invicible-ocean-client'
import vehicleDataValidator from './schedulers/vehicle-data-validator'
import elasticClientService from './service/elastic-client-service'

//TODO: Cache whereever possible
//TODO: Ideally the value should be a map and take in Schema Validator as well. but the ship has sailed
const RouteMap = {
	"GET:/health-check": Handler.healthCheck,
	"GET:/config": Handler.getConfig,
	"GET:/cab-vendors": Handler.getCabVendors,
	"PUT:/cab-vendors/config": Handler.updateCabVendorConfig,
	"POST:/cab-vendors/file-upload": Handler.cabVendorFileUpload,
	"POST:/validate-create-vendor": Handler.validateCabVendorAndTriggerOTP,
	"POST:/validate-create-driver": Handler.validateDriverAndTriggerOTP,
	"POST:/v2/cab-vendors": Handler.createCabVendorV2,
	"POST:/cab-vendors": Handler.createCabVendor,
	"POST:/cab-vendors/add-vehicle": Handler.addVehicle,
	"POST:/cab-vendors/add-driver": Handler.addDriver,
	"POST:/cab-vendors/refresh-vehicle": Handler.refreshVehicle,
	"POST:/cab-vendors/refresh-driver": Handler.refreshDriver,
	"POST:/cab-vendors/delete-vehicle": Handler.deleteVehicle,
	"POST:/cab-vendors/delete-driver": Handler.deleteDriver,

	"PUT:/cab-vendors/edit-name": Handler.editVendorName,
	"POST:/cab-vendors/validate-vendor-edit-phone": Handler.validateVendorEditPhoneNoTriggerOTP,
	"PUT:/cab-vendors/vendor-edit-phone": Handler.verifyAndEditVendorPhoneNumber,

	"PUT:/cab-vendors/upgrade-v2": Handler.UpgradeV2,
	
	"GET:/rateCards": Handler.getRateCards,
	"GET:/rides": Handler.getRideDetails, // Params : bookingIds
	"PUT:/rides": Handler.updateRideByCabVendor,
	"PUT:/rides/close": Handler.closeRide,
	"GET:/rides/unassigned": Handler.getUnAssignedRides,
	"POST:/rides": Handler.postRides,
	"PUT:/notes" : Handler.addNotes,
	
	"GET:/cf": Handler.getItemFromCF,
	"POST:/cf": Handler.putItemToCF,

	"POST:/wallet/razorpay-order": Handler.createRazorPayOrder,
	"POST:/wallet/verify-razorpay-order": Handler.verifyRazorpayPaymentAndUpdateWallet,
	"GET:/wallet/transactions": Handler.getWalletTransactionsById,
	"POST:/razorpay-hook": Handler.razorpayWebHook,

	"POST:/otp": Handler.processOtpRequest,

	"POST:/documents/generate-url": Handler.generateDocumentMgmtUrl,

	"POST:/cabvendor/location": Handler.processVendorLocation,

	"PUT:/admin/rateCards": Handler.updateRateCardConfig,
	"PUT:/admin/cancelRide": Handler.adminCancelRide,
	"POST:/admin/peformAction": Handler.processAdminRequest,
	"POST:/admin/create-item": Handler.createItem,
	"PUT:/admin/ride": Handler.updateRide,
	"PUT:/admin/updateWallet": Handler.updateWallet,

	"POST:/admin/send-sms": Handler.sendDummySMS,

	"POST:/generic": Handler.getGenericEnteries,
	"PUT:/generic": Handler.updateGenericEnteries,

	"POST:/customer": Handler.createCustomerAccount,

	"POST:/elastic": Handler.proxyElastic
}

const CommonHeaders = {
	'content-type': 'application/json;charset=UTF-8',
	'Access-Control-Allow-Origin': '*',
	'Access-Control-Allow-Methods': '*',
	'Access-Control-Allow-Headers': '*',
	'Access-Control-Max-Age': '86400'
}

function paramsToObject(entries = []) {
	const result = {}
	for (const [key, value] of entries) { // each 'entry' is a [key, value] tupple
		result[key] = value;
	}
	return result;
}

function handleOptions(request) {
	if (request.headers.get("Origin") !== null &&
		request.headers.get("Access-Control-Request-Method") !== null &&
		request.headers.get("Access-Control-Request-Headers") !== null) {
		// Handle CORS pre-flight request.
		return new Response(null, {
			headers: CommonHeaders
		})
	} else {
		// Handle standard OPTIONS request.
		return new Response(null, {
			headers: {
				"Allow": "GET, HEAD, POST, OPTIONS",
			}
		})
	}
}

let initializedWithEnvVariables = false;

function init(env, ctx){
	if(initializedWithEnvVariables){
		return;
	}

	configInit(env, ctx)
	initializeDynamo(CONFIG.dynamodb)
	initializeSqs(CONFIG.sqs)
	initializeS3(CONFIG.s3)
	initializeInvicible(CONFIG)
	//Todo: This can break in future !!
	Set.prototype.toJSON = function () {
		return [...this]
	}
	initializedWithEnvVariables = true
}

export default {
	async scheduled(event, env, ctx) {
		// wrangler dev --test-scheduled
		// curl "http://localhost:8787/__scheduled?cron=30+18+*+*+*"
		switch (event.cron) {
			case "30 18 * * *":
			// case "* * * * *":
			  // Every 18:30 UTC or midnight
			  console.log("Starting vehicle validator")
			  await elasticClientService.fetchCompleteVehicleIndex()
			//   await vehicleDataValidator.validateAllVehicles()
			  //await Handler.refreshVehicle({}, true /*fromCron*/)
			  break;
			default:
				console.warn("Cron conf not setup")
		  }
		console.log("Cron success", event.cron)
	  },
	 
	async fetch(request, env, ctx) {
		let body = {}

		try { 
			init(env, ctx)
			const httpVerb = request.method
			const { pathname, searchParams } = new URL(request.url)
			let handler = RouteMap[`${httpVerb}:${pathname}`]

			if (httpVerb == "OPTIONS") {
				return handleOptions(request) 
			}
			else if (pathname.includes("cab-vendors/file-upload")) {
				body = await request.formData()
			}
			else if (httpVerb != "GET" && httpVerb != "OPTIONS") {
				body = await request.json()
			}

			if (!handler) {
				return new Response("Route denied", { status: 400, headers: CommonHeaders })
			} else {
				let {response, statusCode=200, headers={} } = await handler({payload: body, env, headers: request.headers, params: paramsToObject(searchParams.entries())})
				return new Response(JSON.stringify(response), { status: statusCode, headers: Object.assign(headers, CommonHeaders) })
			}
			
		} catch (err) {
			if (err.errorCode) {
				return new Response(JSON.stringify(err), {status: 400, headers: CommonHeaders})
			}
			if(err.response) {
				return new Response(JSON.stringify(err.response), { status: err.statusCode, headers: CommonHeaders })
			}
			else {
				console.error({msg: "Uncaught error", url: request.url, err, body, errMsg: err.$response})
				let errorToSend = {
					route: `${request.method}:${request.url}`, 
					source: "Cloudflare", 
					payload: body,
					err
				} 
				// if(err.constructor.class == "Error") {
					errorToSend.name = err.name
					errorToSend.message = err.message
					errorToSend.stack = err.stack
					errorToSend.cause = err.cause
					errorToSend.$response = err.$response
				//}
				await sendDevMessage(errorToSend)
				return new Response("Server error. Call gogo customer care", {
					status: 500,
					headers: Object.assign({}, CommonHeaders, { 'Content-type': 'text' })
				})
			}
		}
	}
};
