export const ErrorCodes = {
        RideAlreadyAssinged: "RIDE_ALREADY_ASSINGED",
        ResourceExists: "RESOURCE_ALREADY_EXISTS",
        RideNotAvailable: "RIDE_NOT_AVAILABLE",
        InsufficientBalance: "INSUFFICIENT_BALANCE",
        ResourceNotFound: "RESOURCE_NOT_FOUND",
        InvalidRateCardId: "INVALID_RATE_CARD_ID",
        VendorNotApproved: "VENDOR_NOT_APPROVED",
        DriverNotApproved: "DRIVER_NOT_APPROVED",
        InvalidAdminRequest: "INVALID_ADMIN_REQUEST",
        VendorVehicleNotApproved: "VENDOR_VEHICLE_NOT_APPROVED",
        RetryBooking: "RETRY_BOOKING", 
        VehicleRegNoRequired: "VEHICLE_REG_NO_REQUIRED",
        CabVendorAndRideMismatch: "CABVENDOR_RIDE_MISMATCH",
        InvalidRideStatus: "INVALID_RIDE_STATUS",
        InvalidPhoneNumber: "INVALID_PHONE_NUMBER",
        InvalidVendorName: "INVALID_NAME",
        FileSizeExceedLimit: "FILE_SIZE_EXCEED_LIMIT",
        InvalidLicenseNumber: "INVALID_LICENSE_NUMBER",
        InvalidVehicleNumber: "INVALID_VEHICLE_NUMBER",
        InvalidNamespace: "INVALID_NAMESPACE",
        InvalidDate: "INVALID_DATE",
        OtpAttemptsExhausted: "OTP_ATTEMPS_EXHAUSTED",
        OtpResendLimitReached: "OTP_RESEND_LIMIT_REACHED",
        OtpResendRequestEarly: "OTP_RESENT_REQUEST_EARLY",
        OtpEntryNotFound: "OTP_ENTRY_NOT_FOUND",
        OtpInvalid: "OTP_INVALID",
        LicenseNotFound: "LICENSE_NOT_FOUND",
        VehicleNotFound: "VEHICLE_NOT_FOUND",
        VehicleUnauthorized: "VEHICLE_UNAUTHORIZED",
        LocationInfoStale: "LOCATION_INFO_STALE",
        InvalidAccessToken: "INVALID_ACCESS_TOKEN",
        ServiceUnAvailable: "SERVICE_UNAVAILABLE"
}

export const RideStatus = {
    UnPublished: "UNPUBLISHED",
    UnApproved: "UNAPPROVED",
    UnAssigned: "UNASSIGNED",
    Assigned: "ASSIGNED",
    OnGoing: "ONGOING",
    Completed: "COMPLETED",
    CancelledByCustomer: "CANCELLED_BY_CUSTOMER"
}

export const TripTypes = {
    OneWay: "ONE_WAY",
    RoundTrip: "ROUND_TRIP"
}

export const WallerLedgerAction = {
    AddMoney: "ADD_MONEY",
    DeductMoney: "DEDUCT_MONEY",
    RefundMoney: "REFUND_MONEY",
    VehicleAddMoney: "VEHICLE_ADD_MONEY",
    DriverAddMoney: "DRIVER_ADD_MONEY",
    VehicleRefreshMoney: "VEHICLE_REFRESH_MONEY",
    DriverRefreshMoney: "DRIVER_REFRESH_MONEY"
}

export const GenericTableNamespace = {
    PhoneNoVerification: "PHONE_NO_VERIFICATION",
    VendorCreationPhoneNoVerification: "VENDOR_CREATION_PHONE_NO_VERIFICATION",
    DriverAdditionPhoneNoVerification: "DRIVER_ADDITION_PHONE_NO_VERIFICATION",
    VendorEditPhoneNoVerification: "VENDOR_EDIT_PHONE_NO_VERIFICATION",
    CustomerLogin: "CUSTOMER_LOGIN",
    CustomerAccount: "CUSTOMER_ACCOUNT"
}

export const BookingSource = {
    CustomerApp: "CUSTOMERAPP",
    WebApp: "WEBAPP",
    Admin: "ADMIN",
    Unknown: "UNKNOWN"
}

export const SQSMessageType = {
    DeviceConfigUpdate: "DEVICE_CONFIG_UPDATE",
    VendorApprovalPendingNotification: "VENDOR_APPROVAL_PENDING",
    CabVendorCreated: "CAB_VENDOR_CREATED",
    RideAssigned: "RIDE_ASSIGNED",
    RideClosed: "RIDE_CLOSED",
    LocationUpdate: "LOCATION_UPDATE"
}

// ADD new request types to schema.js and build it. Dont forget
export const ADMIN_REQUEST_TYPES = {
    GET_RIDES: "GET_RIDES",
    CANCEL_RIDES: "CANCEL_RIDES",
    GET_VENDORS: "GET_VENDORS",
    GET_VENDORS_BY_STATUS: "GET_VENDORS_BY_STATUS",
    UPDATE_VENDOR_STATE: "UPDATE_VENDOR_STATE",
    DUMP_TABLE: "DUMP_TABLE",
    UPDATE_VENDOR: "UPDATE_VENDOR",
    UPDATE_RIDE: "UPDATE_RIDE",
    UPDATE_VENDOR_META: "UPDATE_VENDOR_META"
}

export const VendorStatus = {
    UnApproved: "UNAPPROVED",
    PaymentPending: "PAYMENT_PENDING",
    Approved: "APPROVED",
    Blocked: "BLOCKED"
}

export const VendorVehicleStatus = {
    UnApproved: "UNAPPROVED",
    Pending: "PENDING",
    Approved: "APPROVED",
    Blocked: "BLOCKED"
}

export const PaymentGateway = {
    Razorpay: "RAZORPAY",
    Ippopay: "IPPOPAY"
}

export const FileUploadCategory = {
    Driver: "DRIVER",
    Vehicle: "VEHICLE",
    CabVendor: "CABVENDOR"
}

export const AuditNamespace = {
    Ride: "AUDIT_RIDE",
    Cabvendor: "AUDIT_CAB_VENDOR",
    Admin: "AUDIT_ADMIN"
}


export const AuditAction = {
    UpdateShadowBan: "UPDATE_SHADOWBAN",
    ForceV2Migration: "FORCE_V2_MIGRATION",
    UpdateWallet: "UPDATE_WALLET",
    UpdateRidePrice: "UPDATE_RIDE_PRICE",
    UpdateRideCustomerInfo: "UPDATE_RIDE_CUSTOMER_INFO",
    CloseRide: "CLOSE_RIDE",
    CancelRide: "CANCEL_RIDE"
}

export const RateCardConfigKey = "RATE_CARD_KEY"