
import DefaultConfig from "./default.json" assert { type: "json" }
import ProdConfig from './production.json' assert { type: "json" }
import { RateCardConfigKey } from './constants.js';
import RateCardDefaultConfig from './rateCardConfig.json' assert { type: "json" }
import auditService from "../service/audit-service.js";

export let CONFIG = ProdConfig;

async function initRateCardConfig(){
    if(this.rateCardConfig){
        console.info("Using the override rate card", this.rateCardConfig)
        return this.rateCardConfig
    }
    let rateCardConfig = await this.env.KV.get(RateCardConfigKey, {type: "json"});
    if(!rateCardConfig){
        console.warn("Rate card not present in KV store")
    }
    this.rateCardConfig = rateCardConfig || RateCardDefaultConfig;
    console.info("Using the rate card", this.rateCardConfig)
    return this.rateCardConfig
}

export function configInit(env, context) {
    if(env.ENV == 'PROD'){
        //console.log("Using production config")
        // Add Env variables and secrets in the correct place
        CONFIG.dynamodb.credentials = {
            accessKeyId: env.AWS_ACCESS_KEY_ID,
            secretAccessKey: env.AWS_SECRET_ACCESS_KEY
        }
        CONFIG.sqs.credentials = {
            accessKeyId: env.AWS_ACCESS_KEY_ID,
            secretAccessKey: env.AWS_SECRET_ACCESS_KEY
        }
        CONFIG.s3.credentials = {
            accessKeyId: env.AWS_ACCESS_KEY_ID,
            secretAccessKey: env.AWS_SECRET_ACCESS_KEY
        }
        CONFIG.elasticSearchConfig.credentials = {
            username: env.ES_USERNAME,
            password: env.ES_PASSWORD
        }
        CONFIG.razorpay.keyId = env.RAZORPAY_KEYID
        CONFIG.razorpay.keySecret = env.RAZORPAY_KEYSECRET
        CONFIG.firebase.privateKey = env.firebasePrivateKey
        CONFIG.jwtSecret = env.JWT_SECRET

        CONFIG.whatsapp.token = env.WHATSAPP_TOKEN
    } else {
        CONFIG = DefaultConfig
        CONFIG.firebase.privateKey = env.firebasePrivateKey
    }
    CONFIG.env = env
    CONFIG.getRateCardConfig = initRateCardConfig
    auditService.init(context)
}
