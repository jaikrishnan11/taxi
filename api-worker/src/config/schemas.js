const fs = require("fs")
const path = require("path")
const Ajv = require("ajv")
const standaloneCode = require("ajv/dist/standalone").default

let schemas = [
  
  {
  $id: "CreateVehicle",
  type: "object",
  properties: {
    vehicleRegNo: {type: "string" },
    type: {type: "string" }
  },
  additionalProperties: true
},
{
  $id: "CreateDriver",
  type: "object",
  properties: {
    name: {type: "string" },
    phoneNo: {type: ["integer", "string"]}
  },
  additionalProperties: true
},

{
  $id: "CreateCabVendorProfile",
  type: "object",
  properties: {
    name: {type : "string"},
    phoneNo: {type: ["integer", "string"]},
    email: {type: "string"}
  }
},
{
  $id: "RateCardId",
  type: "object",
  properties: {
    rate: {type: "number"},
    bata: {type: "number"}
  },
  required: ["rate"]
},

// Uber Request Objects

{
  $id: "CreateCabVendor",
  type: "object",
  properties: {
    cabVendorId: { type: "string" },
    vehicles: { type: "object", properties: {".*" : {"$ref": "CreateVehicle" }}},
    profile: { "$ref": "CreateCabVendorProfile"},
    drivers: {type: "object", properties: {".*": {"$ref": "CreateDriver"}}}
  },
  required: ["cabVendorId", "vehicles", "profile"],
  additionalProperties: true
},
{
  $id: "CreateCabVendorV2",
  type: "object",
  properties: {
    cabVendorId: { type: "string" },
    otpCode: { type: "string" },
    profile: { "$ref": "CreateCabVendorProfile"},
  },
  required: ["cabVendorId", "otpCode", "profile"],
  additionalProperties: true
},
{
  $id: "AddDriver",
  type: "object",
  properties: {
    cabVendorId: { type: "string" },
    phoneNumber: { type: "string" },
    licenseNumber: { type: "string" },
    dateOfBirth: { type: "string" },
  },
  required: ["cabVendorId", "phoneNumber", "licenseNumber", "dateOfBirth"],
  additionalProperties: true
},
{
  $id: "AddVehicle",
  type: "object",
  properties: {
    cabVendorId: { type: "string" },
    vehicleNumber: { type: "string" },
    chassisNumber: { type: "string" },
    isAdmin : { type: "boolean" },
    googleUserId: { type: "string" },
    googleIdToken: { type: "string" },
  },
  required: ["cabVendorId", "vehicleNumber", "chassisNumber"],
  additionalProperties: true
},
{
  $id: "CreateRazorpayOrder",
  type: "object",
  properties: {
    cabVendorId: {type: "string" },
    amount: {type: "integer", minimum: 100, maximum: 1000000 },
    //Optional
    bookingId: { type: "string" },
    notes: { type: "object",  properties: { ".*": {type: "string" } },  maxProperties: 5 }
  },
  required: ["cabVendorId", "amount"]
},

{
  $id: "VerifyRazorpayPayment",
  type: "object",
  properties: {
    cabVendorId: {type: "string"},
    orderCreatedDate: {type:["number", "string"]},
    orderId: {type:"string"},
    paymentId: {type:"string"},
    signature: {type: "string"},
    isRegistration: {type: "boolean"}
  },
  required: ["cabVendorId", "orderCreatedDate", "orderId", "paymentId", "signature"]
},
{
  $id: "UpdateVendorConfig",
  type: "object",
  properties: {
    cabVendorId: {type: "string"},
    deviceToken: {type: "string"}    
  }
},
{
  $id: "GetRateCards",
  type: "object",
  properties: {
    tripType: {enum: ["ONE_WAY", "ROUND_TRIP"]},
    pickupDate: {type: "string"},
    dropDate: {type: "string"},
    pickupPlaceId:  {type: "string"},
    dropPlaceId:  {type: "string"},
    keepCab: {type: "string"},
    latitude: {type: "string"},
    longitude: {type: "string"},
    intermediates: {type: "string"}
  },
  required: ["tripType", "pickupPlaceId", "dropPlaceId"]
},
{
  $id: "UpdateRideByDriver",
  type: "object",
  properties: {
    cabVendorId: {type: "string"},
    bookingId: {type: "string"},
    vehicleRegNo: {type: "string"},
    driverLicenseNo: {type: "string"},
    action: {enum: ["ASSIGN_DRIVER", "CANCEL_BY_DRIVER", "START_RIDE"]} 
  },
  required: ["cabVendorId", "action", "bookingId"]
},
{
  $id: "PostRides",
  type: "object",
  properties: {
    bookingDetails: { "$ref": "GetRateCards" },
    rateCardId: {type: "number" },
    customerDetails: {
      type: "object",
      properties: {
        phoneNumber: {type: "string"},
        name: {type: "string"},
        email: {type: "string"} 
      },
      required: ["phoneNumber", "name"]
    },
    bookingSource: {
      type: "object",
      properties: {
        source: {enum: ["WEBAPP", "CUSTOMERAPP", "AFFLIATE", "ADMIN"]},
        accessToken: {type: "string"}
      },
      required: []
    }
  }
},
{
  $id: "UpdateRateCardConfig",
  type: "object",
  properties: {
    "rateCards": {
      type: "object",
      properties: {
        ".*": {"$ref": "RateCardId"}
      }
    }
  }
},
{
  $id: "ProcessAdminRequest",
  type: "object",
  properties: {
    googleUserId: {type: "string"},
    googleIdToken: {type: "string"},
    requestType: {enum: ["GET_RIDES", "CANCEL_RIDES", "GET_VENDORS", "UPDATE_VENDOR_STATE", "UPDATE_VENDOR", "GET_VENDORS_BY_STATUS", "DUMP_TABLE", "UPDATE_VENDOR_META"] },
    payload: {type: "object"}
  }
},
{
  $id: "AdminCancelRideRequest",
  type: "object",
  properties: {
    googleUserId: {type: "string"},
    googleIdToken: {type: "string"},
    bookingId: {type: "string"},
    curRideStatus: {enum: ["UNAPPROVED", "UNASSIGNED", "ASSIGNED", "ONGOING", "COMPLETED"]},
    cabVendorId: {type: "string"}
  }
},
{
  $id: "OtpRequest",
  type: "object",
  properties: {
    phoneNumber: {type: "string"},
    otpType: {enum: ["PHONE_NO_VERIFICATION", "CUSTOMER_LOGIN"]},
    action: {enum: ["GENERATE", "RESEND", "VERIFY"]},
    otpCode: {type: "integer"},
    carrier: {enum: ["WHATSAPP", "SMS"]}
  },
  required: ["phoneNumber", "otpType", "action"]
},
{
  $id: "DocumentMgmtUrl",
  type: "object",
  properties: {
    category: {enum: ["CABVENDOR"]},
    action: {enum: ["POST", "GET"]},
    groupId: {type: "string"}
  }
}, {
  $id: "UpdateRide",
  type: "object",
  properties: {
    googleUserId: {type: "string"},
    googleIdToken: {type: "string"},
    bookingId: {type: "string"},
    dryRun: {type: "boolean"},
    customer: { 
      type: "object",  
      properties: { 
        "name": {type: "string" },
        "phoneNumber": {type: "string"},
        "email": {type: "string"}
      } 
    },
    price: {
      type: "object",
      properties: {
        "driverBata": {type: "integer"},
        "ratePerKm": {type: "integer"},
        "totalAmount": {type: "integer"}
      }
    }
  },
  required: ["googleUserId", "googleIdToken", "bookingId", "dryRun"]
}, {
  $id: "CloseRide",
  type: "object",
  properties: {
    googleUserId: {type: "string"},
    googleIdToken: {type: "string"},
    cabVendorId: {type:"string"},
    bookingId: {type: "string"},
    isAdmin: {type: "boolean"},
    distance: {type: "integer"},
    petCharge: {type: "integer"},
    hillCharge: {type: "integer"},
    waitingCharge: {type: "integer"},
    parkingCharge: {type: "integer"},
    tollAmount: {type: "integer"},
    amountCollectedFromCustomer: {type: "integer"}
  },
  required: ["cabVendorId", "googleIdToken", "bookingId", "distance"]
},
{
  $id: "LocData",
  type: "object",
  properties: {
    googleUserId: {type: "string"},
    googleIdToken: {type: "string"},
    cabVendorId: {type:"string"},
    coordinates: {type:"array", items: {type: "number"}, "minItems": 2, "maxItems": 2},
    locationTime: {type: "string"}
  },
  required: ["cabVendorId", "coordinates", "locationTime"]
},
{
  $id: "UpdateWallet",
  type: "object",
  properties: {
    googleUserId: {type: "string"},
    googleIdToken: {type: "string"},
    cabVendorId: {type:"string"},
    amount: {type: "integer"},
    action: {enum: ["ADD", "DEDUCT"]},
    comment: {type: "string"}
  },
  required: ["cabVendorId", "amount", "action", "googleUserId", "googleIdToken"]
}, {
 $id: "CustomerAccountInfo",
 type: "object",
 properties: {
  phoneNo: {type: "string"},
  name: {type: "string"}
 },
 required: ["phoneNo"]
},{
  $id: "CreateCustomerAccount", 
  type: "object",
  properties: {
    customer: {"$ref": "CustomerAccountInfo"},
    generateOTP: {type: "boolean"},
    otpCarrier: {type: "string"}
  },
  required: ["customer", "generateOTP"]
}
]

schemas = schemas.map(schema => {
  return {
    ...schema,
    required: schema.required ? schema.required : Object.keys(schema.properties) 
  }
})

// https://ajv.js.org/options.html#code
const ajv = new Ajv({schemas, strict:"log", code: {source: true, esm: true, lines: true}})
let moduleCode = standaloneCode(ajv, {
  "validateCreateCabVendor": "CreateCabVendor",
  "validateCreateCabVendorV2" : "CreateCabVendorV2",
  "validateAddVehicle" : "AddVehicle",
  "validateAddDriver" : "AddDriver",
  "validateCreateRazorpayOrder": "CreateRazorpayOrder",
  "validateRazorpayPayment": "VerifyRazorpayPayment",
  "validateUpdateRideByDriver": "UpdateRideByDriver",
  "validateUpdateVendorConfig": "UpdateVendorConfig",
  "validateGetRateCards": "GetRateCards",
  "validatePostRides": "PostRides",
  "validateRateCardConfigUpdate": "UpdateRateCardConfig",
  "validateOtpRequest": "OtpRequest",
  "validateAdminRequest": "ProcessAdminRequest",
  "validateAdminCancelRideRequest": "AdminCancelRideRequest",
  "validateDocumentMgmtUrl": "DocumentMgmtUrl",
  "validateAdminUpdateRide": "UpdateRide",
  "validateAdminUpdateWallet": "UpdateWallet",
  "validateCloseRideEndpoint": "CloseRide",
  "validateLocationData": "LocData",
  "validateCreateCustomerAccount": "CreateCustomerAccount"
})

// Now you can write the module code to file
fs.writeFileSync(path.join(__dirname, "./schema-validators.js"), moduleCode)
