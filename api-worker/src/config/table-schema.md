# TABLE SCHEMA

## 1.wallet-book-keeping

```
      cabVendorId,  # Partition key 
      createdDate, # Sort key,
      action, # Enum: [ "ADD_MONEY", "DEBIT_MONEY" ],
      opDetails:    
            // If ADD_MONEY   
            status, # Enum: ["INITIATED", "COMPLETED", "FAILED"]
            paymentGateway, # Enum : [ "RAZORPAY" ]
            amount,
            bookingId , # optional

            // IF DEBIT_MONEY
            bookingId,
            amount
```

## 2. cab-vendors

```
      cabVendorId, # Partition key
      vendorStatus,  [UNAPPROVED, APPROVED, BLOCKED]
      version,
      devices: {
            cabVendorDevice: {
                  deviceToken
            }
      }
      wallet {
            razorpay {
                  paymentUrl
                  qrId
                  qrImageUrl
            }
            total, #  VERY IMPORTANT !!! ==> this is in PAISE
      }
      vehicles {
            "regNumber": {
                  vehicleRegNo,
                  type,
                  vehicleStatus,  
            }
      }
      drivers { # from v2 version
            "licenseNo": {
                  phoneNo,
                  name
            }
      }
      rides {
            completed,
            confirmed,
            onGoing,
            cancelled
      },
      stats: {
                totalEarning: 0,
                rides: {
                    pending: 0,
                    onGoing: 0,
                    completed: 0,
                    cancelledByVendor: 0,
                    cancelledByCustomer: 0
                }
      },
      profile: {
            name,
            phoneNo, // this is integer what a bad idea !!
            email,
            isVerified # from v2 version
            panCardNo # from v2 version
      },
      location: {
            "driverId": {
                  geoLocation {lat, lon}
                  locationTime, //ISOTimestamp of time the location was calculated
            }
      }
      meta: {
            shadowban {banExpiry, rideDelayMinutes},
            forceV2Migration
      }
```

## 3. rides

```
      bookingId, # Partition key,
      rideStatus, # Enum [UNASSIGNED, ASSIGNED, CANCELLED_BY_CUSTOMER],
      createdDate
      customerDetails
            customerId, // not yet implemented
            name
            phoneNumber,
            email
      bookingDetails
            pickup
                  placeId
                  shortName
                  fullAddress

            drop
                  placeId
                  shortName
                  fullAddress
            intermediates
                  placeId
                  shortName
                  fullAddress
            tripStartTime
            returnStartTime # valid for roundTrip
            estRideEndTime
            estDistance
            estTripDuration
            tripType
            cabType

            rideCloseTime
            actualDistanceCovered

      cabVendor  // Only if Assigned and later states
            cabVendorId
            assignedDate
         
            // Only for v2 or later
            driver: {
                  driverName: cabVendor.drivers[payload.driverLicense].name,
                  driverPhoneNo: cabVendor.drivers[payload.driverLicense].phoneNumber
                  driverLicenseNo: cabVendor.drivers[payload.driverLicense].phoneNumber
            }, vehicle: {
                  vehicleRegNo: payload.vehicleRegNo
            }
                    

      priceDetails
            pricePerKm // Not used anymore I think 
            rateCard
                  id
                  fare
                  bata
                  rate
                  hillCharge
                  tollAmount
                  petCharge
                  waitingCharge
                  total
                  priceSplit

                  previousEstimatedFare

            cabVendor
                  transactionFee // Amount driver needs to pay to Gogo 

      
      estRideEndTime // Computed in the backed before storing. This is needed to change the status from OnGoing to Completed

GSI: booking-status-index - rideStatus,createdDate
```

## 4. generic-table (namespace, primaryKey)

### PHONE_NO_VERIFICATION , phoneNumber (hopefully 10 digit)

data: {
      phoneNumber
      generatedOtp
      isVerified
      noOfTimesOtpSent
      lastSentTime
}

### Audit changes. We use same generic table
  - Namespaces: AUDIT_RIDE, AUDIT_CAB_VENDOR, AUDIT_ADMIN
  - primaryKey: bookingId, cabVendorId, AUDIT_ADMIN

  - events[]
      - performedBy:
      - action: 
      - eventTime:
      - details: {

      }

### Customer 
- Namespace: CUSTOMER_ACCOUNT
- primaryKey: phoneNumber with country code without "+"

- id
- profile: 
      - name
      - <dob>
- rides: {
        pending: [],
        completed: [],
        cancelled: []
  }
- contact:
      - default
      - whatsapp
