"use strict";
export const validateCreateCabVendor = validate10;
const schema11 = {"$id":"CreateCabVendor","type":"object","properties":{"cabVendorId":{"type":"string"},"vehicles":{"type":"object","properties":{".*":{"$ref":"CreateVehicle"}}},"profile":{"$ref":"CreateCabVendorProfile"},"drivers":{"type":"object","properties":{".*":{"$ref":"CreateDriver"}}}},"required":["cabVendorId","vehicles","profile"],"additionalProperties":true};
const schema12 = {"$id":"CreateVehicle","type":"object","properties":{"vehicleRegNo":{"type":"string"},"type":{"type":"string"}},"additionalProperties":true,"required":["vehicleRegNo","type"]};
const schema13 = {"$id":"CreateCabVendorProfile","type":"object","properties":{"name":{"type":"string"},"phoneNo":{"type":["integer","string"]},"email":{"type":"string"}},"required":["name","phoneNo","email"]};
const schema14 = {"$id":"CreateDriver","type":"object","properties":{"name":{"type":"string"},"phoneNo":{"type":"integer"}},"additionalProperties":true,"required":["name","phoneNo"]};

function validate10(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="CreateCabVendor" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.vehicles === undefined) && (missing0 = "vehicles"))) || ((data.profile === undefined) && (missing0 = "profile"))){
validate10.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs2 = errors;
if(typeof data.cabVendorId !== "string"){
validate10.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs2 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.vehicles !== undefined){
let data1 = data.vehicles;
const _errs4 = errors;
if(errors === _errs4){
if(data1 && typeof data1 == "object" && !Array.isArray(data1)){
if(data1[".*"] !== undefined){
let data2 = data1[".*"];
const _errs7 = errors;
if(errors === _errs7){
if(data2 && typeof data2 == "object" && !Array.isArray(data2)){
let missing1;
if(((data2.vehicleRegNo === undefined) && (missing1 = "vehicleRegNo")) || ((data2.type === undefined) && (missing1 = "type"))){
validate10.errors = [{instancePath:instancePath+"/vehicles/.*",schemaPath:"CreateVehicle/required",keyword:"required",params:{missingProperty: missing1},message:"must have required property '"+missing1+"'"}];
return false;
}
else {
if(data2.vehicleRegNo !== undefined){
const _errs10 = errors;
if(typeof data2.vehicleRegNo !== "string"){
validate10.errors = [{instancePath:instancePath+"/vehicles/.*/vehicleRegNo",schemaPath:"CreateVehicle/properties/vehicleRegNo/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid3 = _errs10 === errors;
}
else {
var valid3 = true;
}
if(valid3){
if(data2.type !== undefined){
const _errs12 = errors;
if(typeof data2.type !== "string"){
validate10.errors = [{instancePath:instancePath+"/vehicles/.*/type",schemaPath:"CreateVehicle/properties/type/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid3 = _errs12 === errors;
}
else {
var valid3 = true;
}
}
}
}
else {
validate10.errors = [{instancePath:instancePath+"/vehicles/.*",schemaPath:"CreateVehicle/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
}
}
else {
validate10.errors = [{instancePath:instancePath+"/vehicles",schemaPath:"#/properties/vehicles/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs4 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.profile !== undefined){
let data5 = data.profile;
const _errs14 = errors;
const _errs15 = errors;
if(errors === _errs15){
if(data5 && typeof data5 == "object" && !Array.isArray(data5)){
let missing2;
if((((data5.name === undefined) && (missing2 = "name")) || ((data5.phoneNo === undefined) && (missing2 = "phoneNo"))) || ((data5.email === undefined) && (missing2 = "email"))){
validate10.errors = [{instancePath:instancePath+"/profile",schemaPath:"CreateCabVendorProfile/required",keyword:"required",params:{missingProperty: missing2},message:"must have required property '"+missing2+"'"}];
return false;
}
else {
if(data5.name !== undefined){
const _errs17 = errors;
if(typeof data5.name !== "string"){
validate10.errors = [{instancePath:instancePath+"/profile/name",schemaPath:"CreateCabVendorProfile/properties/name/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid5 = _errs17 === errors;
}
else {
var valid5 = true;
}
if(valid5){
if(data5.phoneNo !== undefined){
let data7 = data5.phoneNo;
const _errs19 = errors;
if((!(((typeof data7 == "number") && (!(data7 % 1) && !isNaN(data7))) && (isFinite(data7)))) && (typeof data7 !== "string")){
validate10.errors = [{instancePath:instancePath+"/profile/phoneNo",schemaPath:"CreateCabVendorProfile/properties/phoneNo/type",keyword:"type",params:{type: schema13.properties.phoneNo.type},message:"must be integer,string"}];
return false;
}
var valid5 = _errs19 === errors;
}
else {
var valid5 = true;
}
if(valid5){
if(data5.email !== undefined){
const _errs21 = errors;
if(typeof data5.email !== "string"){
validate10.errors = [{instancePath:instancePath+"/profile/email",schemaPath:"CreateCabVendorProfile/properties/email/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid5 = _errs21 === errors;
}
else {
var valid5 = true;
}
}
}
}
}
else {
validate10.errors = [{instancePath:instancePath+"/profile",schemaPath:"CreateCabVendorProfile/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs14 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.drivers !== undefined){
let data9 = data.drivers;
const _errs23 = errors;
if(errors === _errs23){
if(data9 && typeof data9 == "object" && !Array.isArray(data9)){
if(data9[".*"] !== undefined){
let data10 = data9[".*"];
const _errs26 = errors;
if(errors === _errs26){
if(data10 && typeof data10 == "object" && !Array.isArray(data10)){
let missing3;
if(((data10.name === undefined) && (missing3 = "name")) || ((data10.phoneNo === undefined) && (missing3 = "phoneNo"))){
validate10.errors = [{instancePath:instancePath+"/drivers/.*",schemaPath:"CreateDriver/required",keyword:"required",params:{missingProperty: missing3},message:"must have required property '"+missing3+"'"}];
return false;
}
else {
if(data10.name !== undefined){
const _errs29 = errors;
if(typeof data10.name !== "string"){
validate10.errors = [{instancePath:instancePath+"/drivers/.*/name",schemaPath:"CreateDriver/properties/name/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid8 = _errs29 === errors;
}
else {
var valid8 = true;
}
if(valid8){
if(data10.phoneNo !== undefined){
let data12 = data10.phoneNo;
const _errs31 = errors;
if(!(((typeof data12 == "number") && (!(data12 % 1) && !isNaN(data12))) && (isFinite(data12)))){
validate10.errors = [{instancePath:instancePath+"/drivers/.*/phoneNo",schemaPath:"CreateDriver/properties/phoneNo/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid8 = _errs31 === errors;
}
else {
var valid8 = true;
}
}
}
}
else {
validate10.errors = [{instancePath:instancePath+"/drivers/.*",schemaPath:"CreateDriver/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
}
}
else {
validate10.errors = [{instancePath:instancePath+"/drivers",schemaPath:"#/properties/drivers/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs23 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
else {
validate10.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate10.errors = vErrors;
return errors === 0;
}

export const validateCreateCabVendorV2 = validate11;
const schema15 = {"$id":"CreateCabVendorV2","type":"object","properties":{"cabVendorId":{"type":"string"},"otpCode":{"type":"string"},"profile":{"$ref":"CreateCabVendorProfile"}},"required":["cabVendorId","otpCode","profile"],"additionalProperties":true};

function validate11(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="CreateCabVendorV2" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.otpCode === undefined) && (missing0 = "otpCode"))) || ((data.profile === undefined) && (missing0 = "profile"))){
validate11.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs2 = errors;
if(typeof data.cabVendorId !== "string"){
validate11.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs2 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.otpCode !== undefined){
const _errs4 = errors;
if(typeof data.otpCode !== "string"){
validate11.errors = [{instancePath:instancePath+"/otpCode",schemaPath:"#/properties/otpCode/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs4 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.profile !== undefined){
let data2 = data.profile;
const _errs6 = errors;
const _errs7 = errors;
if(errors === _errs7){
if(data2 && typeof data2 == "object" && !Array.isArray(data2)){
let missing1;
if((((data2.name === undefined) && (missing1 = "name")) || ((data2.phoneNo === undefined) && (missing1 = "phoneNo"))) || ((data2.email === undefined) && (missing1 = "email"))){
validate11.errors = [{instancePath:instancePath+"/profile",schemaPath:"CreateCabVendorProfile/required",keyword:"required",params:{missingProperty: missing1},message:"must have required property '"+missing1+"'"}];
return false;
}
else {
if(data2.name !== undefined){
const _errs9 = errors;
if(typeof data2.name !== "string"){
validate11.errors = [{instancePath:instancePath+"/profile/name",schemaPath:"CreateCabVendorProfile/properties/name/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs9 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data2.phoneNo !== undefined){
let data4 = data2.phoneNo;
const _errs11 = errors;
if((!(((typeof data4 == "number") && (!(data4 % 1) && !isNaN(data4))) && (isFinite(data4)))) && (typeof data4 !== "string")){
validate11.errors = [{instancePath:instancePath+"/profile/phoneNo",schemaPath:"CreateCabVendorProfile/properties/phoneNo/type",keyword:"type",params:{type: schema13.properties.phoneNo.type},message:"must be integer,string"}];
return false;
}
var valid2 = _errs11 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data2.email !== undefined){
const _errs13 = errors;
if(typeof data2.email !== "string"){
validate11.errors = [{instancePath:instancePath+"/profile/email",schemaPath:"CreateCabVendorProfile/properties/email/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs13 === errors;
}
else {
var valid2 = true;
}
}
}
}
}
else {
validate11.errors = [{instancePath:instancePath+"/profile",schemaPath:"CreateCabVendorProfile/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs6 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
else {
validate11.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate11.errors = vErrors;
return errors === 0;
}

export const validateAddVehicle = validate12;
const schema17 = {"$id":"AddVehicle","type":"object","properties":{"cabVendorId":{"type":"string"},"vehicleNumber":{"type":"string"},"chassisNumber":{"type":"string"},"isAdmin":{"type":"boolean"},"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"}},"required":["cabVendorId","vehicleNumber","chassisNumber"],"additionalProperties":true};

function validate12(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="AddVehicle" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.vehicleNumber === undefined) && (missing0 = "vehicleNumber"))) || ((data.chassisNumber === undefined) && (missing0 = "chassisNumber"))){
validate12.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs2 = errors;
if(typeof data.cabVendorId !== "string"){
validate12.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs2 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.vehicleNumber !== undefined){
const _errs4 = errors;
if(typeof data.vehicleNumber !== "string"){
validate12.errors = [{instancePath:instancePath+"/vehicleNumber",schemaPath:"#/properties/vehicleNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs4 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.chassisNumber !== undefined){
const _errs6 = errors;
if(typeof data.chassisNumber !== "string"){
validate12.errors = [{instancePath:instancePath+"/chassisNumber",schemaPath:"#/properties/chassisNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs6 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.isAdmin !== undefined){
const _errs8 = errors;
if(typeof data.isAdmin !== "boolean"){
validate12.errors = [{instancePath:instancePath+"/isAdmin",schemaPath:"#/properties/isAdmin/type",keyword:"type",params:{type: "boolean"},message:"must be boolean"}];
return false;
}
var valid0 = _errs8 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleUserId !== undefined){
const _errs10 = errors;
if(typeof data.googleUserId !== "string"){
validate12.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs10 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs12 = errors;
if(typeof data.googleIdToken !== "string"){
validate12.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs12 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
}
else {
validate12.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate12.errors = vErrors;
return errors === 0;
}

export const validateAddDriver = validate13;
const schema18 = {"$id":"AddDriver","type":"object","properties":{"cabVendorId":{"type":"string"},"phoneNumber":{"type":"string"},"licenseNumber":{"type":"string"},"dateOfBirth":{"type":"string"}},"required":["cabVendorId","phoneNumber","licenseNumber","dateOfBirth"],"additionalProperties":true};

function validate13(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="AddDriver" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.phoneNumber === undefined) && (missing0 = "phoneNumber"))) || ((data.licenseNumber === undefined) && (missing0 = "licenseNumber"))) || ((data.dateOfBirth === undefined) && (missing0 = "dateOfBirth"))){
validate13.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs2 = errors;
if(typeof data.cabVendorId !== "string"){
validate13.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs2 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.phoneNumber !== undefined){
const _errs4 = errors;
if(typeof data.phoneNumber !== "string"){
validate13.errors = [{instancePath:instancePath+"/phoneNumber",schemaPath:"#/properties/phoneNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs4 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.licenseNumber !== undefined){
const _errs6 = errors;
if(typeof data.licenseNumber !== "string"){
validate13.errors = [{instancePath:instancePath+"/licenseNumber",schemaPath:"#/properties/licenseNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs6 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.dateOfBirth !== undefined){
const _errs8 = errors;
if(typeof data.dateOfBirth !== "string"){
validate13.errors = [{instancePath:instancePath+"/dateOfBirth",schemaPath:"#/properties/dateOfBirth/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs8 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
else {
validate13.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate13.errors = vErrors;
return errors === 0;
}

export const validateCreateRazorpayOrder = validate14;
const schema19 = {"$id":"CreateRazorpayOrder","type":"object","properties":{"cabVendorId":{"type":"string"},"amount":{"type":"integer","minimum":100,"maximum":1000000},"bookingId":{"type":"string"},"notes":{"type":"object","properties":{".*":{"type":"string"}},"maxProperties":5}},"required":["cabVendorId","amount"]};

function validate14(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="CreateRazorpayOrder" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.amount === undefined) && (missing0 = "amount"))){
validate14.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs1 = errors;
if(typeof data.cabVendorId !== "string"){
validate14.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.amount !== undefined){
let data1 = data.amount;
const _errs3 = errors;
if(!(((typeof data1 == "number") && (!(data1 % 1) && !isNaN(data1))) && (isFinite(data1)))){
validate14.errors = [{instancePath:instancePath+"/amount",schemaPath:"#/properties/amount/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
if(errors === _errs3){
if((typeof data1 == "number") && (isFinite(data1))){
if(data1 > 1000000 || isNaN(data1)){
validate14.errors = [{instancePath:instancePath+"/amount",schemaPath:"#/properties/amount/maximum",keyword:"maximum",params:{comparison: "<=", limit: 1000000},message:"must be <= 1000000"}];
return false;
}
else {
if(data1 < 100 || isNaN(data1)){
validate14.errors = [{instancePath:instancePath+"/amount",schemaPath:"#/properties/amount/minimum",keyword:"minimum",params:{comparison: ">=", limit: 100},message:"must be >= 100"}];
return false;
}
}
}
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.bookingId !== undefined){
const _errs5 = errors;
if(typeof data.bookingId !== "string"){
validate14.errors = [{instancePath:instancePath+"/bookingId",schemaPath:"#/properties/bookingId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.notes !== undefined){
let data3 = data.notes;
const _errs7 = errors;
if(errors === _errs7){
if(data3 && typeof data3 == "object" && !Array.isArray(data3)){
if(Object.keys(data3).length > 5){
validate14.errors = [{instancePath:instancePath+"/notes",schemaPath:"#/properties/notes/maxProperties",keyword:"maxProperties",params:{limit: 5},message:"must NOT have more than 5 properties"}];
return false;
}
else {
if(data3[".*"] !== undefined){
if(typeof data3[".*"] !== "string"){
validate14.errors = [{instancePath:instancePath+"/notes/.*",schemaPath:"#/properties/notes/properties/.*/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
}
}
}
else {
validate14.errors = [{instancePath:instancePath+"/notes",schemaPath:"#/properties/notes/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
else {
validate14.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate14.errors = vErrors;
return errors === 0;
}

export const validateRazorpayPayment = validate15;
const schema20 = {"$id":"VerifyRazorpayPayment","type":"object","properties":{"cabVendorId":{"type":"string"},"orderCreatedDate":{"type":["number","string"]},"orderId":{"type":"string"},"paymentId":{"type":"string"},"signature":{"type":"string"},"isRegistration":{"type":"boolean"}},"required":["cabVendorId","orderCreatedDate","orderId","paymentId","signature"]};

function validate15(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="VerifyRazorpayPayment" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.orderCreatedDate === undefined) && (missing0 = "orderCreatedDate"))) || ((data.orderId === undefined) && (missing0 = "orderId"))) || ((data.paymentId === undefined) && (missing0 = "paymentId"))) || ((data.signature === undefined) && (missing0 = "signature"))){
validate15.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs1 = errors;
if(typeof data.cabVendorId !== "string"){
validate15.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.orderCreatedDate !== undefined){
let data1 = data.orderCreatedDate;
const _errs3 = errors;
if((!((typeof data1 == "number") && (isFinite(data1)))) && (typeof data1 !== "string")){
validate15.errors = [{instancePath:instancePath+"/orderCreatedDate",schemaPath:"#/properties/orderCreatedDate/type",keyword:"type",params:{type: schema20.properties.orderCreatedDate.type},message:"must be number,string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.orderId !== undefined){
const _errs5 = errors;
if(typeof data.orderId !== "string"){
validate15.errors = [{instancePath:instancePath+"/orderId",schemaPath:"#/properties/orderId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.paymentId !== undefined){
const _errs7 = errors;
if(typeof data.paymentId !== "string"){
validate15.errors = [{instancePath:instancePath+"/paymentId",schemaPath:"#/properties/paymentId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.signature !== undefined){
const _errs9 = errors;
if(typeof data.signature !== "string"){
validate15.errors = [{instancePath:instancePath+"/signature",schemaPath:"#/properties/signature/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs9 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.isRegistration !== undefined){
const _errs11 = errors;
if(typeof data.isRegistration !== "boolean"){
validate15.errors = [{instancePath:instancePath+"/isRegistration",schemaPath:"#/properties/isRegistration/type",keyword:"type",params:{type: "boolean"},message:"must be boolean"}];
return false;
}
var valid0 = _errs11 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
}
else {
validate15.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate15.errors = vErrors;
return errors === 0;
}

export const validateUpdateRideByDriver = validate16;
const schema21 = {"$id":"UpdateRideByDriver","type":"object","properties":{"cabVendorId":{"type":"string"},"bookingId":{"type":"string"},"vehicleRegNo":{"type":"string"},"driverLicenseNo":{"type":"string"},"action":{"enum":["ASSIGN_DRIVER","CANCEL_BY_DRIVER","START_RIDE"]}},"required":["cabVendorId","action","bookingId"]};

function validate16(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="UpdateRideByDriver" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.action === undefined) && (missing0 = "action"))) || ((data.bookingId === undefined) && (missing0 = "bookingId"))){
validate16.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs1 = errors;
if(typeof data.cabVendorId !== "string"){
validate16.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.bookingId !== undefined){
const _errs3 = errors;
if(typeof data.bookingId !== "string"){
validate16.errors = [{instancePath:instancePath+"/bookingId",schemaPath:"#/properties/bookingId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.vehicleRegNo !== undefined){
const _errs5 = errors;
if(typeof data.vehicleRegNo !== "string"){
validate16.errors = [{instancePath:instancePath+"/vehicleRegNo",schemaPath:"#/properties/vehicleRegNo/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.driverLicenseNo !== undefined){
const _errs7 = errors;
if(typeof data.driverLicenseNo !== "string"){
validate16.errors = [{instancePath:instancePath+"/driverLicenseNo",schemaPath:"#/properties/driverLicenseNo/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.action !== undefined){
let data4 = data.action;
const _errs9 = errors;
if(!(((data4 === "ASSIGN_DRIVER") || (data4 === "CANCEL_BY_DRIVER")) || (data4 === "START_RIDE"))){
validate16.errors = [{instancePath:instancePath+"/action",schemaPath:"#/properties/action/enum",keyword:"enum",params:{allowedValues: schema21.properties.action.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs9 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
else {
validate16.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate16.errors = vErrors;
return errors === 0;
}

export const validateUpdateVendorConfig = validate17;
const schema22 = {"$id":"UpdateVendorConfig","type":"object","properties":{"cabVendorId":{"type":"string"},"deviceToken":{"type":"string"}},"required":["cabVendorId","deviceToken"]};

function validate17(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="UpdateVendorConfig" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.deviceToken === undefined) && (missing0 = "deviceToken"))){
validate17.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.cabVendorId !== undefined){
const _errs1 = errors;
if(typeof data.cabVendorId !== "string"){
validate17.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.deviceToken !== undefined){
const _errs3 = errors;
if(typeof data.deviceToken !== "string"){
validate17.errors = [{instancePath:instancePath+"/deviceToken",schemaPath:"#/properties/deviceToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
}
}
}
else {
validate17.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate17.errors = vErrors;
return errors === 0;
}

export const validateGetRateCards = validate18;
const schema23 = {"$id":"GetRateCards","type":"object","properties":{"tripType":{"enum":["ONE_WAY","ROUND_TRIP"]},"pickupDate":{"type":"string"},"dropDate":{"type":"string"},"pickupPlaceId":{"type":"string"},"dropPlaceId":{"type":"string"},"keepCab":{"type":"string"},"latitude":{"type":"string"},"longitude":{"type":"string"},"intermediates":{"type":"string"}},"required":["tripType","pickupPlaceId","dropPlaceId"]};

function validate18(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="GetRateCards" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.tripType === undefined) && (missing0 = "tripType")) || ((data.pickupPlaceId === undefined) && (missing0 = "pickupPlaceId"))) || ((data.dropPlaceId === undefined) && (missing0 = "dropPlaceId"))){
validate18.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.tripType !== undefined){
let data0 = data.tripType;
const _errs1 = errors;
if(!((data0 === "ONE_WAY") || (data0 === "ROUND_TRIP"))){
validate18.errors = [{instancePath:instancePath+"/tripType",schemaPath:"#/properties/tripType/enum",keyword:"enum",params:{allowedValues: schema23.properties.tripType.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.pickupDate !== undefined){
const _errs2 = errors;
if(typeof data.pickupDate !== "string"){
validate18.errors = [{instancePath:instancePath+"/pickupDate",schemaPath:"#/properties/pickupDate/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs2 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.dropDate !== undefined){
const _errs4 = errors;
if(typeof data.dropDate !== "string"){
validate18.errors = [{instancePath:instancePath+"/dropDate",schemaPath:"#/properties/dropDate/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs4 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.pickupPlaceId !== undefined){
const _errs6 = errors;
if(typeof data.pickupPlaceId !== "string"){
validate18.errors = [{instancePath:instancePath+"/pickupPlaceId",schemaPath:"#/properties/pickupPlaceId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs6 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.dropPlaceId !== undefined){
const _errs8 = errors;
if(typeof data.dropPlaceId !== "string"){
validate18.errors = [{instancePath:instancePath+"/dropPlaceId",schemaPath:"#/properties/dropPlaceId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs8 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.keepCab !== undefined){
const _errs10 = errors;
if(typeof data.keepCab !== "string"){
validate18.errors = [{instancePath:instancePath+"/keepCab",schemaPath:"#/properties/keepCab/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs10 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.latitude !== undefined){
const _errs12 = errors;
if(typeof data.latitude !== "string"){
validate18.errors = [{instancePath:instancePath+"/latitude",schemaPath:"#/properties/latitude/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs12 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.longitude !== undefined){
const _errs14 = errors;
if(typeof data.longitude !== "string"){
validate18.errors = [{instancePath:instancePath+"/longitude",schemaPath:"#/properties/longitude/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs14 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.intermediates !== undefined){
const _errs16 = errors;
if(typeof data.intermediates !== "string"){
validate18.errors = [{instancePath:instancePath+"/intermediates",schemaPath:"#/properties/intermediates/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs16 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
}
}
}
}
else {
validate18.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate18.errors = vErrors;
return errors === 0;
}

export const validatePostRides = validate19;
const schema24 = {"$id":"PostRides","type":"object","properties":{"bookingDetails":{"$ref":"GetRateCards"},"rateCardId":{"type":"number"},"customerDetails":{"type":"object","properties":{"phoneNumber":{"type":"string"},"name":{"type":"string"},"email":{"type":"string"}},"required":["phoneNumber","name"]}},"required":["bookingDetails","rateCardId","customerDetails"]};

function validate19(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="PostRides" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.bookingDetails === undefined) && (missing0 = "bookingDetails")) || ((data.rateCardId === undefined) && (missing0 = "rateCardId"))) || ((data.customerDetails === undefined) && (missing0 = "customerDetails"))){
validate19.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.bookingDetails !== undefined){
let data0 = data.bookingDetails;
const _errs1 = errors;
const _errs2 = errors;
if(errors === _errs2){
if(data0 && typeof data0 == "object" && !Array.isArray(data0)){
let missing1;
if((((data0.tripType === undefined) && (missing1 = "tripType")) || ((data0.pickupPlaceId === undefined) && (missing1 = "pickupPlaceId"))) || ((data0.dropPlaceId === undefined) && (missing1 = "dropPlaceId"))){
validate19.errors = [{instancePath:instancePath+"/bookingDetails",schemaPath:"GetRateCards/required",keyword:"required",params:{missingProperty: missing1},message:"must have required property '"+missing1+"'"}];
return false;
}
else {
if(data0.tripType !== undefined){
let data1 = data0.tripType;
const _errs4 = errors;
if(!((data1 === "ONE_WAY") || (data1 === "ROUND_TRIP"))){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/tripType",schemaPath:"GetRateCards/properties/tripType/enum",keyword:"enum",params:{allowedValues: schema23.properties.tripType.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid2 = _errs4 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.pickupDate !== undefined){
const _errs5 = errors;
if(typeof data0.pickupDate !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/pickupDate",schemaPath:"GetRateCards/properties/pickupDate/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs5 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.dropDate !== undefined){
const _errs7 = errors;
if(typeof data0.dropDate !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/dropDate",schemaPath:"GetRateCards/properties/dropDate/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs7 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.pickupPlaceId !== undefined){
const _errs9 = errors;
if(typeof data0.pickupPlaceId !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/pickupPlaceId",schemaPath:"GetRateCards/properties/pickupPlaceId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs9 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.dropPlaceId !== undefined){
const _errs11 = errors;
if(typeof data0.dropPlaceId !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/dropPlaceId",schemaPath:"GetRateCards/properties/dropPlaceId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs11 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.keepCab !== undefined){
const _errs13 = errors;
if(typeof data0.keepCab !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/keepCab",schemaPath:"GetRateCards/properties/keepCab/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs13 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.latitude !== undefined){
const _errs15 = errors;
if(typeof data0.latitude !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/latitude",schemaPath:"GetRateCards/properties/latitude/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs15 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.longitude !== undefined){
const _errs17 = errors;
if(typeof data0.longitude !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/longitude",schemaPath:"GetRateCards/properties/longitude/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs17 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.intermediates !== undefined){
const _errs19 = errors;
if(typeof data0.intermediates !== "string"){
validate19.errors = [{instancePath:instancePath+"/bookingDetails/intermediates",schemaPath:"GetRateCards/properties/intermediates/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs19 === errors;
}
else {
var valid2 = true;
}
}
}
}
}
}
}
}
}
}
}
else {
validate19.errors = [{instancePath:instancePath+"/bookingDetails",schemaPath:"GetRateCards/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.rateCardId !== undefined){
let data10 = data.rateCardId;
const _errs21 = errors;
if(!((typeof data10 == "number") && (isFinite(data10)))){
validate19.errors = [{instancePath:instancePath+"/rateCardId",schemaPath:"#/properties/rateCardId/type",keyword:"type",params:{type: "number"},message:"must be number"}];
return false;
}
var valid0 = _errs21 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.customerDetails !== undefined){
let data11 = data.customerDetails;
const _errs23 = errors;
if(errors === _errs23){
if(data11 && typeof data11 == "object" && !Array.isArray(data11)){
let missing2;
if(((data11.phoneNumber === undefined) && (missing2 = "phoneNumber")) || ((data11.name === undefined) && (missing2 = "name"))){
validate19.errors = [{instancePath:instancePath+"/customerDetails",schemaPath:"#/properties/customerDetails/required",keyword:"required",params:{missingProperty: missing2},message:"must have required property '"+missing2+"'"}];
return false;
}
else {
if(data11.phoneNumber !== undefined){
const _errs25 = errors;
if(typeof data11.phoneNumber !== "string"){
validate19.errors = [{instancePath:instancePath+"/customerDetails/phoneNumber",schemaPath:"#/properties/customerDetails/properties/phoneNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid3 = _errs25 === errors;
}
else {
var valid3 = true;
}
if(valid3){
if(data11.name !== undefined){
const _errs27 = errors;
if(typeof data11.name !== "string"){
validate19.errors = [{instancePath:instancePath+"/customerDetails/name",schemaPath:"#/properties/customerDetails/properties/name/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid3 = _errs27 === errors;
}
else {
var valid3 = true;
}
if(valid3){
if(data11.email !== undefined){
const _errs29 = errors;
if(typeof data11.email !== "string"){
validate19.errors = [{instancePath:instancePath+"/customerDetails/email",schemaPath:"#/properties/customerDetails/properties/email/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid3 = _errs29 === errors;
}
else {
var valid3 = true;
}
}
}
}
}
else {
validate19.errors = [{instancePath:instancePath+"/customerDetails",schemaPath:"#/properties/customerDetails/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs23 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
else {
validate19.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate19.errors = vErrors;
return errors === 0;
}

export const validateRateCardConfigUpdate = validate20;
const schema26 = {"$id":"UpdateRateCardConfig","type":"object","properties":{"rateCards":{"type":"object","properties":{".*":{"$ref":"RateCardId"}}}},"required":["rateCards"]};
const schema27 = {"$id":"RateCardId","type":"object","properties":{"rate":{"type":"number"},"bata":{"type":"number"}},"required":["rate"]};

function validate20(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="UpdateRateCardConfig" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((data.rateCards === undefined) && (missing0 = "rateCards")){
validate20.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.rateCards !== undefined){
let data0 = data.rateCards;
const _errs1 = errors;
if(errors === _errs1){
if(data0 && typeof data0 == "object" && !Array.isArray(data0)){
if(data0[".*"] !== undefined){
let data1 = data0[".*"];
const _errs4 = errors;
if(errors === _errs4){
if(data1 && typeof data1 == "object" && !Array.isArray(data1)){
let missing1;
if((data1.rate === undefined) && (missing1 = "rate")){
validate20.errors = [{instancePath:instancePath+"/rateCards/.*",schemaPath:"RateCardId/required",keyword:"required",params:{missingProperty: missing1},message:"must have required property '"+missing1+"'"}];
return false;
}
else {
if(data1.rate !== undefined){
let data2 = data1.rate;
const _errs6 = errors;
if(!((typeof data2 == "number") && (isFinite(data2)))){
validate20.errors = [{instancePath:instancePath+"/rateCards/.*/rate",schemaPath:"RateCardId/properties/rate/type",keyword:"type",params:{type: "number"},message:"must be number"}];
return false;
}
var valid3 = _errs6 === errors;
}
else {
var valid3 = true;
}
if(valid3){
if(data1.bata !== undefined){
let data3 = data1.bata;
const _errs8 = errors;
if(!((typeof data3 == "number") && (isFinite(data3)))){
validate20.errors = [{instancePath:instancePath+"/rateCards/.*/bata",schemaPath:"RateCardId/properties/bata/type",keyword:"type",params:{type: "number"},message:"must be number"}];
return false;
}
var valid3 = _errs8 === errors;
}
else {
var valid3 = true;
}
}
}
}
else {
validate20.errors = [{instancePath:instancePath+"/rateCards/.*",schemaPath:"RateCardId/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
}
}
else {
validate20.errors = [{instancePath:instancePath+"/rateCards",schemaPath:"#/properties/rateCards/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
}
}
}
else {
validate20.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate20.errors = vErrors;
return errors === 0;
}

export const validateOtpRequest = validate21;
const schema28 = {"$id":"OtpRequest","type":"object","properties":{"phoneNumber":{"type":"string"},"otpType":{"enum":["PHONE_NO_VERIFICATION","CUSTOMER_LOGIN"]},"action":{"enum":["GENERATE","RESEND","VERIFY"]},"otpCode":{"type":"integer"},"carrier":{"enum":["WHATSAPP","SMS"]}},"required":["phoneNumber","otpType","action"]};

function validate21(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="OtpRequest" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.phoneNumber === undefined) && (missing0 = "phoneNumber")) || ((data.otpType === undefined) && (missing0 = "otpType"))) || ((data.action === undefined) && (missing0 = "action"))){
validate21.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.phoneNumber !== undefined){
const _errs1 = errors;
if(typeof data.phoneNumber !== "string"){
validate21.errors = [{instancePath:instancePath+"/phoneNumber",schemaPath:"#/properties/phoneNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.otpType !== undefined){
let data1 = data.otpType;
const _errs3 = errors;
if(!((data1 === "PHONE_NO_VERIFICATION") || (data1 === "CUSTOMER_LOGIN"))){
validate21.errors = [{instancePath:instancePath+"/otpType",schemaPath:"#/properties/otpType/enum",keyword:"enum",params:{allowedValues: schema28.properties.otpType.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.action !== undefined){
let data2 = data.action;
const _errs4 = errors;
if(!(((data2 === "GENERATE") || (data2 === "RESEND")) || (data2 === "VERIFY"))){
validate21.errors = [{instancePath:instancePath+"/action",schemaPath:"#/properties/action/enum",keyword:"enum",params:{allowedValues: schema28.properties.action.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs4 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.otpCode !== undefined){
let data3 = data.otpCode;
const _errs5 = errors;
if(!(((typeof data3 == "number") && (!(data3 % 1) && !isNaN(data3))) && (isFinite(data3)))){
validate21.errors = [{instancePath:instancePath+"/otpCode",schemaPath:"#/properties/otpCode/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.carrier !== undefined){
let data4 = data.carrier;
const _errs7 = errors;
if(!((data4 === "WHATSAPP") || (data4 === "SMS"))){
validate21.errors = [{instancePath:instancePath+"/carrier",schemaPath:"#/properties/carrier/enum",keyword:"enum",params:{allowedValues: schema28.properties.carrier.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
else {
validate21.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate21.errors = vErrors;
return errors === 0;
}

export const validateAdminRequest = validate22;
const schema29 = {"$id":"ProcessAdminRequest","type":"object","properties":{"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"},"requestType":{"enum":["GET_RIDES","CANCEL_RIDES","GET_VENDORS","UPDATE_VENDOR_STATE","UPDATE_VENDOR","GET_VENDORS_BY_STATUS","DUMP_TABLE","UPDATE_VENDOR_META"]},"payload":{"type":"object"}},"required":["googleUserId","googleIdToken","requestType","payload"]};

function validate22(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="ProcessAdminRequest" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((((data.googleUserId === undefined) && (missing0 = "googleUserId")) || ((data.googleIdToken === undefined) && (missing0 = "googleIdToken"))) || ((data.requestType === undefined) && (missing0 = "requestType"))) || ((data.payload === undefined) && (missing0 = "payload"))){
validate22.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.googleUserId !== undefined){
const _errs1 = errors;
if(typeof data.googleUserId !== "string"){
validate22.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs3 = errors;
if(typeof data.googleIdToken !== "string"){
validate22.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.requestType !== undefined){
let data2 = data.requestType;
const _errs5 = errors;
if(!((((((((data2 === "GET_RIDES") || (data2 === "CANCEL_RIDES")) || (data2 === "GET_VENDORS")) || (data2 === "UPDATE_VENDOR_STATE")) || (data2 === "UPDATE_VENDOR")) || (data2 === "GET_VENDORS_BY_STATUS")) || (data2 === "DUMP_TABLE")) || (data2 === "UPDATE_VENDOR_META"))){
validate22.errors = [{instancePath:instancePath+"/requestType",schemaPath:"#/properties/requestType/enum",keyword:"enum",params:{allowedValues: schema29.properties.requestType.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.payload !== undefined){
let data3 = data.payload;
const _errs6 = errors;
if(!(data3 && typeof data3 == "object" && !Array.isArray(data3))){
validate22.errors = [{instancePath:instancePath+"/payload",schemaPath:"#/properties/payload/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
var valid0 = _errs6 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
else {
validate22.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate22.errors = vErrors;
return errors === 0;
}

export const validateAdminCancelRideRequest = validate23;
const schema30 = {"$id":"AdminCancelRideRequest","type":"object","properties":{"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"},"bookingId":{"type":"string"},"curRideStatus":{"enum":["UNAPPROVED","UNASSIGNED","ASSIGNED","ONGOING","COMPLETED"]},"cabVendorId":{"type":"string"}},"required":["googleUserId","googleIdToken","bookingId","curRideStatus","cabVendorId"]};

function validate23(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="AdminCancelRideRequest" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((((data.googleUserId === undefined) && (missing0 = "googleUserId")) || ((data.googleIdToken === undefined) && (missing0 = "googleIdToken"))) || ((data.bookingId === undefined) && (missing0 = "bookingId"))) || ((data.curRideStatus === undefined) && (missing0 = "curRideStatus"))) || ((data.cabVendorId === undefined) && (missing0 = "cabVendorId"))){
validate23.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.googleUserId !== undefined){
const _errs1 = errors;
if(typeof data.googleUserId !== "string"){
validate23.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs3 = errors;
if(typeof data.googleIdToken !== "string"){
validate23.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.bookingId !== undefined){
const _errs5 = errors;
if(typeof data.bookingId !== "string"){
validate23.errors = [{instancePath:instancePath+"/bookingId",schemaPath:"#/properties/bookingId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.curRideStatus !== undefined){
let data3 = data.curRideStatus;
const _errs7 = errors;
if(!(((((data3 === "UNAPPROVED") || (data3 === "UNASSIGNED")) || (data3 === "ASSIGNED")) || (data3 === "ONGOING")) || (data3 === "COMPLETED"))){
validate23.errors = [{instancePath:instancePath+"/curRideStatus",schemaPath:"#/properties/curRideStatus/enum",keyword:"enum",params:{allowedValues: schema30.properties.curRideStatus.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.cabVendorId !== undefined){
const _errs8 = errors;
if(typeof data.cabVendorId !== "string"){
validate23.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs8 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
else {
validate23.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate23.errors = vErrors;
return errors === 0;
}

export const validateDocumentMgmtUrl = validate24;
const schema31 = {"$id":"DocumentMgmtUrl","type":"object","properties":{"category":{"enum":["CABVENDOR"]},"action":{"enum":["POST","GET"]},"groupId":{"type":"string"}},"required":["category","action","groupId"]};

function validate24(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="DocumentMgmtUrl" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.category === undefined) && (missing0 = "category")) || ((data.action === undefined) && (missing0 = "action"))) || ((data.groupId === undefined) && (missing0 = "groupId"))){
validate24.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.category !== undefined){
const _errs1 = errors;
if(!(data.category === "CABVENDOR")){
validate24.errors = [{instancePath:instancePath+"/category",schemaPath:"#/properties/category/enum",keyword:"enum",params:{allowedValues: schema31.properties.category.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.action !== undefined){
let data1 = data.action;
const _errs2 = errors;
if(!((data1 === "POST") || (data1 === "GET"))){
validate24.errors = [{instancePath:instancePath+"/action",schemaPath:"#/properties/action/enum",keyword:"enum",params:{allowedValues: schema31.properties.action.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs2 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.groupId !== undefined){
const _errs3 = errors;
if(typeof data.groupId !== "string"){
validate24.errors = [{instancePath:instancePath+"/groupId",schemaPath:"#/properties/groupId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
else {
validate24.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate24.errors = vErrors;
return errors === 0;
}

export const validateAdminUpdateRide = validate25;
const schema32 = {"$id":"UpdateRide","type":"object","properties":{"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"},"bookingId":{"type":"string"},"dryRun":{"type":"boolean"},"customer":{"type":"object","properties":{"name":{"type":"string"},"phoneNumber":{"type":"string"},"email":{"type":"string"}}},"price":{"type":"object","properties":{"driverBata":{"type":"integer"},"ratePerKm":{"type":"integer"},"totalAmount":{"type":"integer"}}}},"required":["googleUserId","googleIdToken","bookingId","dryRun"]};

function validate25(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="UpdateRide" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((((data.googleUserId === undefined) && (missing0 = "googleUserId")) || ((data.googleIdToken === undefined) && (missing0 = "googleIdToken"))) || ((data.bookingId === undefined) && (missing0 = "bookingId"))) || ((data.dryRun === undefined) && (missing0 = "dryRun"))){
validate25.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.googleUserId !== undefined){
const _errs1 = errors;
if(typeof data.googleUserId !== "string"){
validate25.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs3 = errors;
if(typeof data.googleIdToken !== "string"){
validate25.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.bookingId !== undefined){
const _errs5 = errors;
if(typeof data.bookingId !== "string"){
validate25.errors = [{instancePath:instancePath+"/bookingId",schemaPath:"#/properties/bookingId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.dryRun !== undefined){
const _errs7 = errors;
if(typeof data.dryRun !== "boolean"){
validate25.errors = [{instancePath:instancePath+"/dryRun",schemaPath:"#/properties/dryRun/type",keyword:"type",params:{type: "boolean"},message:"must be boolean"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.customer !== undefined){
let data4 = data.customer;
const _errs9 = errors;
if(errors === _errs9){
if(data4 && typeof data4 == "object" && !Array.isArray(data4)){
if(data4.name !== undefined){
const _errs11 = errors;
if(typeof data4.name !== "string"){
validate25.errors = [{instancePath:instancePath+"/customer/name",schemaPath:"#/properties/customer/properties/name/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid1 = _errs11 === errors;
}
else {
var valid1 = true;
}
if(valid1){
if(data4.phoneNumber !== undefined){
const _errs13 = errors;
if(typeof data4.phoneNumber !== "string"){
validate25.errors = [{instancePath:instancePath+"/customer/phoneNumber",schemaPath:"#/properties/customer/properties/phoneNumber/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid1 = _errs13 === errors;
}
else {
var valid1 = true;
}
if(valid1){
if(data4.email !== undefined){
const _errs15 = errors;
if(typeof data4.email !== "string"){
validate25.errors = [{instancePath:instancePath+"/customer/email",schemaPath:"#/properties/customer/properties/email/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid1 = _errs15 === errors;
}
else {
var valid1 = true;
}
}
}
}
else {
validate25.errors = [{instancePath:instancePath+"/customer",schemaPath:"#/properties/customer/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs9 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.price !== undefined){
let data8 = data.price;
const _errs17 = errors;
if(errors === _errs17){
if(data8 && typeof data8 == "object" && !Array.isArray(data8)){
if(data8.driverBata !== undefined){
let data9 = data8.driverBata;
const _errs19 = errors;
if(!(((typeof data9 == "number") && (!(data9 % 1) && !isNaN(data9))) && (isFinite(data9)))){
validate25.errors = [{instancePath:instancePath+"/price/driverBata",schemaPath:"#/properties/price/properties/driverBata/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid2 = _errs19 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data8.ratePerKm !== undefined){
let data10 = data8.ratePerKm;
const _errs21 = errors;
if(!(((typeof data10 == "number") && (!(data10 % 1) && !isNaN(data10))) && (isFinite(data10)))){
validate25.errors = [{instancePath:instancePath+"/price/ratePerKm",schemaPath:"#/properties/price/properties/ratePerKm/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid2 = _errs21 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data8.totalAmount !== undefined){
let data11 = data8.totalAmount;
const _errs23 = errors;
if(!(((typeof data11 == "number") && (!(data11 % 1) && !isNaN(data11))) && (isFinite(data11)))){
validate25.errors = [{instancePath:instancePath+"/price/totalAmount",schemaPath:"#/properties/price/properties/totalAmount/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid2 = _errs23 === errors;
}
else {
var valid2 = true;
}
}
}
}
else {
validate25.errors = [{instancePath:instancePath+"/price",schemaPath:"#/properties/price/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs17 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
}
else {
validate25.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate25.errors = vErrors;
return errors === 0;
}

export const validateAdminUpdateWallet = validate26;
const schema33 = {"$id":"UpdateWallet","type":"object","properties":{"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"},"cabVendorId":{"type":"string"},"amount":{"type":"integer"},"action":{"enum":["ADD","DEDUCT"]},"comment":{"type":"string"}},"required":["cabVendorId","amount","action","googleUserId","googleIdToken"]};

function validate26(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="UpdateWallet" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.amount === undefined) && (missing0 = "amount"))) || ((data.action === undefined) && (missing0 = "action"))) || ((data.googleUserId === undefined) && (missing0 = "googleUserId"))) || ((data.googleIdToken === undefined) && (missing0 = "googleIdToken"))){
validate26.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.googleUserId !== undefined){
const _errs1 = errors;
if(typeof data.googleUserId !== "string"){
validate26.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs3 = errors;
if(typeof data.googleIdToken !== "string"){
validate26.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.cabVendorId !== undefined){
const _errs5 = errors;
if(typeof data.cabVendorId !== "string"){
validate26.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.amount !== undefined){
let data3 = data.amount;
const _errs7 = errors;
if(!(((typeof data3 == "number") && (!(data3 % 1) && !isNaN(data3))) && (isFinite(data3)))){
validate26.errors = [{instancePath:instancePath+"/amount",schemaPath:"#/properties/amount/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.action !== undefined){
let data4 = data.action;
const _errs9 = errors;
if(!((data4 === "ADD") || (data4 === "DEDUCT"))){
validate26.errors = [{instancePath:instancePath+"/action",schemaPath:"#/properties/action/enum",keyword:"enum",params:{allowedValues: schema33.properties.action.enum},message:"must be equal to one of the allowed values"}];
return false;
}
var valid0 = _errs9 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.comment !== undefined){
const _errs10 = errors;
if(typeof data.comment !== "string"){
validate26.errors = [{instancePath:instancePath+"/comment",schemaPath:"#/properties/comment/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs10 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
}
else {
validate26.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate26.errors = vErrors;
return errors === 0;
}

export const validateCloseRideEndpoint = validate27;
const schema34 = {"$id":"CloseRide","type":"object","properties":{"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"},"cabVendorId":{"type":"string"},"bookingId":{"type":"string"},"isAdmin":{"type":"boolean"},"distance":{"type":"integer"},"petCharge":{"type":"integer"},"hillCharge":{"type":"integer"},"waitingCharge":{"type":"integer"},"parkingCharge":{"type":"integer"},"tollAmount":{"type":"integer"},"amountCollectedFromCustomer":{"type":"integer"}},"required":["cabVendorId","googleIdToken","bookingId","distance"]};

function validate27(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="CloseRide" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.googleIdToken === undefined) && (missing0 = "googleIdToken"))) || ((data.bookingId === undefined) && (missing0 = "bookingId"))) || ((data.distance === undefined) && (missing0 = "distance"))){
validate27.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.googleUserId !== undefined){
const _errs1 = errors;
if(typeof data.googleUserId !== "string"){
validate27.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs3 = errors;
if(typeof data.googleIdToken !== "string"){
validate27.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.cabVendorId !== undefined){
const _errs5 = errors;
if(typeof data.cabVendorId !== "string"){
validate27.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.bookingId !== undefined){
const _errs7 = errors;
if(typeof data.bookingId !== "string"){
validate27.errors = [{instancePath:instancePath+"/bookingId",schemaPath:"#/properties/bookingId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.isAdmin !== undefined){
const _errs9 = errors;
if(typeof data.isAdmin !== "boolean"){
validate27.errors = [{instancePath:instancePath+"/isAdmin",schemaPath:"#/properties/isAdmin/type",keyword:"type",params:{type: "boolean"},message:"must be boolean"}];
return false;
}
var valid0 = _errs9 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.distance !== undefined){
let data5 = data.distance;
const _errs11 = errors;
if(!(((typeof data5 == "number") && (!(data5 % 1) && !isNaN(data5))) && (isFinite(data5)))){
validate27.errors = [{instancePath:instancePath+"/distance",schemaPath:"#/properties/distance/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs11 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.petCharge !== undefined){
let data6 = data.petCharge;
const _errs13 = errors;
if(!(((typeof data6 == "number") && (!(data6 % 1) && !isNaN(data6))) && (isFinite(data6)))){
validate27.errors = [{instancePath:instancePath+"/petCharge",schemaPath:"#/properties/petCharge/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs13 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.hillCharge !== undefined){
let data7 = data.hillCharge;
const _errs15 = errors;
if(!(((typeof data7 == "number") && (!(data7 % 1) && !isNaN(data7))) && (isFinite(data7)))){
validate27.errors = [{instancePath:instancePath+"/hillCharge",schemaPath:"#/properties/hillCharge/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs15 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.waitingCharge !== undefined){
let data8 = data.waitingCharge;
const _errs17 = errors;
if(!(((typeof data8 == "number") && (!(data8 % 1) && !isNaN(data8))) && (isFinite(data8)))){
validate27.errors = [{instancePath:instancePath+"/waitingCharge",schemaPath:"#/properties/waitingCharge/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs17 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.parkingCharge !== undefined){
let data9 = data.parkingCharge;
const _errs19 = errors;
if(!(((typeof data9 == "number") && (!(data9 % 1) && !isNaN(data9))) && (isFinite(data9)))){
validate27.errors = [{instancePath:instancePath+"/parkingCharge",schemaPath:"#/properties/parkingCharge/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs19 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.tollAmount !== undefined){
let data10 = data.tollAmount;
const _errs21 = errors;
if(!(((typeof data10 == "number") && (!(data10 % 1) && !isNaN(data10))) && (isFinite(data10)))){
validate27.errors = [{instancePath:instancePath+"/tollAmount",schemaPath:"#/properties/tollAmount/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs21 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.amountCollectedFromCustomer !== undefined){
let data11 = data.amountCollectedFromCustomer;
const _errs23 = errors;
if(!(((typeof data11 == "number") && (!(data11 % 1) && !isNaN(data11))) && (isFinite(data11)))){
validate27.errors = [{instancePath:instancePath+"/amountCollectedFromCustomer",schemaPath:"#/properties/amountCollectedFromCustomer/type",keyword:"type",params:{type: "integer"},message:"must be integer"}];
return false;
}
var valid0 = _errs23 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
else {
validate27.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate27.errors = vErrors;
return errors === 0;
}

export const validateLocationData = validate28;
const schema35 = {"$id":"LocData","type":"object","properties":{"googleUserId":{"type":"string"},"googleIdToken":{"type":"string"},"cabVendorId":{"type":"string"},"coordinates":{"type":"array","items":{"type":"number"},"minItems":2,"maxItems":2},"locationTime":{"type":"string"}},"required":["cabVendorId","coordinates","locationTime"]};

function validate28(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="LocData" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if((((data.cabVendorId === undefined) && (missing0 = "cabVendorId")) || ((data.coordinates === undefined) && (missing0 = "coordinates"))) || ((data.locationTime === undefined) && (missing0 = "locationTime"))){
validate28.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.googleUserId !== undefined){
const _errs1 = errors;
if(typeof data.googleUserId !== "string"){
validate28.errors = [{instancePath:instancePath+"/googleUserId",schemaPath:"#/properties/googleUserId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.googleIdToken !== undefined){
const _errs3 = errors;
if(typeof data.googleIdToken !== "string"){
validate28.errors = [{instancePath:instancePath+"/googleIdToken",schemaPath:"#/properties/googleIdToken/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs3 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.cabVendorId !== undefined){
const _errs5 = errors;
if(typeof data.cabVendorId !== "string"){
validate28.errors = [{instancePath:instancePath+"/cabVendorId",schemaPath:"#/properties/cabVendorId/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs5 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.coordinates !== undefined){
let data3 = data.coordinates;
const _errs7 = errors;
if(errors === _errs7){
if(Array.isArray(data3)){
if(data3.length > 2){
validate28.errors = [{instancePath:instancePath+"/coordinates",schemaPath:"#/properties/coordinates/maxItems",keyword:"maxItems",params:{limit: 2},message:"must NOT have more than 2 items"}];
return false;
}
else {
if(data3.length < 2){
validate28.errors = [{instancePath:instancePath+"/coordinates",schemaPath:"#/properties/coordinates/minItems",keyword:"minItems",params:{limit: 2},message:"must NOT have fewer than 2 items"}];
return false;
}
else {
var valid1 = true;
const len0 = data3.length;
for(let i0=0; i0<len0; i0++){
let data4 = data3[i0];
const _errs9 = errors;
if(!((typeof data4 == "number") && (isFinite(data4)))){
validate28.errors = [{instancePath:instancePath+"/coordinates/" + i0,schemaPath:"#/properties/coordinates/items/type",keyword:"type",params:{type: "number"},message:"must be number"}];
return false;
}
var valid1 = _errs9 === errors;
if(!valid1){
break;
}
}
}
}
}
else {
validate28.errors = [{instancePath:instancePath+"/coordinates",schemaPath:"#/properties/coordinates/type",keyword:"type",params:{type: "array"},message:"must be array"}];
return false;
}
}
var valid0 = _errs7 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.locationTime !== undefined){
const _errs11 = errors;
if(typeof data.locationTime !== "string"){
validate28.errors = [{instancePath:instancePath+"/locationTime",schemaPath:"#/properties/locationTime/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs11 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
}
}
else {
validate28.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate28.errors = vErrors;
return errors === 0;
}

export const validateCreateCustomerAccount = validate29;
const schema36 = {"$id":"CreateCustomerAccount","type":"object","properties":{"customer":{"$ref":"CustomerAccountInfo"},"generateOTP":{"type":"boolean"},"otpCarrier":{"type":"string"}},"required":["customer","generateOTP"]};
const schema37 = {"$id":"CustomerAccountInfo","type":"object","properties":{"phoneNo":{"type":"string"},"name":{"type":"string"}},"required":["phoneNo"]};

function validate29(data, {instancePath="", parentData, parentDataProperty, rootData=data}={}){
/*# sourceURL="CreateCustomerAccount" */;
let vErrors = null;
let errors = 0;
if(errors === 0){
if(data && typeof data == "object" && !Array.isArray(data)){
let missing0;
if(((data.customer === undefined) && (missing0 = "customer")) || ((data.generateOTP === undefined) && (missing0 = "generateOTP"))){
validate29.errors = [{instancePath,schemaPath:"#/required",keyword:"required",params:{missingProperty: missing0},message:"must have required property '"+missing0+"'"}];
return false;
}
else {
if(data.customer !== undefined){
let data0 = data.customer;
const _errs1 = errors;
const _errs2 = errors;
if(errors === _errs2){
if(data0 && typeof data0 == "object" && !Array.isArray(data0)){
let missing1;
if((data0.phoneNo === undefined) && (missing1 = "phoneNo")){
validate29.errors = [{instancePath:instancePath+"/customer",schemaPath:"CustomerAccountInfo/required",keyword:"required",params:{missingProperty: missing1},message:"must have required property '"+missing1+"'"}];
return false;
}
else {
if(data0.phoneNo !== undefined){
const _errs4 = errors;
if(typeof data0.phoneNo !== "string"){
validate29.errors = [{instancePath:instancePath+"/customer/phoneNo",schemaPath:"CustomerAccountInfo/properties/phoneNo/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs4 === errors;
}
else {
var valid2 = true;
}
if(valid2){
if(data0.name !== undefined){
const _errs6 = errors;
if(typeof data0.name !== "string"){
validate29.errors = [{instancePath:instancePath+"/customer/name",schemaPath:"CustomerAccountInfo/properties/name/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid2 = _errs6 === errors;
}
else {
var valid2 = true;
}
}
}
}
else {
validate29.errors = [{instancePath:instancePath+"/customer",schemaPath:"CustomerAccountInfo/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
var valid0 = _errs1 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.generateOTP !== undefined){
const _errs8 = errors;
if(typeof data.generateOTP !== "boolean"){
validate29.errors = [{instancePath:instancePath+"/generateOTP",schemaPath:"#/properties/generateOTP/type",keyword:"type",params:{type: "boolean"},message:"must be boolean"}];
return false;
}
var valid0 = _errs8 === errors;
}
else {
var valid0 = true;
}
if(valid0){
if(data.otpCarrier !== undefined){
const _errs10 = errors;
if(typeof data.otpCarrier !== "string"){
validate29.errors = [{instancePath:instancePath+"/otpCarrier",schemaPath:"#/properties/otpCarrier/type",keyword:"type",params:{type: "string"},message:"must be string"}];
return false;
}
var valid0 = _errs10 === errors;
}
else {
var valid0 = true;
}
}
}
}
}
else {
validate29.errors = [{instancePath,schemaPath:"#/type",keyword:"type",params:{type: "object"},message:"must be object"}];
return false;
}
}
validate29.errors = vErrors;
return errors === 0;
}
