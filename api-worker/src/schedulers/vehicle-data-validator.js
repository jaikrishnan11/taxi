import elasticClientService from "../service/elastic-client-service"

class VehicleValidator { 
    constructor() {

    }

    async validateAllVehicles() {
        try {
            let allVehicleDetails = await elasticClientService.fetchCompleteVehicleIndex()
            console.log("Printing the vehicle length", allVehicleDetails.length)
        } catch (err) {
            console.log("Error while fetching all the vehicle details", err)
        }
    }
}

export default new VehicleValidator()