const VEHICLES = {
    "SEDAN": "SEDAN",
    "ETIOS": "ETIOS",
    "SUV": "SUV",
    "INNOVA": "INNOVA",
    "OTHERS": "OTHERS"
};

const LANGUAGES = {
    "TAMIL": "TAMIL",
    "ENGLISH": "ENGLISH",
    "TELUGU": "TELUGU",
    "HINDI": "HINDI",
    "KANNADA": "KANNADA",
    "MALAYALAM": "MALAYALAM"
};

export { VEHICLES, LANGUAGES };