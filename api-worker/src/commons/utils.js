const moment = require("moment")
const VEHICLE_REGEX = /^[A-Z]{2}\d{2}[A-Z]{0,2}\d{4}$/
const LICENSE_REGEX = /^([A-Z]{2}\d{13})$/;
import { VEHICLES } from './models'
import _ from 'lodash'
const DATE_FORMAT = "DD-MMM-YYYY"

export default {
    removeCountryCode(phoneNumber) {
        const countryCode = "+91";
        const countryCode2 = "91";
        const countryCode3 = "+";
        const formattedNumber = phoneNumber.trim().replace(/\s+/g, ""); // remove whitespace and format number

        if (formattedNumber.startsWith(countryCode)) {
            return formattedNumber.substr(countryCode.length);
        } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode2)) {
            return formattedNumber.substr(countryCode2.length);
        } else if (formattedNumber.length > 10 && formattedNumber.startsWith(countryCode3)) {
            return formattedNumber.substr(countryCode3.length);
        } else {
            return formattedNumber;
        }
    },
    parseVehicleNumber(vehicleNumber) {
        return vehicleNumber.toUpperCase().replace(/\s/g, '');
    },
    validateVehicleNumber(vehicleNumber) {
        return vehicleNumber.match(VEHICLE_REGEX)
    },
    parseLicenseNumber(licenseNumber) {
        return licenseNumber.toUpperCase().replace(/\s/g, '')
        .replace('-','');
    },
    validateLicenseNumber(licenseNumber) {
        return licenseNumber.length > 14 && licenseNumber.length <= 16
        //return licenseNumber.match(LICENSE_REGEX)
    },
    fixVehicleDateFields(vehicleInfo) {
        if(moment(vehicleInfo.rcExpiryDate).isValid()) {
            vehicleInfo.rcExpiryDate = moment(vehicleInfo.rcExpiryDate).format(DATE_FORMAT)
        }
        if(moment(vehicleInfo.vehicleInsuranceUpto).isValid()) {
            vehicleInfo.vehicleInsuranceUpto = moment(vehicleInfo.vehicleInsuranceUpto).format(DATE_FORMAT)
        }
        if(moment(vehicleInfo.puccUpto).isValid()) {
            vehicleInfo.puccUpto = moment(vehicleInfo.puccUpto).format(DATE_FORMAT)
        }
        return vehicleInfo
    },
    validateVehicleInfo(vehicleInfo, dateForValidation) {
        let validationFailures = []
        if(dateForValidation == undefined) {
            dateForValidation = moment()
        } else {
            dateForValidation = moment(dateForValidation)
        }
        // check the active status
        // if (vehicleInfo.status != "ACTIVE") {
        //     validationFailures.push(`vehicle status is ${vehicleInfo.status}`)
        // }

        // checking the blacklist status
        if (vehicleInfo.blacklistStatus != "NA" && !_.isEmpty(vehicleInfo.blacklistStatus)) {
            validationFailures.push("Black list details is not empty " + JSON.stringify(vehicleInfo.blacklistDetails))
        }

        // check whether its commercial vehicle or not
        if (!vehicleInfo.isCommercial && !_.lowerCase(vehicleInfo.permitType).includes("maxi cab permit")) {
            validationFailures.push("Vehicle is not registered as commercial")
        }

        // rc verification
        if (moment(vehicleInfo.rcExpiryDate, DATE_FORMAT).isBefore(dateForValidation, 'day')) {
            validationFailures.push("RC is expired")
        }
        // insurance verification
        if (moment(vehicleInfo.vehicleInsuranceUpto, DATE_FORMAT).isBefore(dateForValidation, 'day')) {
            validationFailures.push("Insurance is expired")
        }

        // Pollution certificate
        if (moment(vehicleInfo.puccUpto, DATE_FORMAT).isBefore(dateForValidation, 'day')) {
            validationFailures.push("Pollution certificate got expired")
        }
        console.log("validation failures", JSON.stringify(validationFailures))
        return {
            approved: validationFailures.length == 0,
            validationFailures
        }
        //TODO: check based on manufacturing details
    },
    validateLicenseInfo(licenseInfo, dateForValidation) {
        let validationFailures = []
        if(dateForValidation == undefined) {
            dateForValidation = moment()
        } else {
            dateForValidation = moment(dateForValidation)
        }
    
        // let isTransportLicensePresent = (licenseInfo?.transport_doe != "1800-01-01" && licenseInfo?.vehicle_classes.includes("LMV-TR"))
        // if(!isTransportLicensePresent) {
        //     validationFailures.push("Transport license not availed for this license")
        // }
        if(_.isEmpty(licenseInfo?.doe)) {
            validationFailures.push("Transport license date is empty")
        } else {
            let licenseExpiry = moment(licenseInfo?.doe, "YYYY-MM-DD")
            if (licenseExpiry.isBefore(dateForValidation)) {
                validationFailures.push("Transport license is expired")
            }
        }

        return {
            approved: validationFailures.length == 0,
            validationFailures
        }
    },
    getVehicleType(vehicleInfo) {
        if (parseInt(vehicleInfo.vehicleSeatCapacity) == 5) {
            if (vehicleInfo.model.includes("ETIOS")) {
                //TODO: etios also has hatch back need to handle this
                return VEHICLES.ETIOS
            }
            return VEHICLES.SEDAN
        }
        if (parseInt(vehicleInfo.vehicleSeatCapacity) == 8) {
            if (vehicleInfo.model == "INNOVA") {
                return VEHICLES.INNOVA
            }
            return VEHICLES.SUV
        }
        return VEHICLES.OTHERS
    }
}