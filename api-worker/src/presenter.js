import _ from "lodash";
import moment from "moment";
import { RideStatus } from "./config/constants";

function  presentMapUrl(ride) {
    let srcPlace = ride.bookingDetails.pickup
    let dstPlace = ride.bookingDetails.drop
    let intermediates = ride.bookingDetails.intermediates
    
    if (intermediates && intermediates.length > 0) {
        const waypoint_place_ids = intermediates.map(i => i.placeId).join("|")
        const waypoints = intermediates.map(i => i.fullAddress).join("|")
        ride.bookingDetails.googleMapsDistanceUrl = encodeURI(`https://www.google.com/maps/dir/?api=1&origin_place_id=${srcPlace.placeId}&destination_place_id=${dstPlace.placeId}&origin=${srcPlace.fullAddress}&destination=${dstPlace.fullAddress}&waypoints=${waypoints}&waypoint_place_ids=${waypoint_place_ids}`)
    } else {
        ride.bookingDetails.googleMapsDistanceUrl = encodeURI(`https://www.google.com/maps/dir/?api=1&origin_place_id=${srcPlace.placeId}&destination_place_id=${dstPlace.placeId}&origin=${srcPlace.fullAddress}&destination=${dstPlace.fullAddress}`)
    }
    return ride
}

class Presenter {

    presentCabVendor(cabVendor){
        let rides = cabVendor.rides;
        // BUG : !!! Remove pending once UI is fixed !!!!!!!! this is not good
        ['completed', 'confirmed', 'onGoing', 'cancelled', 'pending'].forEach(status => {
            if(!rides[status]){
                rides[status] = [];
            }
        })
        if (!cabVendor.meta) {
            cabVendor.meta = {}
        }
        if (!cabVendor?.meta?.forceV2Migration) {
            cabVendor.meta["forceV2Migration"] = false
        }
        // delete cabVendor.meta.shadowban
        return cabVendor
    }

    presentRide(ride, cabVendor){
        if(_.isArray(ride)){
            return ride.map(r => this.presentRide(r, cabVendor)).filter(_.identity)
        }
        presentMapUrl(ride)
        console.log(cabVendor)
        if (cabVendor?.meta?.shadowban) {
            const shadownbanDetails = cabVendor?.meta?.shadowban;
            
            console.log("HERE", RideStatus.UnAssigned == ride.rideStatus, moment(shadownbanDetails.banExpiry).isAfter(moment()),
            moment(ride.createdDate).add(shadownbanDetails.rideDelayMinutes, "minutes").isAfter(moment()))
            console.log(ride.createdDate, shadownbanDetails.rideDelayMinutes, moment())

            if (RideStatus.UnAssigned == ride.rideStatus  && 
                moment(shadownbanDetails.banExpiry).isAfter(moment()) && 
                moment(ride.createdDate).add(shadownbanDetails.rideDelayMinutes, "minutes").isAfter(moment())) {
                console.log(`Hiding ride ${ride.bookingId} from cabVendor`)
                return false;
            }
        }
        return ride
    }


}

export default new Presenter()