import dynamodbClient from "../clients/dynamodb-client";
import whatsappClient from "../clients/whatsapp-client"
import digimileClient from "../clients/digimile-client"
import { ErrorCodes, GenericTableNamespace } from "../config/constants"
import { CONFIG } from '../config'
import _ from 'lodash'
import moment from 'moment'
import { OTP_CLIENTS } from "../commons/otp_clients"
import { validatePhoneNo } from "../utilility";
import cryptoService from  "../service/crypto-service"

class OtpService {
    constructor(){}

    getClient(otpAgent) {
        console.log(otpAgent, OTP_CLIENTS.SMS, OTP_CLIENTS.SMS == otpAgent)
        if(otpAgent == OTP_CLIENTS.SMS) {
            return digimileClient;
        }
        if(otpAgent == OTP_CLIENTS.WHATSAPP) {
            return whatsappClient;
        }
        throw new Error("OTP Agent not available...")
    }

    async generateOtp(otpAgent, namespace, phoneNumber){
        let isPhoneNoValid = validatePhoneNo(phoneNumber)
        if(!phoneNumber || !isPhoneNoValid){
            throw { response: { errorCode: ErrorCodes.InvalidPhoneNumber }, statusCode: 400 }
        }

        if(Object.values(GenericTableNamespace).indexOf(namespace) == -1) {
            throw { response: { errorCode: ErrorCodes.InvalidNamespace }, statusCode: 400 }
        }

        let payload = {
            phoneNumber,
            generatedOtp: this.generatedOtpCode(),
            isVerified: false,
            noOfTimesOtpSent: 1,
            noOfFailedAttempts: 0,
            lastSentTime: new Date().toISOString()
        }
        console.log("OTP agent", otpAgent)
        const client = this.getClient(otpAgent)
        let otpResult = await dynamodbClient.insertOrGetOtpRecord(phoneNumber, payload, namespace, {lastSentThresholdHours: 24})
        if(!otpResult){
            await client.sendOtp(phoneNumber, payload.generatedOtp)
            return { response: { message: `OTP Sent to ${phoneNumber}` } }
        }
        otpResult = otpResult.data
        if(otpResult.isVerified){
            return {response: {message: "Already verified !!"}}
        }

        let hoursSinceLastSend = moment().diff(moment(otpResult.lastSentTime), 'hours');
        if(hoursSinceLastSend > 12){
            await dynamodbClient.updateOtpRecord(phoneNumber, otpResult, namespace)
            await client.sendOtp(phoneNumber, payload.generatedOtp)
            return { response: { message: `OTP Sent to ${phoneNumber}` } }
        }
        // if otpResult is not undefined then we have already inserted OTP record. handle this scenario
        return this.regenerateOtp(otpAgent, namespace, otpResult)
    }

    async regenerateOtp(otpAgent, namespace, phoneNumberOrOtpRecord){
        const client = this.getClient(otpAgent)
        let otpRecord = phoneNumberOrOtpRecord
        if(typeof phoneNumberOrOtpRecord == 'string'){
            otpRecord = await dynamodbClient.getOtpRecord(phoneNumberOrOtpRecord, namespace)   
        }
        if(otpRecord.data){
            otpRecord = otpRecord.data
        }
        if(otpRecord.noOfTimesOtpSent > 3)
            throw { response: { errorCode: ErrorCodes.OtpResendLimitReached }, statusCode: 400 }

        let secondsSinceLastSend = moment().diff(moment(otpRecord.lastSentTime), 'seconds');
        if(secondsSinceLastSend < 20){
            throw { statusCode: 400, response: { errorCode: ErrorCodes.OtpResendRequestEarly, message: `Request otp again in ${20 - secondsSinceLastSend} seconds`}}
        }
        
        await client.sendOtp(otpRecord.phoneNumber, otpRecord.generatedOtp)

        otpRecord.noOfTimesOtpSent += 1
        otpRecord.lastSentTime = new Date().toISOString()
        await dynamodbClient.updateOtpRecord(otpRecord.phoneNumber, otpRecord, namespace)
        return { response: {message: "OTP resent"}}
    }

    async verifyOtpCode(namespace, phoneNo, otpCode){
        let otpRecord = await dynamodbClient.getOtpRecord(phoneNo.toString(), namespace)
        console.log("verifyOtpCode", namespace, phoneNo, otpCode, otpRecord)
        if (!otpRecord) {
            console.error("No OTP record found", otpRecord)
            throw { response: {errorCode: "close the app and try again"}, statusCode: 500}
        }
        otpRecord = otpRecord?.data
       
         if (otpRecord?.noOfFailedAttempts >= 3) {
            return { response: { errorCode: ErrorCodes.OtpAttemptsExhausted, message: "Try again after few hours" }, statusCode: 400 }
        } else if(otpRecord?.generatedOtp == otpCode){
            otpRecord.isVerified = true
            await dynamodbClient.updateOtpRecord(otpRecord.phoneNumber, otpRecord, namespace)
            if (namespace == GenericTableNamespace.CustomerLogin) {
                const authToken = await cryptoService.jwtEncrypt({ phoneNo: otpRecord.phoneNumber, otpVerified: true  }, {expirationTime: "4weeks"})
                return { response: { authToken, message: "OTP Verified !!", msgFromDev: "Trying to hack ?"} }
            }
            return { response: { message: "OTP Verified !!" } }
        } else {
            otpRecord.noOfFailedAttempts  = otpRecord?.noOfFailedAttempts ? otpRecord.noOfFailedAttempts + 1 : 1;
            await dynamodbClient.updateOtpRecord(otpRecord.phoneNumber, otpRecord, namespace)
            throw { response: { errorCode: ErrorCodes.OtpInvalid, message: "OTP invalid." }, statusCode: 400 }
        }
    }

    async sendDummySms() {
        try {
            await digimileClient.sendDriverMessage()
        } catch(err) {
            console.log(err)
        }
    }

    generatedOtpCode(){
        //if(CONFIG.isProduction)
            return Math.floor(Math.random()*90000) + 10000;
        //else
           // return 3159374 // send same otp in dev, for integration tests
    }
}

export default new OtpService()