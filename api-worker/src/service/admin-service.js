import { VendorStatus, ADMIN_REQUEST_TYPES, ErrorCodes, RideStatus, AuditNamespace, AuditAction } from "../config/constants";
import dynamodbClient from "../clients/dynamodb-client";
import auditService from "./audit-service";
import sqsClient from "../clients/sqs-client"
import { RateCardService } from "./ratecard-service";
import RateCardConfigDefault from '../config/rateCardConfig.json'
import _ from 'lodash'

class AdminService {
    constructor(){}

    async updateVendorState(payload){
        let {cabVendorId, vendorStatus, vehicleStatus} = payload
        let cabVendorDetails = await dynamodbClient.updateVendorStatus(cabVendorId, vendorStatus, vehicleStatus)
        return cabVendorDetails;
    }

    async updateVendor(payload){
        let {cabVendorId, cabVendor} = payload
        const ALLOWED_FIELDS_TO_UPDATE  = ['profile', 'stats', 'vehicles', 'vendorStatus', 'drivers']
        if(_.difference(Object.keys(cabVendor), ALLOWED_FIELDS_TO_UPDATE) != 0){
            return {statusCode: 400, response: {errorCode: ErrorCodes.InvalidAdminRequest, message: `cabVendor should only contain any of ${ALLOWED_FIELDS_TO_UPDATE}`}}
        }
        let currentCabVendorData = await dynamodbClient.getCabVendorAllData(cabVendorId)
        // !!!! Potentially dangerous.... what if deep merge is not working as intended ????
        let newCabVendorData = _.merge(currentCabVendorData, cabVendor, {lastUpdatedTime: new Date().toISOString()})
        await dynamodbClient.updateCabVendorByAdmin(cabVendorId, newCabVendorData)
        return {cabVendor: newCabVendorData};
    }

    async getVendorsByStatus(payload){
        let {vendorStatus, lastEvaluatedKey} = payload
        return await dynamodbClient.getCabVendorByStatus(vendorStatus, lastEvaluatedKey)
    }

    async getRides(payload){
        let {rideStatus, lastEvaluatedKey, bookingIds} = payload
        if(!_.isEmpty(bookingIds)){
            let rides =  await dynamodbClient.getRideByBookingIds(bookingIds)
            return {rides}
        }
        return await dynamodbClient.queryRidesByStatus(rideStatus, lastEvaluatedKey)
    }

    async getVendors(payload){
        let {cabVendors, phoneNumbers, regNo, emailIds} = payload;
        if(_.isArray(cabVendors)){
            return await dynamodbClient.getCabVendorsByIds(cabVendors)
        } else if(_.isArray(phoneNumbers)){
            phoneNumbers = phoneNumbers.map(phoneNo => _.parseInt(phoneNo))
            return await dynamodbClient.getCabVendorByPhoneNos(phoneNumbers)
        } else if(!_.isEmpty(regNo)){
            return await dynamodbClient.getCabVendorByRegNo(regNo)
        } else if (_.isArray(emailIds)) {
            return await dynamodbClient.getCabVendorByEmailIds(emailIds)
        }
    }

    async getAllData(payload){
        let response = await dynamodbClient.dumpTable(payload.tableName, payload.inFull, payload.lastEvaluatedKey)
        return response;
    }

    async cancelRide(payload){
        let {bookingId, cabVendorId, curRideStatus} = payload
        let ride = await dynamodbClient.getRideByBookingId(bookingId, true /*getAllAttributes*/)
        if(!ride){
            return {statusCode: 400, response: {errorCode: ErrorCodes.ResourceNotFound}}
        }
        else if(cabVendorId && ride.cabVendor.cabVendorId != cabVendorId){
            return {statusCode: 400, response: {errorCode: ErrorCodes.CabVendorAndRideMismatch, message: "CabVendorId and ride not matching"}}
        } else if(ride.rideStatus != curRideStatus){
            return {statusCode: 400, response: {errorCode: ErrorCodes.InvalidRideStatus, message: "RideStatus and curRideStatus dont match"}}
        }

        if(_.includes([RideStatus.Assigned, RideStatus.OnGoing, RideStatus.Completed], curRideStatus)){
            return await dynamodbClient.cancelAssignedRide(ride, cabVendorId)
        } else if(_.includes([RideStatus.UnApproved, RideStatus.UnAssigned], curRideStatus)){

        } else {
            return {statusCode: 400, response:{errorCode: ErrorCodes.InvalidRideStatus, message: "இல்லை எனக்கு புரியவில்லை.."}}
        }
    }

    async updateRide(payload, adminGoogleDetails) {
        console.log(payload)
        let {bookingId, customer, price, dryRun, pickupDateAndTime} = payload
        if(customer != null && !dryRun) {
            let updatedRide = await dynamodbClient.updateRideCustomerInfoByAdmin(bookingId, customer)
            auditService.logEvent(AuditNamespace.Ride, bookingId, AuditAction.UpdateRideCustomerInfo, adminGoogleDetails.email, {...payload})
            return {response: {ride: updatedRide}}
        }
        
        let ride = await dynamodbClient.getRideByBookingId(bookingId, true)
        if(!ride) {
            console.error("Ride not found for updateRide", payload)
            return {statusCode: 400, response:{errorCode: ErrorCodes.ResourceNotFound}}
        } else if (![RideStatus.UnApproved, RideStatus.UnAssigned].includes(ride.rideStatus)) {
            return {statusCode: 400, response:{errorCode: ErrorCodes.InvalidAdminRequest, message: "Ride price cannot be updated for ride as the ride status is not allowlisted"}}
        }
        let bookingDetails = ride.bookingDetails
        if(!dryRun && pickupDateAndTime != undefined && bookingDetails.tripType == "ONE_WAY") {
            console.log("updating the pickup time")
            // TODO compare with the current timestamp
            bookingDetails.tripStartTime = pickupDateAndTime;
            bookingDetails.returnStartTime = pickupDateAndTime;
            let updatedRide = await dynamodbClient.updateBookingDetails(bookingId, bookingDetails)
            return {statusCode: 200, response: {updatedRide}}
        }

        let rateCardOverride  = _.cloneDeep(RateCardConfigDefault);
        if(ride.bookingDetails.tripType == "ONE_WAY") {
            delete rateCardOverride.rateCards.roundTrip
        } else {
            delete rateCardOverride.rateCards.oneway
        }

        let matchingRateCard = _.find(rateCardOverride.rateCards.oneway ||rateCardOverride.rateCards.roundTrip, rateCard => rateCard.id == ride.priceDetails.rateCard.id)
        if(!matchingRateCard){
            console.error("Couldnt find a matching ratecard", payload)
            return {statusCode: 400, response: {errorCode: ErrorCodes.InvalidRateCardId}}
        }

        if(price.driverBata != null) { 
            if(price.driverBata < 0) {return {statusCode:400, response: {message: "Enna try panringa sir ??"}} }
            matchingRateCard.bata = price.driverBata 
        }
        if(price.ratePerKm != null) { 
            if(price.ratePerKm < 10) {return {statusCode:400, response: {message: "Trying to cheat ???"}} }
            matchingRateCard.rate = price.ratePerKm 
        }

        rateCardOverride.rateCards[ride.bookingDetails.tripType == "ONE_WAY" ? "oneway" : "roundtrip"] = [matchingRateCard]
        //console.log("rateCardOverride", rateCardOverride)

        let rateCardService = new RateCardService(rateCardOverride)
        let rateCardOutput = await rateCardService.getRateCards({
            pickupPlaceId: bookingDetails.pickup.placeId,
            dropPlaceId: bookingDetails.drop.placeId,
            pickUpDate: bookingDetails.tripStartTime,
            dropDate: bookingDetails.returnStartTime,
            keepCab: bookingDetails.keepCab,
            tripType: bookingDetails.tripType,
            cabType: bookingDetails.cabType
        })
        //console.log(rateCardOutput.rateCards)
        
        let newRateCard = rateCardOutput.rateCards.oneway ? rateCardOutput.rateCards.oneway[0] : rateCardOutput.rateCards.roundTrip[0]
        
        if(!dryRun){ 
            // Important !! The transactionFee logic should be same as the one in goog-lambda sqs processor as well
            let transactionFee = Math.round(newRateCard.total * 0.1 * 100)/100
            let updatedRide = await dynamodbClient.updateRideRateCard(bookingId, newRateCard, transactionFee)
            await sqsClient.publishUpdatedRideByAdmin(updatedRide)
            auditService.logEvent(AuditNamespace.Ride, bookingId, AuditAction.UpdateRidePrice, adminGoogleDetails.email, {...payload})
            return {statusCode: 200, response: { newRateCard, updatedRide }}
        } else {
            return {statusCode: 200, response: {newRateCard}}
        }
    }

    async updateVendorMeta(payload, adminUserDetails) {
        let {cabVendorId, forceV2Migration, shadowban} = payload
        let updatedCabVendor = await dynamodbClient.updateCabVendorMetaData(cabVendorId, {forceV2Migration, shadowban})
        if (forceV2Migration != null) 
            auditService.logEvent(AuditNamespace.Cabvendor, cabVendorId, AuditAction.ForceV2Migration, adminUserDetails.email, {forceV2Migration})
        if (shadowban)
            auditService.logEvent(AuditNamespace.Cabvendor, cabVendorId, AuditAction.UpdateShadowBan, adminUserDetails.email, {...shadowban})

        return {statusCode: 200, response: {cabVendor: updatedCabVendor}}
    }

    async updateWallet(cabVendorId, payload, adminEmail) {
        let {amount, action, comment} = payload

        if (amount <= 0 || amount > 1500) {
            return {statusCode: 400, response: {errorCode: "INVALID_AMOUNT", msg: "Amount should be >0 <1500 but was " + amount}}
        }

        if (action == "DEDUCT") { amount = amount * -1 }

        let updatedCabVendor = await dynamodbClient.updateCabVendorWallet(cabVendorId, amount*100, comment, adminEmail)
        auditService.logEvent(AuditNamespace.Cabvendor, cabVendorId, AuditAction.UpdateWallet, adminEmail, {...payload})
        return {statusCode: 200, response: {cabVendor: updatedCabVendor}}
    }

    async processRequest(requestType, payload, adminGoogleDetails){
        const requestToFuncMap = {
            [ADMIN_REQUEST_TYPES.UPDATE_VENDOR_STATE]               : this.updateVendorState,
            [ADMIN_REQUEST_TYPES.GET_VENDORS_BY_STATUS]             : this.getVendorsByStatus,
            [ADMIN_REQUEST_TYPES.GET_RIDES]                         : this.getRides,
            [ADMIN_REQUEST_TYPES.GET_VENDORS]                       : this.getVendors,
            [ADMIN_REQUEST_TYPES.DUMP_TABLE]                        : this.getAllData,
            [ADMIN_REQUEST_TYPES.UPDATE_VENDOR]                     : this.updateVendor,
            [ADMIN_REQUEST_TYPES.UPDATE_RIDE]                       : this.updateRide,
            [ADMIN_REQUEST_TYPES.UPDATE_VENDOR_META]                : this.updateVendorMeta
        }

        if(!requestToFuncMap[requestType])
            return {statusCode: 400, response: {errorCode: ErrorCodes.InvalidAdminRequest}}

        return await requestToFuncMap[requestType](payload, adminGoogleDetails)
    }
}

export default new AdminService()