import { RateCardService } from "./ratecard-service";
import { ErrorCodes, RideStatus, BookingSource, VendorStatus, VendorVehicleStatus } from "../config/constants"
import dynamodbClient from "../clients/dynamodb-client"
import sqsClient from "../clients/sqs-client"
import { getPlaceDetails } from "../clients/googlemaps-client"
import { customAlphabet } from 'nanoid'
import _ from "lodash"
import moment from "moment"
import utils from "../commons/utils"

const nanoid = customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', 6)

class RidesService {

    async publishRide(payload){
        let {customerSelectedRateCard, tripInfo} = await this.getRateCardById(payload.rateCardId, payload.bookingDetails)
        //TODO: to prevent collision of bookingId, append it with customer phoneNumber 
        //and cut it out before sending to frontend
        let intermediates = []
        if(payload.bookingDetails.intermediates) {
            intermediates = payload.bookingDetails.intermediates.split(",").map(placeId => tripInfo.placeDetailsMap.intermediates[placeId])
        }
        const bookingDetails = payload.bookingDetails
        let bookingId = "ID" + await dynamodbClient.getNextBookingId()
        let rideDetails =  {
            bookingId: bookingId,
            rideStatus: RideStatus.UnPublished,
            customerDetails: payload.customerDetails,
            bookingDetails: {
                pickup: {
                    placeId: bookingDetails.pickupPlaceId,
                    ...tripInfo.placeDetailsMap.src,
                    geo: tripInfo.coOrd.src
                },
                drop: {
                    placeId: bookingDetails.dropPlaceId,
                    ...tripInfo.placeDetailsMap.dst,
                    geo: tripInfo.coOrd.dst
                },
                intermediates,
                tripStartTime: bookingDetails.pickUpDate,
                returnStartTime: bookingDetails.dropDate,
                keepCab: bookingDetails.keepCab,
                tripType: bookingDetails.tripType,
                cabType: bookingDetails.cabType,
                estDistance: Math.round(tripInfo.distance*100)/100,
                estTripDuration: Math.round(tripInfo.durationInMinutes*100)/100,
                estRideEndTime: this.getEstRideEndTime(bookingDetails, tripInfo.durationInMinutes)
            },
            createdDate: new Date().toISOString(),
            lastUpdatedDate: new Date().toISOString(),
            priceDetails: {
                rateCard: customerSelectedRateCard
            }, 
            bookingSource: {
                source: payload?.bookingSource?.source || BookingSource.Unknown,
                sourceId: payload?.bookingSource?.sourceId
            }
        }
        console.log(rideDetails)
        await Promise.all([dynamodbClient.createRide(rideDetails), sqsClient.publishRide({bookingId: rideDetails.bookingId})])
        return _.pick(rideDetails, ['bookingId', 'bookingDetails'])
    }

    async assignRide(payload){
        let {cabVendor, ride} = await dynamodbClient.getVendorAndRideById(payload.cabVendorId, payload.bookingId)
        let cabVendorVersion = cabVendor.version || "v1"
        let validationError = await this.validateRideAssignment(cabVendor, ride, payload.vehicleRegNo, payload.driverLicenseNo)
       if(validationError){
         throw {errorCode: validationError, ride: _.pick(ride, ['rideStatus', 'bookingId', 'bookingDetails'])}
       }
       
       if (cabVendorVersion == "v2") {
            await dynamodbClient.assignRideToCabVendor(payload.bookingId, 
                payload.cabVendorId, 
                {   
                    vendorDetails: {
                        driver: {
                            driverName: cabVendor.drivers[payload.driverLicenseNo].name,
                            driverPhoneNo: cabVendor.drivers[payload.driverLicenseNo].phoneNumber,
                            driverLicenseNo: cabVendor.drivers[payload.driverLicenseNo].licenseNumber,
                            driverAccountNo: cabVendor.externalId
                        }, vehicle: {
                            vehicleRegNo: payload.vehicleRegNo
                        }
                    },
                    amountToBeDeductedInPaise: Math.floor(ride.priceDetails.cabVendor.transactionFee * 100)
                })
       } else {
            await dynamodbClient.assignRideToCabVendor(payload.bookingId, 
                            payload.cabVendorId, 
                            { 
                                amountToBeDeductedInPaise: Math.floor(ride.priceDetails.cabVendor.transactionFee * 100)
                            })
        }
       return { response: { ride: {
          ...ride,
          rideStatus: RideStatus.Assigned
        }
      }}
    }

    async startRide({bookingId, cabVendorId}) {
        //TODO: We don't need to fetch cabVendor
        let {cabVendor, ride} = await dynamodbClient.getVendorAndRideById(cabVendorId, bookingId)
        if (ride.rideStatus != RideStatus.Assigned) {
            throw {errorCode: ErrorCodes.RideNotAvailable, message: `Unexpected ride status ${ride.rideStatus}`}
        } else if (ride.cabVendor.cabVendorId != cabVendorId) {
            throw {errorCode: ErrorCodes.ResourceNotFound, message: `Hmmmmm how is this possible`}
        }
        await dynamodbClient.startRideByCabVendor(bookingId, cabVendorId)
        return {response: {message: "Updated Ride"}}   
    }

    async validateRideAssignment(cabVendor, ride, vehicleRegNo, driverLicense){
        if(cabVendor == null || ride == null)
            return ErrorCodes.ResourceNotFound;
        else if(!vehicleRegNo)
            return ErrorCodes.VehicleRegNoRequired;
        else if (cabVendor.vendorStatus != VendorStatus.Approved)
            return ErrorCodes.VendorNotApproved;
        else if (cabVendor.vehicles[vehicleRegNo].vehicleStatus != VendorVehicleStatus.Approved)
            return ErrorCodes.VendorVehicleNotApproved;
        else if (ride.rideStatus == RideStatus.Assigned)
            return ErrorCodes.RideAlreadyAssinged;
        else if(ride.rideStatus != RideStatus.UnAssigned)
            return ErrorCodes.RideNotAvailable;


        if(cabVendor.version == "v2") {
            if(!cabVendor.drivers[driverLicense]) 
                return ErrorCodes.ResourceNotFound
            else if(cabVendor.drivers[driverLicense].approved == false)
                return ErrorCodes.DriverNotApproved

             let vehicleParivahanData = await dynamodbClient.getVehicleInfo(vehicleRegNo)
             let driverParivahanData = await dynamodbClient.getDriverInfo(driverLicense)

             // TODO: proper error codes
             if(!utils.validateVehicleInfo(vehicleParivahanData.vehicleInfo.result.data, ride.bookingDetails.tripStartTime).approved){
               return ErrorCodes.VendorVehicleNotApproved
             }
             if(!utils.validateLicenseInfo(driverParivahanData.licenseInfo.result.data, ride.bookingDetails.tripStartTime).approved){
                return ErrorCodes.DriverNotApproved
             }
        }
        return false;
    }

    getEstRideEndTime(bookingDetails, estDurationOneWay){
        if(bookingDetails.tripType == "ONE_WAY")
            return moment(bookingDetails.pickUpDate).add(estDurationOneWay, 'minutes').toISOString()
        else {
            return moment(bookingDetails.dropDate).add(estDurationOneWay, 'minutes').toISOString()
        }
    }

    async getRateCardById(id, payload){
        //TODO: Maybe call the endpoint instead of the function so that cf may return the cached response ?
        let ratecardService = new RateCardService()
        let rateCardResponse = await ratecardService.getRateCards(payload)
        let rateCards = rateCardResponse.rateCards.oneway || rateCardResponse.rateCards.roundTrip;
        let customerSelectedRateCard = rateCards.find(ratecard => ratecard.id == id)
        if(customerSelectedRateCard == null){
            console.error("Unable to find selected rate card. This should not be possible", id, payload, rateCardResponse)
            throw {
                statusCode: 400, response: {
                    errorCode: ErrorCodes.InvalidRateCardId, 
                    message: "Rate card selected is invalid"
                }
            }
        }
        customerSelectedRateCard.estTollAmount = rateCardResponse.estTollAmount
        customerSelectedRateCard.permitAmount = rateCardResponse.permitAmount[customerSelectedRateCard.type]
        return  {
            customerSelectedRateCard, 
            tripInfo: {
                distance: rateCardResponse.distance,
                durationInMinutes: rateCardResponse.durationInMinutes,
                estTollAmount: rateCardResponse.estTollAmount,
                placeDetailsMap: rateCardResponse.placeDetailsMap,
                coOrd: rateCardResponse.coOrd
            }};
    }

    async closeRide(bookingId, closeRideDetails) {
        let ride = await dynamodbClient.getRideByBookingId(bookingId, true/*returnAllAttributes*/)
        if(ride.rideStatus != RideStatus.OnGoing) {
            throw {errorCode: ErrorCodes.InvalidRideStatus, message: `RideStatus should be ${RideStatus.OnGoing} but its ${ride.rideStatus}` }
        } else if(ride.cabVendor.cabVendorId != closeRideDetails.cabVendorId) {
            throw {errorCode: ErrorCodes.ResourceNotFound, message: `Script kiddie`}
        }
        let ratecardService = new RateCardService();
        let updatedRateCard = await ratecardService.closeRide(ride.bookingDetails, ride.priceDetails.rateCard, closeRideDetails);
        ride.priceDetails.rateCard = updatedRateCard
        ride.bookingDetails.actualDistanceCovered = closeRideDetails.distance
        // Will be usefull when customer makes a pre payment and driver only collects the remaining money
        if(closeRideDetails.amountCollectedFromCustomer && closeRideDetails.amountCollectedFromCustomer != updatedRateCard.total) {
            //TODO: The total we calculated and money got from customer is not matching. What to do ??
            console.log("Amount not matching. Do sth...")
        }
        if(!closeRideDetails.isDryRun) {
            await dynamodbClient.closeRideUpdate(bookingId, updatedRateCard, ride)
            await sqsClient.rideClosed(bookingId)
        }
        return {response: {ride}}
    }

    async addNotes(payload) {
        let bookingId = payload.bookingId
        let updatedResponse = await dynamodbClient.addOrUpdateNotes(bookingId, payload.notes)
        return { response : updatedResponse}
    }
}

export default new RidesService()