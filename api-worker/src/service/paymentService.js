

import * as razorpayClient from '../clients/razorpay-client'
import * as ippopayClient from '../clients/ippopay-client'
import dynamodbClient from '../clients/dynamodb-client'
import { CONFIG } from '../config'
import { PaymentGateway } from '../config/constants'


let razorpay = {
    createOrder: async function (payload) {
        let razorpayResponse = await razorpayClient.createOrder(payload)
        await dynamodbClient.putRazorpayOrder(payload.cabVendorId, razorpayResponse, PaymentGateway.Razorpay)
        let response = {
            "paymentGateway": PaymentGateway.Razorpay,
            "orderId": razorpayResponse.id,
            "orderCreatedDate": razorpayResponse.created_at,
            "amount": razorpayResponse.amount
        }
        return response;
    },

    verifyPayment: async function (payload) {
        return await razorpayClient.verifyPayment(payload)
    }
}

let ippopay = {
    createOrder: async function (payload) {
        let ippopayResponse = await ippopayClient.createOrder(payload)
        await dynamodbClient.putIppopayOrder(payload.cabVendorId, {...ippopayResponse, notes: payload.notes})
        return {
            "paymentGateway": PaymentGateway.Ippopay,
            ...ippopayResponse
        }
    },

    verifyPayment: async function (payload) {
        const isPaymentDone =  await ippopayClient.verifyPayment(payload)
        if(isPaymentDone == false)
            return isPaymentDone;
        
        return await dynamodbClient.getAddMoneyActionDetails(payload.cabVendorId, payload.orderCreatedDate)
    }
}

// Todo: Convert handling of wallet amount from paise to decimal. makes life much easier.
// But need to do before the first driver onboarding.
class PaymentService {
    constructor() {
        this.paymentGateways = { razorpay, ippopay }
    }

    async createOrder(...args) {
        this.curPaymentGateway = CONFIG.paymentGateway || "ippopay"
        return await this.paymentGateways[this.curPaymentGateway].createOrder(...args)
    }

    async verifyPayment(...args) {
        this.curPaymentGateway = CONFIG.paymentGateway || "ippopay"
        return await this.paymentGateways[this.curPaymentGateway].verifyPayment(...args)
    }
}

export default new PaymentService()