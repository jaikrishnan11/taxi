import _, { isArray } from 'lodash'
import moment from 'moment'
import { CONFIG } from "../config"
import { ErrorCodes, TripTypes } from '../config/constants'
import { getPlaceDetails } from "../clients/googlemaps-client"

const isPointInCircle = (center, radius, point) => {
    const toRadians = (degrees) => (degrees * Math.PI) / 180;
  
    const { lat: centerLat, lon: centerLon } = center;
    const { lat: pointLat, lon: pointLon } = point;
  
    const earthRadius = 6371; // Earth radius in kilometers
  
    // Convert latitudes and longitudes from degrees to radians
    const dLat = toRadians(pointLat - centerLat);
    const dLon = toRadians(pointLon - centerLon);
  
    const centerLatRad = toRadians(centerLat);
    const pointLatRad = toRadians(pointLat);
  
    // Haversine formula
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(centerLatRad) *
        Math.cos(pointLatRad) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
  
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = earthRadius * c;
  
    // Check if the distance is within the radius
    return distance <= radius;
  };

  
export class RateCardService {
    constructor(overrideRateCardTemplate) {
        this.overrideRateCardTemplate = overrideRateCardTemplate
    }

    async getRateCardTemplate() {
        if(this.overrideRateCardTemplate) {
            this.rateCardConfig = this.overrideRateCardTemplate;
            this.rateCardTemplate = this.overrideRateCardTemplate.rateCards
            console.warn("Using rateCardOverride", this.overrideRateCardTemplate)
        }
        else if (!this.rateCardTemplate){
            this.rateCardConfig = await CONFIG.getRateCardConfig();
            this.rateCardTemplate = this.rateCardConfig.rateCards
        }
        console.log("Using rateCard", this.rateCardTemplate)
        return this.rateCardTemplate
    }

    async calculateDistance(srcId, dstId) {
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?" + new URLSearchParams({
            origins: "place_id:" + srcId,
            destinations: 'place_id:' + dstId,
            key: "AIzaSyDaaf3IpE8PdgC4YHTXrm17hHNkyTcHJNI",
            mode: "driving"
        }).toString()

        let response = await fetch(url, {
            cf: {
                cacheEverything: true,
                cacheTtl: 3600,
            }
        })
        if (response.status != 200) {
            console.log("Error while requesting google distance api " + response.status)
            let error = await response.json()
            console.error(error)
            throw "Error while requesting google distance api " + JSON.stringify(error)
        }
        response = await response.json()
       // console.log(JSON.stringify(response))
        response = {
            distance: response.rows[0].elements[0].distance.value / 1000, // In Km
            time: response.rows[0].elements[0].duration.value / 60, // In minutes
            durationText: response.rows[0].elements[0].duration.text  //  Sample "6 hours 23 mins"
        }
        return response
    }

    async calculateDistanceV2(srcId, dstId, intermediates = []) {
        let request = {
            "origin": {
                "placeId": srcId
            },
            "destination": {
                "placeId": dstId
            },
            "intermediates": intermediates.map(inter => {return {"placeId": inter}}),
            "travelMode": "DRIVE",
            "extraComputations": [
                "TOLLS"
            ],
            "routeModifiers": {
                "tollPasses": ["IN_FASTAG"]
            },
            "languageCode": "en-IN",
            "regionCode": "in"
        }
        console.log(request)
        let response = await fetch("https://routes.googleapis.com/directions/v2:computeRoutes", {
            method: "POST",
            body: JSON.stringify(request),
            headers: {
                "Content-Type": "application/json",
                "X-Goog-Api-Key": "AIzaSyA-O1_PezYhTQc0CKm3qDwc0IxsDJQ2zSI",
                "X-Goog-FieldMask": "routes.duration,routes.distanceMeters,routes.travelAdvisory.tollInfo,routes.localizedValues,routes.legs.startLocation,routes.legs.endLocation"
            }
        })
        
        if(!response.ok) {
            console.error("Error getting distance")
            throw await response.json()
        }

        let requestBody = await response.json()
        let tollAmount =  _.reduce(requestBody.routes[0]?.travelAdvisory?.tollInfo?.estimatedPrice, function(sum, toll) {
            return sum + parseInt(toll.units)
        }, 0)
        let timeInSecondsString = requestBody.routes[0].duration
        let timeInSeconds = timeInSecondsString.substring(0, timeInSecondsString.length - 1);
        let srcCoordinates = requestBody.routes[0].legs[0].startLocation.latLng;
        let dstCoordinates = requestBody.routes[0].legs.at(-1).endLocation.latLng;
        return  {
            distance: requestBody.routes[0].distanceMeters/1000 , //In kms
            time: Math.round(parseInt(timeInSeconds)*100/ 60)/100,
            durationText: requestBody.routes[0].localizedValues.duration.text,
            estTollAmount: parseInt(tollAmount),
            srcCoordinates: {lat: srcCoordinates.latitude, lon: srcCoordinates.longitude},
            dstCoordinates: {lat: dstCoordinates.latitude, lon: dstCoordinates.longitude}
        }
    }

    async getDistance(queryDetails) {
        //const distanceDetails = await this.calculateDistance(queryDetails.pickupPlaceId, queryDetails.dropPlaceId)
        let intermediates = []
        if(queryDetails.intermediates) {
            intermediates = queryDetails.intermediates.split(',')
        }
        const distanceDetails = await this.calculateDistanceV2(queryDetails.pickupPlaceId, queryDetails.dropPlaceId, intermediates)
        Object.assign(queryDetails, distanceDetails)
        return queryDetails
    }

    async applyRateCardOverridesFromConfig(allPlaces, pickupDate) {
        if (!pickupDate || isNaN(new Date(pickupDate))) {
            console.warn("Pickup date null when applying override config")
            return {
                overriden: false,
                requestRejected: false
            }
        }
        this.rateCardOverrideConfig = await CONFIG.env.KV.get("rateCardOverride", {type: 'json'})
        if (!this.rateCardOverrideConfig || this.overrideRateCardTemplate) {
            return {
                overriden: false,
                requestRejected: false
            }
        }

        let deniedPlaces = this.rateCardOverrideConfig?.denyService || []
        for (let deniedPlace of deniedPlaces) {
            for (let term of deniedPlace?.terms || []) {
                let places = allPlaces[term]
                places = isArray(places) ? places : [places]
                for(let p of places) {
                    if (isPointInCircle(deniedPlace.coord, deniedPlace.radius, p.geo)){
                        return {
                            overriden: true,
                            requestRejected: true,
                            reason: deniedPlace.reason
                        }
                    }
                }
            }
        }

        for (let overrideConfig of this.rateCardOverrideConfig?.rateCardOverride || []) {
            pickupDate = new Date(pickupDate);
            if (pickupDate < new Date(overrideConfig.window.start)  || pickupDate > new Date(overrideConfig.window.end)) {
                continue
            }
            for (let term of overrideConfig?.terms || []) {
                let places = allPlaces[term]
                places = isArray(places) ? places : [places]
                for(let p of places) {
                    for (let overridenPlace of overrideConfig.places) {
                    if (isPointInCircle(overridenPlace, overridenPlace.radius, p.geo)){
                        this.overrideRateCardTemplate = overrideConfig.rateCard
                        return {
                            overriden: true,
                            rateCard: overrideConfig.rateCard
                        }
                    }
                }
                }
            }
        }
        return {
            overriden: false,
            requestRejected: false
        }
    }

    async getRateCards(queryDetails) {
        queryDetails = await this.getDistance(queryDetails)
        // This can return null
        let placeDetailsMap = (await getPlaceDetails([queryDetails.pickupPlaceId, queryDetails.dropPlaceId])) || {} 
        queryDetails.placeDetailsMap = {
            src: placeDetailsMap[queryDetails.pickupPlaceId],
            dst: placeDetailsMap[queryDetails.dropPlaceId],
            intermediates: await getPlaceDetails(queryDetails?.intermediates?.split(","))
        }

        let {requestRejected, overriden, reason, rateCard:overridenRateCard} = await this.applyRateCardOverridesFromConfig(queryDetails.placeDetailsMap, queryDetails.pickUpDate)
        if (requestRejected) {
            throw {
                response: {
                    errorCode: ErrorCodes.ServiceUnAvailable,
                    message: reason
                },
                statusCode: 400
            }
        } else if (overriden) {
            console.log("Ratecard overriden", overridenRateCard)
        }

        queryDetails.permitAmount = this.calculatePermitCharges(queryDetails);
        let result
        if (queryDetails.tripType == "ROUND_TRIP") {
            result = await this.getRTRateCards(queryDetails)
        }
        else {
            result = await this.getOneWayRateCards(queryDetails);
        }

        //Move this logic somewhere. Chain of Responsibilty pattern ;P
        //result.bannerText = queryDetails.src.address.stateCode != queryDetails.dst.address.stateCode ? this.rateCardConfig.interStateRateCardBanner : this.rateCardConfig.withinStateRateCardBanner
        this.applyAppDiscount(result, queryDetails.isApp)
        // adding lat and lon from requests to support favourite routes
        result["latitude"] = queryDetails.latitude
        result["longitude"] = queryDetails.longitude
        return Object.assign(result, {
            durationText: queryDetails.durationText, 
            durationInMinutes: queryDetails.time, 
            estTollAmount: queryDetails.estTollAmount,
            permitAmount: queryDetails.permitAmount,
            placeDetailsMap: queryDetails.placeDetailsMap,
            coOrd: {src: queryDetails.srcCoordinates, dst: queryDetails.dstCoordinates}
        });
    }

    calculatePermitCharges(queryDetails) {
        if (queryDetails?.placeDetailsMap?.src == null || queryDetails?.placeDetailsMap?.dst?.stateCode == null) {
            console.warn("PlaceDetails null. Not calculating permit amount", queryDetails)
            return 
        }
        let statesTravelling = [queryDetails.placeDetailsMap.src.stateCode]
        statesTravelling = statesTravelling.concat(Object.values(queryDetails.placeDetailsMap?.intermediates || {}).map(intermediate => intermediate.stateCode))
        statesTravelling.push(queryDetails.placeDetailsMap.dst.stateCode)


        let permitAmount = {
            "sedan": 0,
            "innova": 0
        }
        statesTravelling = _.uniq(statesTravelling)
        for (let i = 1; i < statesTravelling.length; i++) {
            let stateCode = statesTravelling[i]
            let isTN = stateCode == "TN";
            permitAmount.sedan += CONFIG.permitCharges.sedan[statesTravelling[isTN ? i-1 : i]] || 0
            permitAmount.innova += CONFIG.permitCharges.innova[statesTravelling[isTN ? i-1 : i]] || 0
        }
        permitAmount["etios"] = permitAmount.sedan
        permitAmount["xylo"] = permitAmount.innova
        return permitAmount
    }

    async getOneWayRateCards(queryDetails) {
        let distance = queryDetails.distance
        let rateCards = _.cloneDeep(await this.getRateCardTemplate())
        if(rateCards.roundTrip) {
            delete rateCards.roundTrip;
        }
        let remainingDistanceToCover = Math.max(this.rateCardConfig.oneWayMinDistance - distance, 0)
        _.forEach(rateCards.oneway, tripType => {
            this.applyOneWayRateCard(distance, this.rateCardConfig, tripType, queryDetails)
        })

        return {
            distance, // Keeping this for backwards compatibility TODO: Deprecate this after checking frontend
            oneWayDistance: distance, 
            rateCards
        }
    }

    applyOneWayRateCard(distance, rateCardConfig, rateCard, queryDetails) {
        let remainingDistanceToCover = Math.max(rateCardConfig.oneWayMinDistance - distance, 0)
        rateCard.fare = _.round(distance * rateCard.rate)
        rateCard.minDailyCharge = remainingDistanceToCover * rateCard.rate
        rateCard.totalBeforeEstCharges = Math.round(rateCard.fare + rateCard.bata + rateCard.minDailyCharge)
        rateCard.total = Math.round(rateCard.totalBeforeEstCharges + queryDetails.estTollAmount + (queryDetails?.permitAmount[rateCard.type] || 0))
        rateCard.priceSplit = this.getFareSplit(rateCard, distance, rateCardConfig.oneWayMinDistance)
    }

    applyAppDiscount(result, isApp) {
        if (!isApp)
            return result;
        let discountApplier = (rateCards) => {
            rateCards.forEach(rateCard => {
                rateCard.appDiscount = this.rateCardConfig.appDiscount
                if (rateCard.total)
                    rateCard.discountedPrice = rateCard.total - rateCard.appDiscount
            })
        }

        discountApplier(result.rateCards.oneway || [])
        discountApplier(result.rateCards.roundTrip || [])
    }

    convertTimeToIST(utcDate) {
        var timeZoneOffset = 330; // Indian Standard Time (IST) offset is 5 hours 30 minutes = 330 minutes

        // Convert the UTC timestamp to the Indian timestamp
        return new Date(new Date(utcDate).getTime() + timeZoneOffset * 60000);
    }

    async getRTRateCards(searchCabsRequest) {
        let rateCards = _.cloneDeep(await this.getRateCardTemplate())
        delete rateCards.oneway

        let { pickUpDate, dropDate, distance: oneWayDistance } = searchCabsRequest
        console.log(pickUpDate, dropDate)
        console.log("Converted date", this.convertTimeToIST(dropDate), this.convertTimeToIST(pickUpDate))
        // pickUpDate = this.convertTimeToIST(pickUpDate)
        // dropDate = this.convertTimeToIST(dropDate)
        let duration = moment.duration(moment(new Date(this.convertTimeToIST(dropDate))).endOf('day').diff(moment(new Date(this.convertTimeToIST(pickUpDate))).startOf('day')));
        console.log("start and end date", moment(new Date(this.convertTimeToIST(dropDate))).endOf('day'), moment(new Date(this.convertTimeToIST(pickUpDate))).startOf('day'))
        // console.log("start and end date", moment(new Date(dropDate)).endOf('day'), moment(new Date(pickUpDate)).startOf('day'))
        // let duration = moment.duration(moment(new Date(dropDate)).endOf('day').diff(moment(new Date(pickUpDate)).startOf('day')));
        // console.log("duration", duration)
        const noOfDays = duration.days() + 1 ; // including pickupDate and dropDate

        /*
            If pickup and drop on the same day.  Pick up at day1 23:59 and drop at day2 00:01 is still considered as two days
                distance = Max ( MinDistance, distance )
            else keep Car
                distance = 2 * Max(oneWayDt, MinDistance ) + MinDistance * (totalDays - 2 )
            else
                Consider it as two one way trips with distance as oneWayDt
        */
        if(searchCabsRequest.keepCab){
            let distance = this.getDistanceForRTCalculation(noOfDays, oneWayDistance)
            _.forEach(rateCards.roundTrip, tripType => {
                tripType.fare = _.round(distance * tripType.rate)
                tripType.roundTripCalculatedDistance = distance
                tripType.totalBeforeEstCharges = Math.round(tripType.fare + noOfDays * tripType.bata)
                tripType.total = Math.round(tripType.totalBeforeEstCharges + searchCabsRequest.estTollAmount + (searchCabsRequest?.permitAmount[tripType.type] || 0))
                tripType.priceSplit = this.getRoundTripFareSplit(noOfDays, oneWayDistance, tripType)
            })
            return {
                rateCards,
                roundTripDistance: oneWayDistance * 2,
                oneWayDistance: oneWayDistance,
                distance: oneWayDistance * 2, // Keeping this for backwards compatibility
                minPerDayDistance: this.rateCardConfig.minDistanceRoundTripPerDay
            }
        } else {
            return this.getOneWayRateCards(searchCabsRequest)
        }
    }

    getDistanceForRTCalculation(noOfDays, oneWayDistance){
        let minPerDayDistance = this.rateCardConfig.minDistanceRoundTripPerDay
         //Ethically it should be <= 24hours... but alas we dont have control
         //Hope this gets changed someday ;P
        if(noOfDays == 1){
            return Math.max(oneWayDistance * 2, minPerDayDistance);
        } else {
            // let distanceFirstAndLastDay =  minPerDayDistance * 2 // TODO : Add this back ? Math.max(oneWayDistance, minPerDayDistance) * 2;
            // return distanceFirstAndLastDay + (noOfDays - 2) * minPerDayDistance
            return Math.max(noOfDays * minPerDayDistance, oneWayDistance * 2)
        }
    }

    getRoundTripFareSplit(noOfDays, oneWayDistance, tripType){
        let minPerDayDistance = this.rateCardConfig.minDistanceRoundTripPerDay
        let splitMap = {
            "DriverBata": {
                calculation: `${noOfDays} day(s) * ₹${tripType.bata}`,
                value: noOfDays * tripType.bata
            },
            "EstToll": {
                value: `₹${tripType.estTollAmount}`
            },
            "Permit": {
                value: `₹${tripType.permitAmount}`
            }
        }
        if(noOfDays == 1 && oneWayDistance * 2 < minPerDayDistance){
            splitMap["MinBaseFare"] = {
                calculation: `${minPerDayDistance} km X ₹${tripType.rate}`,
                value: tripType.fare
            }
            splitMap["BaseKm"] = {
                calculation : `${minPerDayDistance} km`,
                value : minPerDayDistance * tripType.rate
            }
        } else if(noOfDays == 1) {
            splitMap["BaseFare"] = {
                calculation: `${oneWayDistance * 2} km X ₹${tripType.rate}`,
                value: tripType.fare
            }
            splitMap["BaseKm"] = {
                calculation : `${oneWayDistance * 2} km`,
                value : oneWayDistance * 2
            }
        } else if(oneWayDistance * 2 < minPerDayDistance * noOfDays){
            splitMap["MinBaseFare"]= {
                calculation: `${noOfDays} days X ${minPerDayDistance} km X ₹${tripType.rate}`,
                value: tripType.fare
            }
            splitMap["BaseKm"] = {
                calculation : `${noOfDays} days X ${minPerDayDistance} km`,
                value : noOfDays * minPerDayDistance
            }
        } else {
            let calculation = `2 days(first and last) X ${oneWayDistance} km X ₹${tripType.rate}`
            // if(noOfDays > 2)
            //     calculation = calculation + ` + ${noOfDays -2} day(s) X ${minPerDayDistance} km X ₹${tripType.rate}`

            splitMap["BaseFare"] = {
                calculation: calculation,
                value: tripType.fare
            }
            splitMap["BaseKm"] = {
                calculation : `2 days(first and last) X ${oneWayDistance} km`,
                value : 2 * oneWayDistance
            }
        }
        return splitMap
    }

    getFareSplit(tripDetails, actualDistance, minDistance) {
        let splitMap = {}
        splitMap["DriverBata"] = {
            value: tripDetails.bata
        }
        if (tripDetails.minDailyCharge <= 0) {
            splitMap["BaseFare"] = {
                calculation: `${actualDistance} km X ₹${tripDetails.rate}`,
                value: `₹${tripDetails.fare}`
            }
        }
        else {
            splitMap["MinBaseFare"] = {
                calculation: `${minDistance} km X ₹${tripDetails.rate}`,
                value: `₹${Math.round(tripDetails.fare + tripDetails.minDailyCharge)}`
            }
        }

        splitMap["EstToll"] = {
            value: `₹${tripDetails.estTollAmount}`
        }
        splitMap["Permit"] = {
            value: `₹${tripDetails.permitAmount || 0}`
        }
        return splitMap;
    }

    async closeRide(bookingDetails, rateCard, closeRideDetails) {
        console.log("CloseRide. current PriceDetails", rateCard)
        await this.getRateCardTemplate()
        if(closeRideDetails.hillCharge) {
          rateCard.hillCharge = closeRideDetails.hillCharge
          rateCard.priceSplit["HillCharge"] = {
            value: `₹${closeRideDetails.hillCharge}`
          }
        }

        if(closeRideDetails.waitingCharge) {
            rateCard.waitingCharge = closeRideDetails.waitingCharge
            rateCard.priceSplit["WaitingCharge"] = {
                value: `₹${closeRideDetails.waitingCharge}`
            }
        }

        if(closeRideDetails.petCharge) {
            rateCard.petCharge = closeRideDetails.petCharge
            rateCard.priceSplit["PetCharge"] = {
                value: `₹${closeRideDetails.petCharge}`
            }
        }

        if(closeRideDetails.tollAmount) {
            rateCard.tollAmount = closeRideDetails.tollAmount
            rateCard.priceSplit["TollAmount"] = {
                value: `₹${closeRideDetails.tollAmount}`
            }
        }

        if(closeRideDetails.parkingCharge) {
            rateCard.parkingCharge = closeRideDetails.parkingCharge
            rateCard.priceSplit["ParkingCharge"] = {
                value: `₹${closeRideDetails.parkingCharge}`
            }
        }

        let actualDistance = closeRideDetails.distance
        if (bookingDetails.tripType == TripTypes.OneWay) {
            if (actualDistance < this.rateCardConfig.oneWayMinDistance) {
                console.log("Not changing fare as actualDistance is less than minOneWayDt " + this.rateCardConfig.oneWayMinDistance)
            }
            else if (actualDistance > bookingDetails.estDistance  && actualDistance > this.rateCardConfig.oneWayMinDistance) {
                console.log(`Closing distance ${actualDistance} is > than estimated ${bookingDetails.estDistance}`)
                rateCard.previousEstimatedFare = rateCard.fare
                rateCard.fare = _.round(actualDistance * rateCard.rate)
                rateCard.minDailyCharge = 0;
            } else if(actualDistance < bookingDetails.estDistance) {
                console.log(`Rode less kms than estimated !! Estimated: ${bookingDetails.estDistance}`)
                rateCard.minDailyCharge = 0;
                rateCard.previousEstimatedFare = rateCard.fare
                rateCard.fare = _.round(actualDistance * rateCard.rate)
            }
        } else if (bookingDetails.tripType == TripTypes.RoundTrip) {
            if (rateCard.roundTripCalculatedDistance && actualDistance > rateCard.roundTripCalculatedDistance) {
                rateCard.previousEstimatedFare = rateCard.fare
                rateCard.fare = _.round(actualDistance * rateCard.rate)
            } else {
                console.log(`Not changing fare for round trip`)
            }
        }

        let valuesToAdd = [rateCard.fare, rateCard.bata, rateCard.hillCharge, rateCard.waitingCharge, rateCard.petCharge, rateCard.minDailyCharge, rateCard.tollAmount, rateCard.parkingCharge]
        rateCard.total = Math.round(_.sum(_.filter(valuesToAdd, _.isFinite)))
        return rateCard
      }
}

// dont use this. will result in unexpected behaviour. 
// Initialize new object every time
export default new RateCardService(); 