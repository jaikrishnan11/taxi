class ElasticSearchService {
    constructor() {
        this.urlPath = "http://ec2-18-61-216-82.ap-south-2.compute.amazonaws.com:9200";
    }

    async proxyElastic({ payload }) {
        let { urlPath: inputUrlPath, body } = payload;
        let response = await fetch(`${this.urlPath}/${inputUrlPath}`, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic ZWxhc3RpYzptaWVQcjhOckxUQUpsT3dYZDZjWg==" // Adjust the Authorization header as needed
            }
        });
        let status = response.status;
        response = await response.json();
        return { response, status };
    }

    async scrollSearch(index, scrollTimeout = "1m", size = 1000) {
        let results = [];
        let payload = {
            urlPath: `${index}/_search?scroll=${scrollTimeout}`,
            body: {
                size: size,
                query: {
                    match_all: {}
                }
            }
        };

        // Initial search request to get the first batch and scroll_id
        let { response } = await this.proxyElastic({ payload });

        let scrollId = response._scroll_id;
        results = results.concat(response.hits.hits);

        // Continue scrolling until no more hits are returned
        while (response.hits.hits.length > 0) {
            payload = {
                urlPath: `_search/scroll`,
                body: {
                    scroll: scrollTimeout,
                    scroll_id: scrollId
                }
            };

            let { response } = await this.proxyElastic({ payload });

            scrollId = response._scroll_id; // Update scrollId
            results = results.concat(response.hits.hits);
        }

        return results;
    }

    async fetchCompleteVehicleIndex() {
        let index = "table-cab-vehicles"; // Index name
        try {
            let data = await this.scrollSearch(index);
            console.log(`Fetched ${data.length} records from index ${index}`);
            return data;
        } catch (error) {
            console.error(`Error fetching data from index ${index}:`, error);
        }
    }
}

// Usage example
export default new ElasticSearchService();
// esService.fetchCompleteIndex().then(data => {
//     // Do something with the fetched data
//     console.log("Complete data:", data);
// });
