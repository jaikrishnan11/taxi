import dynamodbClient from "../clients/dynamodb-client";

class AuditService {

    init(context) {
        this.context = context;
    }

    logEvent(namespace, actionId, action, performedBy, details) {
        details = Object.assign({}, details)
        delete details.googleIdToken
        
        this.context.waitUntil(dynamodbClient.addAuditEntry(namespace, actionId, {
            performedBy,
            action,
            eventTime: new Date().toISOString(),
            details
        }))
    }
}

export default new AuditService()