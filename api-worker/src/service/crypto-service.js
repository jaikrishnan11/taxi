import * as jose from 'jose'
import { CONFIG } from '../config'
import { ErrorCodes } from '../config/constants'
class Crypto {

    async jwtEncrypt(data, {expirationTime='48h'}) {
       
        const JWT_SECRET = jose.base64url.decode(CONFIG.jwtSecret)
        console.log("JWTToken", JWT_SECRET)
        const jwt = await new jose.EncryptJWT(data)
        .setProtectedHeader({ alg: 'dir', enc: 'A128CBC-HS256' })
        .setIssuedAt()
        .setAudience("gogo")
        .setIssuer("gogoWorker")
        .setExpirationTime(expirationTime)
        .encrypt(JWT_SECRET)
        return jwt
    }

    async jwtDecrypt(jwtToken) {
        try {
            const JWT_SECRET = jose.base64url.decode(CONFIG.jwtSecret)
            const { payload, protectedHeader } = await jose.jwtDecrypt(jwtToken, JWT_SECRET, {
                audience: "gogo",
                issuer: "gogoWorker"
            })
            console.log("JWTToken exp", payload.exp, Date.now())
            let isExpired = payload.exp < Date.now() / 1000
            return  { payload, protectedHeader, isExpired }
        } catch (error) {
            console.log({errMsg: "Error decrypting token", error})
            throw {statusCode: 400, response: {errorCode: ErrorCodes.InvalidAccessToken, msg: "Bro... stop it"}}
        }
    }

}

export default new Crypto();