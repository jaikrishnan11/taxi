import { CONFIG } from "./config";

const CLIENT_ID = "516468745525-0sjt0hhsh78o7iqjjpv5gllpre4us0j6.apps.googleusercontent.com";

export async function verifyGoogleToken(googleIdToken, googleUserId) {
    // TODO : Google says this endpoint should only be used for debugging purpose. But their nodeJS library is not working in Cloudflare
    let response = await fetch("https://oauth2.googleapis.com/tokeninfo?id_token="+googleIdToken)
    response = await response.json();
    let googleUserDetails = {
      email: response.email,
      googleId: response.sub,
      clientId: response.aud,
      name: response.name
    }
    if(googleUserDetails.googleId != googleUserId ||  googleUserDetails.clientId != CLIENT_ID){
      console.error("Google Token not matching !!!", googleIdToken, googleUserId, googleUserDetails)
      throw {
        response: {
          message: "Google token tampered / invalid",
          statusCode: 400
        }
      }
    }
    return googleUserDetails
}

export async function verifyAdmin(googleIdToken, googleUserId){
    //Only for testing dont abuse
    if(CONFIG.skipTokenVerification) return {email: "test@test.com"};

    let googleUserDetails = await verifyGoogleToken(googleIdToken, googleUserId);
    if(CONFIG.admins.includes(googleUserDetails.email) == false){
        throw { response: {message: "You are not a admin. Who are you !?", googleUserId, statusCode: 400}}
    }
    return googleUserDetails
}

export function validatePhoneNo(phoneNumber) {
  // Define a regex pattern for a valid phone number
  // With or without country code and any number of digits
  const phoneRegex = /^(\+\d{1,4})?(\d{1,20})$/;
    
  // Test the phone number against the regex pattern
  return phoneRegex.test(phoneNumber);
}