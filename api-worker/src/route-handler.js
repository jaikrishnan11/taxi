import { CONFIG } from "./config"
import { RideStatus, ErrorCodes, BookingSource, SQSMessageType, VendorStatus, VendorVehicleStatus, ADMIN_REQUEST_TYPES, GenericTableNamespace, FileUploadCategory, AuditNamespace, AuditAction } from "./config/constants"
import * as razorpayClient from './clients/razorpay-client'
import dynamodbClient from "./clients/dynamodb-client"
import invicibleOceanClient from "./clients/invicible-ocean-client.js"
import s3Client from "./clients/s3-client"
import sqsClient from "./clients/sqs-client"
import _ from "lodash"
import { RateCardService } from "./service/ratecard-service"
import ridesService from "./service/rides-service"
import adminService from "./service/admin-service"
import paymentService from "./service/paymentService"
import cryptoService from "./service/crypto-service"
import esClient from "./clients/elasticsearch-client"
import otpService from "./service/otp-service"
import * as Validator from './config/schema-validators'
import { RateCardConfigKey } from "./config/constants"
import DefaultRateCardConfig from "./config/rateCardConfig.json"
import { verifyAdmin, verifyGoogleToken } from "./utilility"
import presenter from "./presenter"
import { OTP_CLIENTS } from "./commons/otp_clients"
import { PutCommand } from "@aws-sdk/lib-dynamodb"
import utils from './commons/utils.js'
import moment from "moment"
import auditService from "./service/audit-service.js"
import { LANGUAGES } from "./commons/models.js"
//import * as firebaseAdminClient from 'firebase-admin/app';
import {sendDevMessage, sendTextMessageToDev} from './clients/telegram-client'

/*
- [ ] Driver endpoints should only work in android app
- [ ] Presentation layer
*/

let handlerObjects = {
  async healthCheck() {
    return {
      statusCode: 200,
      response: {
        "working": true
      }
    }
  },

  async proxyElastic({ payload }) {
    let { urlPath, body } = payload
    let response = await fetch(`http://ec2-18-61-216-82.ap-south-2.compute.amazonaws.com:9200/${urlPath}`, {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic no"
      }
    })
    let status = response.status;
    response = await response.json()
    return { response }
  },

  async processVendorLocation({ payload }) {
    validate(payload, Validator.validateLocationData)
    let document = {
      locationOwnerId: "cabVendor",
      locationTime: payload.locationTime,
      cabVendorId: payload.cabVendorId,
      geoLocation: {
        lat: payload.coordinates[0],
        lon: payload.coordinates[1]
      }
    }
    await sqsClient.sendLocationUpdates(document)
    //await esClient.updateLocation(payload.cabVendorId, document)
    // await dynamodbClient.updateCabVendorLocation(payload.cabVendorId, "cabVendor", {
    //   "geoLocation": {
    //     "lat": payload.coordinates[0],
    //     "lon": payload.coordinates[1]
    //   },
    //   locationTime: payload.locationTime
    // })
    return {
      response: { message: "OKK" }
    }
  },

  async getItemFromCF({ env, params }) {
    return {
      response: await env.KV.get(params.key, { type: 'json' })
    }
  },

  async putItemToCF({ env, payload }) {
    let { googleUserId, googleIdToken } = payload
    // let adminDetails = await verifyAdmin(googleIdToken, googleUserId)
    return {
      response: await env.KV.put(payload.key, JSON.stringify(payload.data))
    }
  },

  async createItem({ payload }) {
    if (CONFIG.isProduction) {
      throw "With great power comes great responsibility !"
    }
    await dynamodbClient.ddbClient.send(new PutCommand({
      TableName: payload.tableName,
      Item: payload.item
    }))
    return {
      response: { message: "OKK" }
    }
  },

  async createCabVendor({ payload }) {
    validate(payload, Validator.validateCreateCabVendor)
    // All the vehicles added during the registration process is approved automatically. 
    _.values(payload.vehicles).forEach(vehicle => vehicle.vehicleStatus = VendorVehicleStatus.Approved)
    if (!payload.drivers) {
      //Till UI asks for driver details add cabVendor as one of the driver
      payload.drivers = {
        [payload.profile.phoneNo]: {
          phoneNo: payload.profile.phoneNo,
          name: payload.profile.name
        }
      }
    }
    let cabVendor = await dynamodbClient.createCabVendor(payload)
    // We toggle between asking for payment vs allowing directly. so depending on the vendor status we decide whether to send approval notificatin to admin 
    // now or after receiving payment. 

    if (cabVendor.vendorStatus == VendorStatus.UnApproved) {
      await sqsClient.sendVendorApprovalNotification(payload.cabVendorId)
    }
    return {
      response: { cabVendor }
    }
  },

  async createCabVendorV2({ payload }) {
    // verify the OTP and create cab vendor v2
    validate(payload, Validator.validateCreateCabVendorV2)
    console.log("calling createCabVendorV2 ...", payload);

    const otpResponse = await otpService.verifyOtpCode(GenericTableNamespace.VendorCreationPhoneNoVerification, payload.profile.phoneNo, payload.otpCode)
    if (otpResponse?.response?.message?.errorCode == ErrorCodes.OtpInvalid) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.OtpInvalid,
          message: "Given OTP is invalid"
        }
      }
    }
    console.log("OTP verified, inserting cab vendor data")
    let cabVendorV2 = await dynamodbClient.createCabVendorV2({
      cabVendorId: payload.cabVendorId,
      profile: {
        name: payload.profile.name,
        phoneNo: payload.profile.phoneNo,
        email: payload.profile.email,
        panCardNo: payload.profile.panCardNo
      }
    })
    await sqsClient.cabVendorV2Created(payload.cabVendorId, { version: "v2" })
    return { response: { cabVendorV2 } }
  },

  async getConfig() {
    return {
      response: {
        rateCard: await CONFIG.getRateCardConfig()
      }
    }
  },

  async createCustomerAccount({ payload }) {
    validate(payload, Validator.validateCreateCustomerAccount)
    let customerAccountInfo = payload.customer
    if (customerAccountInfo.phoneNo.startsWith('+') || customerAccountInfo.phoneNo.length > 15 || customerAccountInfo.phoneNo.length < 10) {
      return {
        statusCode: 400, response: {
          errorCode: ErrorCodes.InvalidPhoneNumber,
          message: "Phone number should not start with +, include country code and legth >=10 <15"
        }
      }
    }
    let otpPromise;
    if (payload.generateOTP) {
      otpPromise = otpService.generateOtp(OTP_CLIENTS.SMS, GenericTableNamespace.CustomerLogin, customerAccountInfo.phoneNo)
    }
    console.log({ "msg": "Customer creation", payload })
    let customerAccountInfoDB = {
      id: customerAccountInfo.phoneNo,
      createdDate: new Date().toISOString(),
      profile: {
        name: customerAccountInfo.name
      },
      contactDetails: {
        default: customerAccountInfo.phoneNo
      },
      rides: {
      }
    }
    await dynamodbClient.createCustomerAccount(customerAccountInfo.phoneNo, customerAccountInfoDB)
    await otpPromise
    return { statusCode: 200, response: { message: "Verify OTP to login" } }
  },

  async validateDriverAndTriggerOTP({ payload }) {
    console.log("calling validateDriverAndTriggerOTP ...", payload);
    // TODO: validate payload data
    let phoneNumber = payload.phoneNo
    let licenseNumber = payload.licenseNo
    let cabVendorId = payload.cabVendorId
    let dateOfBirth = payload.dateOfBirth

    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Cab vendor not found for this login"
        }
      }
    }
    console.log("vendor response", vendorResponse)
    let walletBalance = parseInt(vendorResponse?.wallet?.total)
    if (!isNaN(walletBalance) && parseInt(walletBalance / 100) >= CONFIG.vendorCharges.driverAddOrRefresh) {
      console.log("wallet balance available !!!")
    } else {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InsufficientBalance,
          message: `Please recharge minimum ${CONFIG.vendorCharges.driverAddOrRefresh} to the account`
        }
      }
    }

    licenseNumber = utils.parseLicenseNumber(licenseNumber)
    if (!utils.validateLicenseNumber(licenseNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidLicenseNumber,
          message: "License number is not in proper format"
        }
      }
    }

    phoneNumber = utils.removeCountryCode(phoneNumber.toString())
    if (phoneNumber.length != 10) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidPhoneNumber,
          message: "Phone number should be in length 10"
        }
      }
    }

    const dateFormat = 'YYYY-MM-DD';
    const parsedDate = moment(dateOfBirth, dateFormat)
    const isValidDate = parsedDate.isValid();
    console.log("validating date", isValidDate)
    if (!isValidDate) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidDate,
          message: "Date should be in " + dateFormat + " format"
        }
      }
    }

    if (_.includes(_.keys(vendorResponse.drivers), licenseNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.ResourceExists,
          message: "Driver License is already registered with this vendor"
        }
      }
    }

    //TODO: validate the license in the payload
    _.each(vendorResponse.drivers, (value, key) => {
      if (value["phoneNumber"] == phoneNumber) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.ResourceExists,
            message: "Driver phoneNo is already registered with this vendor"
          }
        }
      }
    })

    let existingDrivers = await dynamodbClient.getAllCabDriversWithLicenseOrPhoneNumber(licenseNumber, undefined)
    // console.log(existingDrivers)
    if (existingDrivers != undefined && existingDrivers.length > 0) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.ResourceExists,
          message: "License number is already registered with another vendor, please remove it from there to add here"
        }
      }
    }

    existingDrivers = await dynamodbClient.getAllCabDriversWithLicenseOrPhoneNumber(undefined, phoneNumber)
    if (existingDrivers != undefined && existingDrivers.length > 0) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.ResourceExists,
          message: "Phone number is already registered with another vendor, please remove it from there to add here"
        }
      }
    }

    console.log("calling DB to get the driver info")
    let licenseDbResponse = await dynamodbClient.getDriverInfo(licenseNumber)
    console.log(licenseDbResponse)
    if (_.isEmpty(licenseDbResponse)) {
      console.log("calling invincible client to fetch driver info", licenseNumber, dateOfBirth)
      let licenseApiResponse = await invicibleOceanClient.getLicenseFromApiClub(licenseNumber, dateOfBirth)
      if (licenseApiResponse?.code != 200) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.LicenseNotFound,
            message: "Error while getting license info"
          }
        }
      }
      console.log("saving driver info", licenseNumber, licenseApiResponse)
      await dynamodbClient.saveDriverInfo(licenseNumber, licenseApiResponse)
      licenseDbResponse = await dynamodbClient.getDriverInfo(licenseNumber)
    }
    if (!_.isEmpty(licenseDbResponse)) {
      if (!moment(licenseDbResponse?.licenseInfo?.result?.data?.dob).isSame(moment(parsedDate))) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.InvalidDate,
            message: "Date of birth and license is not matching"
          }
        }
      }
    }
    console.log("db response", licenseDbResponse)
    if (_.isEmpty(licenseDbResponse.licenseInfo.result)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.LicenseNotFound,
          message: "License not found in database"
        }
      }
    }


    console.log("generating otp for", phoneNumber)
    let otpResponse = await otpService.generateOtp(OTP_CLIENTS.SMS, GenericTableNamespace.DriverAdditionPhoneNoVerification, phoneNumber)
    // console.log(otpResponse)
    return {
      statusCode: 200,
      response: otpResponse
    }
  },

  async validateCabVendorAndTriggerOTP({ payload }) {
    console.log("calling validateCabVendorAndTriggerOTP ...", payload);
    if (payload == undefined && payload.phoneNo == undefined) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidPhoneNumber,
          message: "Phone number not present in the request"
        }
      }
    }
    let phoneNumber = payload.phoneNo
    let existingResult = await dynamodbClient.getCabVendorByPhoneNos([phoneNumber])
    console.log("existing result", existingResult)
    if (existingResult != undefined && existingResult.length > 0) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.ResourceExists,
          message: "Phone number already registered"
        }
      }
    }

    console.log("generating otp for", phoneNumber)
    let otpResponse = await otpService.generateOtp(OTP_CLIENTS.SMS, GenericTableNamespace.VendorCreationPhoneNoVerification, phoneNumber)
    console.log(otpResponse)
    return { response: otpResponse }
  },

  async addVehicle({ payload }) {
    console.log("addVehicle", payload)
    validate(payload, Validator.validateAddVehicle)
    let { vehicleNumber, chassisNumber, googleIdToken, googleUserId, cabVendorId } = payload
    if (payload.isAdmin) {
      await verifyAdmin(googleIdToken, googleUserId)
    } else {

    }
    // // Driver app should send the token
    // await verifyGoogleToken(googleIdToken, cabVendorId)

    vehicleNumber = utils.parseVehicleNumber(vehicleNumber)
    // console.log(vehicleNumber)
    if (!utils.validateVehicleNumber(vehicleNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidVehicleNumber,
          message: "Invalid vehicle number is provided"
        }
      }
    }

    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Cab vendor Id not found"
        }
      }
    }

    if (_.includes(_.keys(vendorResponse.vehicles), vehicleNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.ResourceExists,
          message: "Vehicle is already registered with the same vendor"
        }
      }
    }

    // checking with all the other vendors
    let existingResult = await dynamodbClient.getAllCabVehiclesWithVehicleNumber(vehicleNumber)
    if (!_.isEmpty(existingResult)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.ResourceExists,
          message: "Vehicle is already registered with other vendors"
        }
      }
    }

    console.log("calling DB to get the vehicle info")
    let vehicleDbResponse = await dynamodbClient.getVehicleInfo(vehicleNumber)
    console.log(vehicleDbResponse)
    if (_.isEmpty(vehicleDbResponse)) {
      // TODO: deduct money from the account
      console.log("calling invincible client to fetch vehicle info", vehicleNumber)
      let vehicleApiResponse = await invicibleOceanClient.getVehicleInfoFromApiClub(vehicleNumber)
      if (vehicleApiResponse?.code != 200) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.VehicleNotFound,
            message: "Error while getting vehicle info from parivahan"
          }
        }
      }
      console.log("saving vehicle info", vehicleNumber)
      await dynamodbClient.saveVehicleInfo(vehicleNumber, vehicleApiResponse)
      vehicleDbResponse = await dynamodbClient.getVehicleInfo(vehicleNumber)
    }
    console.log("db response", vehicleDbResponse)
    let vehicleData = vehicleDbResponse?.vehicleInfo?.result?.data
    if (vehicleData == undefined) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.VehicleNotFound,
          message: "Vehicle data is missing in DB"
        }
      }
    }
    if (!vehicleData.chassis.startsWith(chassisNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.VehicleUnauthorized,
          message: "Chassis Number is not matching"
        }
      }
    }
    vehicleData = utils.fixVehicleDateFields(vehicleData)
    let validationResult = utils.validateVehicleInfo(vehicleData)
    let vehicles = vendorResponse.vehicles

    vehicles[vehicleNumber] = {
      type: utils.getVehicleType(vehicleData),
      fuel: vehicleData.type,
      model: vehicleData.model,
      vehicleRegNo: vehicleNumber,
      vehicleStatus: (validationResult.validationFailures.length == 0) ? VendorVehicleStatus.Approved : VendorVehicleStatus.Pending,
      insuranceInfo: {
        name: vehicleData.vehicleInsuranceCompanyName,
        expiry: vehicleData.vehicleInsuranceUpto
      },
      rcInfo: {
        expiry: vehicleData.rcExpiryDate
      },
      lastRefreshedDate: moment(new Date().toISOString()).format("MMMM Do YYYY, h:mm:ss a"), // Output: December 25th 2023, 12:00:00 pm
      pendingActions: validationResult.validationFailures // some data like insurance expired etc
    }

    let updatedResponse = await dynamodbClient.updateVehicleInfoAndChargeMoney(cabVendorId, vehicles, CONFIG.vendorCharges.vehicleAddOrRefresh, { newVehicleRegNo: vehicleNumber, isRefreshVehicle : false })
    return {
      statusCode: 200,
      response: updatedResponse
    }
  },

  async addDriver({ payload }) {
    //validate the OTP code
    console.log(payload)
    validate(payload, Validator.validateAddDriver)
    let { cabVendorId, googleIdToken, phoneNumber, otpCode, licenseNumber, panCardNumber, dateOfBirth, languages } = payload
    // Driver app should send the token
    // await verifyGoogleToken(googleIdToken, cabVendorId)

    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Cab vendor not found for this login"
        }
      }
    }
    console.log(vendorResponse)
    let walletBalance = parseInt(vendorResponse?.wallet?.total)
    if (!isNaN(walletBalance) && parseInt(walletBalance / 100) >= CONFIG.vendorCharges.driverAddOrRefresh) {
      console.log("wallet balance available !!!")
    } else {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InsufficientBalance,
          message: `Please recharge minimum ${CONFIG.vendorCharges.driverAddOrRefresh} to the account`
        }
      }
    }

    licenseNumber = utils.parseLicenseNumber(licenseNumber)
    if (!utils.validateLicenseNumber(licenseNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidLicenseNumber,
          message: "License number is not in proper format"
        }
      }
    }

    let licenseDbResponse = await dynamodbClient.getDriverInfo(licenseNumber)
    console.log("db response", licenseDbResponse)
    if (_.isEmpty(licenseDbResponse.licenseInfo.result)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.LicenseNotFound,
          message: "License not found in database"
        }
      }
    }

    let validationResult = utils.validateLicenseInfo(licenseDbResponse?.licenseInfo?.result?.data)
    const otpResponse = await otpService.verifyOtpCode(GenericTableNamespace.DriverAdditionPhoneNoVerification, phoneNumber, otpCode)
    if (otpResponse?.response?.message?.errorCode == ErrorCodes.OtpInvalid) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.OtpInvalid,
          message: "Given OTP is invalid"
        }
      }
    }
    console.log("OTP verified, validating the driver data")

    let drivers = vendorResponse.drivers
    drivers[licenseNumber] = {
      licenseNumber,
      "dateOfBirth": moment(licenseDbResponse?.licenseInfo?.result?.data?.dob).format('DD-MM-YYYY'),
      panCardNumber,
      phoneNumber,
      approved: validationResult.approved,
      expiry: moment(licenseDbResponse?.licenseInfo?.result?.data?.doe).format('DD-MM-YYYY'),
      name: licenseDbResponse?.licenseInfo?.result?.data?.name,
      knownLanguages: languages,
      pendingActions: validationResult.validationFailures
    }
    // Persist in DB
    console.log("updating the below driver info", drivers)
    let updatedResponse = await dynamodbClient.updateDriverInfoAndChargeMoney(cabVendorId, drivers, CONFIG.vendorCharges.driverAddOrRefresh, { newDriverLicenseNo: licenseNumber, isRefreshDriver : false })
    return {
      statusCode: 200,
      response: updatedResponse
    }
    // upload(vendorId, licenseNo, file[0](profile photot), files[2](license info))
  },

  async refreshVehicle({ payload }) {
    console.log(payload)
    // validate(payload, Validator.validateAddDriver)
    let { cabVendorId, googleIdToken, googleId, vehicleNumber, fromCron, isDashboard, isDeductMoney, isParivahanCall } = payload

    if (fromCron) {
      console.log("skipping is Admin validation as the request is from cron !!!")
    } else {
      console.log("verifying admin as fromCron is false !!!")
      await verifyAdmin(googleIdToken, googleId)
    }

    // if (payload.isAdmin && payload.fromCron == false) {
    //   await verifyAdmin(googleIdToken, googleId)
    // } else {
    // Driver app should send the token
    // await verifyGoogleToken(googleIdToken, cabVendorId)
    // }

    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Vendor not found for the given vendor Id"
        }
      }
    }
    console.log(vendorResponse)
    vehicleNumber = utils.parseVehicleNumber(vehicleNumber)
    console.log(vehicleNumber)
    if (!utils.validateVehicleNumber(vehicleNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidVehicleNumber,
          message: "Vehicle number is in invalid format"
        }
      }
    }
    if (!_.includes(_.keys(vendorResponse.vehicles), vehicleNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.VehicleNotFound,
          message: "Vehicle is not already registered with the same vendor"
        }
      }
    }
    //TODO: deduct wallet money for vehicle refresh

    if (isParivahanCall) {
      console.log("calling invincible client to fetch vehicle info", vehicleNumber)
      let vehicleApiResponse = await invicibleOceanClient.getVehicleInfoFromApiClub(vehicleNumber)
      if (vehicleApiResponse?.code != 200) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.VehicleNotFound,
            message: "Error while getting vehicle info from parivahan"
          }
        }
      }
      console.log("saving vehicle info", vehicleNumber)
      await dynamodbClient.saveVehicleInfo(vehicleNumber, vehicleApiResponse)
    } else {
      console.log("skipping call to invincible client")
    }

    let vehicleDbResponse = await dynamodbClient.getVehicleInfo(vehicleNumber)
    let vehicleData = utils.fixVehicleDateFields(vehicleDbResponse?.vehicleInfo?.result?.data)
    let validationResult = utils.validateVehicleInfo(vehicleData)
    let vehicles = vendorResponse.vehicles

    vehicles[vehicleNumber] = {
      type: utils.getVehicleType(vehicleData),
      fuel: vehicleData.type,
      model: vehicleData.model,
      vehicleRegNo: vehicleNumber,
      vehicleStatus: (validationResult.validationFailures.length == 0) ? VendorVehicleStatus.Approved : VendorVehicleStatus.Pending,
      insuranceInfo: {
        name: vehicleData.vehicleInsuranceCompanyName,
        expiry: vehicleData.vehicleInsuranceUpto
      },
      rcInfo: {
        expiry: vehicleData.rcExpiryDate
      },
      lastRefreshedDate: moment(new Date().toISOString()).format("MMMM Do YYYY, h:mm:ss a"), // Output: December 25th 2023, 12:00:00 pm
      pendingActions: validationResult.validationFailures // some data like insurance expired etc
    }

    if (fromCron || (isDashboard && !isDeductMoney)) {
      console.log("updating the vehicle info")
      let updatedResponse = await dynamodbClient.updateVehicleInfo(cabVendorId, vehicles)
      return {
        statusCode: 200,
        response: updatedResponse
      }
    } else if (isParivahanCall) {
      console.log("deducting parivahan amount")
      let updatedResponse = await dynamodbClient.updateVehicleInfoAndChargeMoney(cabVendorId, vehicles, CONFIG.vendorCharges.vehicleRefresh, { newVehicleRegNo: vehicleNumber, isRefreshVehicle : true })
      return {
        statusCode: 200,
        response: updatedResponse
      }
    } else {
      console.log("deducting refresh amount")
      let updatedResponse = await dynamodbClient.updateVehicleInfoAndChargeMoney(cabVendorId, vehicles, CONFIG.vendorCharges.vehicleGenericRefresh, { newVehicleRegNo: vehicleNumber, isRefreshVehicle : true })
      return {
        statusCode: 200,
        response: updatedResponse
      }
    }

  },
  async refreshDriver({ payload }) {
    console.log(payload)
    // validate(payload, Validator.validateAddDriver)
    let { cabVendorId, googleIdToken, googleId, licenseNumber, fromCron, isDashboard, isDeductMoney, isParivahanCall } = payload

    if (fromCron) {
      console.log("skipping is Admin validation as the request is from cron !!!")
    } else {
      console.log("verifying admin as fromCron is false !!!")
      await verifyAdmin(googleIdToken, googleId)
    }

    // if (payload.isAdmin && payload.fromCron == false) {
    //   await verifyAdmin(googleIdToken, googleId)
    // } else {
    // Driver app should send the token
    // await verifyGoogleToken(googleIdToken, cabVendorId)
    // }

    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Vendor not found for the given vendor Id"
        }
      }
    }
    console.log(vendorResponse)

    licenseNumber = utils.parseLicenseNumber(licenseNumber)
    if (!utils.validateLicenseNumber(licenseNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidLicenseNumber,
          message: "License number is not in proper format"
        }
      }
    }

    let drivers = vendorResponse.drivers
    let driverObject = drivers[licenseNumber]

    if (_.isEmpty(driverObject)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.LicenseNotFound,
          message: "Driver is not already registered with the same vendor"
        }
      }
    }
    let dateOfBirth = driverObject.dateOfBirth
    if (isParivahanCall) {
      console.log("calling invicible client to fetch driver info", licenseNumber, dateOfBirth)
      let licenseApiResponse = await invicibleOceanClient.getLicenseFromApiClub(licenseNumber, dateOfBirth)
      console.log(licenseApiResponse)
      if (licenseApiResponse?.code != 200) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.LicenseNotFound,
            message: "Error while getting license info"
          }
        }
      }
      console.log("saving driver info", licenseNumber, licenseApiResponse)
      await dynamodbClient.saveDriverInfo(licenseNumber, licenseApiResponse)
    } else {
      console.log("Skipping the invincible client call")
    }

    let licenseDbResponse = await dynamodbClient.getDriverInfo(licenseNumber)
    console.log("db response", licenseDbResponse)
    if (_.isEmpty(licenseDbResponse.licenseInfo.result)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.LicenseNotFound,
          message: "License not found in database"
        }
      }
    }
    let validationResult = utils.validateLicenseInfo(licenseDbResponse?.licenseInfo?.result?.data)

    let knownLanguages = driverObject.knownLanguages
    let phoneNumber = driverObject.phoneNumber
    drivers[licenseNumber] = {
      ...drivers[licenseNumber],
      licenseNumber,
      dateOfBirth: moment(dateOfBirth, "DD-MM-YYYY").format('YYYY-MM-DD'),
      phoneNumber,
      approved: validationResult.approved,
      expiry: moment(licenseDbResponse?.licenseInfo?.result?.data?.doe, "YYYY-MM-DD").format('DD-MM-YYYY'),
      name: licenseDbResponse?.licenseInfo?.result?.data?.name,
      knownLanguages,
      pendingActions: validationResult.validationFailures
    }
    // Persist in DB
    if (fromCron || (isDashboard && !isDeductMoney)) {
      console.log("updating the below driver info", drivers)
      let updatedResponse = await dynamodbClient.updateDriverInfo(cabVendorId, drivers)
      return {
        statusCode: 200,
        response: updatedResponse 
      }
    } else if(isParivahanCall) {
      console.log("deducting parivahan amount")
      let updatedResponse = await dynamodbClient.updateDriverInfoAndChargeMoney(cabVendorId, drivers, CONFIG.vendorCharges.driverRefresh, { newDriverLicenseNo: licenseNumber, isRefreshDriver : true })
      return { statusCode: 200, response: updatedResponse }
    } else {
      console.log("deducting refresh amount")
      let updatedResponse = await dynamodbClient.updateDriverInfoAndChargeMoney(cabVendorId, drivers, CONFIG.vendorCharges.driverGenericRefresh, { newDriverLicenseNo: licenseNumber, isRefreshDriver : true })
      return {
        statusCode: 200,
        response: updatedResponse
      }
    }
  },
  async deleteVehicle({ payload }) {
    console.log(payload)
    // validate(payload, Validator.validateAddDriver)
    let { cabVendorId, googleIdToken, vehicleNumber } = payload
    // Driver app should send the token
    // await verifyGoogleToken(googleIdToken, cabVendorId)
    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Vendor not found for the given vendor Id"
        }
      }
    }
    console.log(vendorResponse)
    vehicleNumber = utils.parseVehicleNumber(vehicleNumber)
    console.log(vehicleNumber)
    if (!utils.validateVehicleNumber(vehicleNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidVehicleNumber,
          message: "Vehicle number is in invalid format"
        }
      }
    }
    if (!_.includes(_.keys(vendorResponse.vehicles), vehicleNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.VehicleNotFound,
          message: "Vehicle is not already registered with the same vendor"
        }
      }
    }
    let vehicles = vendorResponse.vehicles
    delete vehicles[vehicleNumber]
    console.log("updating the vehicle info")
    let updatedResponse = await dynamodbClient.updateVehicleInfo(cabVendorId, vehicles)
    console.log("deleting the vehicle cache ...")
    await dynamodbClient.deleteVehicleInfo(vehicleNumber)
    return { response: updatedResponse }

  },
  async deleteDriver({ payload }) {
    console.log(payload)
    // validate(payload, Validator.validateAddDriver)
    let { cabVendorId, googleIdToken, licenseNumber } = payload
    // Driver app should send the token
    // await verifyGoogleToken(googleIdToken, cabVendorId)
    let vendorResponse = await dynamodbClient.getCabVendorsById(cabVendorId)
    if (_.isEmpty(vendorResponse)) {
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound,
          message: "Vendor not found for the given vendor Id"
        }
      }
    }
    console.log(vendorResponse)

    licenseNumber = utils.parseLicenseNumber(licenseNumber)
    if (!utils.validateLicenseNumber(licenseNumber)) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.InvalidLicenseNumber,
          message: "License number is not in proper format"
        }
      }
    }
    let drivers = vendorResponse.drivers
    let phoneNumber = drivers[licenseNumber]["phoneNumber"]
    delete drivers[licenseNumber]
    let updatedResponse = await dynamodbClient.updateDriverInfo(cabVendorId, drivers)
    console.log("deleting the driver cache ...")
    await dynamodbClient.deleteDriverInfo(licenseNumber)
    await dynamodbClient.deleteGenericEnteries(GenericTableNamespace.DriverAdditionPhoneNoVerification, phoneNumber)
    return { response: updatedResponse }
  },
  // This returns only rides with customer information  which are assigned to the given cabVendorId. 
  // This is to prevent returning unassigned rides with customer information
  async getRideDetails({ params }) {
    let bookingIds = params.bookingIds.split(',')
    let cabVendorId = params.cabVendorId
    let rides = []
    if (bookingIds.length == 1)
      rides = [await dynamodbClient.getRideByBookingId(bookingIds[0], true)]
    else
      rides = await dynamodbClient.getRideByBookingIds(bookingIds)

    rides = rides.filter(ride => ride?.cabVendor?.cabVendorId == cabVendorId)
      .map(presenter.presentRide)
    //Todo : We are sending all the data from the DB. Create a presentation layer
    return {
      response: { rides }
    }
  },

  async getUnAssignedRides({ params }) {
    //Todo: Order rides by relevance based on cabVendorId
    let cabVendorId = params.cabVendorId
    let ridesPromise = dynamodbClient.queryRidesByStatus(RideStatus.UnAssigned, "bookingId, bookingDetails, priceDetails, createdDate, rideStatus, driverNotes")
    let cabVendorMetaPromise = cabVendorId ? dynamodbClient.getCabVendorsById(cabVendorId, "meta") : Promise.resolve({})
    let [rides, cabVendorMeta] = await Promise.all([ridesPromise, cabVendorMetaPromise])
    rides.rides = presenter.presentRide(rides.rides, cabVendorMeta)
    return { response: rides }
  },

  async updateRideByCabVendor({ payload }) {
    validate(payload, Validator.validateUpdateRideByDriver)
    try {
      let action = payload.action
      if (action == 'ASSIGN_DRIVER') {
        let response = await ridesService.assignRide(payload)
        await sqsClient.publishRideAssignment({ bookingId: payload.bookingId, cabVendorId: payload.cabVendorId })
        return response;
      } else if (action == "START_RIDE") {
        return await ridesService.startRide(payload)
      } else {
        return {
          response: { errorCode: "NOT_IMPLEMENTED" },
          statusCode: 501
        }
      }
    } catch (err) {
      if (err.errorCode)
        return {
          response: err,
          statusCode: 400
        }
      throw err
    }
  },

  async UpgradeV2({ payload }) {
    let cabVendorId = payload.cabVendorId
    let panCardNo = payload.panCardNumber

    // upgrading to V2
    let currentCabVendorData = await dynamodbClient.getCabVendorAllData(cabVendorId)
    // !!!! Potentially dangerous.... what if deep merge is not working as intended ????
    currentCabVendorData["vehicles"] = {}
    currentCabVendorData["drivers"] = {}
    let newCabVendorData = _.merge(currentCabVendorData, {
      profile: { "panCardNo": panCardNo },
      version: "v2",
    }, { lastUpdatedTime: new Date().toISOString() })
    await dynamodbClient.updateCabVendorByAdmin(cabVendorId, newCabVendorData)
    console.log("Upgraded to V2 !!!")
    return {
      statusCode: 200,
      response: newCabVendorData
    }
  },

  async editVendorName({ payload }) {
    let vendorName = payload.name
    if (vendorName == undefined) {
      return {
        statusCode: 400,
        response: {
          message: "Name is empty",
          errorCode: ErrorCodes.InvalidVendorName
        }
      }
    }

    if (vendorName.length < 3 || vendorName.length > 20) {
      return {
        statusCode: 400,
        response: {
          message: "Name length should be greater than 3 and less than 20",
          errorCode: ErrorCodes.InvalidVendorName
        }
      }
    }
    const regex = /^[a-zA-Z0-9 ]+$/;
    if (!regex.test(vendorName)) {
      return {
        statusCode: 400,
        response: {
          message: "Name format is not in proper format, Please enter only characters and numbers",
          errorCode: ErrorCodes.InvalidVendorName
        }
      }
    }
    let cabVendorId = payload.cabVendorId
    let response = await dynamodbClient.getCabVendorsById(payload.cabVendorId)
    if (response == undefined) {
      return {
        statusCode: 400,
        response: {
          message: "Cab vendor not found for the provided cab vendor Id",
          errorCode: ErrorCodes.ResourceNotFound
        }
      }
    }

    let currentCabVendorData = await dynamodbClient.getCabVendorAllData(cabVendorId)
    // !!!! Potentially dangerous.... what if deep merge is not working as intended ????
    let newCabVendorData = _.merge(currentCabVendorData, { profile: { "name": vendorName } }, { lastUpdatedTime: new Date().toISOString() })
    await dynamodbClient.updateCabVendorByAdmin(cabVendorId, newCabVendorData)
    console.log("Cab vendor name got updated !!!")
    return {
      statusCode: 200,
      response: newCabVendorData
    }
  },

  async validateVendorEditPhoneNoTriggerOTP({ payload }) {
    // TODO: Rate Limiting is not developed
    // currently used only for the phone number change
    // validate the request
    let phoneNumber = payload["phoneNumber"]
    if (phoneNumber == undefined) {
      return {
        statusCode: 400,
        response: {
          message: "Phone number is empty",
          errorCode: ErrorCodes.InvalidPhoneNumber
        }
      }
    }
    if (phoneNumber.length != 10) {
      return {
        statusCode: 400,
        response: {
          message: "Phone number length is not equal to 10",
          errorCode: ErrorCodes.InvalidPhoneNumber
        }
      }
    }
    // UI should not allow this call for same number edit
    // let response = await dynamodbClient.getCabVendorsById(payload.cabVendorId)
    // if (response?.profile?.phoneNo == phoneNumber) {
    //   return {
    //     statusCode: 400,
    //     response: {
    //       message: "Phone number is already existing in this same account",
    //       errorCode: ErrorCodes.InvalidPhoneNumber
    //     }
    //   }
    // }
    // Fetching existing cab vendors with the request phone number
    let existingCabVendors = await dynamodbClient.getCabVendorByPhoneNoV2(phoneNumber)
    if (existingCabVendors.length > 0) {
      return {
        statusCode: 400,
        response: {
          message: "Phone number already present in another cab vendor",
          errorCode: ErrorCodes.ResourceExists
        }
      }
    }
    console.log("generating otp for", phoneNumber)
    let otpResponse = await otpService.generateOtp(OTP_CLIENTS.SMS, GenericTableNamespace.VendorEditPhoneNoVerification, phoneNumber)
    // console.log(otpResponse)
    return {
      statusCode: 200,
      response: otpResponse
    }

  },

  async verifyAndEditVendorPhoneNumber({ payload }) {
    let cabVendorId = payload.cabVendorId
    let phoneNumber = payload.phoneNumber
    let otpCode = payload.otpCode
    const otpResponse = await otpService.verifyOtpCode(GenericTableNamespace.VendorEditPhoneNoVerification, phoneNumber, otpCode)
    if (otpResponse?.response?.message?.errorCode == ErrorCodes.OtpInvalid) {
      return {
        statusCode: 400,
        response: {
          errorCode: ErrorCodes.OtpInvalid,
          message: "Given OTP is invalid"
        }
      }
    }
    console.log("OTP verified, updating the phone number to the vendor data")
    let currentCabVendorData = await dynamodbClient.getCabVendorAllData(cabVendorId)
    // !!!! Potentially dangerous.... what if deep merge is not working as intended ????
    let newCabVendorData = _.merge(currentCabVendorData, { profile: { "phoneNo": phoneNumber } }, { lastUpdatedTime: new Date().toISOString() })
    await dynamodbClient.updateCabVendorByAdmin(cabVendorId, newCabVendorData)
    console.log("Cab vendor phone number got updated !!!")
    // Deleting the OTP entry
    console.log("Deleting the generic table entry")
    await dynamodbClient.deleteGenericEnteries(GenericTableNamespace.VendorEditPhoneNoVerification, phoneNumber)
    console.log("Entry got deleted")
    return {
      statusCode: 200,
      response: newCabVendorData
    }
  },

  async getCabVendors({ params }) {
    let response = await dynamodbClient.getCabVendorsById(params.cabVendorId)
    // if(response && response.vendorStatus == VendorStatus.UnApproved){
    //   return {statusCode: 400, response: {errorCode: ErrorCodes.VendorNotApproved, message: "Request admin to approve your account"}}
    // } else 
    if (response)
      return { response: presenter.presentCabVendor(response) }
    else
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound
        }
      }
  },

  async createRazorPayOrder({ payload }) {
    validate(payload, Validator.validateCreateRazorpayOrder)
    const response = await paymentService.createOrder(payload)
    return { response }
  },

  async updateCabVendorConfig({ payload }) {
    validate(payload, Validator.validateUpdateVendorConfig)
    // Todo: app is not sending a token. check
    // await verifyGoogleToken(payload.googleIdToken, payload.cabVendorId)
    console.log(payload)
    //Todo: What about unsubscribing the token ?
    await dynamodbClient.updateDeviceDetails(payload.cabVendorId, payload)
    await sqsClient.pushDeviceUpdateMsg({
      type: SQSMessageType.DeviceConfigUpdate,
      payload
    })
    return {
      response: { message: "OKKd" }
    }
  },

  async getRateCards({ params }) {
    validate(params, Validator.validateGetRateCards)
    let rateCardService = new RateCardService()
    let rateCards = await rateCardService.getRateCards(params)

    return {
      response: rateCards,
      headers: {
        "Cache-Control": "max-age=3600" // 30m * 60
      }
    }
  },

  async getWalletTransactionsById({ params }) {
    let response = await dynamodbClient.getWalletTransactionsById(params.cabVendorId)
    if (response)
      return { response }
    else
      //Bug: this will never be executed becuase ddb will return a empty array if there is no record
      return {
        statusCode: 404,
        response: {
          errorCode: ErrorCodes.ResourceNotFound
        }
      }
  },

  async updateRideStatus() {
    console.log("I'm scheduled");
  },

  async postRides({ payload }) {
    validate(payload, Validator.validatePostRides)
    if (payload?.bookingSource?.source == BookingSource.CustomerApp) {
      if (!payload.bookingSource.accessToken) {
        return {
          statusCode: 400,
          response: {
            errorCode: ErrorCodes.InvalidAccessToken,
            message: "Access token is empty"
          }
        }
      }
      let decryptResponse = await cryptoService.jwtDecrypt(payload.bookingSource.accessToken)
      if (decryptResponse.isExpired) {
        return { statusCode: 400, response: { errorCode: ErrorCodes.InvalidAccessToken, message: "Access token is expired" } }
      }
      payload.bookingSource.sourceId = decryptResponse.payload.phoneNo
    }
    delete payload?.bookingSource?.accessToken
    let response = await ridesService.publishRide(payload)
    return {
      response
    }
  },

  async addNotes({ payload }) {
    await verifyAdmin(payload.googleIdToken, payload.googleUserId)
    let response = await ridesService.addNotes(payload)
    return {
      response
    }
  },

  async razorpayWebHook({payload}) {
    if (payload.event == "order.paid") {
      let orderCreatedDate = payload.payload.order.entity.created_at;
      let cabVendorId = payload.payload.order.entity.notes.cabVendorId;
      orderCreatedDate = isNaN(orderCreatedDate) ? orderCreatedDate : new Date(orderCreatedDate * 1000).toISOString();
      let addMoneyDetails = await dynamodbClient.getAddMoneyActionDetails(cabVendorId, orderCreatedDate)
      if (addMoneyDetails.opDetails.amount != payload.payload.order.entity.amount_paid) {
        throw {"errorMsg": "Amount paid is not matching " + addMoneyDetails.opDetails.amount + " " + payload.payload.order.amount_paid}
      } else if (addMoneyDetails.opDetails.status != "INITIATED") {
        await sendDevMessage("Thaa..., You got paid " + addMoneyDetails.opDetails.amount/100)
        return {statusCode: 200, response: {message: "AlreadyProcessed"}}
      }
      let walletDetails = await dynamodbClient.completePaymentAndAddMoney(cabVendorId, {
        orderCreatedDate: orderCreatedDate,
        amountToBeAdded: addMoneyDetails.opDetails.amount
      });
      await sendDevMessage("Thaa..., You got paid(async) " + addMoneyDetails.opDetails.amount/100 + "  cabvendorId: " + cabVendorId)
      return {statusCode: 200, response: {message: "OK"}}
    } else {
      console.log(payload)
      return {
        statusCode: 200,
        response: {
          message: "SKIPPED"
        }
      }
    }
  },

  async verifyRazorpayPaymentAndUpdateWallet({ payload }) {
    validate(payload, Validator.validateRazorpayPayment)
    let addMoneyDetails = await paymentService.verifyPayment(payload)
    if (addMoneyDetails == false) {
      return {
        statusCode: 400,
        response: {
          errorCode: "INVALID_PAYMENT_SIGNATURE",
          message: "lollllllzzzz"
        }
      }
    }

    let orderCreatedDate = isNaN(payload.orderCreatedDate) ? payload.orderCreatedDate : new Date(payload.orderCreatedDate * 1000).toISOString()
    let walletDetails;
    if (payload["isRegistration"]) {
      //TODO: Add these two statements into one transaction
      walletDetails = await dynamodbClient.completePaymentAndAddMoney(payload.cabVendorId, {
        orderCreatedDate: orderCreatedDate,
        amountToBeAdded: 0
      });
      await sqsClient.sendVendorApprovalNotification(payload.cabVendorId)
    } else {
      walletDetails = await dynamodbClient.completePaymentAndAddMoney(payload.cabVendorId, {
        orderCreatedDate: orderCreatedDate,
        amountToBeAdded: addMoneyDetails.opDetails.amount
      });
    }
    return {
      response: {
        wallet: {
          total: walletDetails.wallet.total
        }
      }
    }
  },

  async processOtpRequest({ payload }) {
    validate(payload, Validator.validateOtpRequest)
    let namespace = payload.otpType;
    if (payload.action == "GENERATE") {
      return await otpService.generateOtp(payload.carrier, namespace, payload.phoneNumber)
    } else if (payload.action == "VERIFY") {
      return await otpService.verifyOtpCode(namespace, payload.phoneNumber, payload.otpCode)
    } else if (payload.action == "RESEND") {
      return await otpService.regenerateOtp(payload.carrier, namespace, payload.phoneNumber)
    }
  },

  async generateDocumentMgmtUrl({ payload }) {
    validate(payload, Validator.validateDocumentMgmtUrl)
    //TODO: Add more validations. 
    // 1. Validate google token and match it with cabVendorId sent
    // 2. Limit no. of times api can be accessed
    let { category, action, groupId } = payload
    if (category == "CABVENDOR" && action == "POST") {
      let response = await s3Client.getPreSignedUrlForCabVendorUpload(payload.groupId)
      return { response }
    } else {
      throw {
        response: {
          errorCode: "NOT_SUPPORTED",
          message: "Given combination not supported"
        },
        statusCode: 400
      }
    }
  },

  async cabVendorFileUpload({ payload }) {
    //let googleUserDetails = await verifyGoogleToken(payload.get("googleIdToken"), payload.get("cabVendorId"));
    let category = payload.get("category")
    let categoryId = payload.get("categoryId")
    let cabVendorId = payload.get("cabVendorId")
    let files = []
    if (category == FileUploadCategory.Driver) {
      let drivingLicense = payload.get("drivingLicense")
      let passportPhoto = payload.get("driverPhoto")
      let driverAadhar = payload.get("driverAadhar")
      files.push(
        { file: drivingLicense, name: "drivingLicense" },
        { file: passportPhoto, name: "passportPhoto" },
        { file: driverAadhar, name: "aadhar" })
    } else if (category == FileUploadCategory.Vehicle) {
      files.push(
        { file: payload.get("vehicleRC"), name: "vehicleRC" },
        { file: payload.get("carPhoto1"), name: "carPhoto1" },
        { file: payload.get("carPhoto2"), name: "carPhoto2" },
        { file: payload.get("carPhoto3"), name: "carPhoto3" }
      )
    } else if (category == FileUploadCategory.CabVendor) {
      files.push({ file: payload.get("cabVendorPan"), name: "cabVendorPan" })
    }

    files = files.filter(file => file.file != null)
      .map(file => {
        return {
          category,
          categoryId,
          cabVendorId,
          "name": file.name,
          file: file.file
        }
      })
    let filesUploadResponse = await s3Client.cabVendorFilesUpload(cabVendorId, files)
    return { response: { message: filesUploadResponse }, statusCode: 200 }
  },

  async closeRide({ payload }) {
    validate(payload, Validator.validateCloseRideEndpoint)
    let adminDetails;
    if (payload.isAdmin) {
      adminDetails = await verifyAdmin(payload.googleIdToken, payload.googleUserId)
    }
    let response = await ridesService.closeRide(payload.bookingId, payload)
    auditService.logEvent(AuditNamespace.Ride, payload.bookingId, AuditAction.CloseRide, adminDetails?.email || payload.cabVendorId, payload)
    return response;
  },

  /* 
      ADMIN endpoints
  */


  async adminCancelRide({ payload }) {
    validate(payload, Validator.validateAdminCancelRideRequest)
    let { googleUserId, googleIdToken } = payload
    let adminDetails = await verifyAdmin(googleIdToken, googleUserId)
    let response = await adminService.cancelRide(payload)
    auditService.logEvent(AuditNamespace.Ride, payload.bookingId, AuditAction.CancelRide, adminDetails.email, payload)
    return response;
  },

  async updateRateCardConfig({ payload }) {
    validate(payload, Validator.validateRateCardConfigUpdate)
    // let rateCardConfig = await CONFIG.env.KV.get(RateCardConfigKey, {type:"json"})
    let rateCardConfig = DefaultRateCardConfig

    rateCardConfig.rateCards.oneway = rateCardConfig.rateCards.oneway.map(rateCard => {
      let newConfig = payload.rateCards[rateCard.id]
      Object.assign(rateCard, newConfig)
      return rateCard
    })

    rateCardConfig.rateCards.roundTrip = rateCardConfig.rateCards.roundTrip.map(rateCard => {
      let newConfig = payload.rateCards[rateCard.id]
      Object.assign(rateCard, newConfig)
      return rateCard
    })
    await CONFIG.env.KV.put(RateCardConfigKey, JSON.stringify(rateCardConfig))
    return { response: rateCardConfig }
  },

  async sendDummySMS({ payload }) {
    let response = await otpService.sendDummySms()
    return { response }
  },

  async updateRide({ payload }) {
    validate(payload, Validator.validateAdminUpdateRide)
    let { googleUserId, googleIdToken } = payload
    let adminDetails = await verifyAdmin(googleIdToken, googleUserId)
    let response = await adminService.processRequest(ADMIN_REQUEST_TYPES.UPDATE_RIDE, payload, adminDetails)
    return response
  },

  async updateWallet({ payload }) {
    validate(payload, Validator.validateAdminUpdateWallet)
    let { googleUserId, googleIdToken } = payload
    let adminGoogleDetails = await verifyAdmin(googleIdToken, googleUserId)
    let response = await adminService.updateWallet(payload.cabVendorId, payload, adminGoogleDetails.email)
    return response;
  },

  async getGenericEnteries({ payload }) {
    console.log(payload)
    let { namespace, primaryKey, googleUserId, googleIdToken } = payload
    await verifyAdmin(googleIdToken, googleUserId)
    let response = await dynamodbClient.getGenericEnteries(namespace, primaryKey)
    return { response }
  },

  async updateGenericEnteries({ payload }) {
    console.log(payload)
    let { googleUserId, googleIdToken } = payload
    await verifyAdmin(googleIdToken, googleUserId)
    delete payload["googleUserId"]
    delete payload["googleIdToken"]
    let response = await dynamodbClient.forceUpdateGenericEntry(payload)
    return { response }
  },

  async processAdminRequest({ payload }) {
    validate(payload, Validator.validateAdminRequest)
    let { googleUserId, googleIdToken } = payload
    let adminUserDetails = await verifyAdmin(googleIdToken, googleUserId)
    let response = await adminService.processRequest(payload.requestType, payload.payload, adminUserDetails);
    return { response }
  }
}

export default handlerObjects

function validate(payload, validator) {
  if (!validator(payload)) {
    return handleValidationError(validator.errors)
  }
}

function handleValidationError(errors) {
  console.log(errors)
  throw {
    response: {
      errorCode: "VALIDATION_ERROR",
      message: errors.map(error => error.schemaPath + " " + error.message)
    },
    statusCode: 400
  }
}