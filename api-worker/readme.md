### Todo

### Customer code flow
1. Account creation apis
    - Create Customer account ( and generate otp for login )
    - Normal otp generation ( empty account creation ?? )
    - OTP validate and return auth token
2. Ride endpoint 
    - Ride source (3rdParty, Web, CustomerApp) 

### API Cloudflare worker

- Write code such that it can run in any serverless environment with only changes in the entry script. 
- AWS credentials are set in cloudflare using `wrangler secrets` cli manually 

### Debugging

- For local development use docker for spinning up dynamodb local `docker-compose up` . The database is persistent on 
subsequenct runs. 
- `npm run start` to start local cloudflare worker
- Download NoSQL Workbench to connect with dynamoDB local for DDL and DML operation
- Razor pay testing - Use these upi ids `success@razorpay` , `failure@razorpay`. For other payment instruments check [here](https://razorpay.com/docs/payments/payments/test-card-upi-details/)

### Config mgmt
  All most all config is static present in config folder. AWS credentials are supplied by cloudflare worker runtime as an argument. Since we can get hold of this value 
  only when the method is invoked, we need to update the CONFIG values after that. Due to this defered initialization of certain config values we need to be carefull about how
  we initialize certain classes (check dynamoDB client initalization). Check `config/index.js` for the logic. 

  Note: **The default config value is Production**. While testing locally make sure you set / pass the env correctly. This is done to ensure we dont need to reinitalize config in production to save
  some time (i hope ;P)

### Blockers faced so far

- **Setting breakpoints** : Seems cloudflare worker doesnt yet support setting breaking points. console.log is the only way
- **Docker local** :
    - On setting the endpoint to localhost:8000 for connecting to docker, was getting JSON serialization error
    - Seems the fucking client was not able to connect, instead of saying connection refused it said serialization error
    - wranlger dev was running from cloudflare server thats why it was unable to connect to ddb local running my host
    - added option --local to run it locally
- **Dynamodb not picking up config**: This is due to live binding of ES6 exports. We reinitalize the config once know the env type and credentials. but we had already passed the dynamodb 
client instance as pass by value to live binding not working as I intended. 