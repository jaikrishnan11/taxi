
var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http'),
    moment = require('moment'),
    _ = require('lodash');
chai.use(chaiHttp);
const chaiThings = require('chai-things');
chai.use(chaiThings);

var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'

describe("Admin actions", function() {
    let cabVendorId = "test-" + Math.random() * 10000
    let bookingId = null;
    let walletAmountInPaise = 99900;
    this.timeout(10000)

    beforeEach(async ()=> {
        cabVendorId = "test-" + Math.random() * 10000
        await createApprovedCabVendor(cabVendorId, walletAmountInPaise)
    })

    describe("Get Vendors", function () {
        it("Get Vendors by PhoneNos", async ()=> {
            response = await chai.request(endpoint)
            .post("/admin/peformAction")
            .send({  
                "googleIdToken": "dummy",
                "googleUserId": "dummy",
                "requestType": "GET_VENDORS",
                "payload": {
                    "phoneNumbers": ["1234567890"]
            }});

            expect(response).to.have.status(200)
            expect(response.body).to.have.length.above(0)
            expect(response.body[0].profile.phoneNo).to.eq(1234567890)
        })
    })

    describe("Admin update vendor meta", function() {
        it("Update cabVendorMeta migration and shadowban", async () => {
            response = await chai.request(endpoint)
            .post("/admin/peformAction")
            .send({  
                "googleIdToken": "dummy",
                "googleUserId": "dummy",
                "requestType": "UPDATE_VENDOR_META",
                "payload": {
                    "cabVendorId": cabVendorId,
                    "forceV2Migration": true,
                    "shadowban": {
                        "banExpiry": moment().toISOString(),
                        "rideDelayMinutes": 30
                    }
            }});
            expect(response).to.have.status(200)
            let currentCabVendor = await helpers.getCabVendorAllData(cabVendorId)
            expect(currentCabVendor.meta).to.deep.include({forceV2Migration: true})
            expect(currentCabVendor.meta.shadowban).to.not.null


            response = await chai.request(endpoint)
            .post("/admin/peformAction")
            .send({  
                "googleIdToken": "dummy",
                "googleUserId": "dummy",
                "requestType": "UPDATE_VENDOR_META",
                "payload": {
                    "cabVendorId": cabVendorId,
                    "forceV2Migration": false
            }});
            expect(response).to.have.status(200)
            currentCabVendor = await helpers.getCabVendorAllData(cabVendorId)
            expect(currentCabVendor.meta).to.deep.include({forceV2Migration: false})
        })
    })

    describe("Admin Update Vendor endpoint", function(){ 
        it("Should update data without loosing existing info", async function(){
            bookingId = await createUnAssignedRide()
            await assignRide(bookingId, cabVendorId)
            let currentCabVendor = await helpers.getCabVendorAllData(cabVendorId)
            response = await chai.request(endpoint).post("/admin/peformAction")
                                 .send({
                                    "googleIdToken": "dummy",
                                    "googleUserId": "dummy",
                                    "requestType": "UPDATE_VENDOR",
                                    "payload": {
                                      "cabVendorId": cabVendorId,
                                      "cabVendor": {
                                        "profile": {
                                          "isVerified": true
                                        }
                                      }
                                    }
                                  })
            expect(response).to.have.status(200)
            let updatedVendor = response.body.cabVendor          
            let compareResults = helpers.deepCompare(currentCabVendor, updatedVendor)
            expect(compareResults.missing_from_first).to.have.members(["lastUpdatedTime", "profile.isVerified"])
            expect(compareResults.missing_from_second).to.be.empty
            expect(compareResults.different).to.be.empty


            // Update another field now. 
            currentCabVendor = updatedVendor
            response = await chai.request(endpoint).post("/admin/peformAction")
            .send({
               "googleIdToken": "dummy",
               "googleUserId": "dummy",
               "requestType": "UPDATE_VENDOR",
               "payload": {
                 "cabVendorId": cabVendorId,
                 "cabVendor": {
                   "profile": {
                     "name": "new name",
                     "phoneNo": "1234"
                   }
                 }
               }
             })
            expect(response).to.have.status(200)
            updatedVendor = response.body.cabVendor          
            compareResults = helpers.deepCompare(currentCabVendor, updatedVendor)
            expect(compareResults.missing_from_first).to.be.empty
            expect(compareResults.missing_from_second).to.be.empty
            expect(compareResults.different).to.have.members(["lastUpdatedTime", "profile.name", "profile.phoneNo"])
        })
    })

    describe("Cancel Ride endpoint", function() {
        it("[Pending] Should cancel Ride and issue refund", async () => {
            bookingId = await createUnAssignedRide()
            let cabVendor = await helpers.getCabVendor(cabVendorId)
            let walletAmountBeforeRideAssignment = cabVendor.wallet.total
            await assignRide(bookingId, cabVendorId)

            let response = await chai.request(endpoint).put("/admin/cancelRide").send(helpers.createCancelAssignedRidePayload(bookingId, cabVendorId))
            expect(response).to.have.status(200)

            let ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("CANCELLED_BY_CUSTOMER")
            
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.wallet.total).to.eq(walletAmountBeforeRideAssignment)
            expect(cabVendor.stats.rides.cancelledByCustomer).to.eq(1)
            expect(cabVendor.stats.rides.pending + cabVendor.stats.rides.onGoing).to.eq(0)
            expect(cabVendor.rides.confirmed).to.be.empty
            expect(cabVendor.rides.cancelled).to.include(bookingId)
        })

        it("[OnGoing] Should cancel Ride and issue refund", async () => {
            bookingId = await createUnAssignedRide(new Date().toISOString())
            await assignRide(bookingId, cabVendorId)

            const processor = require('../../gogo-lambda/src/handlers/customer-server')
            await processor.rideStatusUpdaterCron()
            let ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("ONGOING")

            let response = await chai.request(endpoint).put("/admin/cancelRide").send(helpers.createCancelAssignedRidePayload(bookingId, cabVendorId, "ONGOING"))
            expect(response).to.have.status(200)

            ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("CANCELLED_BY_CUSTOMER")
            
            let cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.wallet.total).to.eq(walletAmountInPaise)
            expect(cabVendor.stats.rides.cancelledByCustomer).to.eq(1) // if you skip the first test this will fail
            expect(cabVendor.stats.rides.pending + cabVendor.stats.rides.onGoing).to.eq(0)
            expect(cabVendor.rides.confirmed).to.be.empty
            expect(cabVendor.rides.cancelled).to.include(bookingId)
        })

        it("[Completed] Should cancel Ride and issue refund", async function() {
            this.timeout(60000)
            bookingId = await createUnAssignedRide()
            await assignRide(bookingId, cabVendorId)

            const processor = require('../../gogo-lambda/src/handlers/customer-server')
            await processor.rideStatusUpdaterCron()
            await processor.rideStatusUpdaterCron()
            let ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("COMPLETED")

            let response = await chai.request(endpoint).put("/admin/cancelRide").send(helpers.createCancelAssignedRidePayload(bookingId, cabVendorId, "COMPLETED"))
            expect(response).to.have.status(200)

            ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("CANCELLED_BY_CUSTOMER")
            
            let cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.wallet.total).to.eq(walletAmountInPaise)
            expect(cabVendor.stats.rides.cancelledByCustomer).to.eq(1) // if you skip the first test this will fail
            expect(cabVendor.stats.rides.pending + cabVendor.stats.rides.onGoing + cabVendor.stats.rides.completed).to.eq(0)
            expect(cabVendor.rides.confirmed).to.be.empty
            expect(cabVendor.rides.completed).to.be.empty
            expect(cabVendor.rides.cancelled).to.include(bookingId)
        })

        it("Should throw error when trying to cancel second time", async() => {
            bookingId = await createUnAssignedRide()
            await assignRide(bookingId, cabVendorId)
            const processor = require('../../gogo-lambda/src/handlers/customer-server')
            let ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("ASSIGNED")

            let response = await chai.request(endpoint).put("/admin/cancelRide").send(helpers.createCancelAssignedRidePayload(bookingId, cabVendorId, "ASSIGNED"))

            expect(response).to.have.status(200)

            response = await chai.request(endpoint).put("/admin/cancelRide").send(helpers.createCancelAssignedRidePayload(bookingId, cabVendorId, "CANCELLED_BY_CUSTOMER"))
            expect(response).to.have.status(400)
        })
    })

    describe("Transactions endpoint", function() {
        it("Get Transaction endpoint- happy path", async function( ){
            bookingId = await createUnAssignedRide()
            await assignRide(bookingId, cabVendorId)

            response = await chai.request(endpoint).get("/wallet/transactions?cabVendorId=" + cabVendorId)
            expect(response).to.have.status(200)
            expect(response.body).to.have.length.at.least(1);
            expect(response.body[0].action).to.eq("DEDUCT_MONEY")
            expect(response.body[0].opDetails).include({bookingId: bookingId })

            response = await chai.request(endpoint).put("/admin/cancelRide").send(helpers.createCancelAssignedRidePayload(bookingId, cabVendorId))
            expect(response).to.have.status(200)

            response = await chai.request(endpoint).get("/wallet/transactions?cabVendorId=" + cabVendorId)
            expect(response).to.have.status(200)
            expect(response.body).to.have.length.at.least(2)
            expect(response.body[0].action).to.eq("REFUND_MONEY")
            expect(response.body[1].action).to.eq("DEDUCT_MONEY")
            expect(response.body[0].opDetails).include({bookingId: bookingId })
            expect(response.body[0].opDetails.amount).to.eq(response.body[1].opDetails.amount)
        })
    })

    describe("Telegram commands", function() {
        it("Cancel UnAssigned ride", async () => {
            bookingId = await createUnAssignedRide()
            let ride = await helpers.getRide(bookingId)
            let telegramProcessor = require('../../gogo-lambda/src/handlers/telegram-bot-processor')
            await telegramProcessor.answerCallbackQuery(helpers.createCallbackQueryCancelRide(bookingId))
             ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("CANCELLED_BY_ADMIN")
        })

        it("Should not be able to cancel assigned ride", async () => {
            bookingId = await createUnAssignedRide()
            await assignRide(bookingId, cabVendorId)
            let telegramProcessor = require('../../gogo-lambda/src/handlers/telegram-bot-processor')
            await telegramProcessor.answerCallbackQuery(helpers.createCallbackQueryCancelRide(bookingId))
            ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("ASSIGNED")
        })
    })

    describe("Update Ride", async function() {
        describe("Validation tests", async function() {
            it("Missing bookingId", async function() {
                let response = await chai.request(endpoint).put("/admin/ride").send({
                    googleIdToken: "token",
                    googleUserId: "userId"
                })
                expect(response).to.have.status(400)
                expect(response.body.message[0]).to.contain("bookingId")
            })
        
        })

        describe("Update Customer info", function() {
            before( async () => {
                bookingId = await createUnAssignedRide()
            })
            
            it("Update name", async function() {
                let response = await chai.request(endpoint).put("/admin/ride").send({
                    googleIdToken: "token",
                    googleUserId: "userId",
                    bookingId,
                    dryRun: false,
                    customer: {
                        name: "newName"
                    }
                })
                expect(response).to.have.status(200)
                expect(response.body.ride.customerDetails).to.include({name: "newName", phoneNumber: "919842975789", email: "uvsankar94@gmail.com"})
            })

            it("Update all customer details", async function() {
                let response = await chai.request(endpoint).put("/admin/ride").send({
                    googleIdToken: "token",
                    googleUserId: "userId",
                    bookingId,
                    dryRun: false,
                    customer: {
                        name: "newName2",
                        email: "dummy@dummy.dummy",
                        phoneNumber: "12345"
                    }
                })
                expect(response).to.have.status(200)
                expect(response.body.ride.customerDetails).to.include({name: "newName2", phoneNumber: "12345", email: "dummy@dummy.dummy"})
                let rideFromDb = await helpers.getRide(bookingId)
                expect(rideFromDb.customerDetails).to.include({name: "newName2", phoneNumber: "12345", email: "dummy@dummy.dummy"})
            })
        })

        describe("Update price details", async function() {
            before( async () => {
                bookingId = await createUnAssignedRide()
            })

            it("Update driver bata", async function() {
                let response = await chai.request(endpoint).put("/admin/ride").send({
                    googleIdToken: "token",
                    googleUserId: "userId",
                    bookingId,
                    price: {
                        driverBata: 100,
                        ratePerKm: 11
                    },
                    dryRun: false
                })
                expect(response).to.have.status(200)
                const processor = require('../../gogo-lambda/src/handlers/customer-server')
                let payload = {
                    "Records": [ {
                            "body": `{"version":"v2", "updatedRide": true, "payload": {"bookingId": "${bookingId}"}}`
                        }
                    ]
                }
                await processor.SQSBookingProcessor(payload)
                //TODO: add validation on the rateCard and transactionFee
                //console.log(response.body)
            })

            it("Update driver bata with invalid amount", async function() {
                let response = await chai.request(endpoint).put("/admin/ride").send({
                    googleIdToken: "token",
                    googleUserId: "userId",
                    bookingId,
                    price: {
                        driverBata: -1,
                        ratePerKm: 11
                    },
                    dryRun: false
                })
                expect(response).to.have.status(400)
                //console.log(response.body)
            })

            it("Update ride on cancelled ride status", async function() {

                let telegramProcessor = require('../../gogo-lambda/src/handlers/telegram-bot-processor')
                await telegramProcessor.answerCallbackQuery(helpers.createCallbackQueryCancelRide(bookingId))
                
                let response = await chai.request(endpoint).put("/admin/ride").send({
                    googleIdToken: "token",
                    googleUserId: "userId",
                    bookingId,
                    price: {
                        driverBata: -1,
                        ratePerKm: 11
                    },
                    dryRun: false
                })
                expect(response).to.have.status(400)
            })

        })
    })

    describe("Update wallet", async function() {
        it("Should be able to update wallet", async function() {
            let cabVendor = await helpers.getCabVendor(cabVendorId)
            let walletBeforeUpdate = cabVendor.wallet.total
            let response = await chai.request(endpoint).put("/admin/updateWallet").send({
                googleIdToken: "token",
                googleUserId: "userId",
                cabVendorId,
                amount: 1500,
                action: "ADD",
                comment: "Adding due to"
            })
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.wallet.total).to.eq(walletBeforeUpdate + 1500*100)
            let ledgerData = await helpers.getLedgerData(cabVendorId)
            expect(ledgerData[0]).to.deep.include({
                opDetails: {
                    amount: 150000,
                    comment: "Adding due to",
                    adminEmail: "test@test.com"
                },
                action: "ADD_MONEY",
                cabVendorId
            })
        })

        it("Should be able to deduct money", async function() {
            let cabVendor = await helpers.getCabVendor(cabVendorId)
            let walletBeforeUpdate = cabVendor.wallet.total
            let response = await chai.request(endpoint).put("/admin/updateWallet").send({
                googleIdToken: "token",
                googleUserId: "userId",
                cabVendorId,
                amount: 1500,
                action: "DEDUCT",
                comment: "Deducting due to"
            })
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.wallet.total).to.eq(walletBeforeUpdate - 1500*100)
            let ledgerData = await helpers.getLedgerData(cabVendorId)
            expect(ledgerData[0]).to.deep.include({
                opDetails: {
                    amount: 150000,
                    comment: "Deducting due to",
                    adminEmail: "test@test.com"
                },
                action: "DEDUCT_MONEY",
                cabVendorId
            })
        })

        it("Should reject if amount is <0 or >1500", async function() {
            let response = await chai.request(endpoint).put("/admin/updateWallet").send({
                googleIdToken: "token",
                googleUserId: "userId",
                cabVendorId,
                amount: 1501,
                action: "ADD",
                comment: "Adding due to"
            })
            expect(response.body).to.deep.include({
                msg: "Amount should be >0 <1500 but was 1501"
            })
            expect(response).to.have.status(400)

            await chai.request(endpoint).put("/admin/updateWallet").send({
                googleIdToken: "token",
                googleUserId: "userId",
                cabVendorId,
                amount: 1,
                action: "DEDUCT",
                comment: "Adding due to"
            })
            expect(response.body).to.deep.include({
                msg: "Amount should be >0 <1500 but was 1501"
            })
            expect(response).to.have.status(400)
        })
    })
})

describe("Lambda Admin actions", function() {
    let endpointHandler = require('../../gogo-lambda/src/handlers/customer-server').customerEndpointHandler
    
    it("Approve ride", async function() {
        let bookingId = await createUnApprovedRide(new Date().getTime() + 1000000)
        let ride = await helpers.getRide(bookingId)
        expect(ride.rideStatus).to.eq("UNAPPROVED")
        let event = helpers.createLambdaAdminActionEvent("APPROVE_RIDE", {bookingId})
        let response = await endpointHandler(event)
        ride = await helpers.getRide(bookingId)
        expect(ride.rideStatus).to.eq("UNASSIGNED")
    })
})

async function assignRide(bookingId, cabVendorId){
    response = await chai.request(endpoint)
          .put("/rides")
          .send({
            "bookingId": bookingId,
            "cabVendorId": cabVendorId,
            "vehicleRegNo": "TN28P007",
            "action": "ASSIGN_DRIVER"
          })
      expect(response).to.have.status(200)
}

async function createUnApprovedRide(tripStartTime) {
    let bookingProcessor = require('../../gogo-lambda/src/handlers/customer-server')
    let requestPayload = helpers.createPublishRidePayload(tripStartTime)
    let response = await chai.request(endpoint).post("/rides").send(requestPayload)
    expect(response).to.have.status(200)
    let bookingId = response.body.bookingId
    await bookingProcessor.SQSBookingProcessor(helpers.createSQSRidePublishV2Payload(bookingId))
    return bookingId
}

async function createUnAssignedRide(tripStartTime){
    let bookingProcessor = require('../../gogo-lambda/src/handlers/customer-server')
    let telegramProcessor = require('../../gogo-lambda/src/handlers/telegram-bot-processor')
    let requestPayload = helpers.createPublishRidePayload(tripStartTime)
    let response = await chai.request(endpoint).post("/rides").send(requestPayload)
    expect(response).to.have.status(200)
    let bookingId = response.body.bookingId
    await bookingProcessor.SQSBookingProcessor(helpers.createSQSRidePublishV2Payload(bookingId))
    response = await telegramProcessor.answerCallbackQuery(helpers.createCallbackQueryApproveRide(bookingId))
    expect(response.code).to.eq("Successful")
    return bookingId
}

async function createApprovedCabVendor(cabVendorId, walletAmountInPaise = 10000000){
    let response = await chai.request(endpoint).post("/cab-vendors").send(helpers.createVendorPayload(cabVendorId))
    expect(response).to.have.status(200);    
    response = await chai.request(endpoint).post("/admin/peformAction").send(helpers.createAdminVendorApprovePayload(cabVendorId))
    expect(response).to.have.status(200)  
    await helpers.updateWallet(cabVendorId, walletAmountInPaise)
}