var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http');
chai.use(chaiHttp);

//chai.use(require('chai-things'));

var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'

describe("Document Mgmt test", function () {

    it("Generate CabVendor doc upload url", async function() {
        let response = await chai.request(endpoint).post("/documents/generate-url").send({category: "CABVENDOR", groupId: "cabVendorId1", action: "POST"})
        expect(response).to.have.status(200)
        //console.log(response.body)
        expect(response.body.uploadUrl).to.eq("https://cabvendors.s3.ap-south-2.amazonaws.com/")
        expect(response.body.requiredFields).to.include({
            bucket: 'cabvendors',
            key: 'cabVendorId1/${filename}',
        })
    })
})