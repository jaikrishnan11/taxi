const itemRepairer = require("../../gogo-lambda/src/service/ItemRepairer")
const helper = require("./helper")

var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    moment = require('moment'),
    chaiHttp = require('chai-http'),
    _ = require('lodash');
chai.use(chaiHttp);
const chaiThings = require('chai-things');
chai.use(chaiThings);

var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'

describe.skip("Item repairer tests", () => {
    let cabVendorId = "test-" + Math.random() * 10000

    before(async () => {
        cabVendorId = "test-" + Math.random() * 10000
        await chai.request(endpoint).post("/cab-vendors").send(helpers.createVendorPayload(cabVendorId))
        // Get the ES client and create cab vendor index if it doesn't exist
        await helper.createCabVendorIndex()
    })

    it ("repair meta and external vendor id", async () => {
        let cabVendor = await helper.getCabVendorAllData(cabVendorId);
        delete cabVendor.meta
        delete cabVendor.externalId

        await itemRepairer.repairCabVendor(cabVendor)
        cabVendor = await helper.getCabVendorAllData(cabVendorId);
        expect(cabVendor.meta).to.deep.eq({})
        expect(cabVendor.externalId).to.be.not.null

        await itemRepairer.repairCabVendor(cabVendor)
    })

    it ("repair only external vendor id", async () => {
        let cabVendor = await helper.getCabVendorAllData(cabVendorId);
        delete cabVendor.externalId

        await itemRepairer.repairCabVendor(cabVendor)
        cabVendor = await helper.getCabVendorAllData(cabVendorId);
        expect(cabVendor.meta).to.deep.eq({})
        expect(cabVendor.externalId).to.be.not.null

        await itemRepairer.repairCabVendor(cabVendor)
    })

    it ("repair everything", async () => {
        let cabVendor = await helper.getCabVendorAllData(cabVendorId);
        delete cabVendor.meta
        delete cabVendor.createdDate
        delete cabVendor.externalId

        await itemRepairer.repairCabVendor(cabVendor)
        cabVendor = await helper.getCabVendorAllData(cabVendorId);
        expect(cabVendor.meta).to.deep.eq({})
        expect(cabVendor.externalId).to.be.not.null

        await itemRepairer.repairCabVendor(cabVendor)
    })
})