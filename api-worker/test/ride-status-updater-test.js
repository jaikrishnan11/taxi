
var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http');
chai.use(chaiHttp);
const moment = require('moment')
var helpers = require('./helper')
let rideStatusUpdationService = require('../../gogo-lambda/src/service/rideStatusUpdationService')
const endpoint = 'http://127.0.0.1:8787'

describe("Ride Status updater Cron", function() {
    this.timeout(60000)
    let cabVendorId = "test-" + Math.random() * 10000
    let bookingId = null;
   
    //chai.request(endpoint).post("/cab-vendors").send(helpers.createVendorPayload(cabVendorId))

    describe("UnApproved rides", function (){
        it("Should move it to expired", async function(){
            bookingId = await createUnApprovedride()
            await rideStatusUpdationService.updateUnApprovedRides()
            let ride = await getRideDetails(bookingId)
            expect(ride.rideStatus).to.be.eq("EXPIRED_UNAPPROVED")
        })

        it("Should not move it to expired if created date is in future", async function(){
            bookingId = await createUnApprovedride(moment().toISOString())
            await rideStatusUpdationService.updateUnApprovedRides()
            let ride = await getRideDetails(bookingId)
            expect(ride.rideStatus).to.be.eq("UNAPPROVED")
        })

        it("Should not move it to expired if tripStart is in past but tripend time is in future", async function(){
            bookingId = await createUnApprovedride(moment().add(-1, 'h').toISOString())
            await rideStatusUpdationService.updateUnApprovedRides()
            let ride = await getRideDetails(bookingId)
            expect(ride.rideStatus).to.be.eq("UNAPPROVED")
        })

        it("Should not move it to expired if tripStart, tripend is in past ", async function(){
            bookingId = await createUnApprovedride(moment().add(-8, 'h').toISOString())
            await rideStatusUpdationService.updateUnApprovedRides()
            let ride = await getRideDetails(bookingId)
            expect(ride.rideStatus).to.be.eq("EXPIRED_UNAPPROVED")
        })
    })
})


async function createUnApprovedride(tripStartTime){
    let bookingProcessor = require('../../gogo-lambda/src/handlers/customer-server')
    let requestPayload = helpers.createPublishRidePayload(tripStartTime)
    let response = await chai.request(endpoint).post("/rides").send(requestPayload)
    expect(response).to.have.status(200)
    let bookingId = response.body.bookingId
    await bookingProcessor.SQSBookingProcessor(helpers.createSQSRidePublishV2Payload(bookingId))
    return bookingId
}

async function getRideDetails(bookingId){
    let response = await chai.request(endpoint)
    .post("/admin/peformAction")
    .send({  
      "googleIdToken": "dummy",
      "googleUserId": "dummy",
      "requestType": "GET_RIDES",
      "payload": {
        "bookingIds": [bookingId]
      }})
    expect(response).to.have.status(200)
    expect(response.body.rides).to.not.be.empty
    return  response.body.rides[0]
}