
let {ddbStreamsProcessor} = require('../../gogo-lambda/src/handlers/opensearch-ingestor')

describe.skip("Opensearch Ingestion test", () => {
    describe.skip("Rides Table", () => {
        it("Rides entry old and new", async () => {
            await ddbStreamsProcessor(rideIngest, null)
        })
    })

    describe("CabVendors test", () => {
      it("CabVendors test", async () => {
        await ddbStreamsProcessor(cabVendorIngest, null)
      })

      it("CabVendor v1 ingest", async () => {
        let records = Object.assign({}, cabVendorIngest)
        records.Records[0].dynamodb.NewImage = cabVendorV1
        await ddbStreamsProcessor(records, null)
      })
    })
})

var cabVendorIngest = {Records:[{
  eventID: '2983e8734011661ce904f0152b033851',
  eventName: 'MODIFY',
  eventVersion: '1.1',
  eventSource: 'aws:dynamodb',
  awsRegion: 'ap-south-1',
  dynamodb: {
    "ApproximateCreationDateTime": 1712497862,
    "NewImage": {
      "cabVendorId": {
        "S": "107651755685807598146"
      },
      "createdDate": {
        "S": "2024-03-11T05:40:17.527Z"
      },
      "devices": {
        "M": {
          "cabVendorDevice": {
            "M": {
              "deviceToken": {
                "S": "eaHJj2_HTgG0a0RftFzPdU:APA91bGnzjRHOs77CVcmcdc4YVg3bXI1iHnavG3lJ4iWZZsmhncQgxnUUJmKgjs2iU2iiWmWaYAyqkFeCIowNvJ9NNA1C_DGFK3SVNYtBDJHmsLJrnKVaI8YzP3rAHeBenHyEFHWMfWY"
              }
            }
          }
        }
      },
      "drivers": {
        "M": {
          "TN3120110008382": {
            "M": {
              "approved": {
                "BOOL": true
              },
              "dateOfBirth": {
                "S": "11-02-1988"
              },
              "expiry": {
                "S": "17-10-2026"
              },
              "knownLanguages": {
                "S": "TAMIL, ENGLISH"
              },
              "licenseNumber": {
                "S": "TN3120110008382"
              },
              "name": {
                "S": "NANDHAGOPAL.  K"
              },
              "pendingActions": {
                "L": []
              },
              "phoneNumber": {
                "S": "9944464940"
              }
            }
          }
        }
      },
      "profile": {
        "M": {
          "email": {
            "S": "nandha9944464940@gmail.com"
          },
          "name": {
            "S": "NANDHAGOPAL K"
          },
          "panCardNo": {
            "S": "IAWPK0403D"
          },
          "phoneNo": {
            "N": "9944464940"
          }
        }
      },
      "rides": {
        "M": {}
      },
      "stats": {
        "M": {
          "rides": {
            "M": {
              "cancelledByCustomer": {
                "N": "0"
              },
              "cancelledByVendor": {
                "N": "0"
              },
              "completed": {
                "N": "0"
              },
              "onGoing": {
                "N": "0"
              },
              "pending": {
                "N": "0"
              }
            }
          },
          "totalEarning": {
            "N": "0"
          }
        }
      },
      "vehicles": {
        "M": {
          "TN11V3647": {
            "M": {
              "fuel": {
                "S": "DIESEL"
              },
              "insuranceInfo": {
                "M": {
                  "expiry": {
                    "S": "12-Feb-2025"
                  },
                  "name": {
                    "S": "Royal Sundaram General Insurance Co. Ltd"
                  }
                }
              },
              "lastRefreshedDate": {
                "S": "March 12th 2024, 1:20:05 am"
              },
              "model": {
                "S": "TOYOTA ETIOS GD"
              },
              "pendingActions": {
                "L": []
              },
              "rcInfo": {
                "M": {
                  "expiry": {
                    "S": "29-Jan-2025"
                  }
                }
              },
              "type": {
                "S": "OTHERS"
              },
              "vehicleRegNo": {
                "S": "TN11V3647"
              },
              "vehicleStatus": {
                "S": "APPROVED"
              }
            }
          }
        }
      },
      "vendorStatus": {
        "S": "APPROVED"
      },
      "version": {
        "S": "v2"
      },
      "wallet": {
        "M": {
          "razorpay": {
            "M": {
              "paymentUrl": {
                "S": "upi://pay?ver=01&mode=19&pa=rzppnlnukgogocab@yesbank&pn=Gogocab&tr=RZPYNkw60OnUuGZtUaqrv2&cu=INR&mc=5817&qrMedium=04&tn=PaymenttoGogocab"
              },
              "qrId": {
                "S": "qr_Nkw60OnUuGZtUa"
              },
              "qrImageUrl": {
                "S": "https://rzp.io/i/mJudzjTNXx"
              }
            }
          },
          "total": {
            "N": "20000"
          }
        }
      }
    },
    "SequenceNumber": '1785121000000000011309926494',
    "SizeBytes": 1789,
    "StreamViewType": 'NEW_AND_OLD_IMAGES'
  },
  eventSourceARN: 'arn:aws:dynamodb:ap-south-1:849398159878:table/cab-vendors/stream/2024-04-06T22:42:11.692'
}]
}

var cabVendorV1 = {
  "cabVendorId": {
    "S": "106614648362143524538"
  },
  "createdDate": {
    "S": "2024-01-23T10:19:07.447Z"
  },
  "devices": {
    "M": {
      "cabVendorDevice": {
        "M": {
          "deviceToken": {
            "S": "c4xxg59HT62NUsu_JL9ltW:APA91bEoEv1eaNMKDWqnqJAGwEcYrsVyN4bupym6Q339JfJPUswYB3MVbq3i86OO8xdZKXIIv47hJrckpPtLsW0kc-ydcS1Sj5WStJDJge2AOGXdzMb303gkkAsYmSFP2dPxH3wLWN_l"
          }
        }
      }
    }
  },
  "drivers": {
    "M": {
      "9578072052": {
        "M": {
          "name": {
            "S": "MAHARAJA KING"
          },
          "phoneNo": {
            "N": "9578072052"
          }
        }
      }
    }
  },
  "profile": {
    "M": {
      "email": {
        "S": "maharajadae@gmail.com"
      },
      "name": {
        "S": "MAHARAJA KING"
      },
      "phoneNo": {
        "N": "9578072052"
      }
    }
  },
  "rides": {
    "M": {}
  },
  "stats": {
    "M": {
      "rides": {
        "M": {
          "cancelledByCustomer": {
            "N": "0"
          },
          "cancelledByVendor": {
            "N": "0"
          },
          "completed": {
            "N": "0"
          },
          "onGoing": {
            "N": "0"
          },
          "pending": {
            "N": "0"
          }
        }
      },
      "totalEarning": {
        "N": "0"
      }
    }
  },
  "vehicles": {
    "M": {
      "TN14Q6160": {
        "M": {
          "type": {
            "S": "Sedan"
          },
          "vehicleRegNo": {
            "S": "TN14Q6160"
          },
          "vehicleStatus": {
            "S": "APPROVED"
          }
        }
      }
    }
  },
  "vendorStatus": {
    "S": "APPROVED"
  },
  "version": {
    "S": "v1"
  },
  "wallet": {
    "M": {
      "razorpay": {
        "M": {
          "paymentUrl": {
            "S": "upi://pay?ver=01&mode=19&pa=rzppnlnukgogocab@yesbank&pn=Gogocab&tr=RZPYNkV5XDNcLgwFLdqrv2&cu=INR&mc=5817&qrMedium=04&tn=PaymenttoGogocab"
          },
          "qrId": {
            "S": "qr_NkV5XDNcLgwFLd"
          },
          "qrImageUrl": {
            "S": "https://rzp.io/i/dto4RvAU"
          }
        }
      },
      "total": {
        "N": "0"
      }
    }
  }
}

var rideIngest = {Records:[{
    eventID: '2983e8734011661ce904f0152b033851',
    eventName: 'MODIFY',
    eventVersion: '1.1',
    eventSource: 'aws:dynamodb',
    awsRegion: 'ap-south-1',
    dynamodb: {
      "ApproximateCreationDateTime": 1712497862,
      "NewImage": {
        "bookingId": {
          "S": "7HOJZA"
        },
        "bookingDetails": {
          "M": {
            "cabType": {
              "S": "sedan"
            },
            "drop": {
              "M": {
                "fullAddress": {
                  "S": "Udumalaipettai, Tamil Nadu, India"
                },
                "placeId": {
                  "S": "ChIJQ0o6WVPMqTsR6UP3sYK-JSo"
                },
                "shortName": {
                  "S": "Udumalaipettai"
                }
              }
            },
            "estDistance": {
              "N": "232.08"
            },
            "estRideEndTime": {
              "S": "2023-06-04T12:35:31.000Z"
            },
            "estTripDuration": {
              "N": "250.52"
            },
            "keepCab": {
              "S": "true"
            },
            "pickup": {
              "M": {
                "fullAddress": {
                  "S": "Kovilpatti, Tamil Nadu, India"
                },
                "placeId": {
                  "S": "ChIJKY-ZcGWyBjsRm2Ejq4_aXNk"
                },
                "shortName": {
                  "S": "Kovilpatti"
                }
              }
            },
            "returnStartTime": {
              "S": ""
            },
            "tripStartTime": {
              "S": "2023-06-04T08:25:00.000Z"
            },
            "tripType": {
              "S": "ONE_WAY"
            }
          }
        },
        "createdDate": {
          "S": "2023-06-03T15:46:00.449Z"
        },
        "customerDetails": {
          "M": {
            "email": {
              "S": "anburythm_1982@yahoo.com"
            },
            "name": {
              "S": "Kavitha "
            },
            "phoneNumber": {
              "S": "7358555193"
            }
          }
        },
        "priceDetails": {
          "M": {
            "cabVendor": {
              "M": {
                "transactionFee": {
                  "N": "341.7"
                }
              }
            },
            "rateCard": {
              "M": {
                "bata": {
                  "N": "400"
                },
                "fare": {
                  "N": "3017"
                },
                "fareSplitText": {
                  "M": {
                    "BaseFare": {
                      "M": {
                        "calculation": {
                          "S": "232.08 km X ₹13"
                        },
                        "value": {
                          "S": "₹3017"
                        }
                      }
                    },
                    "DriverBata": {
                      "M": {
                        "value": {
                          "N": "400"
                        }
                      }
                    }
                  }
                },
                "id": {
                  "N": "101"
                },
                "minDailyCharge": {
                  "N": "0"
                },
                "rate": {
                  "N": "13"
                },
                "total": {
                  "N": "3417"
                },
                "type": {
                  "S": "sedan"
                }
              }
            }
          }
        },
        "rideStatus": {
          "S": "CANCELLED_BY_ADMIN"
        },
        "version": {
          "S": "v1"
        }
      },
      "SequenceNumber": '1785121000000000011309926494',
      "SizeBytes": 1789,
      "StreamViewType": 'NEW_AND_OLD_IMAGES'
    },
    eventSourceARN: 'arn:aws:dynamodb:ap-south-1:849398159878:table/rides/stream/2024-04-06T04:01:26.599'
  }]
}