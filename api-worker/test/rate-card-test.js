
var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    moment = require('moment'),
    chaiHttp = require('chai-http'),
    _ = require('lodash');
chai.use(chaiHttp);
const chaiThings = require('chai-things');
chai.use(chaiThings);
let config = require('../src/config/default.json')
var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'

describe("RateCard test", function() {
    this.timeout(5000)
    beforeEach(async () => {
        await chai.request(endpoint).post("/cf")
                             .send({
                                key: "rateCardOverride",
                                data: {}
                             })
    })

    describe("OneWay", async () => {
        it("OneWay without intermediates", async () => {
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU",
                dropPlaceId:"ChIJjzmVJAcvqjsRG599H3r8fJQ"
            })
            expect(response, `Body: ${response.body}`).to.have.status(200)
            expect(response.body.distance).to.greaterThan(70).and.lessThan(80)
            expect(response.body.durationInMinutes).to.greaterThan(60).and.lessThan(90)
            expect(response.body.rateCards.oneway).to.have.lengthOf(4)
            expect(response.body.estTollAmount).to.greaterThan(40).lessThan(100)
            expect(response.body.permitAmount).to.include({
                "sedan": 0,
                "etios": 0,
                "xylo": 0,
                "innova": 0
            })
        })

        it("OneWay different state", async () => {
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4", // TN
                dropPlaceId:"ChIJbU60yXAWrjsR4E9-UejD3_g" // KA
            })
            expect(response, `Body: ${response.body}`).to.have.status(200)
            expect(response.body.distance).to.greaterThan(300).and.lessThan(400)
            expect(response.body.rateCards.oneway).to.have.lengthOf(4)
            expect(response.body.estTollAmount).to.greaterThan(200).lessThan(1000)
            expect(response.body.permitAmount).to.include({
                "sedan": 10,
                "etios": 10,
                "xylo": 100,
                "innova": 100
            })
        })

        it("OneWay different state2", async () => {
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJbU60yXAWrjsR4E9-UejD3_g", // KA
                dropPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4" // TN
            })
            expect(response, `Body: ${response.body}`).to.have.status(200)
            expect(response.body.distance).to.greaterThan(300).and.lessThan(400)
            expect(response.body.rateCards.oneway).to.have.lengthOf(4)
            expect(response.body.estTollAmount).to.greaterThan(200).lessThan(1000)
            expect(response.body.permitAmount).to.include({
                "sedan": 10,
                "etios": 10,
                "xylo": 100,
                "innova": 100
            })
        })

        it("OneWay different state", async () => {
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                intermediates: "ChIJWXnloXZhUzoRul7TO_m8wvk",
                dropPlaceId:"ChIJbU60yXAWrjsR4E9-UejD3_g"
            })
            expect(response, `Body: ${response.body}`).to.have.status(200)
            expect(response.body.permitAmount).to.include({
                "sedan": 70,
                "etios": 70,
                "xylo": 700,
                "innova": 700
            })
            let bookingId = await helpers.createUnAssingedRide(new Date().toISOString(), {
                "bookingDetails": {
                    tripType: "ONE_WAY",
                    pickupPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                    intermediates: "ChIJWXnloXZhUzoRul7TO_m8wvk",
                    dropPlaceId:"ChIJbU60yXAWrjsR4E9-UejD3_g",
                    pickUpDate: new Date().toISOString()
                },
                "customerDetails": {
                  "phoneNumber": "919842975789",
                  "name": "sankar",
                  "email": "uvsankar94@gmail.com"
                },
                "rateCardId": 102
              })

            let ride = await helpers.getRide(bookingId)
            expect(ride.priceDetails.rateCard.permitAmount).to.eq(700)
        })

        it("OneWay without toll", async () => {
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU",
                dropPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU"
            })
            expect(response, `Body: ${response.body}`).to.have.status(200)
            expect(response.body.rateCards.oneway).to.have.lengthOf(4)
            expect(response.body.estTollAmount).to.eq(0)
        })


    })

    describe("RoundTrip", async () => {
        it("Using intermediates", async () => {
            //PickUp and DropSame 
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU",
                intermediates: "ChIJjzmVJAcvqjsRG599H3r8fJQ",
                dropPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU"
            })
            expect(response, `Body: ${response.body}`).to.have.status(200)
            expect(response.body.distance).to.greaterThan(140).and.lessThan(160)
            expect(response.body.durationInMinutes).to.greaterThan(120).and.lessThan(180)
            expect(response.body.rateCards.oneway).to.have.lengthOf(4)
            expect(response.body.estTollAmount).to.greaterThan(80).lessThan(200)
            expect(response.body.permitAmount).to.include({
                "sedan": 0,
                "etios": 0,
                "xylo": 0,
                "innova": 0
            })

            let bookingId = await helpers.createUnAssingedRide(undefined, helpers.createPublishRidePayloadWithIntermediates())
            let ride = await helpers.getRide(bookingId)
            expect(ride.priceDetails.rateCard.estTollAmount).to.be.greaterThanOrEqual(0)
            expect(ride.bookingDetails.intermediates[0]).to.deep.include({
                placeId: "ChIJjzmVJAcvqjsRG599H3r8fJQ",
                shortName: "Karur"
            })
            expect(ride.bookingDetails.intermediates[0].fullAddress).to.be.not.null
            expect(ride.priceDetails.rateCard.permitAmount).to.eq(0)
            expect(Object.keys(ride.bookingDetails.pickup.geo)).to.include.members(["lat", "lon"])
            expect(Object.keys(ride.bookingDetails.drop.geo)).to.include.members(["lat", "lon"])
            expect(ride.bookingDetails.pickup.geo.lat).to.be.a("number")
            expect(ride.bookingDetails.drop.geo.lon).to.be.a("number")

            response = await chai.request(endpoint).get("/rides/unassigned")
            expect(response).to.have.status(200)
        })
    })

    describe("Destination block", async function ()  {
        it ("rate card rejected for a blocked place", async () => {
            let response = await chai.request(endpoint).post("/cf")
                .send({
                    key: "rateCardOverride",
                    data: {
                        "denyService": [
                            {
                                "name": "Blocking rasipuram area",
                                "coord": {
                                    "lat": 11.4615,
                                    "lon": 78.1855
                                },
                                "radius": 50,
                                "reason": "Blocked due to flood",
                                "terms": [
                                    "dst"
                                ]
                            }
                        ]
                    }
                })
            
            expect(response).to.have.status(200)
            response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                pickupPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                dropPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU"
            })
            expect(response).to.have.status(400)
            expect(response.body).to.deep.include({
                "errorCode": "SERVICE_UNAVAILABLE",
                "message": "Blocked due to flood"
            })
        })

        it("rate card rejected for a blocked place", async () => {
            let response = await chai.request(endpoint).post("/cf")
                .send({
                    key: "rateCardOverride",
                    data: {
                        "denyService": [
                            {
                                "name": "Blocking rasipuram area",
                                "coord": {
                                    "lat": 11.4615,
                                    "lon": 78.1855
                                },
                                "radius": 50,
                                "reason": "Blocked due to flood",
                                "terms": [
                                    "dst"
                                ]
                            }
                        ]
                    }
                })
            
            expect(response).to.have.status(200)
            response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                dropPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU"
            })
            expect(response).to.have.status(200)
        })
    })

    describe("RateCard override", async () => {
        it("override rate card for location", async () => {
            let pickUpDate = moment().add(2, "days").toISOString()
            let response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                dropPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU",
                pickUpDate
            })
            expect(response).to.have.status(200)
            let beforeOverrideResponse = response.body
            
            response = await chai.request(endpoint).post("/cf")
                .send({
                    key: "rateCardOverride",
                    data: {
                        rateCardOverride: helpers.createRateOverrideConfig(pickUpDate)
                    }
                })
            expect(response).to.have.status(200)

            response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                dropPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU",
                pickUpDate
            })
            expect(response).to.have.status(200)
            let withOverrideResponse = response.body

            response = await chai.request(endpoint).post("/cf").send({key: "rateCardOverride",data: null})
            expect(response).to.have.status(200)

            response = await chai.request(endpoint).get("/rateCards").query({
                tripType: "ONE_WAY",
                dropPlaceId:"ChIJYTN9T-plUjoRM9RjaAunYW4",
                pickupPlaceId:"ChIJb_tqwA7CqzsRqW37El98uUU",
                pickUpDate
            })
            expect(response).to.have.status(200)
            let afterRemovingOverrideResponse = response.body

            expect(beforeOverrideResponse).to.deep.equal(afterRemovingOverrideResponse)
            expect(beforeOverrideResponse).to.not.deep.equal(withOverrideResponse)
        })
    })
})