
var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    moment = require('moment'),
    chaiHttp = require('chai-http'),
    _ = require('lodash');
chai.use(chaiHttp);
const chaiThings = require('chai-things');
chai.use(chaiThings);
let config = require('../src/config/default.json')
var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'
const generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor');
const { RideStatus } = require('../../gogo-lambda/src/service/constants');

describe("CreateCabVendorV2 Tests", function () {
    let cabVendorId;
    let phoneNo = "1" + helpers.generateRandomDigits(9)
    let bookingId;

    before(async function () {
        // await helpers.setupTable()
    })

    beforeEach(async function () {
        await helpers.setupTable()
        cabVendorId = "test-" + Math.random() * 10000;
        phoneNo = "1" + helpers.generateRandomDigits(9)
    })

    afterEach(async function () {
        // await helpers.deleteCabVendor(cabVendorId)
    })


  describe("Razorpay webhook", async function() {
    this.timeout(60000)
    it("Razorpay order", async function() {
        let cabVendor = await helpers.createCabVendorV2(cabVendorId, phoneNo);
        response = await chai.request(endpoint)
                                .post("/wallet/razorpay-order")
                                .send({  
                                  "cabVendorId": cabVendorId,
                                  "amount": 50000
                                })
        expect(response).to.have.status(200)
        console.log(response.body)
        response = await chai.request(endpoint)
                            .post("/razorpay-hook")
                            .send(helpers.createOrderPaidWebhook(cabVendorId, response.body.orderId, response.body.orderCreatedDate))
        expect(response).to.have.status(200)
    })

  })

    describe("validateCabVendorAndTriggerOTP", function () {
        it("Should reject for invalid phone No", async function () {
            let phoneNo = "123e4"
            let response = await chai.request(endpoint).post("/validate-create-vendor")
                .send({
                    "phoneNo": phoneNo,
                    "cabVendorId": cabVendorId
                });
            expect(response).to.have.status(400)
            expect(response.body.errorCode).to.include("INVALID_PHONE_NUMBER")


            response = await chai.request(endpoint).post("/validate-create-vendor")
                .send({
                    "phoneNo": "1234578aq",
                    "cabVendorId": cabVendorId
                });
            expect(response).to.have.status(400)
            expect(response.body.errorCode).to.include("INVALID_PHONE_NUMBER")
        })

        it("Should validate and trigger otp", async function () {
            let response = await chai.request(endpoint).post("/validate-create-vendor")
                .send({
                    "phoneNo": phoneNo,
                    "cabVendorId": cabVendorId
                });
            expect(response).to.have.status(200)
            expect(response.body.response.message).to.include(phoneNo)
        })
    })

    describe("CreateCabVendor", function () {
        it("Happy path", async function () {
            let response = await chai.request(endpoint).post("/validate-create-vendor")
                .send({
                    "phoneNo": phoneNo + "",
                    "cabVendorId": cabVendorId
                });
            expect(response).to.have.status(200)
            let profile = {
                "name": "testName",
                "phoneNo": phoneNo,
                "email": "test@test.com",
                "panCardNo": "panCard123"
            }
            otpRecord = await helpers.getOTPRecord(phoneNo, "VENDOR_CREATION_PHONE_NO_VERIFICATION")
            response = await chai.request(endpoint).post("/v2/cab-vendors")
                .send({
                    "cabVendorId": cabVendorId,
                    "otpCode": otpRecord.data.generatedOtp + "",
                    "profile": profile
                })
            expect(response).to.have.status(200)
            expect(response.body.cabVendorV2).to.deep.include({
                cabVendorId: cabVendorId,
                vendorStatus: 'APPROVED',
                version: 'v2',
                rides: {},
                drivers: {},
                profile
            })
            expect(response.body.cabVendorV2.wallet).to.deep.eq({ total: 0 })
        })
    })

    describe("SQS processing - Vendor Created", function () {
        this.timeout(100000)
        it("Process SQS", async function () {
            let cabVendor = await helpers.createCabVendorV2(cabVendorId, phoneNo);
            let generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor')
            await generalProcessor.generalProcessor({ Records: [{ body: JSON.stringify({ type: "CAB_VENDOR_CREATED", payload: { cabVendorId, version: "v2" } }) }] })
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(Object.keys(cabVendor.wallet.razorpay)).to.include("paymentUrl", "qrId", "qrImageUrl")
        })
    })

    describe("Add Driver flow", function () {
        this.timeout(10000000)
        it.skip("Happy path - validate and create", async function () {
            /**
             * 1. InSufficient balance
             * 2. Add driver with approved false
             * 3. Check wallet
             * 4. Refresh driver with approved false
             * 5. Check wallet and driver status
             */
            let cabVendor = await helpers.createCabVendorV2(cabVendorId, phoneNo);
            let response = await chai.request(endpoint).post("/validate-create-driver")
                .send({
                    "phoneNo": phoneNo,
                    "cabVendorId": cabVendorId,
                    "licenseNo": "HR5120190002760",
                    "dateOfBirth": "1998-09-20"
                });
            expect(response).to.have.status(400)
            expect(response.body).to.include({
                errorCode: "INSUFFICIENT_BALANCE"
            })
            await helpers.updateWallet(cabVendorId, 50000)

            response = await chai.request(endpoint).post("/validate-create-driver")
                .send({
                    "phoneNo": phoneNo,
                    "cabVendorId": cabVendorId,
                    "licenseNo": "HR5120190002760",
                    "dateOfBirth": "1998-09-20"
                });
            expect(response).to.have.status(200)

            otpRecord = await helpers.getOTPRecord(phoneNo, "DRIVER_ADDITION_PHONE_NO_VERIFICATION")
            expect(otpRecord.data).to.deep.include({
                phoneNumber: phoneNo
            })
            response = await chai.request(endpoint).post("/cab-vendors/add-driver")
                .send({
                    "phoneNumber": phoneNo,
                    "cabVendorId": cabVendorId,
                    "licenseNumber": "HR5120190002760",
                    "dateOfBirth": "1998-09-20",
                    "otpCode": 12345555,
                    "panCardNumber": "EMPPS1123",
                    "languages": "[]"
                })
            expect(response).to.have.status(400)
            expect(response.body).to.include({ errorCode: "OTP_INVALID" })

            response = await chai.request(endpoint).post("/cab-vendors/add-driver")
                .send({
                    "phoneNumber": phoneNo,
                    "cabVendorId": cabVendorId,
                    "licenseNumber": "HR5120190002760",
                    "dateOfBirth": "1998-09-20",
                    "otpCode": otpRecord.data.generatedOtp,
                    "panCardNumber": "EMPPS1123",
                    "languages": "[]"
                })
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.drivers["HR5120190002760"]).to.include({
                panCardNumber: "EMPPS1123",
                phoneNumber: phoneNo,
                approved: false
            })
            expect(cabVendor.wallet).to.include({
                total: 40000
            })
            let ledgerData = await helpers.getLedgerData(cabVendorId)
            expect(ledgerData.length).to.eq(1)
            expect(ledgerData[0]).to.deep.include({
                action: "DEDUCT_MONEY",
                cabVendorId: cabVendorId,
                opDetails: {
                    amount: 10000,
                    reason: `Add / Refresh driver HR5120190002760`
                }
            })
            // Refresh driver with approved true
            response = await chai.request(endpoint).post("/cab-vendors/refresh-driver")
                .send({
                    "cabVendorId": cabVendorId,
                    "licenseNumber": "HR5120190002760"
                })
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.wallet).to.include({
                total: 35000
            })
            expect(cabVendor.drivers["HR5120190002760"]).to.include({
                panCardNumber: "EMPPS1123",
                phoneNumber: phoneNo,
                approved: false
            })
            ledgerData = await helpers.getLedgerData(cabVendorId)
            expect(ledgerData.length).to.eq(2)
        })
    })

    describe("Add Vehicle flow", function () {
        this.timeout(100000)

        it("Happy path", async function () {
            let vehicleNo = "KA13B8962"
            let cabVendor = await helpers.createCabVendorV2(cabVendorId, phoneNo);
            let response = await chai.request(endpoint).post("/cab-vendors/add-vehicle")
                .send({
                    "cabVendorId": cabVendorId,
                    "vehicleNumber": vehicleNo,
                    "chassisNumber": "MAT600"
                });
            expect(response).to.have.status(400)
            expect(response.body).to.include({
                errorCode: "INSUFFICIENT_BALANCE"
            })

            await helpers.updateWallet(cabVendorId, 20000)
            response = await chai.request(endpoint).post("/cab-vendors/add-vehicle")
                .send({
                    "cabVendorId": cabVendorId,
                    "vehicleNumber": vehicleNo,
                    "chassisNumber": "MAT600"
                });
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.vehicles[vehicleNo]).to.not.be.null
            expect(cabVendor.vehicles[vehicleNo]).to.include({
                "vehicleRegNo": "KA13B8962",
                "vehicleStatus": "APPROVED"
            })
            expect(cabVendor.wallet.total).to.be.eq(0)
            let ledgerData = await helpers.getLedgerData(cabVendorId)
            expect(ledgerData.length).to.eq(1)
            expect(ledgerData[0]).to.deep.include({
                action: "DEDUCT_MONEY",
                cabVendorId: cabVendorId,
                opDetails: {
                    amount: 20000,
                    reason: `Add / Refresh vehicle KA13B8962`
                }
            })

            response = await chai.request(endpoint).post("/cab-vendors/refresh-vehicle")
                .send({
                    "cabVendorId": cabVendorId,
                    "vehicleNumber": vehicleNo,
                    "chassisNumber": "MAT600"
                });
            expect(response).to.have.status(400)
            expect(response.body).to.include({
                errorCode: "INSUFFICIENT_BALANCE"
            })

            await helpers.updateWallet(cabVendorId, 10000)

            response = await chai.request(endpoint).post("/cab-vendors/refresh-vehicle")
                .send({
                    "cabVendorId": cabVendorId,
                    "vehicleNumber": vehicleNo,
                    "chassisNumber": "MAT600"
                });
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendor(cabVendorId)
            expect(cabVendor.vehicles[vehicleNo]).to.not.be.null
            expect(cabVendor.vehicles[vehicleNo]).to.include({
                "vehicleRegNo": "KA13B8962",
                "vehicleStatus": "APPROVED"
            })
            expect(cabVendor.wallet.total).to.be.eq(0)
            ledgerData = await helpers.getLedgerData(cabVendorId)
            expect(ledgerData.length).to.eq(2)
        })
    })

    describe("Ride boooking", function () {
        this.timeout(1000000)
        it("Shadowban vendor", async function () {
            let cabVendor = await helpers.createCabVendorV2WithDriverAndVehicles(cabVendorId, phoneNo, {});
            let bookingId = await helpers.createUnAssingedRide(moment().toISOString())
            let response = await chai.request(endpoint)
                .post("/admin/peformAction")
                .send({
                    "googleIdToken": "dummy",
                    "googleUserId": "dummy",
                    "requestType": "UPDATE_VENDOR_META",
                    "payload": {
                        "cabVendorId": cabVendorId,
                        "forceV2Migration": true,
                        "shadowban": {
                            "banExpiry": moment().add(1, "hour").toISOString(),
                            "reason": "test",
                            "rideDelayMinutes": 30
                        }
                    }
                });
            expect(response).to.have.status(200)
            response = await chai.request(endpoint).get("/rides/unassigned?cabVendorId=" + cabVendorId)
            expect(response).to.have.status(200)
            expect(response.body.rides.map(r => r.bookingId)).to.not.include(bookingId)

            let bookingId2 = await helpers.createUnAssingedRide(moment().toISOString())
            await helpers.updateRideData(bookingId2, { createdDate: moment().subtract("32", "minutes").toISOString() })
            response = await chai.request(endpoint).get("/rides/unassigned?cabVendorId=" + cabVendorId)
            expect(response.body.rides.map(r => r.bookingId)).to.include(bookingId2)
            expect(response.body.rides.map(r => r.bookingId)).to.not.include(bookingId)
        })

        it("Shadowban expiry", async () => {
            let cabVendor = await helpers.createCabVendorV2WithDriverAndVehicles(cabVendorId, phoneNo, {});
            let bookingId = await helpers.createUnAssingedRide(moment().toISOString())
            let response = await chai.request(endpoint)
                .post("/admin/peformAction")
                .send({
                    "googleIdToken": "dummy",
                    "googleUserId": "dummy",
                    "requestType": "UPDATE_VENDOR_META",
                    "payload": {
                        "cabVendorId": cabVendorId,
                        "forceV2Migration": true,
                        "shadowban": {
                            "banExpiry": moment().subtract(1, "hour").toISOString(),
                            "reason": "test",
                            "rideDelayMinutes": 30
                        }
                    }
                });
            expect(response).to.have.status(200)
            response = await chai.request(endpoint).get("/rides/unassigned?cabVendorId=" + cabVendorId)
            expect(response).to.have.status(200)
            expect(response.body.rides.map(r => r.bookingId)).to.include(bookingId)
        })

        it.skip("Driver not approved", async function () {
            let cabVendor = await helpers.createCabVendorV2WithDriverAndVehicles(cabVendorId, phoneNo, {});
            let bookingId = await helpers.createUnAssingedRide(moment().toISOString())

            response = await chai.request(endpoint)
                .put("/rides")
                .send({
                    "bookingId": bookingId,
                    "cabVendorId": cabVendorId,
                    "vehicleRegNo": "KA13B8962",
                    "action": "ASSIGN_DRIVER",
                    "driverLicenseNo": "HR5120190002760"
                })
            expect(response).to.have.status(400)
            expect(response.body).to.include({
                errorCode: "DRIVER_NOT_APPROVED"
            })
        })

        it("Insufficient balance", async function () {
            let cabVendor = await helpers.createCabVendorV2WithDriverAndVehicles(cabVendorId, phoneNo, {
                licenseNumber: "TN6320120000028",
                dob: "1992-04-25"
            });
            let bookingId = await helpers.createUnAssingedRide(moment().toISOString())
            response = await chai.request(endpoint)
                .put("/rides")
                .send({
                    "bookingId": bookingId,
                    "cabVendorId": cabVendorId,
                    "vehicleRegNo": "KA13B8962",
                    "action": "ASSIGN_DRIVER",
                    "driverLicenseNo": "TN6320120000028"
                })
            expect(response).to.have.status(400)
            expect(response.body).to.include({
                errorCode: "INSUFFICIENT_BALANCE"
            })
        })

        it("Add notes to ride", async function () {
            let bookingId = await helpers.createUnAssingedRide(moment().toISOString())
            response = await chai.request(endpoint)
                                .put("/notes")
                                .send({
                                    "googleIdToken": "googleToken",
                                    "googleUserId": "id",
                                    "bookingId": bookingId,
                                    "notes": "Test notes"
                                })
            expect(response).to.have.status(200)
            expect(response.body.response).to.include({
                bookingId,
                driverNotes: "Test notes"
            })
            let ride = await helpers.getRide(bookingId)
            expect(ride.driverNotes).to.eq("Test notes")
        })

        it("Assign ride", async function () {
            let cabVendor = await helpers.createCabVendorV2WithDriverAndVehicles(cabVendorId, phoneNo, {
                licenseNumber: "TN6320120000028",
                dob: "1992-04-25"
            });
            let bookingId = await helpers.createUnAssingedRide(moment().toISOString())
            await helpers.updateWallet(cabVendorId, 100000)
            response = await chai.request(endpoint)
                .put("/rides")
                .send({
                    "bookingId": bookingId,
                    "cabVendorId": cabVendorId,
                    "vehicleRegNo": "KA13B8962",
                    "action": "ASSIGN_DRIVER",
                    "driverLicenseNo": "TN6320120000028"
                })
            console.error(response.body)
            expect(response).to.have.status(200)
            let ride = await helpers.getRide(bookingId)
            let generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor')
            response = await generalProcessor.generalProcessor({ Records: [{ body: JSON.stringify({ type: "RIDE_ASSIGNED", payload: { cabVendorId, bookingId } }) }] })
            expect(response).to.eq("OK")
            ride = await helpers.getRide(bookingId);
            expect(ride.rideStatus).to.eq("ASSIGNED")

            response = await chai.request(endpoint)
                .put("/rides")
                .send({
                    "bookingId": bookingId,
                    "cabVendorId": cabVendorId,
                    "vehicleRegNo": "KA13B8962",
                    "action": "ASSIGN_DRIVER",
                    "driverLicenseNo": "TN6320120000028"
                })
            expect(response).to.have.status(400)
            expect(response.body).to.include({
                errorCode: "RIDE_ALREADY_ASSINGED"
            })

            response = await chai.request(endpoint)
                .put("/rides")
                .send({
                    "bookingId": bookingId,
                    "cabVendorId": cabVendorId,
                    "action": "START_RIDE"
                })
            expect(response.body).to.deep.include({ message: "Updated Ride" })
            ride = await helpers.getRide(bookingId)
            expect(ride.rideStatus).to.eq("ONGOING")

            response = await chai.request(endpoint)
            .put("/rides")
            .send({
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "action": "START_RIDE"
            })
        expect(response.body).to.deep.include({ message: "Unexpected ride status ONGOING" })
        expect(response).to.have.status(400)

    })
})

describe("Close ride", function () {
    this.timeout(10000)
    it("Close Ride Admin", async function () {
        bookingId = await createAssignedRide();

        response = await chai.request(endpoint)
            .put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "isAdmin": true,
                "distance": 500,
                "hillCharge": 100
            })
        expect(response.body.errorCode).to.include("INVALID_RIDE_STATUS")

        const processor = require('../../gogo-lambda/src/handlers/customer-server')
        await processor.rideStatusUpdaterCron()
        response = await chai.request(endpoint)
            .put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": "dummyVendorID",
                "isAdmin": true,
                "distance": 500,
                "hillCharge": 100
            })
        expect(response.body.errorCode).to.include("RESOURCE_NOT_FOUND")

        let rideBeforeClose = await helpers.getRide(bookingId)
        let rateCardBeforeClose = rideBeforeClose.priceDetails.rateCard
        expect(rateCardBeforeClose.previousEstimatedFare).to.be.undefined
        let closingDistance = 500
        response = await chai.request(endpoint)
            .put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "isAdmin": true,
                "distance": closingDistance,
                "hillCharge": 100,
                "parkingCharge": 500,
                "waitingCharge": 500
            })
        expect(response).to.have.status(200)
        let rideAfterClose = await helpers.getRide(bookingId)
        let rateCardAfterClose = rideAfterClose.priceDetails.rateCard

        expect(rateCardBeforeClose.fare).to.eq(rateCardAfterClose.previousEstimatedFare)
        expect(rateCardBeforeClose.rate).to.eq(rateCardAfterClose.rate)
        expect(rateCardBeforeClose.bata).to.eq(rateCardAfterClose.bata)

        expect(rateCardAfterClose.fare).to.eq(rateCardBeforeClose.rate * closingDistance)
        expect(rateCardAfterClose.hillCharge).to.eq(100)
        expect(rateCardAfterClose.waitingCharge).to.eq(500)
        expect(rateCardAfterClose.parkingCharge).to.eq(500)
        expect(rateCardAfterClose.tollAmount).to.be.undefined
        expect(rideAfterClose.bookingDetails.actualDistanceCovered).to.eq(closingDistance)
        expect(rateCardAfterClose.total).to.eq(8000)
        expect(rideAfterClose.bookingDetails.rideCloseTime).to.be.not.null
    })

    it("Close ride - CabVendor", async function () {
        bookingId = await createAssignedRide();
        const processor = require('../../gogo-lambda/src/handlers/customer-server')
        expect(await processor.rideStatusUpdaterCron()).to.eq("ok")
        let rideBeforeClose = await helpers.getRide(bookingId)
        let rateCardBeforeClose = rideBeforeClose.priceDetails.rateCard
        response = await chai.request(endpoint).put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": "differentCabVendorId",
                "isDryRun": true,
                "distance": 500,
                "hillCharge": 100,
                "waitingCharge": 500
            })
        expect(response.body).to.include({ errorCode: "RESOURCE_NOT_FOUND" })
        let closingDistance = 500
        response = await chai.request(endpoint).put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "distance": closingDistance,
                "hillCharge": 100,
                "waitingCharge": 500,
                "tollAmount": 100
            })

        expect(response).to.have.status(200)
        let rideAfterClose = await helpers.getRide(bookingId)
        let rateCardAfterClose = rideAfterClose.priceDetails.rateCard

        expect(rateCardBeforeClose.fare).to.eq(rateCardAfterClose.previousEstimatedFare)
        expect(rateCardBeforeClose.rate).to.eq(rateCardAfterClose.rate)
        expect(rateCardBeforeClose.bata).to.eq(rateCardAfterClose.bata)

        expect(rateCardAfterClose.fare).to.eq(rateCardBeforeClose.rate * closingDistance)
        expect(rideAfterClose.bookingDetails.actualDistanceCovered).to.eq(closingDistance)
        expect(rateCardAfterClose.hillCharge).to.eq(100)
        expect(rateCardAfterClose.waitingCharge).to.eq(500)
        expect(rateCardAfterClose.tollAmount).to.eq(100)
        expect(rateCardAfterClose.total).to.eq(7600)
        expect(rideAfterClose.bookingDetails.rideCloseTime).to.be.not.null


        response = await generalProcessor.generalProcessor({ Records: [{ body: JSON.stringify({ type: "RIDE_CLOSED", payload: { bookingId } }) }] });
        expect(response).to.eq("OK")
        let rideAfterLambda = await helpers.getRide(bookingId)
        expect(rideAfterLambda.rideStatus).to.eq(RideStatus.Completed)
        cabVendor = await helpers.getCabVendor(cabVendorId)
        expect(cabVendor.stats.rides).to.deep.include({ completed: 1, onGoing: 0, pending: 0 })
        expect(cabVendor.stats.totalEarning).to.greaterThan(7000).and.lessThan(7600) //Todo: Should be accurate
        expect(cabVendor.rides).to.deep.include({ completed: [bookingId], onGoing: [], pending: [] })
    })

    it("Close Ride Dry run", async function () {
        bookingId = await createAssignedRide();
        const processor = require('../../gogo-lambda/src/handlers/customer-server')
        expect(await processor.rideStatusUpdaterCron()).to.eq("ok")
        let rideBeforeClose = await helpers.getRide(bookingId)
        let closingDistance = 500
        response = await chai.request(endpoint).put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "isAdmin": true,
                "isDryRun": true,
                "distance": closingDistance,
                "hillCharge": 100,
                "waitingCharge": 500
            })
        expect(response, `Got ${response.status} : ${JSON.stringify(response.body)}`).to.have.status(200)
        let rideAfterClose = await helpers.getRide(bookingId)
        expect(rideAfterClose.bookingDetails.rideCloseTime).to.be.undefined
        expect(rideAfterClose).to.deep.eq(rideBeforeClose)


        response = await chai.request(endpoint).put("/rides/close")
            .send({
                "googleIdToken": "idToken",
                "googleUserId": "userID",
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "isAdmin": false,
                "isDryRun": true,
                "distance": closingDistance,
                "hillCharge": 100,
                "waitingCharge": 500
            })
        expect(response, `Got ${response.status} : ${JSON.stringify(response.body)}`).to.have.status(200)
        rideAfterClose = await helpers.getRide(bookingId)
        expect(rideAfterClose.bookingDetails.rideCloseTime).to.be.undefined
        expect(rideAfterClose).to.deep.eq(rideBeforeClose)
    })

    it("Should create consecutive booking ids", async () => {
        const promises = Array.from({ length: 50 }, (_, index) => helpers.createUnAssingedRide());
        const resolved = await Promise.all(promises);
        expect(new Set(resolved).size).to.eq(50)
    })
})

async function createAssignedRide() {
    let cabVendor = await helpers.createCabVendorV2WithDriverAndVehicles(cabVendorId, phoneNo, {
        licenseNumber: "TN6320120000028",
        dob: "1992-04-25"
    });
    bookingId = await helpers.createUnAssingedRide(moment().add(-1, "month").toISOString());
    await helpers.updateWallet(cabVendorId, 100000);
    response = await chai.request(endpoint)
        .put("/rides")
        .send({
            "bookingId": bookingId,
            "cabVendorId": cabVendorId,
            "vehicleRegNo": "KA13B8962",
            "action": "ASSIGN_DRIVER",
            "driverLicenseNo": "TN6320120000028"
        });
    expect(response).to.have.status(200);
    let generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor');
    response = await generalProcessor.generalProcessor({ Records: [{ body: JSON.stringify({ type: "RIDE_ASSIGNED", payload: { cabVendorId, bookingId } }) }] });
    expect(response).to.eq("OK");
    return bookingId;
}
})