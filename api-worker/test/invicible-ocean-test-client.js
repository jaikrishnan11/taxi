import License5375 from '../../gogo-lambda/scripts/data/licenseResponse.json' assert { type: "json" }
import License2760 from '../../gogo-lambda/scripts/data/licenseResponse_new.json' assert {type: "json"}
import KA13B8962 from '../../gogo-lambda/scripts/data/vehicle_data_indica.json' assert { type: "json" }
import License0028 from '../../gogo-lambda/scripts/data/licenseResponse_approved.json' assert {type: "json"}
class Client {
    constructor() {
        this.baseUrl = "https://api.invincibleocean.com/invincible";
        this.vehicleUrl = `${this.baseUrl}/vehicleRcV6`;
        this.stolenVehicleUrl = `${this.baseUrl}/vehicleStolen`;
        this.licenceUrl = "https://api.emptra.com/drivingLicence";
    }

    async getVehicleInfoV6(vehicleNumber) {
        if(vehicleNumber == "KA13B8962") {
            return KA13B8962
        }
    }

    async stolenVehicleDetails(vehicleNumber) {
        const parsedVehicleNumber = utils.parseVehicleNumber(vehicleNumber);
        if (!utils.validateVehicleNumber(parsedVehicleNumber)) {
            throw new Error("Vehicle number validation failed");
        }
        return await this.makePostRequest(this.stolenVehicleUrl, {
            "vehicleNumber": parsedVehicleNumber
        }); 
    }

    async getLicenseFromApiClub(licenseNumber, dateOfBirth) {
        return this.getDrivingLicense(licenseNumber, dateOfBirth)
    }

    async getVehicleInfoFromApiClub(vehicleNumber) {
        return this.getVehicleInfoV6(vehicleNumber)
    }

    async getDrivingLicense(licenseNumber, dateOfBirth) {
        if (licenseNumber == "TN16-20150005374" && dateOfBirth == "11/08/1995") {
            return License5375;
        } else if (licenseNumber == "HR5120190002760" && dateOfBirth == "1998-09-20") {
            return License2760;
        } else if (licenseNumber == "TN6320120000028" && dateOfBirth == "1992-04-25") {
            return License0028
        } else {
            return {
                code: 404,
                result: {
                    message: "DL Number not found",
                    name: "Error",
                    status: 404
                }
            }
        }
    }

    async getVehicleDetailsFromAPIseva(){
        
    }
}

let testClient = new Client();

export { testClient as default }