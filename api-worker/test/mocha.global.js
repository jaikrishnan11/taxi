// mocha.global.js
const originalConsole = {
  log: console.log,
  debug: console.debug,
  warn: console.warn,
  error: console.error
};

let capturedLogs = [];

beforeEach(function () {
  // Reset captured logs for each test
  capturedLogs = [];

  // Override console methods to capture logs
  console.log = (...args) => capturedLogs.push({ type: 'log', message: args.join(' ') });
  console.debug = (...args) => capturedLogs.push({ type: 'debug', message: args.join(' ') });
  console.warn = (...args) => capturedLogs.push({ type: 'warn', message: args.join(' ') });
  console.error = (...args) => capturedLogs.push({ type: 'error', message: args.join(' ') });
});

afterEach(function () {
  // Restore original console methods
  console.log = originalConsole.log;
  console.debug = originalConsole.debug;
  console.warn = originalConsole.warn;
  console.error = originalConsole.error;

  // Print captured logs if the test failed
  if (this.currentTest && this.currentTest.state === 'failed') {
    console.log(`\n--- Logs for Failed Test: "${this.currentTest.fullTitle()}" ---`);

    capturedLogs.forEach(({ type, message }) => {
      originalConsole[type](`[${type.toUpperCase()}] ${message}`);
    });

    console.log('--- End of Logs ---\n');
  }
});
