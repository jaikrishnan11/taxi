aws2 dynamodb create-table \
  --table-name cab-vendors \
  --attribute-definitions AttributeName=cabVendorId,AttributeType=S \
  --key-schema AttributeName=cabVendorId,KeyType=HASH \
  --provisioned-throughput ReadCapacityUnits=3,WriteCapacityUnits=3 \
  --endpoint-url http://localhost:8000

aws2 dynamodb create-table \
  --table-name generic-table \
  --attribute-definitions AttributeName=namespace,AttributeType=S AttributeName=primaryKey,AttributeType=S \
  --key-schema AttributeName=namespace,KeyType=HASH AttributeName=primaryKey,KeyType=RANGE \
  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
  --endpoint-url http://localhost:8000

aws2 dynamodb create-table \
  --table-name rides \
  --attribute-definitions AttributeName=bookingId,AttributeType=S AttributeName=createdDate,AttributeType=S AttributeName=rideStatus,AttributeType=S \
  --key-schema AttributeName=bookingId,KeyType=HASH \
  --provisioned-throughput ReadCapacityUnits=4,WriteCapacityUnits=4 \
  --global-secondary-indexes IndexName=booking-status-index,KeySchema=["{AttributeName=rideStatus,KeyType=HASH}","{AttributeName=createdDate,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=4}" \
  --endpoint-url http://localhost:8000  

aws2 dynamodb create-table \
  --table-name wallet-book-keeping \
  --attribute-definitions AttributeName=cabVendorId,AttributeType=S AttributeName=createdDate,AttributeType=S \
  --key-schema AttributeName=cabVendorId,KeyType=HASH AttributeName=createdDate,KeyType=RANGE \
  --provisioned-throughput ReadCapacityUnits=2,WriteCapacityUnits=2 \
  --endpoint-url http://localhost:8000  