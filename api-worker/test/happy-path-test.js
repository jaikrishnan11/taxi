
var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http');
chai.use(chaiHttp);
//chai.use(require('chai-things'));

var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'

describe("Happy path flow tests", function(){
  let cabVendorId = "test-" + Math.random() * 10000
  let bookingId = null;
  
  before(async () => {
    await helpers.setupTable()
  })

  describe("Vendor creation", function(){
      it("POST cab-vendors", async () => {
          let cabVendorId2 = "test-" + Math.random()* 10000
          let response = await chai.request(endpoint)
              .post("/cab-vendors")
              .send(helpers.createVendorPayload(cabVendorId2))
          expect(response).to.have.status(200);       
          let cabVendor = response.body.cabVendor;
         // expect(cabVendor.vendorStatus).to.be.eq("PAYMENT_PENDING")
          expect(cabVendor.vendorStatus).to.be.eq("UNAPPROVED")
          expect(cabVendor.vehicles.TN28P007.vehicleStatus).to.be.eq("APPROVED")
          expect(cabVendor.createdDate).to.eq(new Date(cabVendor.createdDate).toISOString())
          expect(cabVendor.drivers).to.deep.eq({
            "1234567890": {name: "Tester", phoneNo: 1234567890}
          })
          expect(cabVendor).to.deep.include({
              wallet: {total: 0}
          })
      })

      it("POST cab-vendors with driver payload", async () => {
        let payload = helpers.createVendorPayloadWithDriver(cabVendorId)
        let response = await chai.request(endpoint)
            .post("/cab-vendors")
            .send(payload)
        expect(response).to.have.status(200);       
        let cabVendor = response.body.cabVendor;
       // expect(cabVendor.vendorStatus).to.be.eq("PAYMENT_PENDING")
        expect(cabVendor.vendorStatus).to.be.eq("UNAPPROVED")
        expect(cabVendor.vehicles.TN28P007.vehicleStatus).to.be.eq("APPROVED")
        expect(cabVendor.createdDate).to.eq(new Date(cabVendor.createdDate).toISOString())
        expect(cabVendor.drivers).to.deep.eq({
          "9876543210": {name: "driver1", phoneNo: 9876543210}
        })
        expect(cabVendor).to.deep.include({
            wallet: {total: 0}
        })
    })

      it("POST cab-vendors second time returns 400", async () => {
        let response = await chai.request(endpoint)
            .post("/cab-vendors")
            .send(helpers.createVendorPayload(cabVendorId))
        expect(response).to.have.status(400);       
        expect(response.body.errorCode).to.eq("RESOURCE_ALREADY_EXISTS")
    })
  })

  describe("Vendor approval", function() {
    it("Should return payment pending status", async () => {
      let response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
      expect(response).to.have.status(200)
      // expect(response.body.vendorStatus).to.eq("PAYMENT_PENDING")
      expect(response.body.vendorStatus).to.eq("UNAPPROVED")
      expect(response.body.vehicles.TN28P007.vehicleStatus).to.be.eq("APPROVED")
    })

    it.skip("Test unapproved flow", async function() {
      this.timeout(60000)
     let response = await chai.request(endpoint)
      .post("/admin/peformAction")
      .send({  
        "googleIdToken": "dummy",
        "googleUserId": "dummy",
        "requestType": "UPDATE_VENDOR_STATE",
        "payload": {
        "cabVendorId": cabVendorId,
        "vendorStatus": "UNAPPROVED",
        "vehicleStatus": {
          "TN28P007": "APPROVED"
        }
        }})
      expect(response).to.have.status(200)
      
      let generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor')
      await generalProcessor.generalProcessor({Records: [{body: JSON.stringify({type: "VENDOR_APPROVAL_PENDING", payload: {cabVendorId}})}]})
      
      let vendorMgmtService = require('../../gogo-lambda/src/service/vendorMgmtService')
      let cabVendor = await vendorMgmtService.approveVendor(cabVendorId)
      expect(cabVendor.vendorStatus).to.eq('APPROVED')
      response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
      expect(response).to.have.status(200)
      expect(response.body.vendorStatus).to.eq("APPROVED")
    })

    it("ADMIN block Vehicle status", async () => {
      let response = await chai.request(endpoint)
          .post("/admin/peformAction")
          .send({  
            "googleIdToken": "dummy",
            "googleUserId": "dummy",
            "requestType": "UPDATE_VENDOR_STATE",
            "payload": {
            "cabVendorId": cabVendorId,
            "vehicleStatus": {
              "TN28P007": "BLOCKED"
            }
            }})
      expect(response).to.have.status(200)
      expect(response.body.vehicles.TN28P007.vehicleStatus).to.be.eq("BLOCKED")

      response = await chai.request(endpoint)
      .post("/admin/peformAction")
      .send({  
        "googleIdToken": "dummy",
        "googleUserId": "dummy",
        "requestType": "UPDATE_VENDOR_STATE",
        "payload": {
        "cabVendorId": cabVendorId,
        "vendorStatus": "UNAPPROVED",
        "vehicleStatus": {
          "TN28P007": "APPROVED"
        }
        }})
      expect(response).to.have.status(200)
      expect(response.body.vehicles.TN28P007.vehicleStatus).to.be.eq("APPROVED")
    })

    it("GET cab-vendor should fail when not approved", async () => {
        let response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
        expect(response).to.have.status(200)
        expect(response.body.vendorStatus).to.eq("UNAPPROVED")
    })

    it("ADMIN update cab vendor status", async () => {
        let response = await chai.request(endpoint)
                                  .post("/admin/peformAction")
                                  .send({  
                                    "googleIdToken": "dummy",
                                    "googleUserId": "dummy",
                                    "requestType": "UPDATE_VENDOR_STATE",
                                    "payload": {
                                    "cabVendorId": cabVendorId,
                                    "vendorStatus": "APPROVED",
                                    "vehicleStatus": { "TN28P007": "APPROVED" }
                                    }})
        expect(response).to.have.status(200)
        expect(response.body).to.include({vendorStatus: "APPROVED"})
    })

    it("GET cab-vendor should data after approval", async () => {
        let response = await chai.request(endpoint)
                                  .get("/cab-vendors?cabVendorId=" + cabVendorId)
        expect(response).to.have.status(200)
        expect(response.body.vendorStatus).to.eq("APPROVED")
    })
  })

 // This test actually creates a QR code in Razorpay test mode so skipping. Remove the skip when touching this flow
  describe.skip("Razorpay paymenturl", function() {
    let vendorMgmtService = require('../../gogo-lambda/src/service/vendorMgmtService')
    
    it("Generate payment url", async function() {
      this.timeout(60000)
      await vendorMgmtService.createPaymentUrl(cabVendorId)
      let response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
      expect(response).to.have.status(200)
      const razorpay = response.body.wallet.razorpay
      expect(razorpay.paymentUrl).to.be.not.empty
      expect(razorpay.qrId).to.be.not.empty
      expect(razorpay.qrImageUrl).to.be.not.empty
    })

    it("Should throw err if paymentUrl is already present", async function() {
      await vendorMgmtService.createPaymentUrl(cabVendorId).then(()=>{
        expect.fail("Was expecting an expection")
      }, (ex) => {
        expect(ex).to.include("is already added")
      })
    })
  })

  describe("Ride creation", function() {
    it("POST rides", async () => {
      let requestPayload = helpers.createPublishRidePayload()
      let response = await chai.request(endpoint)
                               .post("/rides")
                               .send(requestPayload)
      expect(response).to.have.status(200)
      expect(response.body.bookingId).to.exist
      bookingId = response.body.bookingId
    })

    it("ADMIN GET rides by bookingId", async () => {
      let response = await chai.request(endpoint)
                              .post("/admin/peformAction")
                              .send({  
                                "googleIdToken": "dummy",
                                "googleUserId": "dummy",
                                "requestType": "GET_RIDES",
                                "payload": {
                                  "bookingIds": [bookingId]
                                }})
      expect(response).to.have.status(200)
      expect(response.body.rides).to.not.be.empty
      let ride = response.body.rides[0]
      expect(ride).to.deep.include({rideStatus: "UNPUBLISHED", bookingId, customerDetails: helpers.createPublishRidePayload().customerDetails })
      expect(ride.priceDetails.rateCard).to.include({
        type: 'sedan',
        id: 101
      })
      expect(Object.keys(ride.priceDetails.rateCard)).to.deep.include.members(["fare", "bata", "total", "rate"])
      //RateCard total is 4764
      expect(ride.priceDetails.rateCard.total).to.be.a('number').above(0)
    })

    it("ADMIN GET rides by status unpublished", async () => {
      let response = await chai.request(endpoint)
                               .post("/admin/peformAction")
                               .send({  
                                 "googleIdToken": "dummy",
                                  "googleUserId": "dummy",
                                  "requestType": "GET_RIDES",
                                  "payload": {
                                    "rideStatus": "UNPUBLISHED"
                                }})
        expect(response).to.have.status(200)
        expect(response.body.rides).to.not.be.empty
        expect(response.body.rides.map(ride => ride.bookingId)).to.include(bookingId)                       
    })
  })

  describe("Ride publishing", function() {
    let bookingProcessor = require('../../gogo-lambda/src/handlers/customer-server')
    let telegramProcessor = require('../../gogo-lambda/src/handlers/telegram-bot-processor')

    it("Update rideStatus", async () => {
      this.timeout(15000);
     
      await bookingProcessor.SQSBookingProcessor(helpers.createSQSRidePublishV2Payload(bookingId))
      let response = await chai.request(endpoint)
                        .post("/admin/peformAction")
                        .send({  
                          "googleIdToken": "dummy",
                          "googleUserId": "dummy",
                          "requestType": "GET_RIDES",
                          "payload": {
                            "bookingIds": [bookingId]
                          }})
        expect(response).to.have.status(200)
        expect(response.body.rides).to.not.be.empty
        let ride = response.body.rides[0]
        expect(ride.rideStatus).to.eq("UNAPPROVED")
    })

    it("Update rideStatus lambda should not throw error for second time", async () => {
      await bookingProcessor.SQSBookingProcessor(helpers.createSQSRidePublishV2Payload(bookingId))
      //No error thrown by lambda
    })

    it("GET unassigned rides Dont return unapproved ride", async () => {
      let response = await chai.request(endpoint).get("/rides/unassigned")
      expect(response).to.have.status(200)
      let rides = response.body.rides
      expect(rides.map(ride => ride.bookingId)).not.to.include(bookingId)
    })

    it("Admin approves the ride via telegram", async () => {
      let response = await telegramProcessor.answerCallbackQuery(helpers.createCallbackQueryApproveRide(bookingId))
      expect(response.code).to.eq("Successful")
      expect(response.inlinekeyboard).length(1)
      expect(response.inlinekeyboard[0].callback_data).to.include(bookingId)
    })

    it("GET unassigned rides", async () => {
      let response = await chai.request(endpoint).get("/rides/unassigned")
      expect(response).to.have.status(200)
      let rides = response.body.rides
      expect(rides.map(ride => ride.bookingId)).to.include(bookingId)
      for(let ride of rides){
        expect(ride).to.not.have.property("customerDetails")
        expect(ride.bookingDetails.googleMapsDistanceUrl).to.eq("https://www.google.com/maps/dir/?api=1&origin_place_id=ChIJhyDl6v3pqzsRVMTnae1mTw4&destination_place_id=ChIJl_cgDZ9nUjoRGSrgZgzx-Vk&origin=Marappan%20Thottam,%20Kamaraj%20Nagar,%20Rasipuram,%20Tamil%20Nadu%20637408,%20India&destination=12,%20Sardar%20Patel%20Rd,%20Anna%20University,%20Guindy,%20Chennai,%20Tamil%20Nadu%20600025,%20India")
      }
    })

    it("ADMIN GET ride should include transaction fee details", async ()=> {
      let response = await chai.request(endpoint)
          .post("/admin/peformAction")
          .send({  
            "googleIdToken": "dummy",
            "googleUserId": "dummy",
            "requestType": "GET_RIDES",
            "payload": {
              "bookingIds": [bookingId]
            }})
      expect(response).to.have.status(200)
      let ride = response.body.rides[0]
      expect(ride.priceDetails.cabVendor.transactionFee).to.be.closeTo(476.5, 10)
      expect(ride.bookingDetails.drop.shortName).to.exist
    })

  })

  describe("Ride assigning to driver", function() {
    it("Should get insufficient balance error when assigning ride", async ()=>{
      let response = await chai.request(endpoint)
            .put("/rides")
            .send({
              "bookingId": bookingId,
              "cabVendorId": cabVendorId,
              "vehicleRegNo": "TN28P007",
              "action": "ASSIGN_DRIVER"
            })
      expect(response).to.have.status(400)
      expect(response.body.errorCode).to.eq("INSUFFICIENT_BALANCE")
    })

    it("Should assign ride after sufficient balance", async () => {
      await helpers.updateWallet(cabVendorId, 50000) //InPaise
      let response = await chai.request(endpoint)
          .put("/rides")
          .send({
            "bookingId": bookingId,
            "cabVendorId": cabVendorId,
            "vehicleRegNo": "TN28P007",
            "action": "ASSIGN_DRIVER"
          })
      expect(response).to.have.status(200)
      expect(response.body.ride).to.include({ bookingId })
      expect(Object.keys(response.body.ride.customerDetails)).to.deep.include.members(["name", "email", "phoneNumber"])
      expect(response.body.ride.customerDetails.phoneNumber).eq("919842975789")
    })

    it("Should have deducted balance from wallet", async ()=> {
      let response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
      expect(response).to.have.status(200)
      expect(response.body.rides.confirmed).to.include(bookingId)
      expect(response.body.stats.rides.pending).to.eq(1)
      expect(response.body.wallet.total).to.be.below(2510).and.to.be.above(2000)
    })

    it("Process Ride Assignment SQS message", async ()=> {
      let generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor')
      let response = await generalProcessor.generalProcessor({Records: [{body: JSON.stringify({type: "RIDE_ASSIGNED", payload: {cabVendorId, bookingId}})}]})
      expect(response).to.eq("OK")
    })
  })

  describe("Ride status update cron", async ()=> {
    it("Should update the ride status to Completed", async function(){
      this.timeout(60000)
      const processor = require('../../gogo-lambda/src/handlers/customer-server')
      await processor.rideStatusUpdaterCron()
      await processor.rideStatusUpdaterCron() // Run two times to ensure the ride to moved to completed

      let response = await chai.request(endpoint).get(`/rides?bookingIds=${bookingId}&cabVendorId=${cabVendorId}`)
      expect(response).to.have.status(200)
      let ride = response.body.rides[0]
      expect(ride.rideStatus).to.eq("COMPLETED")
      expect(ride.bookingId).to.eq(bookingId)
      expect(ride.bookingDetails.googleMapsDistanceUrl).to.eq("https://www.google.com/maps/dir/?api=1&origin_place_id=ChIJhyDl6v3pqzsRVMTnae1mTw4&destination_place_id=ChIJl_cgDZ9nUjoRGSrgZgzx-Vk&origin=Marappan%20Thottam,%20Kamaraj%20Nagar,%20Rasipuram,%20Tamil%20Nadu%20637408,%20India&destination=12,%20Sardar%20Patel%20Rd,%20Anna%20University,%20Guindy,%20Chennai,%20Tamil%20Nadu%20600025,%20India")
      
      response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
      expect(response).to.have.status(200)
      expect(response.body.stats.rides).to.include({
        cancelledByVendor: 0,
        completed: 1,
        cancelledByCustomer: 0,
        onGoing: 0,
        pending: 0
      })
      expect(response.body.stats.totalEarning).to.be.closeTo(4788.5, 50)
      expect(response.body.rides.completed).to.include(bookingId)
      expect(response.body.rides.onGoing || []).to.not.include(bookingId)
      expect(response.body.rides.confirmed || []).to.not.include(bookingId)
    })

  })
})


/* Things not tested 

1. Pushing to SQS
2. Price , rateCard calculation
3. Asserting the timestamps are correct. RideStartTime, estcompletionTime
4. Payment flow
5. amount paise and ruppe conversions
6. Amount - earnings gets added after 2nd rides
7. ride to OnGoing 
8. Vendor with multiple rides in different states
*/