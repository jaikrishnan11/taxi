var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http');
chai.use(chaiHttp);


const endpoint = 'http://127.0.0.1:8787'
var helpers = require('./helper')

describe("Device config update", function(){
    let cabVendorId = "test-" + Math.random() * 10000
    
    it("Should return 400 if cabVendor is not created yet", async function() {
        let response = await chai.request(endpoint)
                                 .put("/cab-vendors/config")
                                 .send({
                                    "deviceToken": "deviceToken1",
                                    "cabVendorId": cabVendorId,
                                    "googleIdToken": "googleIdToken"
                                 })
        expect(response).to.have.status(400)
        expect(response.body.errorCode).to.contain("RESOURCE_NOT_FOUND")
    })

    it("Should update device config", async function() {
        let response = await chai.request(endpoint).post("/cab-vendors").send(helpers.createVendorPayload(cabVendorId))
        expect(response).to.have.status(200);
        
        response = await chai.request(endpoint)
                                 .put("/cab-vendors/config")
                                 .send({
                                    "deviceToken": "deviceToken1",
                                    "cabVendorId": cabVendorId,
                                    "googleIdToken": "googleIdToken"
                                 })
        expect(response).to.have.status(200)

        response = await chai.request(endpoint)
                            .post("/admin/peformAction")
                            .send({  
                                "googleIdToken": "dummy",
                                "googleUserId": "dummy",
                                "requestType": "GET_VENDORS",
                                "payload": {
                                    "cabVendors": [cabVendorId]
                            }});
        expect(response).to.have.status(200)
        expect(response.body[0].devices.cabVendorDevice.deviceToken).to.eq("deviceToken1")
    })
})