
var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    moment = require('moment'),
    chaiHttp = require('chai-http'),
    _ = require('lodash');
chai.use(chaiHttp);
const chaiThings = require('chai-things');
chai.use(chaiThings);

var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'


describe("CreateCabVendorV2 Tests", function() {
    let cabVendorId;
    let phoneNo = "1" + helpers.generateRandomDigits(9)
    let bookingId;

    before(async function() {
       // await helpers.setupTable()
    })

    beforeEach(async function() {
       await helpers.setupTable()
       cabVendorId = "test-" + Math.random() * 10000;
       phoneNo = "1" + helpers.generateRandomDigits(9)
    })

    afterEach(async function() {
        // await helpers.deleteCabVendor(cabVendorId)
    })

    describe("Location update endpoint", function() {
        it("Should not update location - validations", async function() {
            let cabVendor = await helpers.createCabVendorV2(cabVendorId, phoneNo, {});
            let response = await chai.request(endpoint)
            .post("/cabvendor/location")
            .send({
                "cabVendorId": cabVendorId,
                "locationTime": moment().toISOString(),
                "coordinates": [-123.43, 45.234, 234.2]
            })
            expect(response).to.have.status(400)


            response = await chai.request(endpoint)
                .post("/cabvendor/location")
                .send({
                    "cabVendorId": cabVendorId,
                    "coordinates": [-123.43, 45.234]
                })
            expect(response).to.have.status(400)
        })

        it.skip("Should update location", async function() {
            let cabVendor = await helpers.createCabVendorV2(cabVendorId, phoneNo, {});
            let response = await chai.request(endpoint)
            .post("/cabvendor/location")
            .send({
                "cabVendorId": cabVendorId,
                "locationTime": moment().toISOString(),
                "coordinates": [-123.43, 45.234]
            })
            expect(response).to.have.status(200)
            cabVendor = await helpers.getCabVendorAllData(cabVendorId)
            expect(cabVendor.location.cabVendor.geoLocation).to.deep.include({
                lat: -123.43,
                lon: 45.234
            })


            response = await chai.request(endpoint)
                .post("/cabvendor/location")
                .send({
                    "cabVendorId": cabVendorId,
                    "locationTime": moment().subtract(1, "M").toISOString(),
                    "coordinates": [3, 5]
                })
            expect(response).to.have.status(400)
            expect(response.body).to.include({errorCode: "LOCATION_INFO_STALE"})
        })

    })

 
})