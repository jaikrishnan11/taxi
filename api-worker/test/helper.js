const { DynamoDBClient, DeleteItemCommand, DeleteTableCommand, CreateTableCommand, ListTablesCommand } = require("@aws-sdk/client-dynamodb");
const {DynamoDBDocumentClient, UpdateCommand, GetCommand, DeleteCommand, QueryCommand, PutCommand} = require("@aws-sdk/lib-dynamodb")
const _ = require('lodash')
let config = require('../src/config/default.json')
config = config.dynamodb

let client = new DynamoDBClient({ 
  region: config.region, 
  endpoint: config.endpoint,
  credentials: config.credentials
})

let ddbClient = DynamoDBDocumentClient.from(client, {
  "marshallOptions": {
      "convertEmptyValues": false,
      "removeUndefinedValues": true
  }
});

var chai = require('chai'),
    expect = chai.expect,
    esClient = require('../../gogo-lambda/src/clients/elastic-search-client')
    chaiHttp = require('chai-http');
chai.use(chaiHttp);
//chai.use(require('chai-things'));


const endpoint = 'http://127.0.0.1:8787'

module.exports = {
    ddbClient,
    createCabVendorIndex: async function() {
      // TODO: fill this
      let es = esClient._getEsClient()
      // Create index if not exists
      let indexExists = await es.indices.exists({index: 'table-cab-vendors'})
      if (!indexExists.body) {
        await es.indices.create({
          index: 'table-cab-vendors',
        })
      }
    },
    setupTable: async function() {
      let tables = (await ddbClient.send(new ListTablesCommand())).TableNames
      for(let i=0; i< tables.length; i++) {
        await ddbClient.send(new DeleteTableCommand({TableName: tables[i]}))
      }
      await ddbClient.send(new CreateTableCommand({
        TableName: "cab-vendors",
        AttributeDefinitions: [{AttributeName: "cabVendorId", AttributeType: "S"}],
        KeySchema: [{AttributeName: "cabVendorId", KeyType: "HASH"}],
        ProvisionedThroughput: {ReadCapacityUnits: 3, WriteCapacityUnits: 3}
      }))

      await ddbClient.send(new CreateTableCommand({
        TableName: "generic-table",
        AttributeDefinitions: [{AttributeName: "namespace", AttributeType: "S"},{AttributeName: "primaryKey", AttributeType: "S"}],
        KeySchema: [{AttributeName: "namespace", KeyType: "HASH"}, {AttributeName: "primaryKey", KeyType: "RANGE"}],
        ProvisionedThroughput: {ReadCapacityUnits: 3, WriteCapacityUnits: 3}
      }))

      await ddbClient.send(new CreateTableCommand({
        TableName: "rides",
        AttributeDefinitions: [{AttributeName: "bookingId", AttributeType: "S"},{AttributeName: "createdDate", AttributeType: "S"}, {AttributeName: "rideStatus", AttributeType: "S"}],
        KeySchema: [{AttributeName: "bookingId", KeyType: "HASH"}],
        GlobalSecondaryIndexes: [
          {
            IndexName: "booking-status-index", 
            KeySchema: [{AttributeName: "rideStatus", KeyType: "HASH"}, {AttributeName: "createdDate", KeyType: "RANGE"}],
            ProvisionedThroughput: {ReadCapacityUnits: 3, WriteCapacityUnits: 3},
            Projection: {ProjectionType:"ALL"}
          }],
        ProvisionedThroughput: {ReadCapacityUnits: 3, WriteCapacityUnits: 3}
      }))

      await ddbClient.send(new CreateTableCommand({
        TableName: "wallet-book-keeping",
        AttributeDefinitions: [{AttributeName: "cabVendorId", AttributeType: "S"},{AttributeName: "createdDate", AttributeType: "S"}],
        KeySchema: [{AttributeName: "cabVendorId", KeyType: "HASH"}, {AttributeName: "createdDate", KeyType: "RANGE"}],
        ProvisionedThroughput: {ReadCapacityUnits: 3, WriteCapacityUnits: 3}
      }))
    },
    generateRandomDigits: function(length) {
      let result = '';
      const digits = '0123456789';
    
      for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * digits.length);
        result += digits.charAt(randomIndex);
      }
    
      return result;
    },
    createOrderPaidWebhook: function(cabVendorId, orderId, createdAt) {
      return {
        "entity": "event",
        "account_id": "acc_KEEVI2KNG43zpF",
        "event": "order.paid",
        "contains": [
            "payment",
            "order"
        ],
        "payload": {
            "payment": {
                "entity": {
                    "id": "pay_PqM4lLN8G4EpGl",
                    "entity": "payment",
                    "amount": 1000,
                    "currency": "INR",
                    "status": "captured",
                    "order_id": orderId,
                    "invoice_id": null,
                    "international": false,
                    "method": "upi",
                    "amount_refunded": 0,
                    "refund_status": null,
                    "captured": true,
                    "description": "Add money to wallet to book rides",
                    "card_id": null,
                    "bank": null,
                    "wallet": null,
                    "vpa": "poornajai@ybl",
                    "email": "gogocabmail@gmail.com",
                    "contact": "+919962773900",
                    "notes": {
                        "cabVendorId": cabVendorId
                    },
                    "fee": 24,
                    "tax": 4,
                    "error_code": null,
                    "error_description": null,
                    "error_source": null,
                    "error_step": null,
                    "error_reason": null,
                    "acquirer_data": {
                        "rrn": "586141957155"
                    },
                    "created_at": createdAt,
                    "reward": null,
                    "upi": {
                        "payer_account_type": "bank_account",
                        "vpa": "poornajai@ybl"
                    }
                }
            },
            "order": {
                "entity": {
                    "id": orderId,
                    "entity": "order",
                    "amount": 50000,
                    "amount_paid": 50000,
                    "amount_due": 0,
                    "currency": "INR",
                    "receipt": null,
                    "offer_id": null,
                    "status": "paid",
                    "attempts": 1,
                    "notes": {
                        "cabVendorId": cabVendorId,
                    },
                    "created_at": createdAt
                }
            }
        },
        "created_at": 1738392996
    }
    },
    getRide: async function(bookingId){
      let response = await chai.request(endpoint)
      .post("/admin/peformAction")
      .send({  
        "googleIdToken": "dummy",
        "googleUserId": "dummy",
        "requestType": "GET_RIDES",
        "payload": {
          "bookingIds": [bookingId]
        }})
        expect(response).to.have.status(200)
        let ride = response.body.rides[0]
        return ride;
    },

    getCabVendor: async function(cabVendorId){
      let response = await chai.request(endpoint)
                                  .get("/cab-vendors?cabVendorId=" + cabVendorId)
        expect(response).to.have.status(200)
        return response.body
    },
    getCabVendorAllData: async function(cabVendorId){
      let response = await chai.request(endpoint)
      .post("/admin/peformAction")
      .send({  
        "googleIdToken": "dummy",
        "googleUserId": "dummy",
        "requestType": "GET_VENDORS",
        "payload": {
          "cabVendors": [ cabVendorId ]
        }})
        expect(response).to.have.status(200)
        return response.body[0]

    },
    updateWallet: async function(cabVendorId, amountInPaise) {
      let data = await ddbClient.send(new UpdateCommand({
          TableName: config.tables.cabVendors,
          Key: { cabVendorId},
          UpdateExpression: "ADD wallet.#walletTotal :amount",
          ExpressionAttributeValues: {
              ":amount": amountInPaise
          },
          ExpressionAttributeNames: {
              "#walletTotal": "total"
          }
        }))
       
    },

    getCustomerAccount: async function(phoneNo) {
      let data = await ddbClient.send(new GetCommand({
        TableName: config.tables.genericTable,
        Key: {namespace: "CUSTOMER_ACCOUNT", primaryKey: phoneNo}
      }))
      return data.Item
    },
    getOTPRecord: async function(phoneNumber, type = "PHONE_NO_VERIFICATION") {
      let data = await ddbClient.send(new GetCommand({
        TableName: config.tables.genericTable,
        Key: {namespace: type, primaryKey: phoneNumber}
      }))
      return data.Item
    },

    updateOTPRecord: async function(record) {
      let result = await ddbClient.send(new PutCommand({
        TableName: config.tables.genericTable,
        Item: record
      }))
      return result.Attributes
    },

    getLedgerData: async function(cabVendorId) {
      let data = await ddbClient.send(new QueryCommand({        
        TableName: config.tables.walletLedger,
        KeyConditionExpression: "cabVendorId = :pk",
        ExpressionAttributeValues: {
          ":pk": cabVendorId,
        },
        Limit: 20
      }))
      return data.Items
    },

    deleteOTPRecord: async function(phoneNumber, type = "PHONE_NO_VERIFICATION") {
      let data = await ddbClient.send(new DeleteCommand({
        TableName: config.tables.genericTable,
        Key: {namespace: type, primaryKey: phoneNumber}
      }))
    },

    deleteCabVendor: async function(cabVendorId) {
      await ddbClient.send(new DeleteCommand({
        TableName: config.tables.cabVendors,
        Key: {cabVendorId}
      }))
    },
    deleteRide: async function(bookingId) {
      if (!bookingId) return;
      await ddbClient.send(new DeleteCommand({
        TableName: config.tables.rides,
        Key: {bookingId}
      }))
    },
    deleteTable: async function(tableName) {
      await ddbClient.send(new DeleteTableCommand({
        TableName: tableName
      }))
    },

    createGenericTable: async function() {
      await ddbClient.send(new CreateTableCommand({
        TableName: config.tables.genericTable,
        KeySchema:[{
          AttributeName: "namespace",
          KeyType: "HASH"
        }, {
          AttributeName: "primaryKey",
          KeyType: "RANGE"
        }],
        AttributeDefinitions: [    // The names and data types of the primary key attributes
          { AttributeName: "namespace", AttributeType: "S" },
          { AttributeName: "primaryKey", AttributeType: "S"}
        ],
        ProvisionedThroughput: { 
          ReadCapacityUnits: 5, 
          WriteCapacityUnits: 5,
        }
      }))
    },

    createCabVendorV2: async function(cabVendorId, phoneNo) {
      phoneNo = phoneNo + ""
      let response = await chai.request(endpoint).post("/validate-create-vendor")
            .send({
                "phoneNo": phoneNo,
                "cabVendorId": cabVendorId
            });
      expect(response).to.have.status(200)
      let profile = {
          "name": "testName",
          "phoneNo": phoneNo + "",
          "email": "test@test.com"
      }
      otpRecord = await this.getOTPRecord(phoneNo, "VENDOR_CREATION_PHONE_NO_VERIFICATION")
      response = await chai.request(endpoint).post("/v2/cab-vendors")
                          .send({
                              "cabVendorId": cabVendorId,
                              "otpCode": otpRecord.data.generatedOtp + "",
                              "profile": profile
                          })
      expect(response).to.have.status(200)
      return response.body
    },

    // CabVendor wallet will be set to 0
    createCabVendorV2WithDriverAndVehicles: async function(cabVendorId, phoneNo, {licenseNumber="HR5120190002760", dob="1998-09-20"}) {
      await this.createCabVendorV2(cabVendorId, phoneNo)
      await this.updateWallet(cabVendorId, 30000)
      let response = await chai.request(endpoint).post("/validate-create-driver")
            .send({
                "phoneNo": phoneNo,
                "cabVendorId": cabVendorId,
                "licenseNo": licenseNumber,
                "dateOfBirth": dob
            });
      expect(response).to.have.status(200)
      otpRecord = await this.getOTPRecord(phoneNo, "DRIVER_ADDITION_PHONE_NO_VERIFICATION")
      expect(otpRecord.data).to.deep.include({
        phoneNumber: phoneNo
      })
      response = await chai.request(endpoint).post("/cab-vendors/add-driver")
        .send({
            "phoneNumber": phoneNo,
            "cabVendorId": cabVendorId,
            "licenseNumber": licenseNumber,
            "dateOfBirth": dob,
            "otpCode": otpRecord.data.generatedOtp,
            "panCardNumber": "EMPPS1123",
            "languages": "[]"
        })
      expect(response).to.have.status(200)
      response = await chai.request(endpoint).post("/cab-vendors/add-vehicle")
                    .send({
                        "cabVendorId": cabVendorId,
                        "vehicleNumber": "KA13B8962",
                        "chassisNumber": "MAT600"
                    });
      expect(response).to.have.status(200)
      return await this.getCabVendorAllData(cabVendorId)
    },

    createUnAssingedRide: async function(tripStartDate, publishRidePayload) {
      if(!publishRidePayload) {
        publishRidePayload = this.createPublishRidePayload(tripStartDate)
      }
      let response = await chai.request(endpoint).post("/rides").send(publishRidePayload)
      expect(response).to.have.status(200)
      let bookingId = response.body.bookingId
      let bookingProcessor = require('../../gogo-lambda/src/handlers/customer-server')
      let telegramProcessor = require('../../gogo-lambda/src/handlers/telegram-bot-processor')
      await bookingProcessor.SQSBookingProcessor(this.createSQSRidePublishV2Payload(bookingId))
      response = await telegramProcessor.answerCallbackQuery(this.createCallbackQueryApproveRide(bookingId))
      expect(response.code).to.eq("Successful")
      //TODO: Remove this after moving everything to V2
      let generalProcessor = require('../../gogo-lambda/src/handlers/general-sqs-processor')
      response = await generalProcessor.generalProcessor({Records: [{body: JSON.stringify({type: "PUBLISH_RIDE_FOR_V1_VENDORS", payload: {bookingId}})}]})
      expect(response).to.eq("OK")
      return bookingId
    },

    updateRideData: async function(bookingId, data) {
      let ride = await this.getRide(bookingId)
      ride = Object.assign(ride, data)
      let response = await ddbClient.send(new PutCommand({
        TableName: config.tables.rides,
        Key: {bookingId},
        Item: ride
      }))
      return ride
    },
    createVendorPayload: function(cabVendorId = "test1"){
        return {
            "cabVendorId": cabVendorId,
            "vehicles": {
              "TN28P007": {
                "vehicleRegNo": "TN28P007",
                "type": "SUV"
              }
            },
            "profile": {
              "name": "Tester",
              "phoneNo": 1234567890,
              "email": "test@gmail.com"
            }
          }
    },
    createRateOverrideConfig: function(startTime='2025-01-06T02:07:51.866Z', endTime='2029-01-06T02:07:51.866Z'){
      return [
                {
                    "places": [
                        {
                            "lat": 11.4615,
                            "lon": 78.1855,
                            "radius": 50
                        }
                    ],
                    "window": {
                        "start": startTime,
                        "end": endTime
                    },
                    "terms": [
                        "src"
                    ],
                    "rateCard": {
                        "minDistancePerDay": 250,
                        "minDistanceRoundTripPerDay": 250,
                        "oneWayMinDistance": 130,
                        "interStateRateCardBanner": "Toll and State permit charges extra",
                        "withinStateRateCardBanner": "Only Toll charges extra",
                        "appDiscount": 200,
                        "rateCards": {
                            "oneway": [
                                {
                                    "id": 101,
                                    "type": "sedan",
                                    "rate": 18,
                                    "bata": 400
                                },
                                {
                                    "id": 104,
                                    "type": "etios",
                                    "rate": 19,
                                    "bata": 400
                                },
                                {
                                    "id": 102,
                                    "type": "xylo",
                                    "rate": 20,
                                    "bata": 400
                                },
                                {
                                    "id": 103,
                                    "type": "innova",
                                    "rate": 25,
                                    "bata": 500
                                }
                            ],
                            "roundTrip": [
                                {
                                    "id": 201,
                                    "type": "sedan",
                                    "rate": 22,
                                    "bata": 400
                                },
                                {
                                    "id": 204,
                                    "type": "etios",
                                    "rate": 23,
                                    "bata": 400
                                },
                                {
                                    "id": 202,
                                    "type": "xylo",
                                    "rate": 27,
                                    "bata": 500
                                },
                                {
                                    "id": 203,
                                    "type": "innova",
                                    "rate": 28,
                                    "bata": 500
                                }
                            ]
                        }
                    },
                    "name": "Pongal price hikes",
                    "reason": "Fares are adjusted due to Pongal holidays"
                }
            ]
    },
    createLambdaAdminActionEvent: function(action, payload) {
      return {
        rawPath: "/admin-action",
        requestContext: {http: {method: "POST"}},
        body: JSON.stringify({action, payload}),
        headers: {}
      }
    },
    createVendorPayloadWithDriver: function(cabVendorId = "test1"){
      return {
          "cabVendorId": cabVendorId,
          "vehicles": {
            "TN28P007": {
              "vehicleRegNo": "TN28P007",
              "type": "SUV"
            }
          },
          "drivers": {
            "9876543210":{
              "name": "driver1",
              "phoneNo": 9876543210
            }
          },
          "profile": {
            "name": "Tester",
            "phoneNo": 1234567890,
            "email": "test@gmail.com"
          }
        }
  },
    createAdminVendorApprovePayload: function(cabVendorId){
      return {  
        "googleIdToken": "dummy",
        "googleUserId": "dummy",
        "requestType": "UPDATE_VENDOR_STATE",
        "payload": {
        "cabVendorId": cabVendorId,
        "vendorStatus": "APPROVED",
        "vehicleStatus": { "TN28P007": "APPROVED" }
        }}
    },
    createPublishRidePayload: function(pickupDate = "2023-01-09T02:59:46Z"){
        return {
            "bookingDetails": {
              "tripType": "ONE_WAY",
              "pickupPlaceId": "ChIJhyDl6v3pqzsRVMTnae1mTw4",
              "dropPlaceId": "ChIJl_cgDZ9nUjoRGSrgZgzx-Vk",
              "pickUpDate": pickupDate,
              
              "keepCab": "true",
              "cabType": "sedan"
            },
            "customerDetails": {
              "phoneNumber": "919842975789",
              "name": "sankar",
              "email": "uvsankar94@gmail.com"
            },
            "rateCardId": 101
          }
    },

    createPublishRidePayloadWithIntermediates: function(pickUpDate = "2023-01-09T02:59:46Z") {
      return {
        "bookingDetails": {
          "cabType": "sedan",
          "tripType": "ONE_WAY",
          "pickupPlaceId":"ChIJb_tqwA7CqzsRqW37El98uUU",
          "intermediates": "ChIJjzmVJAcvqjsRG599H3r8fJQ",
          "dropPlaceId":"ChIJb_tqwA7CqzsRqW37El98uUU"
        },
        "customerDetails": {
          "phoneNumber": "919842975789",
          "name": "sankar",
          "email": "uvsankar94@gmail.com"
        },
        "rateCardId": 101
      }
    },
    createSQSRidePublishV2Payload: function(bookingId){
      return {
        "Records": [ {
                "body": `{"version":"v2","payload": {"bookingId": "${bookingId}"}}`
            }
        ]
    }
    },

    createCancelAssignedRidePayload: function(bookingId, cabVendorId, curRideStatus = "ASSIGNED"){
      return {
        bookingId,
        cabVendorId,
        curRideStatus,
        "googleIdToken": "dummy",
        "googleUserId": "dummy",
      }
    },
    createCallbackQueryApproveRide: function(bookingId){
      return  {
          id: '3045511683238860686',
          from: {
            id: 709088445,
            is_bot: false,
            first_name: 'Sankar',
            last_name: 'Varadarajan',
            username: 'thanos1_2',
            language_code: 'en'
          },
          message: {
            message_id: 973,
            from: [Object],
            chat: [Object],
            date: 1676783466,
            reply_to_message: [Object],
            text: 'Message',
            reply_markup: [Object]
          },
          chat_instance: '-3734066993266349940',
          data: 'APPROVE_RIDE ' + bookingId
        }
    },

    createCallbackQueryCancelRide: function(bookingId){
      return  {
        id: '3045511683238860686',
        from: {
          id: 709088445,
          is_bot: false,
          first_name: 'Sankar',
          last_name: 'Varadarajan',
          username: 'thanos1_2',
          language_code: 'en'
        },
        message: {
          message_id: 973,
          from: [Object],
          chat: [Object],
          date: 1676783466,
          reply_to_message: [Object],
          text: 'Message',
          reply_markup: [Object]
        },
        chat_instance: '-3734066993266349940',
        data: 'CANCEL_RIDE_BY_ADMIN ' + bookingId
      }
    },
    deepCompare: function (a, b) {
      // Shamelessly copied from https://stackoverflow.com/questions/31683075/how-to-do-a-deep-comparison-between-2-objects-with-lodash
      var result = {
        different: [],
        missing_from_first: [],
        missing_from_second: []
      };
    
      _.reduce(a, (result, value, key) => {
        if (b.hasOwnProperty(key)) {
          if (_.isEqual(value, b[key])) {
            return result;
          } else {
            if (typeof (a[key]) != typeof ({}) || typeof (b[key]) != typeof ({})) {
              //dead end.
              result.different.push(key);
              return result;
            } else {
              var deeper = this.deepCompare(a[key], b[key]);
              result.different = result.different.concat(_.map(deeper.different, (sub_path) => {
                return key + "." + sub_path;
              }));
    
              result.missing_from_second = result.missing_from_second.concat(_.map(deeper.missing_from_second, (sub_path) => {
                return key + "." + sub_path;
              }));
    
              result.missing_from_first = result.missing_from_first.concat(_.map(deeper.missing_from_first, (sub_path) => {
                return key + "." + sub_path;
              }));
              return result;
            }
          }
        } else {
          result.missing_from_second.push(key);
          return result;
        }
      }, result);
    
      _.reduce(b, function (result, value, key) {
        if (a.hasOwnProperty(key)) {
          return result;
        } else {
          result.missing_from_first.push(key);
          return result;
        }
      }, result);
    
      return result;
    },
    createQrCreditPayload: function(cabVendorId, amountInPaise = 10000){
      return {
        body: JSON.stringify({
          "entity": "event",
          "account_id": "acc_KEEVI2KNG43zpF",
          "event": "qr_code.credited",
          "contains": [
              "payment",
              "qr_code"
          ],
          "payload": {
              "payment": {
                  "entity": {
                      "id": "pay_LQ8aRl87ePJbff",
                      "entity": "payment",
                      "amount": amountInPaise,
                      "currency": "INR",
                      "status": "captured",
                      "order_id": null,
                      "invoice_id": null,
                      "international": false,
                      "method": "upi",
                      "amount_refunded": 0,
                      "refund_status": null,
                      "captured": true,
                      "description": "QRv2 Payment",
                      "card_id": null,
                      "bank": null,
                      "wallet": null,
                      "vpa": "jaikrishnan.8186@wasbi",
                      "email": null,
                      "contact": null,
                      "notes": {
                          "cabVendorId": cabVendorId
                      },
                      "fee": 0,
                      "tax": 0,
                      "error_code": null,
                      "error_description": null,
                      "error_source": null,
                      "error_step": null,
                      "error_reason": null,
                      "acquirer_data": {
                          "rrn": "307013940010"
                      },
                      "created_at": 1678520303
                  }
              },
              "qr_code": {
                  "entity": {
                      "id": "qr_LQ8RmYHm1cI759",
                      "entity": "qr_code",
                      "created_at": 1678519811,
                      "name": `CabVendorId#${cabVendorId}`,
                      "usage": "multiple_use",
                      "type": "upi_qr",
                      "image_url": "https://rzp.io/i/oNfeV0ugj",
                      "payment_amount": null,
                      "status": "active",
                      "description": "Add money to wallet",
                      "fixed_amount": false,
                      "payments_amount_received": 800,
                      "payments_count_received": 1,
                      "notes": {
                          "cabVendorId": cabVendorId
                      },
                      "customer_id": null,
                      "close_by": null,
                      "closed_at": null,
                      "close_reason": null,
                      "tax_invoice": []
                  }
              }
          },
          "created_at": 1678520304
      })
      }
    }
}