
var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http'),
    moment = require('moment');
chai.use(chaiHttp);

//chai.use(require('chai-things'));

var helpers = require('./helper')

const endpoint = 'http://127.0.0.1:8787'

describe("OTP endpoint tests", function () {
    phoneNumber = Math.floor(Math.random() * 10000000000).toString();
    //this.timeout(100000)
    before(async function(){
        await helpers.deleteOTPRecord(phoneNumber)
    })

    after(async function(){
        await helpers.deleteOTPRecord(phoneNumber)
    })

    afterEach(async function() {
        moment.now = function () {
            return +new Date();
        }
    })

    it("Generate OTP", async function () {
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "GENERATE"})
        expect(response).to.have.status(200)
        expect(response.body.message).to.include("OTP Sent to")
        let otpRecord = await helpers.getOTPRecord(phoneNumber)
        expect(otpRecord.createdDate).to.be.not.null
        otpRecord = otpRecord.data
        expect(otpRecord.phoneNumber).to.eq(phoneNumber)
        expect(otpRecord.generatedOtp).to.be.a('number')
        expect(otpRecord.isVerified).to.be.false
        expect(otpRecord.noOfTimesOtpSent).to.eq(1)
    })

    it("Generate OTP same number second time", async function () {
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "GENERATE"})
        expect(response).to.have.status(400)
        expect(response.body.message).to.include("Request otp again in")
    })

    it("Generate OTP after 24hrs", async function() {
        let otpRecord = await helpers.getOTPRecord(phoneNumber)
        otpRecord.data.lastSentTime = moment().subtract(2, 'days').toISOString()
        await helpers.updateOTPRecord(otpRecord)
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "GENERATE"})
        expect(response.body.message).to.include("OTP Sent to")
        expect(response).to.have.status(200)
        
        otpRecord = await helpers.getOTPRecord(phoneNumber)
        expect(otpRecord.createdDate).to.be.not.null
        otpRecord = otpRecord.data
        expect(otpRecord.phoneNumber).to.eq(phoneNumber)
        expect(otpRecord.generatedOtp).to.be.a('number')
        expect(otpRecord.isVerified).to.be.false
        expect(otpRecord.noOfTimesOtpSent).to.eq(1)
        expect(moment(otpRecord.lastSentTime).isSame(moment(), 'hour')).to.be.true
    })

    it("Regenerate OTP", async function () {
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "RESEND"})
        expect(response).to.have.status(400)
        expect(response.body.message).to.include("Request otp again in ")
        let otpRecord = await helpers.getOTPRecord(phoneNumber)
        otpRecord = otpRecord.data
        expect(otpRecord.isVerified).to.be.false
        expect(otpRecord.noOfTimesOtpSent).to.eq(1)
    })

    it ("Verify OTP failed attempt", async function() {
        let otpRecord = await helpers.getOTPRecord(phoneNumber)
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: otpRecord.data.generatedOtp + 1})
        expect(response).to.have.status(400)
        expect(response.body.message).to.include("OTP invalid")
        otpRecord = await helpers.getOTPRecord(phoneNumber)
        expect(otpRecord.createdDate).to.be.not.null
        otpRecord = otpRecord.data
        expect(otpRecord.phoneNumber).to.eq(phoneNumber)
        expect(otpRecord.generatedOtp).to.be.a('number')
        expect(otpRecord.isVerified).to.be.false
        expect(otpRecord.noOfFailedAttempts).to.eq(1)
        expect(otpRecord.noOfTimesOtpSent).to.eq(1)

        response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: 33223})
        expect(response).to.have.status(400)
        console.log(response.body.message)
        expect(response.body.message).to.include("OTP invalid")
        otpRecord = await helpers.getOTPRecord(phoneNumber)
        otpRecord = otpRecord.data
        expect(otpRecord.noOfFailedAttempts).to.eq(2)
        expect(otpRecord.isVerified).to.be.false
    })

    it("Verify OTP", async function () {
        let otpRecord = await helpers.getOTPRecord(phoneNumber)
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: otpRecord.data.generatedOtp})
        expect(response).to.have.status(200)
        expect(response.body.message).to.include("Verified")
        otpRecord = await helpers.getOTPRecord(phoneNumber)
        expect(otpRecord.createdDate).to.be.not.null
        otpRecord = otpRecord.data
        expect(otpRecord.phoneNumber).to.eq(phoneNumber)
        expect(otpRecord.generatedOtp).to.be.a('number')
        expect(otpRecord.isVerified).to.be.true
        expect(otpRecord.noOfTimesOtpSent).to.eq(1)
    })

    it("Generate OTP", async function () {
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "GENERATE"})
        expect(response).to.have.status(200)
        expect(response.body.message).to.include("Already verified")
    })


    it("Verify OTP attempt exhaust", async function () {
        let phoneNumber = Math.floor(Math.random() * 10000000000).toString();
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "GENERATE"})
        console.log(response.body.message)
        expect(response).to.have.status(200)
        otpRecord = await helpers.getOTPRecord(phoneNumber)
        response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: otpRecord.data.generatedOtp - 1})
        expect(response.body.message).to.include("OTP invalid")

        response = await chai.request(endpoint).post("/otp").send({ phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: otpRecord.data.generatedOtp + 1})
        expect(response.body.message).to.include("OTP invalid")

        response = await chai.request(endpoint).post("/otp").send({ phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: otpRecord.data.generatedOtp + 10})
        expect(response.body.message).to.include("OTP invalid")

        otpRecord = await helpers.getOTPRecord(phoneNumber)

        response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "PHONE_NO_VERIFICATION", action: "VERIFY", otpCode: otpRecord.data.generatedOtp})
        expect(response.body.message).to.include("Try again after few hours")
        otpRecord = await helpers.getOTPRecord(phoneNumber)
        expect(otpRecord.data.isVerified).to.be.false
        expect(otpRecord.data.noOfTimesOtpSent).to.eq(1)
        expect(otpRecord.data.noOfFailedAttempts).to.eq(3)
    })
}) 

describe("OTP endpoint tests", function () {
    let phoneNumber;
    //this.timeout(100000)
    beforeEach(async function(){
        phoneNumber = Math.floor(Math.random() * 10000000000).toString();
    })

    afterEach(async function(){
        await helpers.deleteOTPRecord(phoneNumber)
    })

    it ("Send auth token for customer login", async function() {
        let response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "CUSTOMER_LOGIN", action: "GENERATE"})
        expect(response.body.message).to.include("OTP Sent to ")
        expect(response).to.have.status(200)

        otpRecord = await helpers.getOTPRecord(phoneNumber, "CUSTOMER_LOGIN")
        response = await chai.request(endpoint).post("/otp").send({phoneNumber, carrier:"WHATSAPP", otpType: "CUSTOMER_LOGIN", action: "VERIFY", otpCode: otpRecord.data.generatedOtp})
        console.log(response.body)
        expect(response.body.message).to.include("Verified")
        expect(response).to.have.status(200)
        expect(response.body.authToken).to.not.be.null
        expect(response.body.authToken).to.be.a('string')
        
    })
})