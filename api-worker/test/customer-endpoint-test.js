var chai = require('chai'),
    expect = chai.expect,
    moment = require('moment'),
    chaiHttp = require('chai-http');
chai.use(chaiHttp);


const endpoint = 'http://127.0.0.1:8787'
var helpers = require('./helper')

describe("Customer endpoints", function(){
    let phoneNo;
    this.timeout(5000)

    beforeEach(async function(){
         phoneNo = Math.floor(1000000000 + Math.random() * 9000000000).toString();
    })
   
    it("Create Cab vendor", async function() {
        let response = await chai.request(endpoint)
                                 .post("/customer")
                                 .send({
                                    "customer": {"phoneNo": phoneNo},
                                    "generateOTP": true
                                 })
        expect(response).to.have.status(200)
        expect(response.body).to.include.property("message")
        console.debug("Created account. Verifying OTP")

        otpRecord = await helpers.getOTPRecord(phoneNo, "CUSTOMER_LOGIN")
        response = await chai.request(endpoint).post("/otp").send({phoneNumber: phoneNo, carrier:"WHATSAPP", otpType: "CUSTOMER_LOGIN", action: "VERIFY", otpCode: otpRecord.data.generatedOtp})
        expect(response.body.message).to.include("Verified")
        expect(response).to.have.status(200)
        expect(response.body.authToken).to.not.be.null
        expect(response.body.authToken).to.be.a('string')
        console.debug("Verified OTP. Booking ride now")

        let ridePayload = helpers.createPublishRidePayload(moment().add(1, "day").toISOString())
        ridePayload.bookingSource = {
            source: "CUSTOMERAPP",
            accessToken: response.body.authToken
        }
        bookingId = await helpers.createUnAssingedRide(moment().toISOString(), ridePayload);
        let ride = await helpers.getRide(bookingId)
        expect(ride.bookingSource).to.deep.include({
            source: "CUSTOMERAPP",
            sourceId: phoneNo
        })
        console.debug("Ride created successfully. SourceId is correct")

        let customerAccount = await helpers.getCustomerAccount(phoneNo)
        expect(customerAccount).to.deep.include({
            rides: {
                pending: new Set([bookingId])
            }
        })
    })

    it("Invalid access token. ride booking", async function() {
        let ridePayload = helpers.createPublishRidePayload(moment().add(1, "day").toISOString())
        ridePayload.bookingSource = {
            source: "CUSTOMERAPP",
            accessToken: "dummy token"
        }
        let response = await chai.request(endpoint).post("/rides").send(ridePayload)
        console.debug(response.body)
        expect(response).to.have.status(400)

        ridePayload.bookingSource = {
            source: "CUSTOMERAPP"
        }
        response = await chai.request(endpoint).post("/rides").send(ridePayload)
        console.debug(response.body)
        expect(response).to.have.status(400)
    })

    it("Login. Account already exists", async function() {
        let response = await chai.request(endpoint)
                                 .post("/customer")
                                 .send({
                                    "customer": {"phoneNo": phoneNo},
                                    "generateOTP": false
                                 })
        expect(response).to.have.status(200)
        expect(response.body).to.include.property("message")
        otpRecord = await helpers.getOTPRecord(phoneNo, "CUSTOMER_LOGIN")
        expect(otpRecord).to.be.undefined
        let customerAccount = await helpers.getCustomerAccount(phoneNo)

        response = await chai.request(endpoint)
        .post("/customer")
        .send({
           "customer": {"phoneNo": phoneNo},
           "generateOTP": true
        })
        expect(response).to.have.status(200)
        let customerAccount2 = await helpers.getCustomerAccount(phoneNo)
        expect(customerAccount).to.deep.equal(customerAccount2)
        
        otpRecord = await helpers.getOTPRecord(phoneNo, "CUSTOMER_LOGIN")
        response = await chai.request(endpoint).post("/otp").send({phoneNumber: phoneNo, carrier:"WHATSAPP", otpType: "CUSTOMER_LOGIN", action: "VERIFY", otpCode: otpRecord.data.generatedOtp})
        expect(response.body.message).to.include("Verified")
        expect(response).to.have.status(200)
        expect(response.body.authToken).to.not.be.null
        expect(response.body.authToken).to.be.a('string')
        console.debug("Verified OTP")
    })
})