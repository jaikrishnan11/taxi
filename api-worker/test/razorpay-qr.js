var chai = require('chai'),
    expect = chai.expect,
    chaiHttp = require('chai-http');
chai.use(chaiHttp);


const endpoint = 'http://127.0.0.1:8787'
var helpers = require('./helper')

describe.skip("QR payment", function(){
    this.timeout(10000)
    let cabVendorId = "test-" + Math.random() * 10000
    let {paymentsWebhook} = require('../../gogo-lambda/src/handlers/payments-webhook')

    it("Should create cabVendor", async function() {
        let response = await chai.request(endpoint).post("/cab-vendors").send(helpers.createVendorPayload(cabVendorId))
        expect(response).to.have.status(200);
    })

    it("Should capture payment", async function() {
        await paymentsWebhook(helpers.createQrCreditPayload(cabVendorId, 12300))
        let response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
        expect(response).to.have.status(200)
        expect(response.body.wallet.total).to.eq(12300)
        await paymentsWebhook(helpers.createQrCreditPayload(cabVendorId, 41200))
        response = await chai.request(endpoint).get("/cab-vendors?cabVendorId=" + cabVendorId)
        expect(response).to.have.status(200)
        expect(response.body.wallet.total).to.eq(53500)

        //TODO: Verify ledger table as well 
    })
})