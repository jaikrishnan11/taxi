function isDashboardPage() {
    return window.location.href.includes("dashboard.html")
}

function isProfilePage() {
    return window.location.href.includes("profile.html")
}

function isRideListPage() {
    return window.location.href.includes("rides_list.html")
}

function isWalletTransaction() {
    return window.location.href.includes("wallet_transactions.html")
}

function isVehicleAndDrivers() {
    return window.location.href.includes("vehicles_and_drivers.html")
}

function isAddVehiclePage() {
    return window.location.href.includes("add-vehicle.html")
}

function isAddDriverPage() {
    return window.location.href.includes("add-driver.html")
}

function populateRideMeta(rides) {
    let ridesMeta = Alpine.store('ridesMeta')
    let finalOutput = rides.reduce((acc, obj) => ({ ...acc, [obj.bookingId]: obj }), ridesMeta);
    Alpine.store('ridesMeta', finalOutput);
}

document.addEventListener('alpine:init', () => {
    //Initialize objects
    Alpine.store('vehicles', [])
    Alpine.store('showFormLink', false);
    Alpine.store('showSpinner', false);
    Alpine.store('showRefreshButton', false);
    Alpine.store('showEditDeleteButton', true);
    Alpine.store('rides', []);
    Alpine.store('ridesMeta', {});
    Alpine.store('completedRides', []);
    Alpine.store('pendingRides', []);
    Alpine.store('walletTransactions', {
        "credit": [],
        "debit": []
    });
    Alpine.store('driverRides', []);
    Alpine.store('driverDetails', {
        "stats": {
            "rides": {
                "cancelledByCustomer": 0,
                "cancelledByVendor": 0,
                "completed": 0,
                "onGoing": 0,
                "pending": 0
            },
            "totalEarning": 0
        },
        "vehicles": {},
        "drivers": {},
        "vendorStatus": "",
        "version": "v0",
        "wallet": {
            "razorpay": {
                "paymentUrl": "",
                "qrId": "",
                "qrImageUrl": ""
            },
            "total": 0
        },
        "rides": {
            "confirmed": [
            ],
            "pending": [
            ],
            "cancelled": [

            ]
        },
        "profile": {
            "name": "",
            "phoneNo": "",
            "email": ""
        }
    });
    Alpine.store('dashboard_modal', {
        heading: "",
        content: "",
        showAddMoneyButton: false,
        confirmRide: false
    })
    // API calls 
    if (isWalletTransaction()) {
        let credit = []
        let debit = []
        window.gogoClient.getWalletTransactions().then((data) => {
            if (!_.isArray(data)) {
                return;
            }
            data.forEach(item => {
                if (item["action"] == "REFUND_MONEY" || (item["action"] == "ADD_MONEY" && item?.opDetails?.status != "INITIATED")) {
                    credit.push(JSON.parse(JSON.stringify(item)))
                }
                if (item["action"] == "DEDUCT_MONEY" || item["action"] == "VEHICLE_REFRESH_MONEY" || item["action"] == "DRIVER_REFRESH_MONEY") {
                    debit.push(JSON.parse(JSON.stringify(item)))
                }
            })
            Alpine.store('walletTransactions', {
                "credit": credit,
                "debit": debit
            })
        });
    }
    let vehicles = []
    let drivers = []
    if (isDashboardPage() || isProfilePage() || isRideListPage() || isWalletTransaction() || isVehicleAndDrivers() || isAddVehiclePage() || isAddDriverPage()) {
        window.gogoClient.getDriverDetails(cabVendorId = getCabVendorId()).then((data) => {
            console.log("ac calling driver api....", data);
            if (_.isEmpty(data)) {
                return
            }
            Alpine.store('driverDetails', data);
            if(data?.version == "v1" && data?.meta?.forceV2Migration) {
                $('#upgrade-v2-modal').modal('show')
                // $('#v2-upgrade-section').hide()
            }
            if(isAddVehiclePage()) {
                if(data?.wallet?.total < 200) {
                    $("#vehicle-low-pricing-modal").modal('show')
                } else {
                    $("#vehicle-pricing-modal").modal('show')
                }
            }
            if(isAddDriverPage()) {
                if(data?.wallet?.total < 100) {
                    $("#driver-low-pricing-modal").modal('show')
                } else {
                    $("#driver-pricing-modal").modal('show')
                }
            }
            let isPendingAction = false
            for (let [key, value] of Object.entries(data["vehicles"])) {
                vehicles.push(value);
                if(value.pendingActions.length > 0) {
                    isPendingAction = true
                }
            }
            Alpine.store('vehicles', vehicles)
            
            if(data["drivers"] != undefined) {
                for (let [key, value] of Object.entries(data["drivers"])) {
                    drivers.push(value);
                    if(value.pendingActions.length > 0) {
                        isPendingAction = true
                    }
                }
                Alpine.store('drivers', drivers)
            }
            
            if (isDashboardPage()) {
                if(isPendingAction) {
                    $("#pending-info-modal").modal('show');
                }
                // same copy in controller please change that when touching this area
                window.gogoClient.getUnassignedRides().then((data) => {
                    console.log("ac calling rides....", data, window.location.href);
                    Alpine.store('rides', data);
                });
                
                if(data?.version == "v2" && vehicles.length == 0) {
                    $('#initialize-vehicle').modal('show')
                }
                if(data?.version == "v2" && drivers.length == 0) {
                    $('#initialize-driver').modal('show')
                }
            }
            if (isRideListPage() || isWalletTransaction()) {
                console.log("ac calling getBookingDetails ...")
                if (data != undefined && data["rides"]) {
                    let rides = data["rides"];
                    if (rides["completed"] && rides["completed"].length > 0) {
                        window.gogoClient.getBookingDetails(rides["completed"], cabVendorId).then((rideData) => {
                            if (rideData != undefined && rideData["rides"] != undefined) {
                                console.log("storing completedRides...")
                                Alpine.store("completedRides", rideData["rides"]);
                                populateRideMeta(rideData["rides"])
                            }
                        })
                    }
                    if (rides["cancelled"] && rides["cancelled"].length > 0) {
                        window.gogoClient.getBookingDetails(rides["cancelled"], cabVendorId).then((rideData) => {
                            if (rideData != undefined && rideData["rides"] != undefined) {
                                console.log("storing cancelledRides...")
                                Alpine.store("cancelledRides", rideData["rides"]);
                                populateRideMeta(rideData["rides"])
                            }
                        })
                    }

                    let pendingRides = []
                    pendingRides = pendingRides.concat(rides["confirmed"], rides["onGoing"])
                    if (pendingRides.length > 0) {
                        window.gogoClient.getBookingDetails(pendingRides, cabVendorId).then((rideData) => {
                            if (rideData != undefined && rideData["rides"] != undefined) {
                                console.log("storing pendingRides...")
                                Alpine.store("pendingRides", rideData["rides"]);
                                populateRideMeta(rideData["rides"])
                            }
                        })
                    }
                }
            }
        });
    }
})



