function toggleEditPhoneNumberFields(disable) {
    $('#edit-phone-no-textbox').attr("disabled", disable)
}

function showVendorEditOtpDiv() {
    $('#edit-phone-otp').show()
    $('#edit-phone-update-btn').show()
}

$(document).ready(function () {

    $('#edit-phone-otp').hide()
    $('#edit-phone-update-btn').hide()

    $('#edit-account-name-btn').click(function () {
        $('#name-change-modal').modal('show')
    })

    $('#edit-phone-no-btn').click(function () {
        $('#phone-no-change-modal').modal('show')
    })

    $('#edit-phone-no-textbox').on('input', function () {
        var input = $(this).val().replace(/^[\D]/g, '').substring(0, 10);
        $(this).val(input);
    });

    $('#edit-phone-otp').on('input', function () {
        var input = $(this).val().replace(/^[\D]/g, '').substring(0, 5);
        $(this).val(input);
    });

    $('#edit-phone-no-otp').click(function () {
        let phoneNumber = $('#edit-phone-no-textbox').val()
        if (phoneNumber.length != 10) {
            alert("Phone number length should be 10")
            return
        }
        $("#loadingDialog").fadeIn(200);
        window.gogoClient.validateVendorEditPhoneNoTriggerOTP(phoneNumber).then((vendorData) => {
            toggleEditPhoneNumberFields(true)
            $("#loadingDialog").fadeOut(200);
            if (vendorData.errorCode != undefined) {
                if (vendorData.errorCode == "OTP_RESENT_REQUEST_EARLY") {
                    alert(vendorData.message)
                } else {
                    alert("Error received ", JSON.stringify(vendorData.errorCode))
                }
                return
            }
            console.log("OTP is triggered to ", phoneNumber, JSON.stringify(vendorData))
            alert("OTP is triggered")
            showVendorEditOtpDiv()
        }).catch((err) => {
            $("#loadingDialog").fadeOut(200);
            toggleEditPhoneNumberFields(false)
            console.log("Error...", err)
            alert(JSON.stringify(err))
        })
    })

    $('#edit-phone-update-btn').click(function () {
        let otpCode = $('#edit-phone-otp').val()
        let phoneNumber = $('#edit-phone-no-textbox').val()
        if (otpCode.length != 5) {
            alert("OTP should be 5 letters length")
            return
        }
        $("#loadingDialog").fadeIn(200);
        window.gogoClient.vendorEditPhoneNumber(phoneNumber, otpCode).then((vendorData) => {
            $("#loadingDialog").fadeOut(200);
            alert("Phone number updated !!!")
            $('#phone-no-change-modal').modal('hide')
            location.reload();
        }).catch((err) => {
            $("#loadingDialog").fadeOut(200);
            console.log("Error...", err)
            alert(JSON.stringify(err))
        })
    })

    $('#edit-vendor-name-btn').click(function() {
        let vendorName = $('#vendor-name-text-box').val()
        if(vendorName == undefined || vendorName.length < 3) {
            alert("Vendor name should be greater than 3 characters !!!")
            return
        }
        $("#loadingDialog").fadeIn(200);
        window.gogoClient.vendorEditName(vendorName).then((vendorData) => {
            $("#loadingDialog").fadeOut(200);
            alert("Name updated !!!")
            $('#name-change-modal').modal('hide')
            location.reload();
        }).catch((err) => {
            $("#loadingDialog").fadeOut(200);
            console.log("Error...", err)
            alert(JSON.stringify(err))
        })
    })

})