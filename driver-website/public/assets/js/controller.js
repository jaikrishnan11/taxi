// const FORM_URL = "https://forms.gle/PaAuKJz12wgTmdQF9";
const MAX_FILE_SIZE = 1024 * 1024 * 5; //5 MB

function openGoogleMap(googleMapsUrl) {
  window.open(googleMapsUrl, '_blank');
}

function makePhoneCall(phoneNumber) {
  window.location.href = `tel:${phoneNumber}`;
}

function getCabVendorDetails() {
  if (!window.loginDetails) {
    let loginDetails = localStorage.getItem('gogoUser')
    if (!loginDetails) return {}
    window.loginDetails = JSON.parse(loginDetails);
  }
  return window.loginDetails;
}

function getCabType(cabType) {
  if (cabType == "xylo") {
    return "Any SUV (AC)"
  }
  return cabType.charAt(0).toUpperCase() + cabType.slice(1) + " (AC)"
}

function getTripEndTime(ride) {
  if (ride?.bookingDetails?.tripType == "ONE_WAY") {
    return "Not Applicable"
  }
  if (ride?.bookingDetails?.returnStartTime) {
    return moment(ride?.bookingDetails?.returnStartTime).format('MMM Do YYYY, h:mm a')
  }
  return "Not Applicable"
}

function getCabVendorId() {
  // return "100146709221843723535";
  return getCabVendorDetails().googleId;
}

function validateFileSize(elementId) {
  var fileInput = document.getElementById(elementId);
  if (fileInput == undefined) {
    alert("File not found for", elementId)
    return false
  }
  var fileSize = fileInput.files[0].size; // Size in bytes

  if (fileSize > MAX_FILE_SIZE) {
    alert(`File size exceeds the limit of ${MAX_FILE_SIZE / 1024 / 1024} MB`);
    fileInput.value = ''; // Clear the file input
    return false
  }
  return true
}

function shareInfo() {
  try {
    const lastExecutionTime = localStorage.getItem('lastExecutionTime');
    const currentTime = new Date().getTime();

    // Check if last execution time is not set or if it's been more than 5 minutes
    if (!lastExecutionTime || currentTime - lastExecutionTime > 5 * 60 * 1000) {
      const data = JSON.stringify({
        type: "SEND_LOCATION_ONCE",
        payload: {
          fetchDurationMillis: 60000
        }
      });

      window.ReactNativeWebView.postMessage(data);

      // Update last execution time in localStorage
      localStorage.setItem('lastExecutionTime', currentTime);
    }
  } catch (err) {
    console.log(err);
  }
}


$("#loadingDialog").fadeIn(200);
$(document).ready(function () {
  $("#loadingDialog").fadeOut(200);
  shareInfo()
  // $("#loadingDialog").fadeIn(200);
  // // Simulate a loading process with setTimeout
  // setTimeout(function() {
  //   $("#loadingDialog").fadeOut(200);
  // }, 3000); // Change 3000 to the duration of your loading process in milliseconds


  $('#login').click(function () {
    location.href = 'dashboard.html';
  });
  $('#profile').click(function () {
    location.href = 'profile.html';
  });
  $('#home').click(function () {
    location.href = 'dashboard.html';
  });
  $('#wallet_transactions, #wallet_details_div').click(function () {
    location.href = 'wallet_transactions.html';
  });
  $('#rides_list,#pending_div, #completed_div').click(function () {
    location.href = 'rides_list.html';
  });

  // $('#formLink').click(function () {
  //     // hrefClone(FORM_URL)
  //     location.href = FORM_URL;
  // });

  $('#sign_up').click(function () {
    location.href = 'registration.html';
  });

  $('#add-vehicle,#add-vehicle-modal-btn').click(function () {
    location.href = 'add-vehicle.html';
  });//vehicles_and_drivers

  $('#vehicles_and_drivers').click(function () {
    location.href = 'vehicles_and_drivers.html';
  });

  $('#add-driver,#add-driver-modal-btn').click(function () {
    location.href = 'add-driver.html';
  });

  $('#inviteDrivers').click(function () {
    hrefClone("whatsapp://send?text=https://play.google.com/store/apps/details?id=com.gogodriverapp")
    // location.href = 'https://forms.gle/PaAuKJz12wgTmdQF9';
  });

  $('#call_customer').click(function () {
    alert("calling customer...")
  })

  // profile page buttons
  $('#profile-add-driver-btn').click(function () {
    location.href = 'add-driver.html'
  })

  $('#profile-add-vehicle-btn').click(function () {
    location.href = 'add-vehicle.html'
  })

  $('#customer-care-call-btn').click(function () {
    makePhoneCall('9962773900');
  })

  $('#customer-care-call-btn-1').click(function () {
    makePhoneCall('9962773900');
  })

  $('#driver-app-whatsapp-issues').click(function () {
    window.open("https://wa.me/+917795545782", "_blank");
  })

  $('#driver-app-whatsapp-issues-1').click(function () {
    window.open("https://wa.me/+917795545782", "_blank");
  })

  $('#log-out').click(function () {
    const data = JSON.stringify({
      type: "SIGN_OUT",
      payload: {
        key: "value"
      }
    })
    window.ReactNativeWebView.postMessage(data);
  })

  // upgrade to V2 button
  $('#validate-upgrade-v2-btn').click(function () {

    let panCardNumber = document.getElementById("reg_vendor_pan").value
    if (panCardNumber == "" || panCardNumber == undefined || panCardNumber.length != 10) {
      alert("PanCard number should be exactly 10 characters")
      return
    }
    if (document.getElementById("vendor-pan-file").files.length == 0) {
      alert("Please upload PAN document")
      return
    }

    if (validateFileSize("vendor-pan-file") == false) {
      // Alerts handled inside
      return
    }

    let phoneNumber = document.getElementById('reg_phone_number').value
    if (phoneNumber == "" || phoneNumber.length > 10 || phoneNumber.length < 10 || phoneNumber.includes("+")) {
      alert("Phone number should be of only 10 digits, Ex:9812398123, No + symbols");
      return
    }

    if (!document.getElementById("reg_terms_checkbox").checked) {
      alert("Please accept the terms and conditions")
      return
    }

    window.gogoClient.validateVendorEditPhoneNoTriggerOTP(phoneNumber).then((data) => {
      $('#reg_vendor_pan').attr("disabled", true)
      $('#vendor-pan-file').attr("disabled", true)
      $('#reg_phone_number').attr("disabled", true)
      $('#reg_terms_checkbox').attr("disabled", true)
      $('#v2-upgrade-section').show()
      alert("Please enter the OTP")

    }).catch((err) => {
      alert(`Please contact gogo support team \n ${err.message}`)
    })


  })

  $("#upgrade-v2-btn").click(function () {
    let panCardNumber = document.getElementById("reg_vendor_pan").value
    let phoneNumber = document.getElementById('reg_phone_number').value
    let otpCode = document.getElementById('upgrade-v2-otp').value
    window.gogoClient.vendorEditPhoneNumber(phoneNumber, otpCode).then((data) => {
      console.log("Phone number got edited !!!")
      window.gogoClient.upgradeV2(phoneNumber, panCardNumber).then((data) => {
        console.log("Vendor got upgraded successfully !!!")
        $("#upgrade-v2-modal").hide()
        alert("Vendor got upgraded successfully !!!")
        window.location.reload();
        var formData = new FormData();
        formData.append('category', 'CABVENDOR')
        formData.append('categoryId', vendorDetails["googleId"])
        // There is a file type validation in the corresponding input tags 
        formData.append("cabVendorPan", document.getElementById("vendor-pan-file").files[0])
        window.gogoClient.uploadFiles(vendorDetails["googleId"], formData).then((data) => {
          console.log("Files uploaded successfully !!!")
        }).catch((err) => {
          console.log("Error in uploading file upload", err)
        })
      }).catch((err) => {
        console.log("Error in upgrading to V2", err)
        alert(`Error in upgrading to V2 \n ${err}`)
      })
    }).catch((err) => {
      console.log("Error in editing phone number", err)
      alert(`Error in editing phone number \n ${err.message}`)
    })

  })

  // API call
  $('#add-vehicle-btn').click(function () {
    let data = Alpine.store('driverDetails')
    if (data?.wallet?.total < 200) {
      $("#vehicle-low-pricing-modal").modal('show')
      return
    }
    // call the api server for validation
    let vehicleNumber = $('#add_vehicle_no').val()
    let chassisNumber = $('#chassis_no').val()
    // vehicle-car-photo-file-0
    if (document.getElementById('vehicle-car-photo-file-0').files.length == 0 ||
      document.getElementById('vehicle-car-photo-file-1').files.length == 0 ||
      document.getElementById('vehicle-car-photo-file-2').files.length == 0) {
      alert("Please upload all the 3 photos !!!")
      return;
    }

    $("#loadingDialog").fadeIn(200);
    var formData = new FormData();
    formData.append('category', 'VEHICLE')
    formData.append('categoryId', vehicleNumber)
    // There is a file type validation in the corresponding input tags 
    formData.append('vehicleRC', document.getElementById('vehicle-rc-file').files[0]);
    formData.append('carPhoto1', document.getElementById('vehicle-car-photo-file-0').files[0]);
    formData.append('carPhoto2', document.getElementById('vehicle-car-photo-file-1').files[0]);
    formData.append('carPhoto3', document.getElementById('vehicle-car-photo-file-2').files[0]);
    //TODO: add file size validation
    window.gogoClient.uploadFiles(vendorDetails["googleId"], formData).then(resp => {
      console.log("Vehicle documents uploaded", resp)
      return window.gogoClient.addVehicle({
        cabVendorId: vendorDetails["googleId"],
        googleIdToken: vendorDetails["googleIdToken"],
        vehicleNumber: vehicleNumber,
        chassisNumber: chassisNumber
      }).then((data) => {
        $("#loadingDialog").fadeOut(200);
        alert("vehicle added successfully, Kindly check the vehicle status")
        location.href = 'vehicles_and_drivers.html';
        // window.location.reload();
      }).catch((err) => {
        $("#loadingDialog").fadeOut(200);
        console.log(err)
        alert(JSON.stringify(err))
      })
    }).then((data) => {
      // $("#loadingDialog").fadeOut(200);
      // alert("vehicle added successfully, Kindly check the vehicle status")
      // location.href = 'vehicles_and_drivers.html';
      // window.location.reload();
    }).catch((err) => {
      $("#loadingDialog").fadeOut(200);
      console.log(err)
      alert(JSON.stringify(err))
    })
  })

  $('#chassis_no').on('input', function () {
    var input = $(this).val().toUpperCase().replace(/[^A-Z0-9]/g, '').substring(0, 5);
    $(this).val(input);
  })

  // vehicle number validator
  $('#add_vehicle_no').on('input', function () {
    var input = $(this).val().toUpperCase().replace(/[^A-Z0-9]/g, '').substring(0, 10);
    $(this).val(input);
  });

  // License text box validator
  $('#add_driver_license_no').on('input', function () {
    var input = $(this).val().toUpperCase().replace(/[^A-Z0-9]/g, '').substring(0, 16);
    // if (input.length > 4) {
    //     let licenseSecondHalf = input.substring(4).replace(/[^0-9]/g, ''); // Replace non-numeric characters with an empty string  
    //     input = input.substring(0, 4) + '-' + licenseSecondHalf;
    // }
    $(this).val(input);
  });

  $('#add_pan_card_no,#reg_vendor_pan').on('input', function () {
    var input = $(this).val().toUpperCase().replace(/[^A-Z0-9]/g, '').substring(0, 10);
    $(this).val(input);
  });

  function toggleAddDriverEditStatus(disable) {
    $('#add_pan_card_no').attr("disabled", disable)
    $("#add_driver_license_no").attr("disabled", disable)
    $("#driver_dob").attr("disabled", disable)
    $("#driver_phone_number").attr("disabled", disable)
  }

  $('#validate_driver_btn').click(function () {
    let data = Alpine.store('driverDetails')
    if (data?.wallet?.total < 100) {
      $("#driver-low-pricing-modal").modal('show')
      return
    }
    let driverLicenseNo = $('#add_driver_license_no').val()
    if (driverLicenseNo == undefined) {
      alert("Please add license number !!!")
      return
    }
    let phoneNumber = $('#driver_phone_number').val()
    if (phoneNumber != undefined && phoneNumber.length != 10) {
      alert("Phone number should be of length 10 !!!")
      return
    }
    if (document.getElementById('driver-license-file').files.length === 0 ||
      document.getElementById('driver-photo-file').files.length === 0 ||
      document.getElementById('driver-aadhar-file').files.length === 0) {
      alert('Please upload all the files');
      return;
    }
    // if (!validatePanCardNumber($('#add_pan_card_no').val())) {
    //     alert("Pan card number is not valid !!!")
    //     return
    // }
    // call the api server for validation
    $("#loadingDialog").fadeIn(200);
    window.gogoClient.validateDriverAndTriggerOTP({
      phoneNo: phoneNumber,
      licenseNo: driverLicenseNo,
      cabVendorId: vendorDetails["googleId"],
      dateOfBirth: $("#driver_dob").val(),
    }).then((data) => {
      $("#loadingDialog").fadeOut(200);
      showOtpDiv()
      toggleAddDriverEditStatus(true)
    }).catch((err) => {
      $("#loadingDialog").fadeOut(200);
      console.log(err)
      alert(JSON.stringify(err))
      toggleAddDriverEditStatus(false)
    })
  })

  $('#add_driver_btn').click(function () {
    let otpCode = $("#driver_otp").val()
    if (otpCode == undefined || otpCode == "" || otpCode.length < 5) {
      alert("OTP is invalid !!!")
      return
    }

    if (document.getElementById('driver-license-file').files.length === 0 ||
      document.getElementById('driver-photo-file').files.length === 0 ||
      document.getElementById('driver-aadhar-file').files.length === 0) {
      alert('Please upload all the files');
      return;
    }
    // Fetching languages data
    var checkboxes = document.getElementsByName('languages');
    var selectedCheckboxes = [];

    for (var i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        selectedCheckboxes.push(checkboxes[i].value);
      }
    }
    let languagesInCaps = selectedCheckboxes.join(', ')
    $("#loadingDialog").fadeIn(200);
    window.gogoClient.addDriver({
      cabVendorId: vendorDetails["googleId"],
      googleIdToken: vendorDetails["googleIdToken"],
      phoneNumber: $("#driver_phone_number").val(),
      otpCode: otpCode,
      licenseNumber: $("#add_driver_license_no").val().replace("-", ""),
      dateOfBirth: $("#driver_dob").val(),
      languages: languagesInCaps
    }).then((data) => {
      $("#loadingDialog").fadeOut(200);
      alert("Driver added successfully")
      location.href = 'vehicles_and_drivers.html';
      // window.location.reload();
    }).catch((err) => {
      $("#loadingDialog").fadeOut(200);
      console.log(err)
      alert(err)
    })



    var formData = new FormData();
    formData.append('category', 'DRIVER')
    formData.append('categoryId', $("#add_driver_license_no").val().replace("-", ""))
    formData.append('drivingLicense', document.getElementById('driver-license-file').files[0]);
    formData.append('driverPhoto', document.getElementById('driver-photo-file').files[0]);
    formData.append('driverAadhar', document.getElementById('driver-aadhar-file').files[0]);
    window.gogoClient.uploadFiles(vendorDetails["googleId"], formData).then(console.log, console.log)
  })

  let addVehicleNoInput = document.getElementById('add_vehicle_no')
  if (addVehicleNoInput != undefined) {
    addVehicleNoInput.addEventListener('input', function (event) {
      // Remove non-digit characters from the input value
      let vehicleNumber = event.target.value.toUpperCase()
      vehicleNumber = vehicleNumber.replace(/[^\w\s]/g, '').replace(/\s/g, '');
      // If the input value is longer than 10 digits, truncate it
      if (vehicleNumber.length > 10) {
        vehicleNumber = vehicleNumber.slice(0, 10);
      }
      addVehicleNoInput.value = vehicleNumber;
    });
  }


  let phoneNumberInput = document.getElementById('reg_phone_number')
  if (phoneNumberInput != undefined) {
    // Add an input event listener to the input field
    phoneNumberInput.addEventListener('input', function (event) {
      // Remove non-digit characters from the input value
      var phoneNumber = event.target.value.replace(/\D/g, '').replace(/\+/g, '');
      // If the input value is longer than 10 digits, truncate it
      if (phoneNumber.length > 10) {
        phoneNumber = phoneNumber.slice(0, 10);
      }

      // Set the input value to the cleaned phone number
      phoneNumberInput.value = phoneNumber;
    });
  }
});
// End of document ready function

function toggleRegistrationFieldDisable(disable) {
  $("#reg_account_name").attr("disabled", disable);
  $("#reg_phone_number").attr("disabled", disable);
  $("#reg_vendor_pan").attr("disabled", disable);
}

let createValidateVendorElement = document.getElementById('register');
if (createValidateVendorElement != undefined) {
  createValidateVendorElement.onclick = function (e) {
    e.preventDefault();
    // let name = document.getElementById('reg_name').value
    let accountName = document.getElementById('reg_account_name').value
    if (accountName == "" || accountName == undefined) {
      alert("Please enter the account name !!!");
      return
    }
    if (accountName.length > 25 || accountName.length < 3) {
      alert("Please enter the account name greater than 3 and less than 25 characters");
      return
    }
    if (!document.getElementById("reg_terms_checkbox").checked) {
      alert("Please accept the terms and conditions")
      return
    }
    const usernameRegex = /^[a-zA-Z0-9_ ]+$/;
    if (!usernameRegex.test(accountName)) {
      alert("Enter only letters or numbers, no special characters");
      return
    }
    // let mailId = document.getElementById('reg_email').value
    let phoneNumber = document.getElementById('reg_phone_number').value
    // let altPhoneNumber = document.getElementById('reg_alt_phone_number').value
    // let regVehicleNumber = document.getElementById('reg_vehicle_number').value
    // let regVehicleType = document.getElementById('reg_vehicle_list').value
    if (phoneNumber == "") {
      alert("Please fill the phone number !!!");
      return
    }
    if (phoneNumber.length > 10 || phoneNumber.length < 10 || phoneNumber.includes("+")) {
      alert("Phone number should be of only 10 digits, Ex:9812398123, No + symbols");
      return
    }

    let panCardNumber = document.getElementById("reg_vendor_pan").value
    if (panCardNumber == "" || panCardNumber == undefined || panCardNumber.length != 10) {
      alert("PanCard number should be exactly 10 characters")
      return
    }
    // Alpine.store("showSpinner", true)
    $("#loadingDialog").fadeIn(200);
    window.gogoClient.validateVendorAndTriggerOTP({
      "phoneNo": phoneNumber,
      "cabVendorId": vendorDetails["googleId"]
    }).then((vendorData) => {
      toggleRegistrationFieldDisable(true)
      $("#loadingDialog").fadeOut(200);
      if (vendorData.errorCode != undefined) {
        if (vendorData.errorCode == "OTP_RESENT_REQUEST_EARLY") {
          alert(vendorData.message)
        } else {
          alert("Error received ", JSON.stringify(vendorData.errorCode))
        }
        return
      }
      console.log("OTP is triggered to ", phoneNumber, JSON.stringify(vendorData))
      alert("OTP is triggered")
      showOtpDiv()
    }).catch((err) => {
      $("#loadingDialog").fadeOut(200);
      toggleRegistrationFieldDisable(false)
      console.log("Error...", err)
      alert(JSON.stringify(err))
    })
    // TODO: vehicles payload section is removed from the API
    // window.gogoClient.createVendor({
    //     "cabVendorId": vendorDetails["googleId"],
    //     "profile": {
    //         "name": vendorDetails["displayName"],
    //         "phoneNo": parseInt(phoneNumber),
    //         "email": vendorDetails["email"]
    //     }
    // }).then((data) => {
    //     console.log("Registered successfully....", data);
    //     console.log("Redirecting...")
    //     // alert("Please upload the documents to verify...")
    //     // window.open(FORM_URL);
    //     showOtpDiv()
    // }).catch((err) => {
    //     console.log("Error....", err);
    //     alert("Error while uploading...")
    // });
  }
}

let finalRegister = document.getElementById('final-register');
if (finalRegister != undefined) {
  finalRegister.onclick = function (e) {
    e.preventDefault();
    let otpSection = document.getElementById("reg_otp")
    if (otpSection == undefined) {
      alert("Enter OTP ...")
      return
    }
    let panCardNumber = document.getElementById("reg_vendor_pan").value
    if (panCardNumber == "" || panCardNumber == undefined || panCardNumber.length != 10) {
      alert("PanCard number should be exactly 10 characters")
      return
    }
    if (document.getElementById("vendor-pan-file").files.length == 0) {
      alert("Please upload PAN document")
      return
    }
    if (!document.getElementById("reg_terms_checkbox").checked) {
      alert("Please accept the terms and conditions")
      return
    }
    let formData = new FormData()
    formData.append('category', 'CABVENDOR')
    formData.append('categoryId', vendorDetails["googleId"])
    // There is a file type validation in the corresponding input tags 
    formData.append("cabVendorPan", document.getElementById("vendor-pan-file").files[0])
    $("#loadingDialog").fadeIn(200);
    return window.gogoClient.createVendorV2({
      "otpCode": otpSection.value,
      "cabVendorId": vendorDetails["googleId"],
      "profile": {
        "name": $('#reg_account_name').val(),
        "phoneNo": parseInt($('#reg_phone_number').val()),
        "email": vendorDetails["email"],
        "panCardNo": panCardNumber
      }
    }).then((createVendorData) => {
      return window.gogoClient.uploadFiles(vendorDetails["googleId"], formData).then(resp => {
        console.log("CabVendor documents uploaded")
      }).then((vendorData) => {
        $("#loadingDialog").fadeOut(200);
        window.location.href = "/dashboard.html"
        // window.open("/dashboard.html"); // This will open in new tab
        alert("Registered successfully")
        console.log("Registered successfully....", vendorData);
      }).catch((uploadFilesError) => {
        $("#loadingDialog").fadeOut(200);
        $("#reg_terms_checkbox").attr("disabled", false);
        console.log("Error during uploading documents vendor v2 ", JSON.stringify(uploadFilesError))
        alert("Document upload error ", JSON.stringify(uploadFilesError))
      })
    }).catch((createVendorErr) => {
      $("#loadingDialog").fadeOut(200);
      $("#reg_terms_checkbox").attr("disabled", false);
      console.log("Error during create vendor v2 ", JSON.stringify(createVendorErr))
      alert(JSON.stringify(createVendorErr))
    })
  }
}

setTimeout(() => {
  console.log("reloading the page")
  location.reload();
}, 1000 * 30 * 60) //30 minutes once reloading the page

let vendorDetails = getCabVendorDetails();
let mailData = vendorDetails["email"]
regEmail = document.getElementById('reg_email')
if (regEmail != undefined) {
  regEmail.value = mailData;
  regEmail.disabled = true;
}

function showOtpDiv() {
  var element = document.getElementById("otp-section");
  element.style.display = "block"; // or "inline-block", or any other valid display value
}

function hrefClone(hrefUrl) {
  console.log("hrefClone started", hrefUrl)
  const data = JSON.stringify({
    type: "OPEN_URL",
    payload: {
      url: hrefUrl
    }
  })
  window.ReactNativeWebView.postMessage(data)
}

const context = window

// same copy in alpine-controller please change that when touching this area
let refreshElement = document.getElementById('ride-refresh');
if (refreshElement != undefined) {
  document.getElementById('ride-refresh').onclick = function (e) {
    $("#loadingDialog").fadeIn(200);
    window.location.reload()
    // window.gogoClient.getUnassignedRides().then((data) => {
    //     console.log("ac calling rides....", data, window.location.href);
    //     $("#loadingDialog").fadeOut(200);
    //     Alpine.store('rides', data);
    // });
  }
}

let addMoneyElement = document.getElementById('wallet-add-money');
if (addMoneyElement != undefined) {
  document.getElementById('wallet-add-money').onclick = function (e) {
    let walletAmountElement = document.getElementById('wallet-amount');
    // || walletAmountElement.value < 500) 
    if (walletAmountElement == undefined) {
      alert("Please enter an amount greater than or equal to 500 !!!");
    } else {
      e.preventDefault();
      // let paymentUrl = _.get(Alpine.store("driverDetails"), "wallet.razorpay.paymentUrl")
      // console.log("opening the payment link", paymentUrl)
      // window.open(paymentUrl);
      makeRazorypayPayment(walletAmountElement.value, false);
    }
  }
}

let profileAddMoneyElement = document.getElementById('wallet-add-money-btn');
if (profileAddMoneyElement != undefined) {
  document.getElementById('wallet-add-money-btn').onclick = function (e) {
    e.preventDefault();
    $('#dashboard_modal').modal('show');
    console.log("button pressed")
    window.gogoClient.putDataToModal("ADD_MONEY")
  }
}

let showQrCodeElement = document.getElementById('show-qr-code');
if (showQrCodeElement != undefined) {
  document.getElementById('show-qr-code').onclick = function (e) {
    e.preventDefault();
    let qrImageUrl = Alpine.store('driverDetails')?.wallet?.razorpay?.qrImageUrl
    if (qrImageUrl == undefined) {
      alert("QR image not found for this cab vendor, contact gogocabs customer care")
    } else {
      $('#qr-modal-image').attr('src', qrImageUrl);
      $('#qr-modal').modal('show');
    }
  }
}

let confirmRide = document.getElementById('confirm-ride');
if (confirmRide != undefined) {
  document.getElementById('confirm-ride').onclick = function (e) {
    e.preventDefault();
    console.log("calling ride endpoints...")
    Alpine.store('showSpinner', true)
    window.gogoClient.callRideEndpoint()
  }
}


//delete vehicle
let deleteVehicleButton = document.getElementById('delete-vehicle-btn');
if (deleteVehicleButton != undefined) {
  document.getElementById('delete-vehicle-btn').onclick = function (e) {
    $('#delete_vehicle_modal').modal('hide');
    e.preventDefault();
    let typedVehicleNumber = document.getElementById('check_delete_vehicle_no').value
    let vehicleNumber = document.getElementById('delete_vehicle_no').value
    if (typedVehicleNumber != vehicleNumber) {
      alert("vehicle number do not match, Kindly try again !!!");
      return
    } else {
      deleteVehicle(vehicleNumber).then((data) => {
        if (data?.response?.message != undefined) {
          alert(JSON.stringify(data?.response?.message))
        } else {
          alert("Vehicle deleted successfully, Kindly check the data again !!!")
          location.href = 'vehicles_and_drivers.html';
          window.location.reload();
        }
      }).catch((err) => {
        console.log(err)
        alert(JSON.stringify(err))
      })
    }
  }
}

// delete driver
let deleteDriverButton = document.getElementById('delete-driver-btn');
if (deleteDriverButton != undefined) {
  document.getElementById('delete-driver-btn').onclick = function (e) {
    $('#delete_driver_modal').modal('hide');
    e.preventDefault();
    let typedLicenseNumber = document.getElementById('check_driver_vehicle_no').value
    let licenseNumber = document.getElementById('delete-driver-textbox').value
    if (typedLicenseNumber != licenseNumber) {
      alert("License number do not match, Kindly try again !!!");
      return
    } else {
      deleteDriver(licenseNumber).then((data) => {
        if (data?.response?.message != undefined) {
          alert(JSON.stringify(data?.response?.message))
        } else {
          alert("Driver deleted successfully, Kindly check the data again !!!")
          location.href = 'vehicles_and_drivers.html';
          window.location.reload();
        }
      }).catch((err) => {
        console.log(err)
        alert(JSON.stringify(err))
      })
    }
  }
}


function showDeleteVehicleModal(vehicleNo) {
  $('#delete_vehicle_modal').modal('show');
  $('#delete_vehicle_no').val(vehicleNo);
  $('#delete_vehicle_no').prop('disabled', true);
}

function validatePanCardNumber(panCardNumber) {
  if ((panCardNumber == undefined) || (panCardNumber == "")) {
    return false
  }
  return panCardNumber.match(/[A-Z]{5}[0-9]{4}[A-Z]{1}/g)
}

function showRefreshDriverModal(licenseNo) {
  $('#refresh_driver_modal').modal('show');
  $('#refresh_driver_textbox_no').val(licenseNo);
  $('#refresh_driver_textbox_no').prop('disabled', true);
}

function showEditDriverModal() {
  $('#edit_driver_modal').modal('show');
}

function showDeleteDriverModal(licenseNumber) {
  $('#delete_driver_modal').modal('show');
  $('#delete-driver-textbox').val(licenseNumber)
  $('#delete-driver-textbox').prop('disabled', true);
}

function showRefreshVehicleModal(vehicleNo) {
  $('#refresh_vehicle_modal').modal('show');
  $('#refresh_vehicle_textbox_no').val(vehicleNo);
  $('#refresh_vehicle_textbox_no').prop('disabled', true);
}

let refreshDriverSubmitBtn = document.getElementById('refresh_driver_submit_btn');
if (refreshDriverSubmitBtn != undefined) {
  document.getElementById('refresh_driver_submit_btn').onclick = function (e) {
    $('#refresh_driver_modal').modal('hide');
    e.preventDefault();
    $("#loadingDialog").fadeIn(200);
    let licenseNo = document.getElementById('refresh_driver_textbox_no').value
    window.gogoClient.refreshDriver(licenseNo).then((data) => {
      $("#loadingDialog").fadeOut(200);
      if (data?.response?.message != undefined) {
        alert(JSON.stringify(data?.response?.message))
      } else {
        alert("Driver successfully refreshed, Kindly check the data again !!!")
        location.href = 'vehicles_and_drivers.html';
        window.location.reload();
      }
    }).catch((err) => {
      $("#loadingDialog").fadeOut(200);
      console.log(err)
      alert("Refresh driver failed, Please take screenshot and check with gogo customer care", JSON.stringify(err))
    })
  }
}


let refreshVehicleSubmitBtn = document.getElementById('refresh_vehicle_submit_btn');
if (refreshVehicleSubmitBtn != undefined) {
  document.getElementById('refresh_vehicle_submit_btn').onclick = function (e) {
    $('#refresh_vehicle_modal').modal('hide');
    e.preventDefault();
    $("#loadingDialog").fadeIn(200);
    let vehicleNumber = document.getElementById('refresh_vehicle_textbox_no').value
    window.gogoClient.refreshVehicle(vehicleNumber).then((data) => {
      $("#loadingDialog").fadeOut(200);
      if (data?.response?.message != undefined) {
        alert(JSON.stringify(data?.response?.message))
      } else {
        alert("Vehicle successfully refreshed, Kindly check the data again !!!")
        location.href = 'vehicles_and_drivers.html';
        window.location.reload();
      }
    }).catch((err) => {
      $("#loadingDialog").fadeOut(200);
      console.log(err)
      alert("Refresh vehicle failed, Please take screenshot and check with gogo customer care", JSON.stringify(err))
    })
  }
}

async function deleteVehicle(vehicleNumber) {
  try {
    return await window.gogoClient.deleteVehicle(vehicleNumber, vendorDetails["googleIdToken"])
  } catch (err) {
    console.log(err);
    alert("Delete vehicle failed, Please take screenshot and check with gogo customer care", JSON.stringify(err))
  }
}

async function deleteDriver(licenseNumber) {
  try {
    return await window.gogoClient.deleteDriver(licenseNumber, vendorDetails["googleIdToken"])
  } catch (err) {
    console.log(err);
    alert("Delete driver failed, Please take screenshot and check with gogo customer care", JSON.stringify(err))
  }
}


// v2 dashboard changes
function showVehicleListingModal() {
  $('#choose-driver-btn').attr('disabled', true)
  $("#vehicles_modal").modal('show')
  // Define array of values
  var values = Alpine.store('vehicles');
  values = _.filter(values, (data) => {
    return data["vehicleStatus"] == "APPROVED"
  })
  $("#vehicleListContainer").empty();
  $.each(values, function (index, value) {
    // Create radio button element
    var radioButton = $('<input type="radio">');

    // Set attributes for the radio button
    radioButton.attr({
      id: 'option' + (index + 1),
      name: 'options',
      value: value['vehicleRegNo']
    });

    // Create label element
    var label = $('<label>');

    // Set attributes for the label
    label.attr('for', 'option' + (index + 1));

    // Set label text
    let labelText = `${value['vehicleRegNo']} (${value['model']})`
    label.text(labelText);

    label.css({
      'font-weight': 'bold'
    });
    radioButton.css('margin-right', '5px');
    // Append radio button and label to container div
    $('#vehicleListContainer').append(radioButton, label, '<br>');
  });
  $('#vehicleListContainer input[type="radio"]').change(function () {
    // Your code to execute when a radio button in the list is changed
    var selectedValue = $(this).val();
    console.log("Selected value: " + selectedValue);
    $('#choose-driver-btn').attr('disabled', false)
    Alpine.store('selectedVehicle', selectedValue)
    $('#choose-driver-btn').click(function () {
      showDriverListingModal()
    })
  });
}


function showDriverListingModal() {
  $("#drivers_modal").modal('show')
  $("#vehicles_modal").modal('hide')
  $('#accept-ride-modal-btn').attr('disabled', true)
  // Define array of values
  var values = Alpine.store('drivers');
  values = _.filter(values, (data) => {
    return data["pendingActions"].length == 0
  })
  $("#driverListContainer").empty();
  $.each(values, function (index, value) {
    // Create radio button element
    var radioButton = $('<input type="radio">');

    // Set attributes for the radio button
    radioButton.attr({
      id: 'option' + (index + 1),
      name: 'options',
      value: value['licenseNumber']
    });

    // Create label element
    var label = $('<label>');

    // Set attributes for the label
    label.attr('for', 'option' + (index + 1));

    // Set label text
    let labelText = `${value['name']} - ${value['phoneNumber']}`
    label.text(labelText);

    label.css({
      'font-weight': 'bold'
    });
    radioButton.css('margin-right', '5px');
    // Append radio button and label to container div
    $('#driverListContainer').append(radioButton, label, '<br>');
  });
  $('#driverListContainer input[type="radio"]').change(function () {
    // Your code to execute when a radio button in the list is changed
    var selectedValue = $(this).val();
    console.log("Selected value: " + selectedValue);
    $('#accept-ride-modal-btn').attr('disabled', false)
    Alpine.store('selectedDriver', selectedValue)
  });
}

$('#accept-ride-modal-btn').click(function () {
  window.gogoClient.callV2RideEndpoint().then(() => {
    $("#drivers_modal").modal('hide')
    $("#vehicles_modal").modal('hide')
    alert("Ride assigned successfully !!!")
    window.location.reload()
  }).catch((err) => {
    $("#drivers_modal").modal('hide')
    $("#vehicles_modal").modal('hide')
    console.log(err)
    alert(`Error while assigning ride \n ${err['errorCode']}`)
  })
})


async function verifyPayment(payload) {
  try {
    await window.gogoClient.verifyPayment(payload);
    Alpine.store('showSpinner', false)
    if (payload["isRegistration"]) {
      alert("Please upload the documents to verify...")
      location.href = FORM_URL;
    } else {
      alert("Payment success. Amount added to wallet");
      location.href = 'dashboard.html';
    }
    if (window.location.href.includes("dashboard.html")) {
      location.reload();
    }
  } catch (err) {
    Alpine.store('showSpinner', false)
    console.log(err);
    alert("Check payment status in few minutes. Else contact gogoCabs customer support")
  }
}
async function makePayment(amountArg, isRegistration = false) {
  Alpine.store('showSpinner', true);
  let amountInPaise = amountArg * 100
  let orderResponse = await window.gogoClient.createRazorPayOrder({
    cabVendorId: getCabVendorId(),
    amount: amountInPaise,
    notes: {
      "cabVendorName": vendorDetails["displayName"],
      "cabVendorNo": "6385588806",
      "cabVendorEmail": vendorDetails["email"]
    }
  })

  // test - pk_test_22zVE32IcU67
  var options = {
    "order_id": orderResponse.orderId,
    "public_key": "pk_test_22zVE32IcU67"
  }
  var ipay = new Ippopay(options);
  ipay.open()

  ippopayHandler(response, function (e) {
    if (e.data.status == 'success') {
      console.log(e.data)
      verifyPayment({
        "paymentGateway": "IPPOPAY",
        "cabVendorId": getCabVendorId(),
        "orderCreatedDate": orderResponse.orderCreatedDate,
        "orderId": orderResponse.orderId,
        "paymentId": e.data.transaction_no,
        "signature": "", // not applicable for ippopay
        "isRegistration": isRegistration
      })
      ipay.close()
    }
    if (e.data.status == 'failure') {
      console.log(e.data)
      alert("payment failed. Retry later")
      ipay.close()
    }
  });

}

async function makeRazorypayPayment(amountArg, isRegistration = false) {
  Alpine.store('showSpinner', true)
  // Assuming no +91 returned from the UI
  let phoneNumber = parseInt(Alpine.store('driverDetails').profile.phoneNo)
  if (phoneNumber > 0) {
    phoneNumber = `+91${phoneNumber.toString()}`
  } else {
    phoneNumber = "+916385588806"
  }
  let amountInPaise = amountArg * 100
  let orderResponse = await window.gogoClient.createRazorPayOrder({ cabVendorId: getCabVendorId(), amount: amountInPaise })
  var options = {
    "key": "rzp_live_iTQeakSXTDFmt9", // Enter the Key ID generated from the Dashboard
    "amount": amountInPaise.toString(), // In Paise
    "currency": "INR",
    "name": "Gogo Cabs",
    "description": "Add money to wallet to book rides",
    "image": "https://gogocab.in/assets/img/gogo.png",
    "order_id": orderResponse.orderId,
    "handler": function (response) {
      let razorpayResponse = {
        // orderCreatedDate: response.orderCreatedDate,
        paymentId: response.razorpay_payment_id,
        orderId: response.razorpay_order_id,
        signature: response.razorpay_signature
      }
      console.log('payment complete', response)
      verifyPayment({
        "paymentGateway": "RAZORPAY",
        "cabVendorId": getCabVendorId(),
        "orderCreatedDate": orderResponse.orderCreatedDate,
        "orderId": orderResponse.orderId,
        "paymentId": razorpayResponse.paymentId,
        "signature": razorpayResponse.signature,
        "isRegistration": isRegistration
      })
    },
    "prefill": {
      "name": vendorDetails["displayName"],
      "email": vendorDetails["email"],
      "contact": phoneNumber
    },
    "notes": {
      "cabVendorId": getCabVendorId()
    },
    "theme": {
      "color": "#fd6b84"
    }
  };
  var rzp1 = new Razorpay(options);
  rzp1.on('payment.failed', function (response) {
    console.warn("Payment failure", response)
    alert("Payment failed")
  });
  rzp1.open();
}