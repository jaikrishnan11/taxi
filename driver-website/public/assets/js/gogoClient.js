
const CONFIRM_RIDE_ASSIGNMENT = "CONFIRM_RIDE_ASSIGNMENT";
const RIDE_ASSIGNMENT_LOADING = "RIDE_ASSIGNMENT_LOADING";
const RIDE_ASSIGNED = "RIDE_ASSIGNED";
const RIDE_FAILED_LOW_BALANCE = "RIDE_FAILED_LOW_BALANCE";
const RIDE_PICKED_BY_SOMEONE = "RIDE_PICKED_BY_SOMEONE";
const RIDE_FAILED_INTERNAL_ERROR = "RIDE_FAILED_INTERNAL_ERROR";
const REGISTRATION_FAILED_INTERNAL_ERROR = "REGISTRATION_FAILED_INTERNAL_ERROR";
const REGISTRATION_BAD_DATA = "REGISTRATION_BAD_DATA";
const REGISTRATION_SUCCESSFUL = "REGISTRATION_SUCCESSFUL";
const VENDOR_NOT_APPROVED = "VENDOR_NOT_APPROVED";
const VENDOR_VEHICLE_NOT_APPROVED = "VENDOR_VEHICLE_NOT_APPROVED";
const ADD_MONEY = "ADD_MONEY";

const GOOGLE_FORM_LINK = "https://forms.gle/PaAuKJz12wgTmdQF9";

class GogoClient {

    constructor() {
        this.url = "https://api-worker-production.gogocabs.workers.dev";
        //this.url = "http://localhost:8787"
        this.loginDetails = getCabVendorDetails();
        this.googleIdToken = this.loginDetails.googleIdToken;
    }

    async createRazorPayOrder(payload) {
        return await this.postData(this.url + "/wallet/razorpay-order", payload);
    }

    async verifyPayment(payload) {
        return await this.postData(this.url + "/wallet/verify-razorpay-order", payload);
    }




    putDataToModal(action) {
        switch (action) {
            case CONFIRM_RIDE_ASSIGNMENT:
                Alpine.store('dashboard_modal', {
                    heading: "Confirm Ride",
                    content: "Are you accepting this ride ...",
                    showAddMoneyButton: false,
                    confirmRide: true
                })
                break;
            case RIDE_ASSIGNMENT_LOADING:
                Alpine.store('dashboard_modal', {
                    heading: "Ride Status",
                    content: "Please wait ride assignment is loading ...",
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case RIDE_ASSIGNED:
                Alpine.store('dashboard_modal', {
                    heading: "Ride Assigned",
                    content: "Wowww !!!! " +
                        " Ride got assigned successfully, check your ride list for customer details",
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case RIDE_FAILED_LOW_BALANCE:
                Alpine.store('dashboard_modal', {
                    heading: "Ride Failed",
                    content: "Please add money to wallet to accept the rides",
                    showAddMoneyButton: true,
                    confirmRide: false
                })
                break;
            case RIDE_PICKED_BY_SOMEONE:
                Alpine.store('dashboard_modal', {
                    heading: "Ride just missed",
                    content: "Ride just got missed, got assigned to some driver",
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case RIDE_FAILED_INTERNAL_ERROR:
                Alpine.store('dashboard_modal', {
                    heading: "Ride Failed",
                    content: "Please try again, Ride assignee failed ...",
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case ADD_MONEY:
                Alpine.store('dashboard_modal', {
                    heading: "Add Money",
                    content: "Please enter amount greater than 500",
                    showAddMoneyButton: true,
                    showQrCode: false,
                    confirmRide: false
                })
                break;
            case REGISTRATION_FAILED_INTERNAL_ERROR:
                Alpine.store('dashboard_modal', {
                    heading: "Registration Failed",
                    content: "Please try again, registration Failed",
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case REGISTRATION_BAD_DATA:
                Alpine.store('dashboard_modal', {
                    heading: "Registration Failed",
                    content: "Please try again, registration Failed, Please retry the process",
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case REGISTRATION_SUCCESSFUL:
                Alpine.store('dashboard_modal', {
                    heading: "Registration Successful",
                    content: `Payment successful, Please upload the vehicle documents \n\n ${GOOGLE_FORM_LINK}`,
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case VENDOR_NOT_APPROVED:
                Alpine.store('dashboard_modal', {
                    heading: "Vendor Not Approved",
                    content: `Please upload the vehicle documents
                    If already done, Please wait for the approval`,
                    formLink: true,
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
            case VENDOR_VEHICLE_NOT_APPROVED:
                Alpine.store('dashboard_modal', {
                    heading: "Vehicle Not Approved",
                    content: `Please upload the vehicle documents,
                    If already done, Please wait for the approval`,
                    formLink: true,
                    showAddMoneyButton: false,
                    confirmRide: false
                })
                break;
        }
    }
    // Example POST method implementation:
    async postData(url = '', data = {}) {
        data["googleIdToken"] = this.googleIdToken;
        // Default options are marked with *
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        //TODO : IF error handle correctly
        return response.json(); // parses JSON response into native JavaScript objects
    }

    async uploadFiles(cabVendorId, formData) {
        // return Promise.resolve()
        formData.append("googleIdToken", this.googleIdToken)
        formData.append("cabVendorId", cabVendorId)
        const response = await fetch(`${this.url}/cab-vendors/file-upload`, {
            method: 'POST',
            body: formData
        })
        const jsonResponse = await response.json()
        if(response.status > 200) {
            console.error("file upload failed", jsonResponse)
            throw jsonResponse
        }
        return jsonResponse
    }

    async getUnassignedRides() {
        if (_.isEmpty(this.googleIdToken)) {
            alert("Login Not successful");
            return {}
        }
        Alpine.store('showSpinner', true)
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("calling getUnassignedRides ...")
        let vendorId = getCabVendorId()
        const response = await fetch(`${this.url}/rides/unassigned?cabVendorId=${vendorId}`, requestOptions);
        Alpine.store('showSpinner', false)
        const jsonResponse = await response.json();
        return jsonResponse["rides"];
    }

    async getWalletTransactions() {
        Alpine.store('showSpinner', true)
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("calling get wallet transactions ...")
        let vendorId = getCabVendorId()
        const response = await fetch(`${this.url}/wallet/transactions?cabVendorId=${vendorId}`, requestOptions);
        Alpine.store('showSpinner', false)
        const jsonResponse = await response.json();
        return jsonResponse;
    }

    async getBookingDetails(bookingIds = [], cabVendorId) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("calling getBookingDetails ...")
        const response = await fetch(`${this.url}/rides?bookingIds=${bookingIds.join(",")}&cabVendorId=${cabVendorId}`, requestOptions);
        const jsonResponse = await response.json();
        return jsonResponse;
    }

    async assignRides(bookingId, cabVendorId) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        let driverDetails = Alpine.store('driverDetails')
        if (driverDetails["version"] == "v1") {
            if(driverDetails?.version == "v1" && driverDetails?.meta?.forceV2Migration) {
                $('#upgrade-v2-modal').modal('show')
                return
            }
            //TODO: remove this and receive this via args
            let vehicles = driverDetails["vehicles"];
            let vehicleRegNo = "";
            if (!_.isEmpty(vehicles)) {
                vehicleRegNo = Object.keys(vehicles)[0];
            }

            var raw = JSON.stringify({
                "bookingId": bookingId,
                "cabVendorId": cabVendorId,
                "action": "ASSIGN_DRIVER",
                "vehicleRegNo": vehicleRegNo,
                "googleIdToken": this.googleIdToken
            });

            var requestOptions = {
                method: 'PUT',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };
            console.log("calling assignRides ...")
            $('#dashboard_modal').modal('show');
            this.putDataToModal(CONFIRM_RIDE_ASSIGNMENT);
            Alpine.store('assignRide', requestOptions)
        }
        if (driverDetails["version"] == "v2") {
            Alpine.store('selectedBookingId', bookingId)
            showVehicleListingModal()
        }
    }

    async callV2RideEndpoint() {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({
            "bookingId": Alpine.store('selectedBookingId'),
            "cabVendorId": getCabVendorId(),
            "action": "ASSIGN_DRIVER",
            "vehicleRegNo": Alpine.store('selectedVehicle'),
            "driverLicenseNo": Alpine.store('selectedDriver'),
            "googleIdToken": this.googleIdToken
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        $('#loadingDialog').fadeIn(200)
        const response = await fetch(`${this.url}/rides`, requestOptions);
        $('#loadingDialog').fadeOut(200)
        const jsonResponse = await response.json();
        if (response['status'] >= 400) {
            throw jsonResponse
        } else if (response['status'] == 200) {
            this.putDataToModal(RIDE_ASSIGNED);
        }
        Alpine.store('selectedBookingId', "")
        Alpine.store('selectedVehicle', "")
        Alpine.store('selectedDriver', "")
    }

    async callRideEndpoint() {
        let requestOptions = Alpine.store('assignRide');
        const response = await fetch(`${this.url}/rides`, requestOptions);
        Alpine.store('showSpinner', false)
        // this.putDataToModal(RIDE_ASSIGNMENT_LOADING);
        const jsonResponse = await response.json();
        if (response['status'] >= 400) {
            if (jsonResponse['errorCode'] == 'VENDOR_NOT_APPROVED') {
                this.putDataToModal(VENDOR_NOT_APPROVED);
            } else if (jsonResponse['errorCode'] == 'VENDOR_VEHICLE_NOT_APPROVED') {
                this.putDataToModal(VENDOR_VEHICLE_NOT_APPROVED);
            } else {
                this.putDataToModal(RIDE_FAILED_INTERNAL_ERROR);
            }
        } else if (response['status'] == 200) {
            this.putDataToModal(RIDE_ASSIGNED);
        }
        Alpine.store('assignRide', {})
        console.log(jsonResponse);
        return jsonResponse;
    }

    async getDriverDetails(cabVendorId) {
        // get driver details from local storage
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("calling getDriverDetails ...")
        const response = await fetch(`${this.url}/cab-vendors?cabVendorId=` + cabVendorId + "&expand=all", requestOptions);
        const jsonResponse = await response.json();
        if (response['status'] == '200') {
            if (jsonResponse["vendorStatus"] == "UNAPPROVED") {
                Alpine.store("showFormLink", true)
                this.putDataToModal(VENDOR_NOT_APPROVED);
                alert(VENDOR_NOT_APPROVED)
            } else if (jsonResponse["vendorStatus"] == "PAYMENT_PENDING") {
                alert("Please complete the registration payment");
                makePayment(1000, true);
            } else {
                return jsonResponse;
            }
        } else if (response['status'] >= '400') {
            if (jsonResponse["errorCode"] == "RESOURCE_NOT_FOUND") {
                location.href = 'registration.html';
                alert("Please register to view the rides !!!");
            } else {
                alert(jsonResponse["errorCode"]);
            }
        } else if (response['status'] >= '500') {
            alert(jsonResponse["errorCode"]);
        }
    }

    async createVendor(payload = {}) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(payload)
        };
        console.log("calling getDriverDetails ...")
        const response = await fetch(`${this.url}/cab-vendors`, requestOptions)
        if (response['status'] >= 500) {
            this.putDataToModal(REGISTRATION_FAILED_INTERNAL_ERROR);
            alert("Registration failed please try again..")
        } else if (response['status'] >= 400) {
            this.putDataToModal(REGISTRATION_BAD_DATA);
            alert("Registration failed please try again..")
        } else if (response['status'] == 200) {
            this.putDataToModal(REGISTRATION_SUCCESSFUL);
        }
        const jsonResponse = await response.json();
        return jsonResponse;
    }

    async validateVendorAndTriggerOTP(payload = {}) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(payload)
        };
        console.log("calling validateVendorAndTriggerOTP ...")
        const response = await fetch(`${this.url}/validate-create-vendor`, requestOptions)
        const jsonResponse = await response.json();
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async addVehicle(payload = {}) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(payload)
        };
        console.log("calling backend to add vehicle ...")
        const response = await fetch(`${this.url}/cab-vendors/add-vehicle`, requestOptions)
        const jsonResponse = await response.json();
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async validateDriverAndTriggerOTP(payload = {}) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(payload)
        };
        console.log("calling valid driver and trigger OTP ...")
        const response = await fetch(`${this.url}/validate-create-driver`, requestOptions)
        const jsonResponse = await response.json();
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async addDriver(payload = {}) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(payload)
        };
        console.log("calling add driver endpoint ...")
        const response = await fetch(`${this.url}/cab-vendors/add-driver`, requestOptions)
        const jsonResponse = await response.json();
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async createVendorV2(payload = {}) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(payload)
        };
        console.log("calling create vendor v2 ...")
        const response = await fetch(`${this.url}/v2/cab-vendors`, requestOptions)
        const jsonResponse = await response.json();
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async getRides_A(bookingId, googleToken) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                "googleUserId": getCabVendorId(),
                "googleIdToken": googleToken,
                "requestType": "GET_RIDES",
                "payload": {
                    "bookingIds": [bookingId]
                }
            })
        };
        const response = await fetch(`${this.url}/admin/peformAction`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async refreshVehicle(vehicleNumber, googleToken) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "vehicleNumber": vehicleNumber,
                "googleIdToken": googleToken
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/refresh-vehicle`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async refreshDriver(licenseNo, googleToken) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "licenseNumber": licenseNo,
                "googleIdToken": googleToken
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/refresh-driver`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async deleteVehicle(vehicleNumber, googleToken) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "vehicleNumber": vehicleNumber,
                "googleIdToken": googleToken
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/delete-vehicle`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async deleteDriver(licenseNumber, googleToken) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "licenseNumber": licenseNumber,
                "googleIdToken": googleToken
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/delete-driver`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async validateVendorEditPhoneNoTriggerOTP(phoneNumber) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "phoneNumber": phoneNumber
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/validate-vendor-edit-phone`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async upgradeV2(phoneNumber, panCardNumber) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "panCardNumber": panCardNumber,
                "phoneNumber" : phoneNumber
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/upgrade-v2`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async vendorEditPhoneNumber(phoneNumber, otpCode) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "otpCode" : otpCode,
                "phoneNumber" : phoneNumber
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/vendor-edit-phone`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async vendorEditName(vendorName) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "name" : vendorName
            })
        };
        const response = await fetch(`${this.url}/cab-vendors/edit-name`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }

    async startRide(bookingId) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({
                "cabVendorId": getCabVendorId(),
                "bookingId": bookingId,
                "action": "START_RIDE"
            })
        };
        const response = await fetch(`${this.url}/rides`, requestOptions)
        let jsonResponse = await response.json()
        if(response.status >= 400) {
            throw jsonResponse
        }
        return jsonResponse;
    }
}
window.gogoClient = new GogoClient()