function setCustomGoogleId(cabVendorId) {
    let payload = {"displayName":"Gogo Cabs","email":"gogocabmail@gmail.com","firebaseId":"dummy - set from web","googleId":"","googleIdToken":"dummy google token"}
    payload["googleId"] = atob(cabVendorId)
    localStorage.setItem("gogoUser", JSON.stringify(payload))
}

$(document).ready(function() {
    // Get the full query string from the URL
    const queryString = window.location.search;

    // Parse query parameters using URLSearchParams
    const urlParams = new URLSearchParams(queryString);

    // Get the 'cabVendorId' parameter value
    const cabVendorId = urlParams.get('cabVendorId');

    if (cabVendorId) {
        // Decode the Base64-encoded value
        // const decodedCabVendorId = atob(cabVendorId);
        let existingCabVendor = localStorage.getItem("gogoUser")
        if (existingCabVendor == "" || existingCabVendor == undefined) {
            setCustomGoogleId(cabVendorId)
            location.reload();
            return
        } else {
            let gogoUser = JSON.parse(existingCabVendor)
            let googleId = gogoUser["googleId"]
            let decryptedCabVendorId = atob(cabVendorId)
            if (decryptedCabVendorId == googleId) {
                console.log("vendor Ids are same")
                return
            }
            setCustomGoogleId(cabVendorId);
            location.reload()
            return
        }
        
        // location.reload();
        // console.log("Decoded cabVendorId: ", payload);
    } else {
        // console.log("cabVendorId parameter not found in the URL");
    }
});